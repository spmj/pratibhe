<%-- 
    Document   : sidebar
    Created on : Aug 24, 2017, 10:51:12 PM
    Author     : jay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<aside id="aside" class="ui-aside ui-aside--light-">
    <ul class="nav" ui-nav>
        <li class="nav-head">
            <h5 class="nav-title text-uppercase light-txt">Navigation</h5>
        </li>
        <li>
            <a href="#/db"><i class="fa fa-dashboard"></i><span> Dashboard </span></a>
        </li>
        <li>
            <a href="#/state/view"><i class="fa fa-flag-o"></i><span> State </span></a>
        </li>
        <li>
            <a href="#/city/view"><i class="fa fa-building"></i><span> City </span></a>
        </li>
        <li>
            <a href="#/events/view"><i class="fa fa-calendar"></i><span> Events </span></a>
        </li>
        <li>
            <a href="#/happenings/view"><i class="fa fa-asterisk"></i><span> Happenings </span></a>
        </li>
        <li>
            <a href="#/eventtype/view"><i class="fa fa-clock-o"></i><span> Event Type </span></a>
        </li>
        <li>
            <a href="#/art/view"><i class="fa fa-paint-brush"></i><span> Art </span></a>
        </li>
        <li>
            <a href="#/students/view"><i class="fa fa-graduation-cap"></i><span> Students </span></a>
        </li>
        <li>
            <a href="#/mentors/view"><i class="fa fa-meetup"></i><span> Mentors </span></a>
        </li>
        <li>
            <a href="#/pages/view"><i class="fa fa-building"></i><span> Pages </span></a>
        </li>
    </ul>
</aside>
