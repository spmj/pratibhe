<%-- 
    Document   : useradd
    Created on : Aug 25, 2017, 12:22:10 PM
    Author     : Mayur
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 

<div class="ui-content-body" ng-init="loadEdit()">

    <div class="ui-container">

        <!--page title and breadcrumb start -->
        <div class="row">
            <div class="col-md-8">
                <h1 class="page-title"> {{header}}                    
                </h1>
            </div>
            <div class="col-md-4 text-right">
                <a ng-click="updateProfile()" class="btn btn-success"><i class="fa fa-check"></i> Update</a>
            </div>
        </div>
        <!--page title and breadcrumb end -->

        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <header class="panel-heading panel-border">
                        <span class="tools pull-right">
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-4">
                                <div class="col-md-12 text-center">
                                    <div class="col-md-12">       
                                        
                                        <img id="imgProfile" style="width:250px; height: 250px; padding: 15px;" src="{{formData['IMAGE']}}" alt="W3Schools.com">
                                    </div>
                                    <div class="col-md-12">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <span class="btn btn-upload btn-file"><span>Choose file</span><input type="file"  id="fileProfile" name="fileProfile" file-model="formData['IMAGE']" 
                                                                                                                 filename="formData['IMAGENAME']" fileread="" filesize="formData['IMAGESIZE']" fileext="formData['IMAGEEXT']" 
                                                                                                                 onchange="angular.element(this).scope().thumbChanged(this);
                                                           angular.element(this).scope().processFiles(this)"/></span>
                                        </div>
                                    </div>
                                </div>
                                <!--<img src="{{product.PRODUCTIMG}}" style="width:250px; height: 250px; padding: 5px;" alt="Sem Product"/>-->    
                            </div>
                            <div class="col-md-4">

                            </div>

                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Name </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  
                                               id="txtName" ng-model="formData['NAME']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Artist Name </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  
                                               id="txtName" ng-model="formData['ARTISTNAME']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Email </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  
                                               id="txtName" ng-model="formData['EMAIL']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Mobile </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  
                                               id="txtMobile" ng-model="formData['MOBILE']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Aadhar Number </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  
                                               id="txtAadhar" ng-model="formData['AADHARNO']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Art Form </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  
                                               id="txtAadhar" ng-model="formData['ARTNAME']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Gender </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"   
                                               id="txtGender" ng-model="formData['GENDER']">
                                    </div>
                                </div>
                            </form>
                        </div>


                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Occupation </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"   
                                               id="txtOccupation" ng-model="formData['OCCUPATION']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Birth Date </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"   
                                               id="txtDob" ng-model="formData['DOB']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Address </label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" 
                                                  ng-model="formData['ADDRESS']"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">State </label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="cmdState" ng-model="formData['STATEID']" ng-change="getCity(formData.STATEID)">
                                            <option></option>
                                            <option ng-repeat="data in comboData.STATENAME" value="{{data.VALUE}}">{{data.TEXT}}</option>        
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>


                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">City </label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="cmdCity" ng-model="formData['CITYID']">
                                            <option></option>
                                            <option ng-repeat="data in cityName.CITYNAME" value="{{data.VALUE}}">{{data.TEXT}}</option>        
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">About Me </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"   
                                               id="txtAboutme" ng-model="formData['ABOUTME']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Other </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"   
                                               id="txtOther" ng-model="formData['OTHER']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div ng-if="formData['TYPE'] == 'MENTOR'" class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Type Of Work </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"   
                                               id="txtOther" ng-model="formData['TYPEOFWORK']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div ng-if="formData['TYPE'] == 'MENTOR'" class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Tenure of Work </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"   
                                               id="txtOther" ng-model="formData['TENUREOFWORK']">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>