<%-- 
    Document   : home.jsp
    Created on : 23 Aug, 2017, 8:36:39 PM
    Author     : jay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html ng-app="PratibheApp">
    <c:set var="path" value="/pratibhe/"/>

    <!-- Mirrored from megadin.lab.themebucket.net/dashboard-compact.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 24 Aug 2017 16:47:31 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- <link rel="shortcut icon" type="image/png" href="/imgs/favicon.png" /> -->
        <title>Home</title>

        <!-- inject:css -->
        <link rel="stylesheet" href="<c:url value="/resource/bower_components/bootstrap/dist/css/bootstrap.min.css"/>">
        <link rel='stylesheet prefetch' href='https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css'>
        <link rel="stylesheet" href="<c:url value="/resource/bower_components/font-awesome/css/font-awesome.min.css"/>">
        <link rel="stylesheet" href="<c:url value="/resource/bower_components/simple-line-icons/css/simple-line-icons.css"/>">
        <link rel="stylesheet" href="<c:url value="/resource/bower_components/weather-icons/css/weather-icons.min.css"/>">
        <link rel="stylesheet" href="<c:url value="/resource/bower_components/themify-icons/css/themify-icons.css"/>">
        <!-- endinject -->
        
        <!--<link rel="stylesheet" href="<c:url value="/resource/assets/css/components.min.css"/>">-->
        <link rel="stylesheet" href="<c:url value="/resource/assets/css/angularjs-datetime-picker.css"/>">

        <!-- Main Style  -->
        <link rel="stylesheet" href="<c:url value="/resource/dist/css/main.css"/>">

        <!-- Rickshaw Chart Depencencies -->
        <link rel="stylesheet" href="<c:url value="/resource/bower_components/rickshaw/rickshaw.min.css"/>">

        <!--easypiechart-->
        <link rel="stylesheet" href="<c:url value="/resource/assets/js/jquery-easy-pie-chart/easypiechart.css"/>">
        
        <link rel="stylesheet" href="<c:url value="/client_resource/css/pnotify.custom.min.css"/>"  />

        <script src="<c:url value="/resource/assets/js/modernizr-custom.js"/>"></script>
    </head>
    <body>

        <div id="ui" class="ui ui-aside-compact">

            <!--header start-->
            <jsp:include page="header.jsp"/>
            <!--header end-->

            <!--sidebar start-->
            <c:if test="${TYPE eq 'ADMIN'}">
                <jsp:include page="adminsidebar.jsp"/>
            </c:if>
            <c:if test="${TYPE eq 'MENTOR'}">
                <jsp:include page="mentorsidebar.jsp"/>
            </c:if>
            <c:if test="${TYPE eq 'STUDENT'}">
                <jsp:include page="studentsidebar.jsp"/>
            </c:if>
            <!--sidebar end-->

            <!--main content start-->
            <div id="content" class="ui-content ui-content-aside-overlay" ng-view ng-init="loadDefault('${TYPE}')">
            </div>
            <!--main content end-->

            <!--footer start-->
            <jsp:include page="footer.jsp"/>
            <!--footer end-->

        </div>

        <!-- inject:js -->
        <script src="<c:url value="/resource/bower_components/jquery/dist/jquery.min.js"/>"></script>
        <script src="<c:url value="/resource/bower_components/bootstrap/dist/js/bootstrap.min.js"/>"></script>
        <script src="<c:url value="/resource/bower_components/jquery.nicescroll/dist/jquery.nicescroll.min.js"/>"></script>
        <script src="<c:url value="/resource/bower_components/autosize/dist/autosize.min.js"/>"></script>
        <!-- endinject -->

        <!--sparkline-->
        <script src="<c:url value="/resource/bower_components/bower-jquery-sparkline/dist/jquery.sparkline.retina.js"/>"></script>
        <script src="<c:url value="/resource/assets/js/init-sparkline.js"/>"></script>

        <!-- Rickshaw Chart Depencencies -->
        <script src="<c:url value="/resource/bower_components/rickshaw/vendor/d3.min.js"/>"></script>
        <script src="<c:url value="/resource/bower_components/rickshaw/vendor/d3.layout.min.js"/>"></script>
        <script src="<c:url value="/resource/bower_components/rickshaw/rickshaw.min.js"/>"></script>
        <script src="<c:url value="/resource/assets/js/init-rickshaw.js"/>"></script>

        <script src="<c:url value='/js/bootbox.min.js' />" type="text/javascript"></script>

        <!--easypiechart-->
        <script src="<c:url value="/resource/assets/js/jquery-easy-pie-chart/jquery.easypiechart.js"/>"></script>
        
        <script src="<c:url value='/client_resource/js/pnotify.custom.min.js' />"></script>
        <script>
                $(function () {
                    $('.chart').easyPieChart({
                        //your configuration goes here
                    });
                });
        </script>

        <!-- Common Script   -->
        <script src="<c:url value="/resource/dist/js/main.js"/>"></script>
        <script src="<c:url value='/resource/assets/js/jquery.bootstrap-growl.min.js' />" type="text/javascript"></script>

        <%--Angular Js--%>
        <script src="<c:url value='/js/angular.min.js'/>" type="text/javascript"></script>
        <script src="<c:url value='/js/angular-route.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/angular-animate.js'/>"></script> 
        <script src="<c:url value='/js/angular-filter.min.js'/>" type="text/javascript"></script> <%--for group by in ng-repeat use--%>
        <script src="<c:url value='/js/angular-touch.js'/>"></script> 

        <script src="<c:url value='/resource/datatable/ui-bootstrap-tpls.min.js' />"></script>
        <script src="<c:url value='/resource/datatable/angular-ui-utils.min.js' />"></script>
        <script src="<c:url value='/resource/datatable/jquery.dataTables.min.js' />"></script>
        <script src="<c:url value='/resource/datatable/dataTables.bootstrap.min.js' />"></script>
        <script src="<c:url value='/resource/assets/js/angularjs-datetime-picker.js'/>" type="text/javascript"></script>
        <script src="//cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>
        <script src="https://s3-ap-southeast-1.amazonaws.com/naveensingh/posts_data/ckeditor_example/ng-ckeditor.min.js"></script>
        <%--General Js--%>
        <script src="<c:url value='/js/require.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/validate.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/checklist-model.min.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/ocLazyLoad.require.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/app.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/approot.js' />" type="text/javascript"></script>

        <script src="<c:url value='/js/Service/commonService.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/Directive/commonDirective.js' />" type="text/javascript"></script>
        
        <script src="<c:url value='/js/Service/eventsService.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/Controller/eventsController.js' />" type="text/javascript"></script>
        
        <script src="<c:url value='/js/Service/stateService.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/Controller/stateController.js' />" type="text/javascript"></script>
        
        <script src="<c:url value='/js/Service/cityService.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/Controller/cityController.js' />" type="text/javascript"></script>
        
        <script src="<c:url value='/js/Service/happeningsService.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/Controller/happeningsController.js' />" type="text/javascript"></script>
        
        <script src="<c:url value='/js/Service/eventtypeService.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/Controller/eventtypeController.js' />" type="text/javascript"></script>
        
        <script src="<c:url value='/js/Service/artService.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/Controller/artController.js' />" type="text/javascript"></script>
        
        <script src="<c:url value='/js/Service/usersService.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/Controller/usersController.js' />" type="text/javascript"></script>
        
        <script src="<c:url value='/js/Service/pagesService.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/Controller/pagesController.js' />" type="text/javascript"></script>
        
        <script src="<c:url value='/js/Service/dbService.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/Controller/dbController.js' />" type="text/javascript"></script>
        
        <script src="<c:url value='/js/Service/Mentor/eventService.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/Controller/Mentor/eventController.js' />" type="text/javascript"></script>
        
        <script src="<c:url value='/js/Service/Mentor/studentService.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/Controller/Mentor/studentController.js' />" type="text/javascript"></script>
        
        <script src="<c:url value='/js/Service/workService.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/Controller/workController.js' />" type="text/javascript"></script>
        
        <script src="<c:url value='/js/Service/inquiryService.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/Controller/inquiryController.js' />" type="text/javascript"></script>
        
    </body>
</html>
