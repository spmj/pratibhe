<%-- 
    Document   : mappingcontent
    Created on : Oct 30, 2017, 9:58:12 PM
    Author     : jay
--%>

<table class="table table-bordered bordered table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Art </th>
            <th>Email</th>
            <th>About Me</th>
            <th>State</th>
            <th>State</th>
            <th>Select Student</th>
        </tr>
    </thead>
    <tbody>
        <tr ng-repeat="data in mappingData">
            <td>{{$index + 1}}</td>
            <td>{{data.NAME}}</td>
            <td>{{data.ARTNAME}}</td>
            <td>{{data.EMAIL}}</td>
            <td>{{data.ABOUTME}}</td>
            <td>{{data.STATENAME}}</td>
            <td>{{data.CITYNAME}}</td>
            <td><input type="checkbox" ng-model="data.MENTORID"></td>

        </tr>
    </tbody>
</table>
