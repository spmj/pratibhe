<%-- 
    Document   : Mentor/db
    Created on : Oct 28, 2017, 7:32:05 PM
    Author     : jay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="ui-content-body" ng-init="getDbData('${TYPE}')">
    <div class="ui-container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel">
                    <header class="panel-heading panel-border">
                        Events with participants
                    </header>
                    <div class="panel-body event-blog">
                        <ul class="task-sum-list">
                            <li>
                                <div class="tsk-title">
                                    No of student to be select <label class="badge badge-info">{{dbData.NOOFAVAILSTU}}</label>
                                </div>
                            </li>
                            <li>
                                <div class="tsk-title">
                                    No of selected students <label class="badge badge-success">
                                        {{dbData.NOOFSELECTEDSTU}}</label>
                                </div>
                                
                            </li>
                            <li>
                                <div class="tsk-title">
                                    Featured Students  <label class="badge badge-danger">
                                        {{dbData.NOOFFETUREDSTU}}</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel">
                    <header class="panel-heading panel-border">
                        Events With Number of Participants 
                    </header>
                    <div class="panel-body event-blog">
                        <ul class="task-sum-list" ng-repeat="data in dbData.EVENTPARTICIPANT">
                            <li>
                                <div class="tsk-title">
                                     {{data.TITLE}}  <label class="badge badge-success">{{data.NOOFPART}}</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel">
                    <header class="panel-heading panel-border">
                        No of work art wise
                    </header>
                    <div class="panel-body event-blog">
                        <ul class="task-sum-list" ng-repeat="data in dbData.ARTCOUNT">
                            <li>
                                <div class="tsk-title">
                                    {{data.ARTNAME}} 
                                    <label class="badge badge-success">{{data.COUNT}}</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel">
                    <header class="panel-heading panel-border">
                        No of work student wise
                    </header>
                    <div class="panel-body event-blog">
                        <ul class="task-sum-list" ng-repeat="data in dbData.STUDENTWORK">
                            <li>
                                <div class="tsk-title">
                                    {{data.NAME}}   <label class="badge badge-warning">{{data.COUNT}}</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
