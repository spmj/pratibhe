<%-- 
    Document   : happeningsadd
    Created on : Aug 25, 2017, 12:22:10 PM
    Author     : jay
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<c:if test="${task eq 'Add'}">
    <div class="ui-content-body" ng-init="loadAdd()">
    </c:if>
    <c:if test="${task eq 'Edit'}">
        <div class="ui-content-body" ng-init="loadEdit()">
        </c:if>

        <div class="ui-container">

            <!--page title and breadcrumb start -->
            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-title"> Happenings                    
                    </h1>
                </div>
                <div class="col-md-4 text-right">
                    <a href="#/mentorhappenings/view" class="btn btn-default"><i class="fa fa-reply"></i> Cancel</a>
                </div>
            </div>
            <!--page title and breadcrumb end -->

            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <header class="panel-heading panel-border">
                            
                            <span class="tools pull-right">

                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Title </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" readonly maxlength="55"
                                                   id="txtTitle" ng-model="formData['TITLE']">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Summery </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" readonly 
                                                   id="txtSummery" ng-model="formData['SUMMERY']">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Detail </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" readonly 
                                                   id="txtDetail" ng-model="formData['DETAILS']">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Address </label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" readonly=""
                                                      ng-model="formData['ADDRESS']"></textarea>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">State </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" readonly 
                                                   id="txtDetail" ng-model="formData['STATENAME']">
                                            
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">City </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" readonly 
                                                   id="txtDetail" ng-model="formData['CITYNAME']">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Art Form </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" readonly 
                                                   id="txtDetail" ng-model="formData['ARTNAME']">
                                            </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Happening Type </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" readonly 
                                                   id="txtDetail" ng-model="formData['EVENTTYPENAME']">
                                           </div>
                                    </div>
                                </form>
                            </div>
                            
                                                      
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Start Date </label>
                                        <div class="col-sm-8">
                                            <input type="text" readonly class="form-control" id="txtStartDate" name="txtStartDate" 
                                                   datetime-picker date-format="{{dhxDateFormate}}" date-only
                                                   ng-model="formData['STARTDATE']"
                                                   onblur="expandDate(this.id);" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">End Date </label>
                                        <div class="col-sm-8">
                                            <input type="text" readonly class="form-control" id="txtStartDate" name="txtEndDate" 
                                                   datetime-picker date-format="{{dhxDateFormate}}" date-only
                                                   ng-model="formData['ENDDATE']"
                                                   onblur="expandDate(this.id);" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Contact Name </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" readonly 
                                                   id="txtContactName" ng-model="formData['CONTACTNAME']">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Contact Email </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" readonly 
                                                   id="txtContactEmail" ng-model="formData['CONTACTEMAIL']">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>