<%-- 
    Document   : happeningsview
    Created on : Aug 25, 2017, 12:22:42 PM
    Author     : jay
--%>
<div class="ui-content-body">

    <div class="ui-container">

        <!--page title and breadcrumb start -->
        <div class="row">
            <div class="col-md-8">
                <h1 class="page-title"> Happenings</h1>
            </div>
        </div>
        <!--page title and breadcrumb end -->

        <div class="row" ng-init="viewDetail()">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading panel-border">
                        View Happenings
                    </header>
                    <div class="panel-body">
                        <div id="page_content"></div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>