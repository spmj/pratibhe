<%-- 
    Document   : useradd
    Created on : Aug 25, 2017, 12:22:10 PM
    Author     : Mayur
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 

<c:if test="${task eq 'Edit'}">
    <div class="ui-content-body" ng-init="loadEdit()">
    </c:if>

    <div class="ui-container">

        <!--page title and breadcrumb start -->
        <div class="row">
            <div class="col-md-8">
                <h1 class="page-title"> Student
                </h1>
            </div>
            <div class="col-md-4 text-right">
                <c:if test="${task eq 'Edit'}">
                    <a ng-click="updateDetail()" class="btn btn-success"><i class="fa fa-check"></i> Update</a>
                    <a ng-click="deleteDetail()" class="btn btn-danger"><i class="fa fa-times"></i> Delete</a>
                </c:if>
                <a href="#mentor/student/view" class="btn btn-default"><i class="fa fa-reply"></i> Cancel</a>
            </div>
        </div>
        <!--page title and breadcrumb end -->

        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <header class="panel-heading panel-border">
                        <span class="tools pull-right">
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-4">
                                <img style="width:250px; height: 250px; padding: 5px;" src="https://www.w3schools.com/images/w3schools_green.jpg" alt="W3Schools.com">
                                <img src="{{product.PRODUCTIMG}}" style="width:250px; height: 250px; padding: 5px;" alt="Sem Product"/>    
                            </div>
                            <div class="col-md-4">

                            </div>

                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Name </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  disabled=""
                                               id="txtName" ng-model="formData['NAME']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Artist Name </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  disabled=""
                                               id="txtName" ng-model="formData['ARTISTNAME']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Email </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  disabled=""
                                               id="txtName" ng-model="formData['EMAIL']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Mobile </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  disabled=""
                                               id="txtMobile" ng-model="formData['MOBILE']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Aadhar Number </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  disabled=""
                                               id="txtAadhar" ng-model="formData['AADHARNO']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Art Form </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  disabled=""
                                               id="txtAadhar" ng-model="formData['ARTNAME']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Gender </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  disabled="" 
                                               id="txtGender" ng-model="formData['GENDER']">
                                    </div>
                                </div>
                            </form>
                        </div>


                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Occupation </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  disabled="" 
                                               id="txtOccupation" ng-model="formData['OCCUPATION']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Birth Date </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  disabled="" 
                                               id="txtDob" ng-model="formData['DOB']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Address </label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" disabled=""
                                                  ng-model="formData['ADDRESS']"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">City </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  disabled="" 
                                               id="txtCity" ng-model="formData['CITYNAME']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">State </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  disabled="" 
                                               id="txtState" ng-model="formData['STATENAME']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">About Me </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  disabled="" 
                                               id="txtAboutme" ng-model="formData['ABOUTME']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Other </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  disabled="" 
                                               id="txtOther" ng-model="formData['OTHER']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div ng-if="formData['TYPE'] == 'MENTOR'" class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Type Of Work </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  disabled="" 
                                               id="txtOther" ng-model="formData['TYPEOFWORK']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div ng-if="formData['TYPE'] == 'MENTOR'" class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Tenure of Work </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  disabled="" 
                                               id="txtOther" ng-model="formData['TENUREOFWORK']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Is Active </label>
                                    <div class="col-sm-8">
                                        <label>
                                            <input type="checkbox" ng-model="formData['ISACTIVE']">
                                        </label>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div ng-if="formData['TYPE'] == 'MENTOR'" class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Is Approve </label>
                                    <div class="col-sm-8">
                                        <label>
                                            <input type="checkbox" ng-model="formData['ISAPPROVED']">
                                        </label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>