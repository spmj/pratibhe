<%-- 
    Document   : eventscontent
    Created on : Sep 10, 2017, 6:07:37 PM
    Author     : jay
--%>

<table class="table table-bordered bordered table-striped table-condensed datatable" ui-jq="dataTable" ui-options="dataTableOpt">
    <thead>
        <tr>
            <th>#</th>
            <th>Name </th>
            <th>Art </th>
            <th>Gender </th>
            <th>Mobile </th>
            <th ng-if="usertype == 'mentors'">Is Approved </th> 
            <th class="text-center">View </th> 
            <th class="text-center">Work </th> 
        </tr>
    </thead>
    <tbody>
        <tr ng-repeat="data in usersData" ng-dblclick="changeView('mentor/student/edit/' + data.USERID)">
            <td>{{$index + 1}}</td>
            <td>{{data.NAME}}</td>
            <td>{{data.ARTNAME}}</td>
            <td>{{data.GENDER}}</td>
            <td>{{data.MOBILE}}</td>
            <td ng-if="usertype == 'mentors'">{{data.ISAPPROVED}}</td>
            <td class="text-center" ng-if="data.TYPE == 'STUDENT'"><a href="#/mentor/student/edit/{{data.USERID}}" ><i class="fa fa-edit"></i></a></a></td>
            <td class="text-center" ng-if="data.TYPE == 'STUDENT'"><a href="#/mentor/student/work/{{data.USERID}}" ><i class="fa fa-tasks"></i></a></a></td>
        </tr>
    </tbody>
</table>