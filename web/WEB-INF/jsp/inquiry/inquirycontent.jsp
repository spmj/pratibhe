<%-- 
    Document   : eventscontent
    Created on : Sep 10, 2017, 6:07:37 PM
    Author     : jay
--%>

<table class="table table-bordered bordered table-striped table-condensed datatable" ui-jq="dataTable" ui-options="dataTableOpt">
    <thead>
        <tr>
            <th>Sr. No.</th>
            <th>Name </th>
            <th>Email </th>
            <th>Contact Number </th>
            <th>Created Time </th>
            <th class="text-center">View </th> 
        </tr>
    </thead>
    <tbody>
        <tr ng-repeat="data in inquiryData" ng-dblclick="changeView('inquiry/edit/' + data.INQUIRYID)">
            <td>{{$index + 1}}</td>
            <td>{{data.NAME}}</td>
            <td>{{data.EMAIL}}</td>
            <td>{{data.MOBILE}}</td>
            <td>{{data.CREATEDDATE}}</td>
            <td class="text-center"><a href="#/inquiry/edit/{{data.INQUIRYID}}" ><i class="fa fa-eye"></i></a></td>
        </tr>
    </tbody>
</table>