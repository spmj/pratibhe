<%-- 
    Document   : useradd
    Created on : Aug 25, 2017, 12:22:10 PM
    Author     : Mayur
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<c:if test="${task eq 'Edit'}">
    <div class="ui-content-body" ng-init="loadEdit()">
    </c:if>

    <div class="ui-container">

        <!--page title and breadcrumb start -->
        <div class="row">
            <div class="col-md-8">
                <h1 class="page-title"> {{header}}                    
                </h1>
            </div>
            <div class="col-md-4 text-right">
                <c:if test="${task eq 'Edit'}">
                    <a ng-click="deleteDetail()" class="btn btn-danger"><i class="fa fa-times"></i> Delete</a>
                </c:if>
                <a href="{{viewLink}}" class="btn btn-default"><i class="fa fa-reply"></i> Cancel</a>
            </div>
        </div>
        <!--page title and breadcrumb end -->

        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <header class="panel-heading panel-border">
                        <span class="tools pull-right">
                        </span>
                    </header>
                    <div class="panel-body">
                        

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Name </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  disabled=""
                                               id="txtName" ng-model="formData['NAME']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Email </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  disabled=""
                                               id="txtName" ng-model="formData['EMAIL']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Mobile </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  disabled=""
                                               id="txtMobile" ng-model="formData['MOBILE']">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Message </label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" disabled=""
                                                  ng-model="formData['MESSAGE']"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>