<%-- 
    Document   : sidebar
    Created on : Aug 24, 2017, 10:51:12 PM
    Author     : jay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<aside id="aside" class="ui-aside ui-aside--light-">
    <ul class="nav" ui-nav>
        <li class="nav-head">
            <h5 class="nav-title text-uppercase light-txt">Navigation</h5>
        </li>
        <li>
            <a href="#student/db"><i class="fa fa-dashboard"></i><span> Dashboard </span></a>
        </li>
        <li>
            <a href="#studenthappenings/view"><i class="fa fa-asterisk"></i><span> Happenings </span></a>
        </li>
        <li>
            <a href="#/student/work/view"><i class="fa fa-tasks"></i><span> Upload Work </span></a>
        </li>
        <li>
            <a href="#/users/inquiry/view"><i class="fa fa-envelope-o"></i><span> Inquiries </span></a>
        </li>
    </ul>
</aside>
