<%-- 
    Document   : about_us
    Created on : Nov 27, 2017, 8:22:38 PM
    Author     : Mayur
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html lang="en" ng-app="PratibheApp">
    <jsp:include page="header.jsp"/>
    <title>Artist</title>

    <body ng-controller="clientController">
        <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
        <!-- start header -->
        <jsp:include page="navbar.jsp"/>
        <!-- end header -->

        <!--start section slider -->
        <section class="banner xs-mt-60">            
            <h1>Artist</h1>
        </section>
        <!--end section slider -->


        <section class="partner-details bg-theme help xs-pt-50">
        </section>

        <!-- start section work -->
        <section class="work artists-details xs-ptb-30" id="work"  ng-init="getArtist()">
            <div class="container" id="change">
                <h2>Artists</h2>
                <p>Lorem ipsum lrem opsum Lorem ipsum lrem opsumLorem ipsum lrem opsum Lorem ipsum lrem opsum</p>

                <div class="row">
                    <div class ="col-md-12">
                        <div ng-repeat="data in ARTISTDATAS" class="col-md-2 col-sm-4 col-xs-6 xs-p-0">
                            <div class="work-area w-100">
                                <div class="image">
                                    <img alt="image" src="{{data.IMAGE}}">
                                </div>
                                <a href="${contextPath}/client/artists/{{data.USERID}}" class="overlay">
                                    <div class="inner">
                                        <h2>{{data.NAME}}</h2>
                                        <p>{{data.ARTISTNAME}}</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8" style="padding: 0">
                            <pagination total-items="ARTISTSDATA.totalItems" ng-click="getArtPageData(ARTISTSDATA.currentPage)" 
                                        page="ARTISTSDATA.currentPage"  max-size="ARTISTSDATA.maxSize" 
                                        class="pagination-sm" boundary-links="true" rotate="false" 
                                        num-pages="numPages" items-per-page="ARTISTSDATA.itemsPerPage" style="margin-left: 15px; margin-top: 0">
                            </pagination>
                        </div>
                        <div class="col-md-4 text-right">
                            {{record = ARTISTSDATA.currentPage * 18;
                                        ""}}
                            {{record - 18 + 1}} to <span ng-if="record < ARTISTSDATA.totalItems">{{record}}</span> 
                            <span ng-if="record > ARTISTSDATA.totalItems">{{ARTISTSDATA.totalItems}}</span> of <span class="badge">{{ARTISTSDATA.totalItems}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end section work -->

        <jsp:include page="footer.jsp"/>

    </body>

</html>
