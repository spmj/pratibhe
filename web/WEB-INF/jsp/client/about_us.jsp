<%-- 
    Document   : about_us
    Created on : Nov 27, 2017, 8:22:38 PM
    Author     : Mayur
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html lang="en" ng-app="PratibheApp">
    <jsp:include page="header.jsp"/>
    <title>About Pratibhey</title>

    <body ng-controller="clientController">
        <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
        <!-- start header -->
        <jsp:include page="navbar.jsp"/>
        <!-- end header -->

         <!--start section slider -->
        <section class="banner xs-mt-60">            
            <h1>About Pratibhe</h1>
        </section>
        <!--end section slider -->


        <!--start happening section  -->
        <section class="partner-details bg-theme help xs-pt-50 art">
            <div class="container text-center">
                <h2>About Pratibhe</h2>
                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking
                    at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters,
                    as opposed to using 'Content here, content here', making be distracted by the readable content of a page
                    when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution
                    of letters, as opposed to using 'Content here, content here', making it look like readable English. Many
                    desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search
                    for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over
                    the years on purpose (injected humour and the like) Find out more
                    <a href="#">here...</a>
                </p>

                <br>
                <br>

                <h5>WHY COLLOBORATE WITH NOISE SINGAPORE?</h5>
                <H5>ESTABLISHED BRAND IN YOUTH ARTS</H5>
                <br>
                <P>It is a long established fact that a reader will be distracted by the readable content of a page when looking
                    at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters,
                    as opposed to using 'Content here, content here', making be distracted by the readable content of a page
                    when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution
                    of letters, as opposed to using 'Content here, content here', making it look like readable English. Many
                    desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search
                    for 'lorem ipsum'</P>
                <BR>
                <H5>WIDE YOUTH FANS BASE</H5>
                <br>
                <P>It is a long established fact that a reader will be distracted by the readable content of a page when looking
                    at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters,
                    as opposed to using 'Content here, content here', making be distracted by the readable content of a page
                    when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution
                    of letters, as opposed to using 'Content here, content here', making it look like readable English. Many
                    desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search
                    for 'lorem ipsum'</P>
                <BR>
                <H5>CONNECTION TO THE CREATIVE COMMUNITY</H5>
                <br>
                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking
                    at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters,
                    as opposed to using 'Content here, content here', making be distracted by the readable content of a page
                    when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution
                    of letters, as opposed to using 'Content here, content here', making it look like readable English. Many
                    desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search
                    for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over
                    the years on purpose (injected humour and the like).

            </div>
        </section>
        <!--end happening section  -->

        <section class="help xs-pt-50 art">
            <div class="container">
                <p class="para">
                    <span>O</span>rganizers</p>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pl-15 xs-pr-0 md-pr-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pl-0  md-p-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pr-0 md-p-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pl-0 md-p-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pr-0 md-p-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pl-0 xs-pr-15 md-pl-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="help art">
            <div class="container">
                <p class="para">
                    <span>V</span>enue Partners</p>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pl-15 xs-pr-0 md-pr-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pl-0  md-p-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pr-0 md-p-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pl-0 md-p-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pr-0 md-p-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pl-0 xs-pr-15 md-pl-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="help art">
            <div class="container">
                <p class="para">
                    <span>S</span>ponspors</p>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pl-15 xs-pr-0 md-pr-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pl-0  md-p-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pr-0 md-p-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pl-0 md-p-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pr-0 md-p-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pl-0 xs-pr-15 md-pl-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="help art">
            <div class="container">
                <p class="para">
                    <span>P</span>rogramme Partners</p>
                <div class="row">
                    <div class="col-md-12">
                       <div class="row">
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pl-15 xs-pr-0 md-pr-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pl-0  md-p-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pr-0 md-p-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pl-0 md-p-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pr-0 md-p-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 xs-pl-0 xs-pr-15 md-pl-0">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--start section help -->
        <section class="help pt-50 art">
            <div class="container">
                <p class="para">
                    <span>P</span>rogramme Partners</p>
                <div class="row">
                    <div class="col-md-3 col-xs-6 col-sm-4 md-pr-0">
                        <ul class="partner-list">
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-xs-6 col-sm-4 md-p-0">
                        <ul class="partner-list">
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-xs-6 col-sm-4 md-p-0">
                        <ul class="partner-list">
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-xs-6 col-sm-4 md-p-0">
                        <ul class="partner-list">
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                            <li>Lorem ipsum dormet</li>
                        </ul>
                    </div>

                </div>

            </div>
        </section>
        <!-- start section work -->

        <!--start section help -->
        <section class="help  art">
            <div class="container">
                <p class="para">
                    <span>O</span>ur Programmes</p>
                <div class="row">
                    <div class="col-md-2 md-pr-0">
                        <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                    </div>
                    <div class="col-md-2 md-p-0">
                        <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                    </div>
                    <div class="col-md-2 md-p-0">
                        <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                    </div>
                    <div class="col-md-2 md-p-0">
                        <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                    </div>
                    <div class="col-md-2 md-p-0">
                        <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                    </div>
                    <div class="col-md-2 md-p-0">
                        <a class="btn-ex-partner" href="#">EXPLORE PARTNERSHIP <i class="fa fa-arrow-right"></i></a>
                    </div>

                </div>

            </div>
        </section>
        <!-- start section work -->

        <jsp:include page="footer.jsp"/>

    </body>

</html>
