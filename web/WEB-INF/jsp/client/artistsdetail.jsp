<%-- 
    Document   : eventdetail
    Created on : Nov 19, 2017, 2:03:32 PM
    Author     : Mayur
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en" ng-app="PratibheApp">
    <title>${TYPE}</title>
    <jsp:include page="header.jsp"/>

    <body ng-controller="clientController">
        <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
        <!-- start header -->
        <jsp:include page="navbar.jsp"/>
        <!--start section slider -->
        <section class="banner xs-mt-60">            
            <h1>Artist Details</h1>
        </section>
        <!--end section slider -->
        <!--start section blog -->
        <section class="bg-theme xs-ptb-50 artist-section">
            <div class="container">                
                <div class="row">
                    <div class="col-md-12 text-center txt-white">
                        <div class="art-pr-details">
                            <img src="<c:url value="/profile/getProfileImage/${USERID}"/>" class="img img-responsive img-circle" width="150px" style="display: inline;"/>
                            <h4> ${NAME} <c:if test="${ARTISTNAME ne null}"> ~ ${ARTISTNAME}</c:if><span></span></h4>
                                <button type="button" data-toggle="modal" data-target=".example-modal-lg" class="btn-p-art">Contact</button>
                                <h4> ${ABOUTME}</h4>
                        </div>
                    </div>
                    <div class="col-md-12 xs-pt-20">
                        <c:forEach var="data" items="${ARTDATA}">
                            <a href="${contextPath}/client/artwork/${data.USERWORKID}">
                                <div class="col-md-6 xs-plr-10">
                                    <img src="${data.FILE}" class="img img-responsive" alt="${data.WORKNAME}"/>
                                </div> 
                            </a>
                        </c:forEach>

                        <!--                        <div class="col-md-6 xs-plr-10">
                                                    <img src="../../client_resource/images/a1.jpg" class="img img-responsive"/>
                                                </div>
                                                <div class="col-md-6 xs-plr-10">
                                                    <img src="../../client_resource/images/a1.jpg" class="img img-responsive"/>
                                                </div>
                                                <div class="col-md-6 xs-plr-10">
                                                    <img src="../../client_resource/images/a1.jpg" class="img img-responsive"/>
                                                </div>
                                                <div class="col-md-6 xs-plr-10">
                                                    <img src="../../client_resource/images/a1.jpg" class="img img-responsive"/>
                                                </div>
                                                <div class="col-md-6 xs-plr-10">
                                                    <img src="../../client_resource/images/a1.jpg" class="img img-responsive"/>
                                                </div>-->


                    </div>
                </div>
            </div>
        </section>

        <div class="modal fade example-modal-lg" id="inqModal" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content bg-theme">

                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="row xs-pb-30">
                                    <div class="col-md-6 text-left">
                                        <h4 class="modal-title txt-white" id="myLargeModalLabel">Send Inquiry</h4>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <button type="button" class="close" id="btnMdlClse" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-lbl">Name <span class="text-danger"> * </span></label>
                                            <input class="form-control control" type="text" maxlength="55"
                                                   id="txtName" placeholder="Your Name" ng-model="artistInq['NAME']">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-lbl">Email <span class="text-danger"> * </span></label>                                   
                                            <input class="form-control control" type="text" maxlength="45"
                                                   id="txtEmail" ng-model="artistInq['EMAIL']">  
                                        </div>
                                    </div>
                                </div>                                                            
                            </div>

                            <div class="col-md-12">

                                <div class="form-group">
                                    <label class="control-lbl">Mobile No <span class="text-danger"> * </span></label>

                                    <input class="form-control control" type="text" maxlength="20"
                                           id="txtMobile" ng-model="artistInq['MOBILE']">

                                </div>

                            </div>

                            <div class="col-md-12">

                                <div class="form-group">
                                    <label class="control-lbl">Message <span class="text-danger"> * </span></label>

                                    <textarea class="form-control control" type="text" 
                                              id="txtMessage" ng-model="artistInq['MESSAGE']">
                                    </textarea>

                                </div>

                            </div>

                            <div class="col-md-12">
                                <button type="button" class="btn-p-art" id="btnAdd" ng-click="insertArtistInquiry('${USERID}')">Add</button>
                                <button type="button" class="btn-p-art" data-dismiss="modal">Close</button>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
    <jsp:include page="footer.jsp"/>

</body>
</html>
