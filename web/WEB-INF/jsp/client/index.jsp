<%-- 
    Document   : index
    Created on : Nov 14, 2017, 9:21:42 PM
    Author     : Mayur
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en" ng-app="PratibheApp">
    <style>
        .imgtest {
            height:370px;
            width:100%;
        }
        audio {
            background-color: #95B9C7;
        }
    </style>

    <c:set var="contextPath"  value="${pageContext.request.contextPath}"/>
    <span ng-init="contextPath = ${pageContext.request.contextPath}"></span>

    <jsp:include page="header.jsp"/>

    <body ng-controller="clientController">

        <!-- start header -->
        <jsp:include page="navbar.jsp"/>
        <!-- end header -->

        <!--start section slider -->
        <section class="slider" id="home">
            <div class="container-fluid" >
                <div class="owl-carousel owl-theme">
                    <c:forEach items="${EVENTBNRDATA}" var="data">
                        <div class="item first" style="background-image: url('${data.BANNER}');">
                            <div class="overlay">
                                <div class="title">
                                    <h1>${data.TITLE}</h1>
                                    <h3>${data.STARTDATE}</h3>
                                    <p>${data.DETAILS} </p>
                                    <a class="a-btn" href="${contextPath}/client/event/${data.EVENTID}">EXPLORE</a>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </section>
        <!--end section slider -->



        <!--start section blog -->
        <section class="blog" id="blog">
            <div class="container">
                <p class="para"><span>H</span>appenings</p>
                <div class="row">
                    <c:forEach items="${HOMEHAPPENINGS}" var="data" varStatus="loop">
                        <div class="col-md-4">
                            <div class="blog-area">
                                <div class="image">
                                    <img alt="image" 
                                         class="happeningimg" src="${data.THUMBIMG}">
                                    <c:choose>
                                        <c:when test="${(loop.index+1) % 2 == 0}">
                                            <div class="blog-overlay first"></div>
                                        </c:when>
                                        <c:when test="${(loop.index+1) % 3 == 0}">
                                            <div class="blog-overlay second"></div>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="blog-overlay third"></div>
                                        </c:otherwise>
                                    </c:choose>
                                </div>

                                <div class="box">
                                    <a href="${contextPath}/client/happenings/${data.HAPPENINGSID}">
                                        <h1>${data.TITLE}</h1>
                                    </a>
                                    <span>${data.STARTDATE}</span>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </section>
        <!--end section blog -->

        <!--start section help -->
        <section class="help">
            <div class="container">
                <p class="para"><span>J</span>oin the Movement</p>
                <div class="row">
                    <div class="col-md-5 box md-pr-0">
                        <div class="item i1">
                            <h3>Plan a Happening</h3>
                            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining
                                essentially unchanged.</p>
                            <a class="a-btn" href="${contextPath}/studentregistration">JOIN PRATIBHEY</a>
                        </div>
                    </div>
                    <div class="col-md-7 md-pl-0">
                        <img class="img-responsive img-h-320" alt="help" src="<c:url value='client_resource/images/a1.jpg'/>">
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-7 md-pr-0">
                        <img class="img-responsive img-h-320" alt="help" src="<c:url value='client_resource/images/a1.jpg'/>">
                    </div>
                    <div class="col-md-5 box md-pl-0">
                        <div class="item i2">
                            <h3>Get mentored</h3>
                            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining
                                essentially unchanged.</p>
                            <a class="a-btn" href="${contextPath}/studentregistration">JOIN PRATIBHEY</a>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-5 box md-pr-0">
                        <div class="item i3">
                            <h3>Partner with us</h3>
                            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining
                                essentially unchanged.</p>
                            <a class="a-btn" href="${contextPath}/studentregistration">JOIN PRATIBHEY</a>
                        </div>
                    </div>
                    <div class="col-md-7 md-pl-0">
                        <img class="img-responsive img-h-320" alt="help" src="<c:url value='client_resource/images/a1.jpg'/>">
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-7 md-pr-0">
                        <img class="img-responsive img-h-320" alt="help" src="<c:url value='client_resource/images/a1.jpg'/>">
                    </div>
                    <div class="col-md-5 box md-pl-0">
                        <div class="item i4">
                            <h3>Parichey movement</h3>
                            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining
                                essentially unchanged.</p>
                            <a class="a-btn" href="${contextPath}/studentregistration">JOIN PRATIBHEY</a>
                        </div>
                    </div>

                </div>



            </div>
        </section>
        <!--end section help -->

        <!-- start section work -->
        <section class="work" id="work">
            <div class="container" id="change">
                <p class="para"><span>F</span>eatured Artists</p>

                <c:forEach items="${ARTISTSDATA}" var="data">
                    <div class="work-area">
                        <div class="image">
                            <img alt="image" src="${data.IMAGE}">
                        </div>
                        <a href="${contextPath}/client/artists/${data.USERID}" class="overlay">
                        <!--<a href="${data.IMAGE}" class="overlay" data-lightbox="image">-->
                            <div class="inner">
                                <h2>${data.NAME}</h2>
                                <p>${data.ARTISTNAME}</p>
                            </div>
                        </a>
                    </div>
                </c:forEach>
                <c:forEach var="ctr" begin="${ARTISTSDATA.size()}" end="13">
                    <div class="work-area">
                        <div class="image">
                            <img alt="image" src="<c:url value='client_resource/images/no_img.png'/>">
                        </div>
                        <a href="" class="overlay">
                            <div class="inner">
                                <h2>No Artists</h2>
                                <p>No,Blog</p>
                            </div>
                        </a>
                    </div>
                </c:forEach>

                <a class="user" href="${contextPath}/artist">MORE ARTISTS <i class="fa fa-arrow-right"></i></a>

            </div>
        </section>
        <!-- end section work -->

        <!-- start section work -->
        <section class="artworks">
            <div class="container">
                <p class="para"><span>F</span>eatured Artworks</p>

                <div class="row">

                    <div class="col-md-4 md-pr-0 xs-mb-10">
                        <c:choose>
                            <c:when test="${FETWORKDATA[0] == null || FETWORKDATA[0]==''}">
                                <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                     style="height: 206px; width: 100%" class="img-responsive">
                            </c:when>   
                            <c:otherwise>
                                <a href="${contextPath}/client/artwork/${FETWORKDATA[0].USERWORKID}">
                                    <img src="<c:url value='${FETWORKDATA[0].FILE}'/>" 
                                         style="height: 206px; width: 100%" class="img-responsive">
                                </a>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="col-md-4 md-pr-0 xs-mb-10" >
                        <c:choose>
                            <c:when test="${FETWORKDATA[1] == null}">
                                <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                     style="height: 206px; width: 100%" class="img-responsive">
                            </c:when>   
                            <c:otherwise>
                                <a href="${contextPath}/client/artwork/${FETWORKDATA[1].USERWORKID}">
                                    <img src="<c:url value='${FETWORKDATA[1].FILE}'/>" 
                                         style="height: 206px; width: 100%" class="img-responsive">
                                </a>
                            </c:otherwise>
                        </c:choose>  
                    </div>
                    <div class="col-md-4 md-pr-0 xs-mb-10" >
                        <c:choose>
                            <c:when test="${FETWORKDATA[2] == null}">
                                <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                     style="height: 206px; width: 100%" class="img-responsive">
                            </c:when>   
                            <c:otherwise>
                                <a href="${contextPath}/client/artwork/${FETWORKDATA[2].USERWORKID}">
                                    <img src="<c:url value='${FETWORKDATA[2].FILE}'/>" 
                                         style="height: 206px; width: 100%" class="img-responsive">
                                </a>
                            </c:otherwise>
                        </c:choose> 
                    </div>
                    <div class="col-md-4 md-pr-0 xs-mb-10" >
                        <c:choose>
                            <c:when test="${FETWORKDATA[3] == null}">
                                <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                     style="height: 206px; width: 100%" class="img-responsive">
                            </c:when>   
                            <c:otherwise>
                                <a href="${contextPath}/client/artwork/${FETWORKDATA[3].USERWORKID}">
                                    <img src="<c:url value='${FETWORKDATA[3].FILE}'/>" 
                                         style="height: 206px; width: 100%" class="img-responsive">
                                </a>
                            </c:otherwise>
                        </c:choose>  
                    </div>
                    <div class="col-md-4 md-pr-0 xs-mb-10" >
                        <c:choose>
                            <c:when test="${FETWORKDATA[4] == null}">
                                <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                     style="height: 206px; width: 100%" class="img-responsive">
                            </c:when>   
                            <c:otherwise>
                                <a href="${contextPath}/client/artwork/${FETWORKDATA[4].USERWORKID}">
                                    <img src="<c:url value='${FETWORKDATA[4].FILE}'/>" 
                                         style="height: 206px; width: 100%" class="img-responsive">
                                </a>
                            </c:otherwise>
                        </c:choose>  
                    </div>
                    <div class="col-md-4 md-pr-0 xs-mb-10" >
                        <c:choose>
                            <c:when test="${FETWORKDATA[5] == null}">
                                <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                     style="height: 206px; width: 100%" class="img-responsive">
                            </c:when>   
                            <c:otherwise>
                                <a href="${contextPath}/client/artwork/${FETWORKDATA[5].USERWORKID}">
                                    <img src="<c:url value='${FETWORKDATA[5].FILE}'/>" 
                                         style="height: 206px; width: 100%" class="img-responsive">
                                </a>
                            </c:otherwise>
                        </c:choose>  
                        <a class="btn-artwork" href="#">MORE ART Work <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
                <%--<div class="row">
            <div class="col-md-6 pr-0">
                <div class="row">
                    <div class="col-md-7 pr-0">

                                <c:choose>
                                    <c:when test="${FETWORKDATA[1] == null}">
                                        <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                             style="width: 283px;height: 232px" class="img-responsive">
                                    </c:when>   
                                    <c:otherwise>
                                        <a href="${contextPath}/client/artwork/${FETWORKDATA[1].USERWORKID}">
                                            <img src="<c:url value='${FETWORKDATA[1].FILE}'/>" 
                                                 style="width: 283px;height: 232px" class="img-responsive">
                                        </a>
                                    </c:otherwise>
                                </c:choose>        

                            </div>
                            <div class="col-md-5 p-0">
                                <c:choose>
                                    <c:when test="${FETWORKDATA[2] == null}">
                                        <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                             style="width: 183px;height: 382px" class="img-responsive">
                                    </c:when>   
                                    <c:otherwise>
                                        <a href="${contextPath}/client/artwork/${FETWORKDATA[2].USERWORKID}">
                                            <img src="<c:url value='${FETWORKDATA[2].FILE}'/>" 
                                                 style="width: 183px;height: 382px" class="img-responsive">                                    
                                        </a>
                                    </c:otherwise>
                                </c:choose>  
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 pl-0">
                        <div class="row">
                            <div class="col-md-4 p-0">
                                <c:choose>
                                    <c:when test="${FETWORKDATA[3] == null}">
                                        <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                             style="width: 170px;height: 152px" class="img-responsive">
                                    </c:when>   
                                    <c:otherwise>
                                        <a href="${contextPath}/client/artwork/${FETWORKDATA[3].USERWORKID}">
                                            <img src="<c:url value='${FETWORKDATA[3].FILE}'/>" 
                                                 style="width: 170px;height: 152px" class="img-responsive"> 
                                        </a>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                            <div class="col-md-2 p-0">
                                <c:choose>
                                    <c:when test="${FETWORKDATA[4] == null}">
                                        <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                             style="width: 85px;height: 152px" class="img-responsive">
                                    </c:when>   
                                    <c:otherwise>
                                        <a href="${contextPath}/client/artwork/${FETWORKDATA[4].USERWORKID}">
                                            <img src="<c:url value='${FETWORKDATA[4].FILE}'/>" 
                                                 style="width: 85px;height: 152px" class="img-responsive">                                    
                                        </a>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                            <div class="col-md-6 pl-0">
                                <c:choose>
                                    <c:when test="${FETWORKDATA[5] == null}">
                                        <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                             style="width: 240px;height: 152px" class="img-responsive">
                                    </c:when>   
                                    <c:otherwise>
                                        <a href="${contextPath}/client/artwork/${FETWORKDATA[5].USERWORKID}">
                                            <img src="<c:url value='${FETWORKDATA[5].FILE}'/>" 
                                                 style="width: 240px;height: 152px" class="img-responsive">
                                        </a>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 p-0">
                                <c:choose>
                                    <c:when test="${FETWORKDATA[6] == null}">
                                        <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                             style="width: 240px;height: 230px" class="img-responsive">
                                    </c:when>   
                                    <c:otherwise>
                                        <a href="${contextPath}/client/artwork/${FETWORKDATA[6].USERWORKID}">
                                            <img src="<c:url value='${FETWORKDATA[6].FILE}'/>" 
                                                 style="width: 240px;height: 230px" class="img-responsive">
                                        </a>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                            <div class="col-md-6 pl-0">
                                <div class="row">
                                    <div class="col-md-12 p-0">
                                        <a class="btn-artwork" href="#">MORE ARTISTS <i class="fa fa-arrow-right"></i></a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 p-0">
                                        <c:choose>
                                            <c:when test="${FETWORKDATA[7] == null}">
                                                <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                                     style="width: 255px;height: 158px" class="img-responsive">
                                            </c:when>   
                                            <c:otherwise>
                                                <a href="${contextPath}/client/artwork/${FETWORKDATA[7].USERWORKID}">
                                                    <img src="<c:url value='${FETWORKDATA[7].FILE}'/>" 
                                                         style="width: 255px;height: 158px" class="img-responsive">
                                                </a>
                                            </c:otherwise>
                                        </c:choose> 
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end section work -->--%>

                <section class="music-player">
                    <div class="container">
                        <p class="para"><span>F</span>eatured Music</p>
                        <div class="row">
                            <c:forEach items="${FETMUSICDATA}" var="data">

                                <div class="col-md-6 xs-mb-10">
                                    <div class="play-list">
                                        <div class="col-md-3 md-pl-0 md-pr-0">
                                            <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                                 class="img-responsive" width="100px" height="100px">
                                        </div>
                                        <div class="col-md-9 text-left txt-white">
                                            <h3 class="xs-mb-30 xs-mt-0">  ${data.WORKNAME} Pratibhe Music.mp3</h3>
                                            <audio controls style preload="none">
                                                <source src="${data.FILE}" type="audio/mpeg">
                                                Your browser does not support the audio element.
                                            </audio>
                                        </div>
                                    </div>

                                </div>

                            </c:forEach>
                        </div>
                    </div>
                </section>

                <jsp:include page="footer.jsp"/>

                </body>

                </html>
