<%-- 
    Document   : happeningdetail
    Created on : Nov 19, 2017, 1:39:10 PM
    Author     : Mayur
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en" ng-app="PratibheApp">

    <jsp:include page="header.jsp"/>

    <body ng-controller="clientController">
        <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
        <!-- start header -->
        <jsp:include page="navbar.jsp"/>
        <!-- end header -->

        <!--start section slider -->
        <!--        <section class="slider" id="home">
                    <div class="container-fluid" >
                        <div class="owl-carousel owl-theme">
                                <div class="item first" style="background-image: url('${BANNER}');">
                                    <div class="overlay">
                                        
                                    </div>
                                </div>
                        </div>
                    </div>
                </section>-->
        <section>        
            <div class="overlay">
                <img alt="image" class="img-responsive" src="${BANNER}" style="width: 100%;  height: 80vh;">        
            </div>
        </section>
        <!--end section slider -->

        <!--start happening section  -->
        <section class="happening-details">  
            <div class="container text-center">
                <h2>${TITLE}</h2>
                <h4>${STARTDATE} , ${ADDRESS}</h4>
                <p>${DETAILS}

                </p>
                <a href="#" class="pull-left">www.eventshigh.com</a>
            </div>   
            <br/>
            <div class="container">
                <input type="button" id="btnInsert" value="PARTICIPATE" ng-click="insertPartHap('${HAPPENINGSID}')" class="search-happenings-btn"/>
            </div>
        </section>
        <!--end happening section  -->

        <!--start section blog -->
        <section class="blog" id="blog">
            <div class="container">
                <p class="para"><span>H</span>appenings</p>
                <div class="row">
                    <c:forEach items="${MOREHAPPENINGS}" var="data" varStatus="loop">
                        <div class="col-md-4">
                            <div class="blog-area">
                                <div class="image">
                                    <img alt="image" 
                                         class="happeningimg" src="${data.THUMBIMG}">
                                    <c:if test="${loop.index eq 0}">
                                        <div class="blog-overlay first"></div>
                                    </c:if>
                                    <c:if test="${loop.index eq 1}">
                                        <div class="blog-overlay second"></div>
                                    </c:if>
                                    <c:if test="${loop.index eq 2}">
                                        <div class="blog-overlay third"></div>
                                    </c:if>
                                </div>

                                <div class="box">
                                    <a href="${contextPath}/client/happenings/${data.HAPPENINGSID}">
                                        <h1>${data.TITLE}</h1>
                                    </a>
                                    <span>${data.STARTDATE}</span>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </section>
        <!--end section blog -->

        <!--start section help -->
        <!--        <section class="help">
                    <div class="container">
        
                        <div class="row">
                            <div class="col-md-5 box left">
                                <div class="item i5">
                                    <h3>Create a Happenings</h3>
                                    <span>It has survived not only five centuries</span>
                                    <p> but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                                    <a class="a-btn" href="#">Submit happenings</a>
                                </div>
                            </div>
                            <div class="col-md-7 box-img left">
                                <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
        
                        </div>
        
        
                    </div>
                </section>-->
        <!--end section help -->
        <jsp:include page="footer.jsp"/>
    </body>
</html>

