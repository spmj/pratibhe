<%-- 
    Document   : gallery
    Created on : Dec 17, 2017, 11:51:56 AM
    Author     : jay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html lang="en" ng-app="PratibheApp">
    <jsp:include page="header.jsp"/>
    <title>Gallery</title>

    <body ng-controller="clientController">
        <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
        <!-- start header -->
        <jsp:include page="navbar.jsp"/>
        <!-- end header -->
        <!--start section slider -->
        <section class="banner xs-mt-60">            
            <h1>Gallery</h1>
        </section>
        <!--end section slider -->

        <section class="happening-filter">
            <div class="container">
                <div class="filter-blog">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="col-md-3">
                                <select class="form-control" name="cmbArtist" ng-model="GALLERYDATA['USERID']">
                                    <option value="">Artist</option>
                                    <c:forEach items="${ARTISTFILTER}" var="data">
                                        <option value="${data.USERID}">${data.NAME}</option>
                                    </c:forEach>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <select class="form-control" name="cmbArtForm" ng-model="GALLERYDATA['ARTID']">
                                    <option value="">Art Form</option>
                                    <c:forEach items="${ARTFILTER}" var="data">
                                        <option value="${data.ARTID}">${data.ARTNAME}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <input type="button" value="SEARCH ART" ng-click="getData();" class="search-happenings-btn"/>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- start section work -->
        <section class="gallery-section bg-theme xs-ptb-30" id="work"  ng-init="getGalleryData()">
            <div class="container" id="change" id="artDiv">
                <div class="row">
                    <div class="col-md-12 txt-white">
                        <h2>Gallery</h2>
                        <!--{{GALLERYDATA}}-->
                    </div>
                </div>
                <div class="container" ng-if="GALLERYDATAS.length == 0">
                    <div class="row listnotfound">
                        <center>
                            No Art found.
                        </center>
                    </div>
                </div>
                <div class="row md-mt-10" ng-if="GALLERYDATAS.length != 0">
                    <div class="col-md-4 md-pr-0 xs-mb-10" ng-repeat="data in GALLERYDATAS">

                        <a href="${contextPath}/client/artwork/{{data.USERWORKID}}">
                            <img src="{{data.FILE}}"
                                 style="height: 206px; width: 100%" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="row" ng-if="GALLERYDATAS.length != 0">
                    <div class="col-md-8" style="padding: 0">
                        <pagination total-items="GALLERYDATA.totalItems" ng-click="getGalleryPageData(GALLERYDATA.currentPage)" 
                                    page="GALLERYDATA.currentPage"  max-size="GALLERYDATA.maxSize" 
                                    class="pagination-sm" boundary-links="true" rotate="false" 
                                    num-pages="numPages" items-per-page="GALLERYDATA.itemsPerPage" style="margin-left: 15px; margin-top: 0">
                        </pagination>
                    </div>
                    <div class="col-md-4 text-right">
                        {{record = GALLERYDATA.currentPage * 9;
                                    ""}}
                        {{record - 9 + 1}} to <span ng-if="record < GALLERYDATA.totalItems">{{record}}</span> 
                        <span ng-if="record > GALLERYDATA.totalItems">{{GALLERYDATA.totalItems}}</span> of <span class="badge">{{GALLERYDATA.totalItems}}</span>
                    </div>
                </div>            
            </div>
        </section>
        <!-- end section work -->

        <section class="music-player" ng-init="getGalleryMusicData()">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 txt-white">
                        <h2>Music</h2>
                    </div>
                </div>
                <div class="container" ng-if="GALLERYMUSICDATAS.length == 0">
                    <div class="row listnotfound">
                        <center>
                            No Music found.
                        </center>
                    </div>
                </div>
                <div class="row" ng-if="GALLERYMUSICDATAS.length != 0">
                    <div class="col-md-6 xs-mb-10" ng-repeat="data in GALLERYMUSICDATAS">
                        <div class="play-list">
                            <div class="col-md-3 md-pl-0 md-pr-0">
                                <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                     class="img-responsive" width="100px" height="100px">
                            </div>
                            <div class="col-md-9 text-left txt-white">
                                <h3 class="xs-mb-30 xs-mt-0">  {{data.WORKNAME}} ( {{data.NAME}} )</h3>
                                <audio controls style preload="none">
                                    <source src="{{data.FILE}}" type="audio/mpeg">
                                    Your browser does not support the audio element.
                                </audio>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" ng-if="GALLERYMUSICDATAS.length != 0">
                    <div class="col-md-8" style="padding: 0">
                        <pagination total-items="GALLERYMUSICDATA.totalItems" ng-click="getGalleryPageData(GALLERYMUSICDATA.currentPage)" 
                                    page="GALLERYMUSICDATA.currentPage"  max-size="GALLERYMUSICDATA.maxSize" 
                                    class="pagination-sm" boundary-links="true" rotate="false" 
                                    num-pages="numPages" items-per-page="GALLERYMUSICDATA.itemsPerPage" style="margin-left: 15px; margin-top: 0">
                        </pagination>
                    </div>
                    <div class="col-md-4 text-right">
                        {{record = GALLERYMUSICDATA.currentPage * 4;
                                    ""}}
                        {{record - 4 + 1}} to <span ng-if="record < GALLERYMUSICDATA.totalItems">{{record}}</span> 
                        <span ng-if="record > GALLERYMUSICDATA.totalItems">{{GALLERYMUSICDATA.totalItems}}</span> of <span class="badge">{{GALLERYMUSICDATA.totalItems}}</span>
                    </div>
                </div>                    
            </div>
        </section>

        <jsp:include page="footer.jsp"/>

    </body>

</html>
