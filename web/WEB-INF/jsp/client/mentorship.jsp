<%-- 
    Document   : about_us
    Created on : Nov 27, 2017, 8:22:38 PM
    Author     : Mayur
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html lang="en" ng-app="PratibheApp">
    <jsp:include page="header.jsp"/>
    <title>Student Registration</title>

    <body ng-controller="registerController">
        <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
        <!-- start header -->
        <jsp:include page="navbar.jsp"/>
        <!-- end header -->
  <!--start section slider -->
        <section class="banner xs-mt-60">            
            <h1>Mentorship</h1>
        </section>
        <!--end section slider -->
        <!--start section slider -->

        <section class="partner-details bg-theme help xs-pt-50">
        </section>

        <!-- start section work -->
        
        <!-- end section work -->


        <!-- start section work -->
        <section class="mentor-details xs-ptb-30">
            <div class="container">
                <h2>Mentorship</h2>
                <p>Lorem ipsum lrem opsum Lorem ipsum lrem opsumLorem ipsum lrem opsum Lorem ipsum lrem opsum</p>

                <div class="row">
                    <div class="col-md-12">                      
                        <ul class="nav nav-tabs theme-tab xs-mt-10">
                            <li class="active"><a data-toggle="tab" href="#home">Pratibhey Music Mentorship</a></li>
                            <li><a data-toggle="tab" href="#menu1">Pratibhey Art  Mentorship</a></li>
              
                        </ul>
                        <div class="tab-content xs-mt-20">
                            <div id="home" class="tab-pane fade in active">
                                <div class="row">
                                    <div class="col-md-4">
                                         <img alt="image" class="img img-responsive" src="https://bloximages.chicago2.vip.townnews.com/indiawest.com/content/tncms/assets/v3/editorial/5/8c/58ce6304-becc-11e4-83e1-873e6f527ee5/54f0e9a048da7.image.jpg"/>
                                    </div>
                                     <div class="col-md-4">
                                         <img alt="image" class="img img-responsive" src="https://bloximages.chicago2.vip.townnews.com/indiawest.com/content/tncms/assets/v3/editorial/5/8c/58ce6304-becc-11e4-83e1-873e6f527ee5/54f0e9a048da7.image.jpg"/>
                                    </div>
                                     <div class="col-md-4">
                                         <img alt="image" class="img img-responsive"  src="https://bloximages.chicago2.vip.townnews.com/indiawest.com/content/tncms/assets/v3/editorial/5/8c/58ce6304-becc-11e4-83e1-873e6f527ee5/54f0e9a048da7.image.jpg"/>
                                    </div>
                                    <div class="col-md-12 xs-pt-10">
                                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
                                        There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc. <a href="#">www.eventshigh.com</a></p>
                                    </div>
                                    <div class="col-md-12 xs-pt-10">
                                       <p class="para"><span>M</span>entors </p>
                                    </div>
                                    <div class="col-md-12 mentor-sub xs-pt-10">
                                        <h3>Clement Yang</h3>
                                        <p>Producer/Arranger/Musician <a href="#"> www.uglyinthemorning.org</a></p>
                                        <div class="row">
                                            <div class="col-md-2 md-pr-0">
                                                <img alt="image" class="img img-responsive" src="<c:url value='client_resource/images/no_img.png'/>">
                                            </div>
                                            <div class="col-md-10">
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                                            </div>
                                        </div>
                                    </div>
                                             <div class="col-md-12 mentor-sub xs-pt-10">
                                        <h3>Clement Yang</h3>
                                        <p>Producer/Arranger/Musician <a href="#"> www.uglyinthemorning.org</a></p>
                                        <div class="row">
                                            <div class="col-md-2 md-pr-0">
                                                <img alt="image" class="img img-responsive" src="<c:url value='client_resource/images/no_img.png'/>">
                                            </div>
                                            <div class="col-md-10">
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                                            </div>
                                        </div>
                                    </div>
                                             <div class="col-md-12 mentor-sub xs-pt-10">
                                        <h3>Clement Yang</h3>
                                        <p>Producer/Arranger/Musician <a href="#"> www.uglyinthemorning.org</a></p>
                                        <div class="row">
                                            <div class="col-md-2 md-pr-0">
                                                <img alt="image" class="img img-responsive" src="<c:url value='client_resource/images/no_img.png'/>">
                                            </div>
                                            <div class="col-md-10">
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                                            </div>
                                        </div>
                                    </div>
                                             <div class="col-md-12 mentor-sub xs-pt-10">
                                        <h3>Clement Yang</h3>
                                        <p>Producer/Arranger/Musician <a href="#"> www.uglyinthemorning.org</a></p>
                                        <div class="row">
                                            <div class="col-md-2 md-pr-0">
                                                <img alt="image" class="img img-responsive" src="<c:url value='client_resource/images/no_img.png'/>">
                                            </div>
                                            <div class="col-md-10">
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                               
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <h3>Menu 1</h3>
                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>



        <jsp:include page="footer.jsp"/>

    </body>

</html>
