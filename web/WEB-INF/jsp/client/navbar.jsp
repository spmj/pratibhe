<%-- 
    Document   : navbar
    Created on : Nov 21, 2017, 8:35:02 PM
    Author     : jay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!-- Start  Loading Section -->
<div class="loading-overlay">
    <div class="loading">
        <div>
            <div class="c1"></div>
            <div class="c2"></div>
            <div class="c3"></div>
            <div class="c4"></div>
        </div>
        <span>loading</span>
    </div>
</div>
<!-- end loading section -->

<header id="pageHeader" class="header navbar">
    <div class="container-fluid">
        <div class="navbar-header md-pl-100">
<!--            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar" aria-expanded="false">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>-->
<a class="navbar-brand" href="#"><img src="client_resource/images/logo.png" /></a>
            <div id="myNav" class="overlay-menu">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                <div class="overlay-content">
                    <a href="${contextPath}">Home</a>
                    <a href="${contextPath}/client/aboutus">About Parichey</a>
                    <a href="${contextPath}/client/ourpartners">Our Partners</a>
                    <a href="${contextPath}/client/happenings">Happenings</a>
                    <a href="${contextPath}/client/programmes">Programmes </a>
                    <a href="${contextPath}/mentorship">Mentorships</a>
                    <a href="${contextPath}/gallery">Gallery</a>
                    <a href="#">Resources</a>
                    <a href="${contextPath}/studentregistration">Join As Artist</a>
                    <a href="${contextPath}/mentorregistration">Join As Mentor</a>
                    <c:if test="${sessionbean.userId eq null }">
                        <a href="${contextPath}/login">Login</a>
                    </c:if>
                    <c:if test="${sessionbean.userId ne null }">
                        <a href="${contextPath}/dashboard">Dashboard</a>
                    </c:if>

                </div>
            </div>
            <span class="toggle-overlay" onclick="openNav()">&#9776;</span>

        </div>
    </div>
</header>
