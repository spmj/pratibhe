<%-- 
    Document   : footer
    Created on : Nov 21, 2017, 8:24:35 PM
    Author     : jay
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!--
<div class="social-icon-div">
    <div class="container">
        <a href="#">
            <i class="fa fa-facebook"></i>
        </a>
        <a href="#">
            <i class="fa fa-twitter"></i>
        </a>
        <a href="#">
            <i class="fa fa-google-plus"></i>
        </a>
        <a href="#">
            <i class="fa fa-instagram"></i>
        </a>
        <p>Copyright © 2017 mezo10, All Rights Reserved.</p>
    </div>
</div>-->

<!--footer-->
<footer class="footer1">
    <div class="container footer-bg">

        <div class="row"><!-- row -->

           


            <div class="col-lg-3 col-md-3"><!-- widgets column left -->

                <ul class="list-unstyled clear-margins"><!-- widgets -->

                    <li class="widget-container widget_nav_menu"><!-- widgets list -->

                        <h1 class="title-widget">Pratibhey</h1>

                        <ul>
                            <li><a  href="#"> About</a></li>
                            <li><a  href="#"> Sponsors</a></li>
                            <li><a  href="#"> Outreach partners</a></li>
                            <li><a  href="#"> Contact Us</a></li>
                            <li><a  href="#">Terms & Conditions</a></li>
                        </ul>

                    </li>

                </ul>


            </div><!-- widgets column left end -->



            <div class="col-lg-3 col-md-3"><!-- widgets column left -->

                <ul class="list-unstyled clear-margins"><!-- widgets -->

                    <li class="widget-container widget_nav_menu"><!-- widgets list -->

                        <h1 class="title-widget">Showcase</h1>

                        <ul>
                            <li><a  href="#"> Gallery</a></li>
                           
                        </ul>
                        <h1 class="title-widget">Mentorship</h1>

                        <ul>
                            <li><a  href="#"> Noise Music Mentorship</a></li>
                            <li><a href="#">Noise Singapore Award</a></li>
                           
                        </ul>

                    </li>

                </ul>


            </div><!-- widgets column left end -->

 <div class="col-lg-3 col-md-3"><!-- widgets column left -->
                <ul class="list-unstyled clear-margins"><!-- widgets -->

                    <li class="widget-container widget_nav_menu"><!-- widgets list -->

                        <h1 class="title-widget">Resources</h1>

                        <ul>
                            <li><a  href="#"> Happenings</a></li>
                            <li><a  href="#"> Noise movement</a></li>
                            <li><a  href="#"> Noise Matchbox</a></li>
                            <li><a  href="#"> Useful Links</a></li>                           
                        </ul>

                        
                         
                    </li>

                </ul>


            </div><!-- widgets column left end -->

            
            <div class="col-lg-3 col-md-3 md-pl-0"><!-- widgets column center -->



                <ul class="list-unstyled clear-margins"><!-- widgets -->

                    <li class="widget-container widget_recent_news txt-white"><!-- widgets list -->

                        <h1 class="title-widget">Pratibhey is in everyone </h1>
                        <p> Stay in touch and get all the latest happenings in your inbox.</p>
                         <div class="input-group">
                             <input id="email" type="text" class="form-control" name="email">
                             <span class="input-group-addon"><button type="button" class="btn-newslleter">Join</button></span>
                                    
                                  </div>
                        
<!--                        <h1 class="title-widget">Struck somewhere? </h1>
                        <p>Drop us a note and well get the noise going with you soon!</p>-->
 <h1 class="title-widget">Tune in to us at</h1>
                         <div class="social-icons">

                            <ul class="nomargin">

                                <a href="#"><i class="fa fa-facebook-square fa-3x social-fb" id="social"></i></a>
                                <a href="#"><i class="fa fa-twitter-square fa-3x social-tw" id="social"></i></a>
                                <a href="#"><i class="fa fa-instagram fa-3x social-gp" id="social"></i></a>
                                

                            </ul>
                        </div>
                        
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!--header-->


<!-- Start Scroll To Top -->
<div id="scroll-top">
    <i class="fa fa-long-arrow-up"></i>
</div>
<!-- end Scroll To Top -->

<script src="<c:url value='/js/angular.js'/>" type="text/javascript"></script>
<!--        <script src="<c:url value='/js/app.js' />" type="text/javascript"></script>
<script src="<c:url value='/js/approot.js' />" type="text/javascript"></script>-->
<script>
    var app = angular.module('PratibheApp', ['ui.bootstrap','angularjs-datetime-picker','checklist-model']);
</script>

<script src="<c:url value='/js/Service/client/clientService.js'/>" type="text/javascript"></script>
<script src="<c:url value='/js/Controller/client/clientController.js'/>" type="text/javascript"></script>
<script src="<c:url value='/js/Service/registerService.js'/>" type="text/javascript"></script>
<script src="<c:url value='/js/Controller/registerController.js'/>" type="text/javascript"></script>
<script src="<c:url value='/js/Directive/commonDirective.js'/>" type="text/javascript"></script>
<!-- end Scroll To Top -->
<!-- inject:js -->
<script src="<c:url value="/resource/bower_components/jquery/dist/jquery.min.js"/>"></script>
<script src="<c:url value="/resource/bower_components/bootstrap/dist/js/bootstrap.min.js"/>"></script>
<script src="<c:url value="/resource/bower_components/jquery.nicescroll/dist/jquery.nicescroll.min.js"/>"></script>
<script src="<c:url value="/resource/bower_components/autosize/dist/autosize.min.js"/>"></script>
<script src="<c:url value='/resource/assets/js/jquery.bootstrap-growl.min.js' />" type="text/javascript"></script>

<script src="<c:url value="/client_resource/js/jquery.min.js" />"></script>
<script src="<c:url value="/client_resource/js/jquery.counterup.min.js" />"></script>
<script src="<c:url value="/client_resource/js/waypoints.min.js" />"></script>
<script src="<c:url value="/client_resource/js/bootstrap.min.js" />"></script>
<script src="<c:url value="/client_resource/js/slick.js" />"></script>
<script src="<c:url value="/client_resource/js/lity.min.js" />"></script>
<script src="<c:url value="/client_resource/js/lightbox.js" />"></script>
<script src="<c:url value="/client_resource/js/mixitup.js" />"></script>
<script src="<c:url value="/client_resource/js/owl.carousel.min.js" />"></script>

<script src="<c:url value="/client_resource/js/custom.js"/>"></script>
<script src="<c:url value="/client_resource/js/wow.min.js"/>"></script>
<script src="<c:url value='/js/validate.js' />" type="text/javascript"></script>
<script src="<c:url value='/client_resource/js/pnotify.custom.min.js' />"></script>
<script src="<c:url value="/js/ui-bootstrap-tpls.min.js"/>" type="text/javascript"></script>
<script src="<c:url value='/js/checklist-model.min.js' />" type="text/javascript"></script>  
<script src="<c:url value='/resource/assets/js/angularjs-datetime-picker.js'/>" type="text/javascript"></script>
<script>
    new WOW().init();
</script>
<script>
    function openNav() {
        document.getElementById("myNav").style.width = "90%";
    }

    function closeNav() {
        document.getElementById("myNav").style.width = "0%";
    }


</script>
