<%-- 
    Document   : about_us
    Created on : Nov 27, 2017, 8:22:38 PM
    Author     : Mayur
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html lang="en" ng-app="PratibheApp">
    <jsp:include page="header.jsp"/>
    <title>Student Registration</title>

    <body ng-controller="registerController">
        <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
        <!-- start header -->
        <jsp:include page="navbar.jsp"/>
        <!-- end header -->

        <!--start section slider -->
        <!--        <section class="banner">
                    <img alt="image" class="img-responsive" src="<c:url value='/client_resource/images/banner1.jpg'/>" style="width: 100%;">
                </section>-->
        <!--end section slider -->

        <section class="partner-details bg-theme help xs-pt-50">
<!--            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="login-box">
                            <div class="logo-img text-center xs-mtb-50">
                                <h1 style="color:#fff;">Pratibhe</h1>
                            </div>
                            <div class="login-form">
                                <div class="form-group">
                                    <label class="control-lbl" for="email">Email</label>
                                    <input type="text" class="form-control control"  placeholder="Enter your Email" >
                                </div>
                                <div class="form-group">
                                    <label class="control-lbl" for="password">Password</label>
                                    <input type="text" class="form-control control" placeholder="Enter password">
                                </div>
                                <div class="xs-mb-10 xs-mt-40">
                                    <button class="btn btn-login">Login</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <div class="container">
                <h2>Register on Pratibhey</h2>
                <p>Be a part of growing community at sharing your work, attending events,blah blah blah.. </p>
                <br>
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-lbl" for="fullname">Full Name <span>*</span></label>
                            <input type="text" class="form-control control"  ng-model="formData['NAME']"
                                   id="txtName" placeholder="Enter Full Name" >
                        </div>

                        <div class="form-group">
                            <label class="control-lbl" for="email">Email <span>*</span></label>
                            <input type="text" class="form-control control" ng-model="formData['EMAIL']"
                                   id="txtEmail" placeholder="Enter enail address" >
                        </div>

                        <div class="form-group">
                            <label class="control-lbl" for="birthday">Birthday <span>*</span></label>
                            <input type="text" class="form-control control" ng-model="formData['DOB']"
                                   datetime-picker date-format="dd-MMM-YYYY" date-only
                                   id="txtDob" placeholder="Enter Birthdate" >
                        </div>

                        <div class="form-group">
                            <label class="control-lbl" for="adhar">Aadhar ID <span>*</span></label>
                            <input type="text" class="form-control control" ng-model="formData['AADHARNO']"
                                   id="txtAadhar" placeholder="Enter Aadhar">
                        </div>

                        <div class="form-group">
                            <label class="control-lbl" for="art">Art <span>*</span></label>
                            <select class="form-control control" id="cmdArt" ng-model="formData['ARTID']">
                                <option value="">Select Art</option>
                                <c:forEach items="${ART}" var="data" varStatus="loop">
                                    <option value="${data.ARTID}">${data.ARTNAME}</option>        
                                </c:forEach>
                            </select>
                        </div>

                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-lbl" for="artistname">Artist Name <span>*</span></label>
                            <input type="text" class="form-control control" ng-model="formData['ARTISTNAME']"
                                   id=""   placeholder="What you would like to be known as.." >
                        </div>

                        <div class="form-group">
                            <label class="control-lbl" for="contact">Contact no. <span>*</span></label>
                            <input type="text" class="form-control control" ng-model="formData['MOBILE']"
                                   id=""   placeholder="Mobile/Landline" >
                        </div>

                        <div class="form-group">
                            <label class="control-lbl" for="email">Occupation <span>*</span></label>
                            <input type="text" class="form-control control" ng-model="formData['OCCUPATION']"
                                   id=""  placeholder="Enter Occupation" >
                        </div>

                        <div class="form-group">
                            <label class="control-lbl" for="website">Website <span>*</span></label>
                            <input type="text" class="form-control control" ng-model="formData['WEBSITE']"
                                   id=""  placeholder="Enter website url" >
                        </div>
                        <div class="form-group">
                            <label class="control-lbl" for="gender" >Gender <span>*</span></label>

                            <div class="radio">
                                <input type="radio" name="optradio" value="MALE" ng-model="formData['GENDER']"> 
                                <label class="radio-inline" for="checkbox4">
                                    Male
                                </label>
                                <input type="radio" name="optradio"  value="FEMALE" ng-model="formData['GENDER']">
                                <label class="radio-inline" for="checkbox4">
                                    Female
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-lbl" for="about">About me </label>
                            <input type="text" class="form-control control" ng-model="formData['ABOUTME']"
                                   id="" placeholder="Express yourself to the world.." >                                                                                                                                        
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-lbl" for="about">Address </label>
                            <input type="text" class="form-control control" ng-model="formData['ADDRESS']"
                                   id="" placeholder="Express yourself to the world.." >
                        </div>
                    </div>
                    <!--<div class="row">-->
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-lbl" for="birthday">State <span>*</span></label>
                            <select class="form-control control" id="cmdState" ng-model="formData['STATEID']" 
                                    ng-change="getCity(formData.STATEID)"ng-change="getCity(formData.STATEID)">
                                <option value="">Select State</option>
                                <c:forEach items="${STATE}" var="data" varStatus="loop">
                                    <option value="${data.VALUE}">${data.TEXT}</option>        
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-lbl" for="birthday">City <span>*</span></label>
                            <select class="form-control control" id="cmdCity" ng-model="formData['CITYID']">
                                <option value="">Select City</option>
                                <option ng-repeat="data in cityName.CITYNAME" value="{{data.VALUE}}">{{data.TEXT}}</option>        
                            </select>
                        </div>
                    </div>
                    <!--</div>-->
                    <div class="col-md-6">
                        <div class="col-md-6">
                            <label class="control-lbl" for="about">Display picture(DP) <span>*</span></label>
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <span class="btn btn-upload btn-file"><span>Choose file</span><input type="file"  id="fileProfile" name="fileProfile" file-model="formData['IMAGE']" 
                                                                                                     filename="formData['IMAGENAME']" fileread="" filesize="formData['IMAGESIZE']" fileext="formData['IMAGEEXT']" 
                                                                                                     onchange="angular.element(this).scope().thumbChanged(this);
                                                                                                                     angular.element(this).scope().processFiles(this)"/></span>
                            </div>
                        </div>
                        <div class="col-md-6">       
                            <img id="imgProfile" ng-src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTEAzhs2acdAj70XFCtn_8Qr6ENkJ_CdXo_1r97sel_isfLgmoK" width="200" height="200" style="border: none;" alt="Profile Photo">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p>Where did you hear about Pratibhey? </p>
                        <ul class="reg-qst">
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox1" type="checkbox" ng-model="formData['']">
                                    <label for="checkbox1">
                                        someone told me about it
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox2" type="checkbox" ng-model="formData['']">
                                    <label for="checkbox2">
                                        Parichey social media loresum ipsum leresum ipsum
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox3" type="checkbox" ng-model="formData['']">
                                    <label for="checkbox3">
                                        someone told me about it
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox4" type="checkbox" ng-model="formData['']">
                                    <label for="checkbox4">
                                        Parichey social media loresum ipsum leresum ipsum
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox5" type="checkbox">
                                    <label for="checkbox5">
                                        someone told me about it
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox2" type="checkbox" ng-model="formData['']">
                                    <label for="checkbox2">
                                        Parichey social media loresum ipsum leresum ipsum
                                    </label>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-lbl" for="password">Password</label>
                                    <input type="text" class="form-control control" ng-model="formData['PASSWORD']"
                                           id="txtPassword"   placeholder="Only letter, number, undescore and space are allowed" >
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-lbl" for="cpass">Confirm Password </label>
                                    <input type="text" class="form-control control" ng-model="formData['REPASSWORD']"
                                           id="txtConfirmation" placeholder="Only letter, number, undescore and space are allowed" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <button ng-click="insertDetail('STUDENT')" class="btn btn-register">Register</button>
                        </div>
                    </div>

                </div>

            </div>
        </section>

        <jsp:include page="footer.jsp"/>

    </body>

</html>
