<%-- 
    Document   : eventdetail
    Created on : Nov 19, 2017, 2:03:32 PM
    Author     : Mayur
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html ng-app="PratibheApp">
    <jsp:include page="header.jsp"/>
    <title>${ARTNAME}</title>

    <body ng-controller="clientController">
        <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
        <jsp:include page="navbar.jsp"/>
        <!--start section slider -->
        <section class="banner xs-mt-60">            
            <h1>Art Work Details</h1>
        </section>
        <!--end section slider -->
        <!--start section blog -->
        <section class="bg-theme xs-ptb-50 artwork-detail-section">
            <div class="container">                
                <div class="row">
                    <div class="col-md-7 text-center txt-white">
                        <img class="img-responsive" alt="help" src="<c:url value='${FILE}'/>">
                    </div>
                    <div class="col-md-5 text-left txt-white">
                        <h2>${ARTNAME}</h2>
                        <p class="text-justify">${DESCRIPTION}</p>   
                        <!--<p>Tags: <span class="txt-theme">Photography</span></p>-->
                        <div class="clearfix"></div>
                        <div class="row xs-pt-30">
                            <!--                                <div class="col-md-3 col-sm-3 xs-pr-0">
                                                             <img class="img-responsive img-circle" alt="help" 
                                                                  src="<c:url value="/profile/getProfileImage/${USERID}"/>">
                                                        </div>-->
                            <div class="col-md-3 col-sm-3 xs-pr-0">
                                <img class="img-responsive img-circle" alt="..." src="<c:url value="/profile/getProfileImage/${USERID}"/>">
                            </div>
                            <div class="col-md-9 col-sm-9 text-left txt-white">
                                <h4> ${NAME} </h4>
                                <button type="button" data-toggle="modal" data-target=".example-modal-lg" class="btn-p-art">Contact</button>
                                <!--                                <button type="button"  class="btn-p-art">Contact</button>-->
                            </div>
                        </div>
                    </div>
                </div>

                <!--                <div class="row">
                                    <div class="col-md-12 xs-pt-30 ">
                                        <h2 class="txt-white">Loreuma Title</h2>
                                    </div>
                                    <div class="col-md-6 md-pr-5">
                                        <img class="img-responsive" style="height: 326px;" alt="help" src="<c:url value='/client_resource/images/85.jpg'/>">
                                    </div>
                                    <div class="col-md-3 md-pr-5 md-pl-5">
                                        <img class="img-responsive" style="height: 326px;" alt="help" src="<c:url value='/client_resource/images/85.jpg'/>">
                                    </div>
                                    <div class="col-md-3 md-pl-5">
                                        <img class="img-responsive" style="height: 326px;" alt="help" src="<c:url value='/client_resource/images/85.jpg'/>">
                                    </div>
                
                                </div>-->
                <!--<div class="row md-mt-10">-->
                <div class="row md-mt-10">
                    <div class="col-md-12 xs-pt-30 ">
                        <h2 class="txt-white">More Work</h2>
                    </div>

                    <div class="col-md-4 md-pr-0 xs-mb-10">
                        <c:choose>
                            <c:when test="${FETWORKDATA[0] == null || FETWORKDATA[0]==''}">
                                <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                     style="height: 206px; width: 100%" class="img-responsive">
                            </c:when>   
                            <c:otherwise>
                                <a href="${contextPath}/client/artwork/${FETWORKDATA[0].USERWORKID}">
                                    <img src="<c:url value='${FETWORKDATA[0].FILE}'/>" 
                                         style="height: 206px; width: 100%" class="img-responsive">
                                </a>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="col-md-4 md-pr-0 xs-mb-10" >
                        <c:choose>
                            <c:when test="${FETWORKDATA[1] == null}">
                                <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                     style="height: 206px; width: 100%" class="img-responsive">
                            </c:when>   
                            <c:otherwise>
                                <a href="${contextPath}/client/artwork/${FETWORKDATA[1].USERWORKID}">
                                    <img src="<c:url value='${FETWORKDATA[1].FILE}'/>" 
                                         style="height: 206px; width: 100%" class="img-responsive">
                                </a>
                            </c:otherwise>
                        </c:choose>  
                    </div>
                    <div class="col-md-4 md-pr-0 xs-mb-10" >
                        <c:choose>
                            <c:when test="${FETWORKDATA[2] == null}">
                                <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                     style="height: 206px; width: 100%" class="img-responsive">
                            </c:when>   
                            <c:otherwise>
                                <a href="${contextPath}/client/artwork/${FETWORKDATA[2].USERWORKID}">
                                    <img src="<c:url value='${FETWORKDATA[2].FILE}'/>" 
                                         style="height: 206px; width: 100%" class="img-responsive">
                                </a>
                            </c:otherwise>
                        </c:choose> 
                    </div>
                    <div class="col-md-4 md-pr-0 xs-mb-10" >
                        <c:choose>
                            <c:when test="${FETWORKDATA[3] == null}">
                                <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                     style="height: 206px; width: 100%" class="img-responsive">
                            </c:when>   
                            <c:otherwise>
                                <a href="${contextPath}/client/artwork/${FETWORKDATA[3].USERWORKID}">
                                    <img src="<c:url value='${FETWORKDATA[3].FILE}'/>" 
                                         style="height: 206px; width: 100%" class="img-responsive">
                                </a>
                            </c:otherwise>
                        </c:choose>  
                    </div>
                    <div class="col-md-4 md-pr-0 xs-mb-10" >
                        <c:choose>
                            <c:when test="${FETWORKDATA[4] == null}">
                                <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                     style="height: 206px; width: 100%" class="img-responsive">
                            </c:when>   
                            <c:otherwise>
                                <a href="${contextPath}/client/artwork/${FETWORKDATA[4].USERWORKID}">
                                    <img src="<c:url value='${FETWORKDATA[4].FILE}'/>" 
                                         style="height: 206px; width: 100%" class="img-responsive">
                                </a>
                            </c:otherwise>
                        </c:choose>  
                    </div>
                    <div class="col-md-4 md-pr-0 xs-mb-10" >
                        <c:choose>
                            <c:when test="${FETWORKDATA[5] == null}">
                                <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                     style="height: 206px; width: 100%" class="img-responsive">
                            </c:when>   
                            <c:otherwise>
                                <a href="${contextPath}/client/artwork/${FETWORKDATA[5].USERWORKID}">
                                    <img src="<c:url value='${FETWORKDATA[5].FILE}'/>" 
                                         style="height: 206px; width: 100%" class="img-responsive">
                                </a>
                            </c:otherwise>
                        </c:choose>  
                        <!--<a class="btn-artwork" href="#">MORE ART Work <i class="fa fa-arrow-right"></i></a>-->
                    </div>
                    <c:if test="${FETWORKDATA.size() > 6}">
                        <div class="col-md-4 md-pr-0 xs-mb-10" >
                            <c:choose>
                                <c:when test="${FETWORKDATA[6] == null}">
                                    <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                         style="height: 206px; width: 100%" class="img-responsive">
                                </c:when>   
                                <c:otherwise>
                                    <a href="${contextPath}/client/artwork/${FETWORKDATA[6].USERWORKID}">
                                        <img src="<c:url value='${FETWORKDATA[6].FILE}'/>" 
                                             style="height: 206px; width: 100%" class="img-responsive">
                                    </a>
                                </c:otherwise>
                            </c:choose>  
                        </div>
                        <div class="col-md-4 md-pr-0 xs-mb-10" >
                            <c:choose>
                                <c:when test="${FETWORKDATA[7] == null}">
                                    <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                         style="height: 206px; width: 100%" class="img-responsive">
                                </c:when>   
                                <c:otherwise>
                                    <a href="${contextPath}/client/artwork/${FETWORKDATA[7].USERWORKID}">
                                        <img src="<c:url value='${FETWORKDATA[7].FILE}'/>" 
                                             style="height: 206px; width: 100%" class="img-responsive">
                                    </a>
                                </c:otherwise>
                            </c:choose>  
                        </div>
                        <div class="col-md-4 md-pr-0 xs-mb-10" >
                            <c:choose>
                                <c:when test="${FETWORKDATA[8] == null}">
                                    <img src="<c:url value='client_resource/images/no_img.png'/>" 
                                         style="height: 206px; width: 100%" class="img-responsive">
                                </c:when>   
                                <c:otherwise>
                                    <a href="${contextPath}/client/artwork/${FETWORKDATA[8].USERWORKID}">
                                        <img src="<c:url value='${FETWORKDATA[8].FILE}'/>" 
                                             style="height: 206px; width: 100%" class="img-responsive">
                                    </a>
                                </c:otherwise>
                            </c:choose>  
                        </div>
                    </c:if>
                    <!--</div>-->

                </div>


            </div>
        </section>





        <%--        <h1>Hello Artwork!</h1>
    ${ARTNAME}<br>
    ${WORKNAME}<br>
    ${NAME}<br>
    ${DESCRIPTION}<br>
    ${SUBWORK} --%> ${SUBWORK}

        <div class="modal fade example-modal-lg" id="inqModal" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content bg-theme">

                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="row xs-pb-30">
                                    <div class="col-md-6 text-left">
                                        <h4 class="modal-title txt-white" id="myLargeModalLabel">Send Inquiry</h4>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <button type="button" class="close" id="btnMdlClse" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-lbl">Name <span class="text-danger"> * </span></label>
                                            <input class="form-control control" type="text" maxlength="55"
                                                   id="txtName" placeholder="Your Name" ng-model="artistInq['NAME']">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-lbl">Email <span class="text-danger"> * </span></label>                                   
                                            <input class="form-control control" type="text" maxlength="45"
                                                   id="txtEmail" ng-model="artistInq['EMAIL']">  
                                        </div>
                                    </div>
                                </div>                                                            
                            </div>

                            <div class="col-md-12">

                                <div class="form-group">
                                    <label class="control-lbl">Mobile No <span class="text-danger"> * </span></label>

                                    <input class="form-control control" type="text" maxlength="20"
                                           id="txtMobile" ng-model="artistInq['MOBILE']">

                                </div>

                            </div>

                            <div class="col-md-12">

                                <div class="form-group">
                                    <label class="control-lbl">Message <span class="text-danger"> * </span></label>

                                    <textarea class="form-control control" type="text" 
                                              id="txtMessage" ng-model="artistInq['MESSAGE']">
                                    </textarea>

                                </div>

                            </div>
                            <div class="col-md-12">
                                <button type="button" class="btn-p-art" id="btnAdd" ng-click="insertArtistInquiry('${USERID}')">Add</button>
                                <button type="button" class="btn-p-art" data-dismiss="modal">Close</button>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </div>

        <jsp:include page="footer.jsp"/>
    </body>
</html>
