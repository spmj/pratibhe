<%-- 
    Document   : our-partners
    Created on : Nov 27, 2017, 8:23:49 PM
    Author     : Mayur
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <jsp:include page="header.jsp"/>
    <title>Our Partners</title>
    <body>


        <!-- start header -->
        <jsp:include page="navbar.jsp"/>
        <!-- end header -->

        <!--start section slider -->
        <section class="banner xs-mt-60">            
            <h1>Our Partner</h1>
        </section>
        <!--end section slider -->


        <!--start happening section  -->
        <section class="partner-details bg-theme">  
            <div class="container text-center">
                ${PARTNERS}
               

            </div>     
        </section>
        <!--end happening section  -->
        <section class="help pt-50">
            <div class="container">
                <p class="para">
                    <span>O</span>ur Partners</p>
                <div class="row">
                    <div class="col-md-12 md-pr-0">
                        <div class="partner-blog">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>SNUPPED x Noise : Artist'Merchandise</h4>
                                </div>
                                <div class="col-md-2">
                                    <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/art/art1.jpg'/>">
                                </div>
                                <div class="col-md-10">
                                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)</p>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>       
                <div class="row">
                    <div class="col-md-12 md-pr-0">
                        <div class="partner-blog">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>SNUPPED x Noise : Artist'Merchandise</h4>
                                </div>
                                <div class="col-md-2">
                                    <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/art/art1.jpg'/>">
                                </div>
                                <div class="col-md-10">
                                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)</p>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>    
                <div class="row">
                    <div class="col-md-12 md-pr-0">
                        <div class="partner-blog">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>SNUPPED x Noise : Artist'Merchandise</h4>
                                </div>
                                <div class="col-md-2">
                                    <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/art/art1.jpg'/>">
                                </div>
                                <div class="col-md-10">
                                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)</p>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>   
                <div class="row">
                    <div class="col-md-12 md-pr-0">
                        <div class="partner-blog">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>SNUPPED x Noise : Artist'Merchandise</h4>
                                </div>
                                <div class="col-md-2">
                                    <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/art/art1.jpg'/>">
                                </div>
                                <div class="col-md-10">
                                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)</p>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>                                                   

            </div>
        </section>
        <jsp:include page="footer.jsp"/>
    </body>

</html>
