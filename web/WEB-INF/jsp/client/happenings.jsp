<%-- 
    Document   : happenings
    Created on : Nov 21, 2017, 7:34:26 PM
    Author     : jay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en" ng-app="PratibheApp">

    <jsp:include page="header.jsp"/>
    <title>Happenings</title>
    <c:set var="contextPath" value="${pageContext.request.contextPath}"/>

    <body ng-controller="clientController">

        <!-- start header -->
        <jsp:include page="navbar.jsp"/>
        <!-- end header -->

        <!--start section slider -->
        <section class="banner xs-mt-60">
            <!--<img alt="image" class="img-responsive" src="<c:url value='/client_resource/images/banner1.jpg'/>" style="width: 100%;">-->
            <h1>Happenings</h1>
        </section>
        <!--end section slider -->

        <!--start section blog -->
        <section class="happening-filter">
            <div class="container">
                <div class="filter-blog">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="col-md-3">
                                <select class="form-control" name="cmbYear" ng-model="HAPPFILDATA['YEAR']">
                                    <option value="">Year</option>
                                    <c:forEach begin="2010" end="2040" varStatus="loop">
                                        <option value="${loop.index}">${loop.index}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select class="form-control" name="cmbMonth" ng-model="HAPPFILDATA['MONTH']">
                                    <option value="">Month</option>
                                    <option value="1">Jan</option>
                                    <option value="2">Feb</option>
                                    <option value="3">Mar</option>
                                    <option value="4">Apr</option>
                                    <option value="5">May</option>
                                    <option value="6">Jun</option>
                                    <option value="7">Jul</option>
                                    <option value="8">Aug</option>
                                    <option value="9">Sep</option>
                                    <option value="10">Oct</option>
                                    <option value="11">Nov</option>
                                    <option value="12">Dec</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select class="form-control" name="cmbEventType" ng-model="HAPPFILDATA['EVNTYPE']">
                                    <option value="">Event Type</option>
                                    <c:forEach items="${EVENTTYPEFILTER}" var="data">
                                        <option value="${data.EVENTTYPEID}">${data.EVENTTYPENAME}</option>
                                    </c:forEach>
                                    <option>drawing</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select class="form-control" name="cmbArtForm" ng-model="HAPPFILDATA['ARTID']">
                                    <option value="">Art Form</option>
                                    <c:forEach items="${ARTFILTER}" var="data">
                                        <option value="${data.ARTID}">${data.ARTNAME}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <input type="button" value="SEARCH HAPPENINGS" ng-click="getFilteredHappenings()" class="search-happenings-btn"/>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!--start section blog -->
        <section class="blog" id="blog" ng-init="getFilteredHappenings()">
            <div class="container" ng-if="HAPPDATA.length == 0">
                <div class="row listnotfound">
                    <center>
                        No Happenings found.
                    </center>
                </div>
            </div>
            <div class="container" ng-if="HAPPDATA.length !== 0">
                <!--<p class="para">-->
                <!--<span>M</span>ore Happenings</p>-->
                <div class="row">
                    <div class="col-md-4" ng-repeat="data in HAPPDATA">
                        <a href="${contextPath}/client/happenings/{{data.HAPPENINGSID}}">
                            <div class="blog-area">
                                <div class="image">
                                    <img alt="image"  class="img-responsive happeningimg" src="{{data.THUMBIMG}}">
                                    <div ng-if="data.CNT == 1 || data.CNT == 5 || data.CNT == 7" class="blog-overlay first"></div>
                                    <div ng-if="data.CNT == 2 || data.CNT == 6 || data.CNT == 9" class="blog-overlay second"></div>
                                    <div ng-if="data.CNT == 3 || data.CNT == 4 || data.CNT == 8" class="blog-overlay third"></div>
                                </div>
                                <div class="box">${data.HAPPENINGSID}
                                    <h1>{{data.TITLE}}</h1>
                                    <span>{{data.STARTDATE}}</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="row col-md-12" >
                        <div class="col-md-8" style="padding: 0">
                            <pagination total-items="HAPPFILDATA.totalItems" ng-click="getPageData(HAPPFILDATA.currentPage)" 
                                        page="HAPPFILDATA.currentPage"  max-size="HAPPFILDATA.maxSize" 
                                        class="pagination-sm" boundary-links="true" rotate="false" 
                                        num-pages="numPages" items-per-page="HAPPFILDATA.itemsPerPage" style="margin-left: 15px; margin-top: 0">
                            </pagination>
                        </div>
                        <div class="col-md-4 text-right page-count">
                            {{record = HAPPFILDATA.currentPage * 9;
                                        ""}}
                            {{record - 9 + 1}} to <span ng-if="record <= HAPPFILDATA.totalItems">{{record}}</span> 
                            <span ng-if="record > HAPPFILDATA.totalItems">{{HAPPFILDATA.totalItems}}</span> out of {{HAPPFILDATA.totalItems}}
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!--end section blog -->

        <!--start section help -->
        <!--        <section class="help">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5 box md-pr-0">
                                <div class="item i1">
                                    <h3>Create a Happening</h3>
                                    <span>It has survived not only five centuries</span>
                                    <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining
                                        essentially unchanged.</p>
                                    <a class="a-btn" href="#">SUBMIT HAPPENINGS</a>
                                </div>
                            </div>
                            <div class="col-md-7 md-pl-0">
                                <img class="img-responsive img-h-320" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
        
                        </div>
                    </div>
                </section>-->
        <!--end section help -->






        <!--start section help -->
        <!--        <section class="help">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5 box md-pr-0">
                                <div class="item i4">
                                    <h3>Create a Happening</h3>
                                    <span>It has survived not only five centuries</span>
                                    <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining
                                        essentially unchanged.</p>
                                    <a class="a-btn" href="#">SUBMIT HAPPENINGS</a>
                                </div>
                            </div>
                            <div class="col-md-7 md-pl-0">
                                <img class="img-responsive img-h-320" alt="help" src="<c:url value='/client_resource/images/a1.jpg'/>">
                            </div>
        
                        </div>
                    </div>
                </section>-->
        <!--end section help -->




        <!-- start section work -->
        <section class="work" id="work">
            <div class="container" id="change">
                <!-- do not remove theme blog -->
            </div>
        </section>
        <!-- end section work -->

        <jsp:include page="footer.jsp"/>

    </body>

</html>
