<%-- 
    Document   : about_us
    Created on : Nov 27, 2017, 8:22:38 PM
    Author     : Mayur
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html lang="en" ng-app="PratibheApp">
    <jsp:include page="header.jsp"/>
    <title>Mentor Registration</title>

    <body ng-controller="registerController">
        <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
        <!-- start header -->
        <jsp:include page="navbar.jsp"/>
        <!-- end header -->
        
        <!--start section slider -->
        <section class="banner xs-mt-60">            
            <h1>Register on Pratibhey</h1>
        </section>
        <!--end section slider -->

        <!--start section slider -->
        <!--        <section class="banner">
                    <img alt="image" class="img-responsive" src="<c:url value='/client_resource/images/banner1.jpg'/>" style="width: 100%;">
                </section>-->
        <!--end section slider -->

        <section class="partner-details bg-theme help xs-pt-50">
            <div class="container">
                <!--<h2>Register on Pratibhey</h2>-->
                <p>Be a part of growing community at sharing your work, attending events,blah blah blah.. </p>
                <br>
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-lbl" for="txtName">Full Name <span>*</span></label>
                            <input type="text" class="form-control control"  ng-model="formData['NAME']"
                                   id="txtName" placeholder="Enter Full Name" >
                        </div>

                        <div class="form-group">
                            <label class="control-lbl" for="txtEmail">Email <span>*</span></label>
                            <input type="text" class="form-control control" ng-model="formData['EMAIL']"
                                   id="txtEmail" placeholder="Enter enail address" >
                        </div>

                        <div class="form-group">
                            <label class="control-lbl" for="txtDob">Birthday <span>*</span></label>
                            <input type="text" class="form-control control" ng-model="formData['DOB']"
                                   datetime-picker date-format="dd-MMM-yyyy" date-only
                                   id="txtDob" placeholder="Enter Birthdate" >
                        </div>

                        <div class="form-group">
                            <label class="control-lbl" for="txtAadhar">Aadhar ID <span>*</span></label>
                            <input type="text" class="form-control control" ng-model="formData['AADHARNO']"
                                   id="txtAadhar" placeholder="Enter Aadhar">
                        </div>

                        <div class="form-group">
                            <label class="control-lbl" for="cmdArt">Art <span>*</span></label>
                            <select class="form-control control" style="height:60px !important;" id="cmdArt" ng-model="formData['ARTID']" multiple="true">
                                <c:forEach items="${ART}" var="data" varStatus="loop">
                                    <option value="${data.ARTID}">${data.ARTNAME}</option>        
                                </c:forEach>
                            </select>
                        </div>

                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-lbl" for="txtArtistname">Artist Name </label>
                            <input type="text" class="form-control control" ng-model="formData['ARTISTNAME']"
                                   id="txtArtistname"   placeholder="What you would like to be known as.." >
                        </div>

                        <div class="form-group">
                            <label class="control-lbl" for="txtMobile">Contact no. <span>*</span></label>
                            <input type="text" class="form-control control" ng-model="formData['MOBILE']"
                                   id="txtMobile"   placeholder="Mobile/Landline" >
                        </div>

                        <div class="form-group">
                            <label class="control-lbl" for="txtOccupation">Occupation </label>
                            <input type="text" class="form-control control" ng-model="formData['OCCUPATION']"
                                   id="txtOccupation"  placeholder="Enter Occupation" >
                        </div>

                        <div class="form-group">
                            <label class="control-lbl" for="txtWebsite">Website </label>
                            <input type="text" class="form-control control" ng-model="formData['WEBSITE']"
                                   id="txtWebsite"  placeholder="Enter website url" >
                        </div>
                        <div class="form-group">
                            <label class="control-lbl" for="gender" >Gender <span>*</span></label>

                            <div class="radio">
                                <input type="radio" id="gendermale" name="optradio" value="MALE" ng-model="formData['GENDER']"> 
                                <label class="radio-inline" for="gendermale">
                                    Male
                                </label>
                                <br/>
                                <input type="radio" id="genderfemale" name="optradio"  value="FEMALE" ng-model="formData['GENDER']">
                                <label class="radio-inline" for="genderfemale">
                                    Female
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-lbl" for="txtAboutme">About me </label>
                            <input type="text" class="form-control control" ng-model="formData['ABOUTME']"
                                   id="txtAboutme" placeholder="Express yourself to the world.." >
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-lbl" for="txtTOW">Type of work </label>
                            <input type="text" class="form-control control" ng-model="formData['TYPEOFWORK']"
                                   id="txtTOW"  placeholder="Enter Type of work" >
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-lbl" for="txtTEOW">Tenure of work</label>
                            <input type="text" class="form-control control" ng-model="formData['TENUREOFWORK']"
                                   id="txtTEOW"  placeholder="Tenure of work" >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-lbl" for="txtAddress">Address </label>
                            <input type="text" class="form-control control" ng-model="formData['ADDRESS']"
                                   id="txtAddress" placeholder="Express yourself to the world.." >
                        </div>
                    </div>

                    <!--<div class="row">-->
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-lbl" for="birthday">State <span>*</span></label>
                            <select class="form-control control" id="cmdState" ng-model="formData['STATEID']" 
                                    ng-change="getCity(formData.STATEID)"ng-change="getCity(formData.STATEID)">
                                <option value="">Select State</option>
                                <c:forEach items="${STATE}" var="data" varStatus="loop">
                                    <option value="${data.VALUE}">${data.TEXT}</option>        
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-lbl" for="birthday">City <span>*</span></label>
                            <select class="form-control control" id="cmdCity" ng-model="formData['CITYID']">
                                <option value="">Select City</option>
                                <option ng-repeat="data in cityName.CITYNAME" value="{{data.VALUE}}">{{data.TEXT}}</option>        
                            </select>
                        </div>
                    </div>
                    <!--</div>-->
                    <div class="col-md-5">
                        <div class="col-md-6 p-0"> 
                            <label class="control-lbl" for="about">Display picture(DP) <span>*</span></label>
                        </div>
                        <div class="col-md-6 p-0"> 
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <span class="btn btn-file"><input style="color:#fff;" type="file"  id="fileProfile" name="fileProfile" file-model="formData['IMAGE']" 
                                                                  filename="formData['IMAGENAME']" fileread="" filesize="formData['IMAGESIZE']" fileext="formData['IMAGEEXT']" 
                                                                  onchange="angular.element(this).scope().thumbChanged(this);
                                                                                  angular.element(this).scope().processFiles(this)"/></span>
                            </div>
                        </div>
                        <br/><br/>
                        <div class="col-md-12 text-center">
                            <img id="imgProfile" ng-src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTEAzhs2acdAj70XFCtn_8Qr6ENkJ_CdXo_1r97sel_isfLgmoK" width="200" height="200" style="border: none;" alt="Profile Photo">
                        </div>                        
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-5">
                        <p>Where did you hear about Pratibhey? </p>
                        <ul class="reg-qst">
                            <li>
                                <div class="checkbox">
                                    <input type="checkbox" id="checkbox1" checklist-model="formData['HEARABOUT']" checklist-value="1">
                                    <label for="checkbox1">
                                        someone told me about it
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <!--<input id="checkbox2" type="checkbox" ng-model="formData['']">-->
                                    <input type="checkbox" id="checkbox2" checklist-model="formData['HEARABOUT']" checklist-value="2">
                                    <label for="checkbox2">
                                        Parichey social media loresum ipsum leresum ipsum
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <!--<input id="checkbox3" type="checkbox" ng-model="formData['']">-->
                                    <input type="checkbox" id="checkbox3" checklist-model="formData['HEARABOUT']" checklist-value="3">
                                    <label for="checkbox3">
                                        someone told me about it
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <!--<input id="checkbox4" type="checkbox" ng-model="formData['']">-->
                                    <input type="checkbox" id="checkbox4" checklist-model="formData['HEARABOUT']" checklist-value="4">
                                    <label for="checkbox4">
                                        Parichey social media loresum ipsum leresum ipsum
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <!--<input id="checkbox5" type="checkbox">-->
                                    <input type="checkbox" id="checkbox5" checklist-model="formData['HEARABOUT']" checklist-value="5">
                                    <label for="checkbox5">
                                        someone told me about it
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <!--<input id="checkbox2" type="checkbox" ng-model="formData['']">-->
                                    <input type="checkbox" id="checkbox6" checklist-model="formData['HEARABOUT']" checklist-value="6">
                                    <label for="checkbox6">
                                        Parichey social media loresum ipsum leresum ipsum
                                    </label>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-lbl" for="txtPassword">Password <span>*</span></label>
                                    <input type="password" class="form-control control" ng-model="formData['PASSWORD']"
                                           id="txtPassword"
                                           placeholder="Only letter, number, undescore and space are allowed" >
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-lbl" for="txtConfirmation">Confirm Password <span>*</span> </label>
                                    <input type="password" class="form-control control" ng-model="formData['REPASSWORD']"
                                           id="txtConfirmation"
                                           placeholder="Only letter, number, undescore and space are allowed" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <button ng-click="insertDetail('MENTOR')" class="btn btn-register">Register</button>
                        </div>
                    </div>

                </div>

            </div>
        </section>

        <jsp:include page="footer.jsp"/>

    </body>

</html>
