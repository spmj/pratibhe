<%-- 
    Document   : happeningdetail
    Created on : Nov 19, 2017, 1:39:10 PM
    Author     : Mayur
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en" ng-app="PratibheApp">

    <jsp:include page="header.jsp"/>

    <body ng-controller="clientController">
        <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
        <!-- start header -->
        <jsp:include page="navbar.jsp"/>
        <!-- end header -->

        <!--start section slider -->
        <!--        <section class="slider" id="home">
                    <div class="container-fluid" >
                        <div class="owl-carousel owl-theme">
                                <div class="item first" style="background-image: url('${BANNER}');">
                                    <div class="overlay">
                                        
                                    </div>
                                </div>
                        </div>
                    </div>
                </section>-->
        <section>        
            <div class="overlay">
                <img alt="image" class="img-responsive" src="${BANNER}" style="width: 100%;  height: 80vh;">        
            </div>
        </section>
        <!--end section slider -->

        <!--start happening section  -->
        <section class="happening-details">  
            <div class="container text-center">
                <h2>${TITLE}</h2>
                <h4>${STARTDATE} , ${ADDRESS}, ${CITYNAME}, ${STATENAME}</h4>
                <p>${DETAILS}

                </p>
                <div class="col-md-12 text-left padding0 cdetail">
                    <label class="col-md-2 padding0">Contact Person : </label><span class="col-md-10 padding0">${CONTACTNAME}</span>
                </div>
                <div class="col-md-12 text-left padding0 cdetail">
                    <label class="col-md-2 padding0">Contact Email : </label><span class="col-md-10 padding0">${CONTACTEMAIL}</span>
                </div>
                <!--<button data-toggle="modal" data-target=".example-modal-lg" >Apply</button>-->
            </div>     
            <div class="container">
                <input type="button" data-toggle="modal" value="PARTICIPATE" data-target=".example-modal-lg" class="search-happenings-btn" style="margin: 20px 0px 0px 0px;"/>
            </div>
        </section>
        <!--end happening section  -->

        <c:if test="${MOREEVENTS.size() ne 0}">
            <!--start section blog -->
            <section class="help pt-50" id="blog">
                <div class="container">

                    <p class="para"><span>E</span>vents</p>

                    <c:forEach items="${MOREEVENTS}" var="data">
                        <div class="row">
                            <div class="col-md-5 box md-pr-0">
                                <div class="item i1">
                                    <h3>${data.TITLE}</h3>
                                    <span>${data.STARTDATE}</span>
                                    <p>${data.SUMMERY}</p>
                                    <a class="a-btn" href="${contextPath}/client/event/${data.EVENTID}">EXPLORE</a>
                                    <!--<a class="a-btn" href="#">EXPLORE</a>-->
                                </div>
                            </div>
                            <div class="col-md-7 md-pl-0">
                                <img class="img-responsive img-h-320" alt="help" src="${data.BANNER}">
                            </div>
                        </div>
                        <!--                            <div class="col-md-4">
                                                        <div class="blog-area">
                                                            <div class="image">
                                                                <img alt="image" 
                                                                     class="happeningimg" src="${data.THUMBIMG}">
                                                                <div class="blog-overlay first"></div>
                                                            </div>
                        
                                                            <div class="box">
                                                                <a href="${contextPath}/client/event/${data.EVENTID}">
                                                                    <h1>${data.TITLE}</h1>
                                                                </a>
                                                                <span>${data.STARTDATE}</span>
                                                            </div>
                                                        </div>
                                                    </div>-->
                    </c:forEach>

                </div>
            </section>
        </c:if>
        <!--end section blog -->

        <!--start section help -->

        <!--end section help -->


        <div class="modal fade example-modal-lg" id="eventModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content bg-theme">
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="row xs-pb-30">
                                    <div class="col-md-6 text-left">
                                        <h4 class="modal-title txt-white" id="myLargeModalLabel">Event Partcipate</h4>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <button type="button" class="close" id="btnMdlClse" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="modal-body">-->

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-lbl">Name <span class="text-danger"> * </span></label>
                                    <input class="form-control control" type="text" maxlength="55"
                                           id="txtName" placeholder="Your Name" ng-model="eventpart['NAME']">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-lbl">Email <span class="text-danger"> * </span></label>
                                    <input class="form-control control" type="text" maxlength="45"
                                           id="txtEmail" placeholder="Your Name" ng-model="eventpart['EMAIL']">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-lbl">Mobile No <span class="text-danger"> * </span></label>
                                    <input class="form-control control" type="text" maxlength="20"
                                           id="txtMobile" placeholder="Enter Mobile number" ng-model="eventpart['MOBILE']">
                                </div>
                            </div>
                            <!--</div>-->

                            <div class="col-md-12">
                                <button type="button" class="btn-p-art" id="btnAdd" ng-click="insertPartEvent('${EVENTID}')">Add</button>
                                <button type="button" class="btn-p-art" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <jsp:include page="footer.jsp"/>

    </body>

</html>

