<%-- 
    Document   : programmes
    Created on : Nov 21, 2017, 9:31:41 PM
    Author     : jay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en" ng-app="PratibheApp">

    <jsp:include page="header.jsp"/>
    <c:set var="contextPath"  value="${pageContext.request.contextPath}"/>

    <body ng-controller="clientController">

        <!-- start header -->
        <jsp:include page="navbar.jsp"/>
        <!-- end header -->

        <!--start section slider -->
        <section class="banner xs-mt-60">            
            <h1>Programmes</h1>
        </section>
        <!--end section slider -->


        <!--start section blog -->
        <section class="happening-filter">
            <div class="container">
                <div class="filter-blog">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="col-md-3">
                                <select class="form-control" name="cmbYear" ng-model="HAPPFILDATA['YEAR']">
                                    <option value="">Year</option>
                                    <c:forEach begin="2010" end="2040" varStatus="loop">
                                        <option value="${loop.index}">${loop.index}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select class="form-control" name="cmbMonth" ng-model="HAPPFILDATA['MONTH']">
                                    <option value="">Month</option>
                                    <option value="1">Jan</option>
                                    <option value="2">Feb</option>
                                    <option value="3">Mar</option>
                                    <option value="4">Apr</option>
                                    <option value="5">May</option>
                                    <option value="6">Jun</option>
                                    <option value="7">Jul</option>
                                    <option value="8">Aug</option>
                                    <option value="9">Sep</option>
                                    <option value="10">Oct</option>
                                    <option value="11">Nov</option>
                                    <option value="12">Dec</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select class="form-control" name="cmbEventType" ng-model="HAPPFILDATA['EVNTYPE']">
                                    <option value="">Event Type</option>
                                    <c:forEach items="${EVENTTYPEFILTER}" var="data">
                                        <option value="${data.EVENTTYPEID}">${data.EVENTTYPENAME}</option>
                                    </c:forEach>
                                    <option>drawing</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select class="form-control" name="cmbArtForm" ng-model="HAPPFILDATA['ARTID']">
                                    <option value="">Art Form</option>
                                    <c:forEach items="${ARTFILTER}" var="data">
                                        <option value="${data.ARTID}">${data.ARTNAME}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <input type="button" value="SEARCH PROGRAMMES" ng-click="getFilterEventDetails()" class="search-happenings-btn"/>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <%--
                <!--start section blog -->
                <section class="blog" id="blog">

                        <div class="container">
                            <p class="para"><span>H</span>appenings</p>
                            <div class="row">
            <c:forEach items="${HOMEHAPPENINGS}" var="data">
                <div class="col-md-4">
                    <div class="blog-area">
                        <div class="image">
                            <img alt="image" 
                                 class="happeningimg" src="${data.THUMBIMG}">
                            <div class="blog-overlay first"></div>
                        </div>

                        <div class="box">
                            <a href="${contextPath}/client/happenings/${data.HAPPENINGSID}">
                                <h1>${data.TITLE}</h1>
                            </a>
                            <span>${data.STARTDATE}</span>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</section>
            <!--end section blog -->

        --%>
        <!--start section help -->
        <section class="help pt-50" ng-init="getFilterEventDetails()">
            <div class="container" ng-if="FILEVENTSDATA.length == 0">
                <div class="row listnotfound" >
                    <center>
                        No Programmes found.
                    </center>
                </div>
            </div>
            <div class="container" ng-if="FILEVENTSDATA.length != 0">
                <div ng-repeat="data in FILEVENTSDATA">
                    <div class="row" ng-if="data.CNT == 1">
                        <div class="col-md-5 box md-pr-0">
                            <div class="item i1">
                                <h3>{{data.TITLE}}</h3>
                                <span>{{data.STARTDATE}}</span>
                                <p>{{data.SUMMERY}}</p>
                                <a class="a-btn" href="${contextPath}/client/event/{{data.EVENTID}}">EXPLORE</a>
                                <!--<a class="a-btn" href="#">EXPLORE</a>-->
                            </div>
                        </div>
                        <div class="col-md-7 md-pl-0">
                            <img class="img-responsive img-h-320" alt="help" src="{{data.BANNER}}">
                        </div>
                    </div>
                    <div class="row" ng-if="data.CNT == 2">
                        <div class="col-md-7 md-pr-0">
                            <img class="img-responsive img-h-320" alt="help" src="{{data.BANNER}}">
                        </div>
                        <div class="col-md-5 box md-pl-0">
                            <div class="item i2">
                                <h3>{{data.TITLE}}</h3>
                                <span>{{data.STARTDATE}}</span>
                                <p>{{data.SUMMERY}}</p>
                                <a class="a-btn" href="${contextPath}/client/event/{{data.EVENTID}}">EXPLORE</a>
                                <!--<a class="a-btn" href="#">EXPLORE</a>-->
                            </div>
                        </div>
                    </div>
                    <div class="row" ng-if="data.CNT == 3">
                        <div class="col-md-5 box md-pr-0">
                            <div class="item i3">
                                <h3>{{data.TITLE}}</h3>
                                <span>{{data.STARTDATE}}</span>
                                <p>{{data.SUMMERY}}</p>
                                <a class="a-btn" href="${contextPath}/client/event/{{data.EVENTID}}">EXPLORE</a>
                                <!--<a class="a-btn" href="#">EXPLORE</a>-->
                            </div>
                        </div>
                        <div class="col-md-7 md-pl-0">
                            <img class="img-responsive img-h-320" alt="help" src="{{data.BANNER}}">
                        </div>
                    </div>
                    <div class="row" ng-if="data.CNT == 4">
                        <div class="col-md-7 md-pr-0">
                            <img class="img-responsive img-h-320" alt="help" src="{{data.BANNER}}">
                        </div>
                        <div class="col-md-5 box md-pl-0">
                            <div class="item i4">
                                <h3>{{data.TITLE}}</h3>
                                <span>{{data.STARTDATE}}</span>
                                <p>{{data.SUMMERY}}</p>
                                <a class="a-btn" href="${contextPath}/client/event/{{data.EVENTID}}">EXPLORE</a>
                                <!--<a class="a-btn" href="#">EXPLORE</a>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8" style="padding: 0">
                        <pagination total-items="HAPPFILDATA.totalItems" ng-click="getEventPageData(HAPPFILDATA.currentPage)" 
                                    page="HAPPFILDATA.currentPage"  max-size="HAPPFILDATA.maxSize" 
                                    class="pagination-sm" boundary-links="true" rotate="false" 
                                    num-pages="numPages" items-per-page="HAPPFILDATA.itemsPerPage" style="margin-left: 15px; margin-top: 0">
                        </pagination>
                    </div>
                    <div class="col-md-4 text-right page-count">
                        {{record = HAPPFILDATA.currentPage * 5;
                                    ""}}
                        {{record - 5 + 1}} to <span ng-if="record <= HAPPFILDATA.totalItems">{{record}}</span> 
                        <span ng-if="record > HAPPFILDATA.totalItems">{{HAPPFILDATA.totalItems}}</span> out of {{HAPPFILDATA.totalItems}}
                    </div>

                </div>
            </div>
        </section>
        <!--end section help -->

        <!--start section help -->
        <section class="help pt-50">
            <div class="container">
                <p class="para">
                    <span>P</span>artnerships</p>
                <div class="row">
                    <div class="col-md-2 col-xs-6 col-sm-4 p-0">
                        <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/art/art1.jpg'/>">
                    </div>
                    <div class="col-md-2 col-xs-6 col-sm-4 p-0">
                        <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/art/art1.jpg'/>">
                    </div>
                    <div class="col-md-2 col-xs-6 col-sm-4 p-0">
                        <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/art/art1.jpg'/>">
                    </div>
                    <div class="col-md-2 col-xs-6 col-sm-4 p-0">
                        <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/art/art1.jpg'/>">
                    </div>
                    <div class="col-md-2 col-xs-6 col-sm-4 p-0">
                        <img class="img-responsive" alt="help" src="<c:url value='/client_resource/images/art/art1.jpg'/>">
                    </div>
                    <div class="col-md-2 col-xs-6 col-sm-4 p-0">
                        <a class="btn-ex-partner" href="#">EXPLORE PARTNERSHIP <i class="fa fa-arrow-right"></i></a>
                    </div>

                </div>

            </div>
        </section>

        <!-- start section work -->
        <section class="work" id="work">
            <div class="container" id="change">
                <!-- do not remove theme blog -->
            </div>
        </section>
        <!-- end section work -->

        <jsp:include page="footer.jsp"/>

    </body>

</html>
