<%-- 
    Document   : commoncss
    Created on : Nov 21, 2017, 8:39:55 PM
    Author     : jay
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<head>
    <meta charset="utf-8" />
    <!-- IE Compatibility Meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- First Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<c:url value="/client_resource/css/bootstrap.css"/>" >
    <link rel="stylesheet" href="<c:url value="/client_resource/css/font-awesome.min.css"/>" >
    <link rel="stylesheet" href="<c:url value="/client_resource/css/owl.carousel.min.css"/>" />
    <link rel="stylesheet" href="<c:url value="/client_resource/css/owl.theme.default.min.css"/>" />
    <link rel="stylesheet" href="<c:url value="/client_resource/css/lity.min.css"/>" />
    <link rel="stylesheet" href="<c:url value="/client_resource/css/animate.css"/>" />
    <link rel="stylesheet" href="<c:url value="/client_resource/css/slick.css"/>" />
    <link rel="stylesheet" href="<c:url value="/client_resource/css/lightbox.css"/>" />
    <link rel="stylesheet" href="<c:url value="/client_resource/css/style.css"/>" />
    <link rel="stylesheet" href="<c:url value="/client_resource/css/pnotify.custom.min.css"/>"  />
    <link rel="stylesheet" href="<c:url value="/client_resource/css/helper.css"/>"  />
<!--    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Raleway:100,200,300,400,500,600,700,800,900"
          rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="stylesheet" href="<c:url value="/client_resource/css/media.css"/>" />
    <link rel="stylesheet" href="<c:url value="/client_resource/css/animate.css"/>" />
    <link rel="stylesheet" href="<c:url value="/client_resource/css/angularjs-datetime-picker.css"/>">
     <style>
            .happeningimg {
                height:370px;
                width:100%;
            }
            .padding0{
                padding: 0px;
            }
        </style>
        <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
    <c:set var="path" value="/pratibhe/"/>
</head>