<%-- 
    Document   : change forgot password
    Created on : Nov 27, 2017, 8:22:38 PM
    Author     : Sandip
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html lang="en" ng-app="PratibheApp">
    <jsp:include page="client/header.jsp"/>
    <title>Student Registration</title>

    <body>
        <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
        <!-- start header -->
        1111 ${contextPath}
        <jsp:include page="client/navbar.jsp"/>
        <!-- end header -->

        <!--start section slider -->
        <!--        <section class="banner">
                    <img alt="image" class="img-responsive" src="<c:url value='/client_resource/images/banner1.jpg'/>" style="width: 100%;">
                </section>-->
        <!--end section slider -->

        <section class="partner-details bg-theme help xs-pt-50" ng-controller="loginController">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="login-box">
                            <div class="logo-img text-center xs-mtb-50">
                                <h1 style="color:#fff;">Pratibhe</h1>
                            </div>
                            <div class="login-form">
                                <div class="form-group">
                                    {{formData['ACTIVATIONCODE'] = '${activationcode}';''}}
                                    {{formData['USERID'] = '${userid}';''}}
                                    <label class="control-lbl" for="txtNewPwd"><i class="fa fa-key"></i>&nbsp;&nbsp;New Password</label>
                                    <input type="text" class="form-control control"  placeholder="Enter New Password" 
                                           id="txtNewPwd" name="txtNewPwd" required="" ng-model="formData['NEWPWD']">
                                </div>
                                <div class="form-group">
                                    <label class="control-lbl" for="txtConfPwd"><i class="fa fa-key"></i>&nbsp;&nbsp;Confirm Password</label>
                                    <input type="text" class="form-control control"  placeholder="Enter Confirm Password" 
                                           id="txtConfPwd" name="txtConfPwd" required="" ng-model="formData['CONFPWD']">
                                </div>
                                <div class="xs-mb-10 xs-mt-40">
                                    <button class="btn btn-login" ng-click="changefgpwd()"><i class="fa fa-refresh"></i>&nbsp;&nbsp;Change Password</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <jsp:include page="client/footer.jsp"/>
        <!-- inject:js -->
        <script src="<c:url value="/resource/bower_components/jquery/dist/jquery.min.js"/>"></script>
        <script src="<c:url value="/resource/bower_components/bootstrap/dist/js/bootstrap.min.js"/>"></script>
        <script src="<c:url value="/resource/bower_components/jquery.nicescroll/dist/jquery.nicescroll.min.js"/>"></script>
        <script src="<c:url value="/resource/bower_components/autosize/dist/autosize.min.js"/>"></script>
        <script src="<c:url value='/resource/assets/js/jquery.bootstrap-growl.min.js' />" type="text/javascript"></script>
        <!-- endinject -->


        <!-- Common Script   -->
        <!--<script src="<c:url value="/resource/dist/js/main.js"/>"></script>-->

        <%--Angular Js--%>
        <script src="<c:url value='/js/angular.js'/>" type="text/javascript"></script>

        <!--        <script>
                            var app = angular.module('PratibheApp', []);
                </script>-->

        <script src="<c:url value='/js/validate.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/Controller/loginController.js' />" type="text/javascript"></script>
        <script src="<c:url value='/js/Service/loginService.js' />" type="text/javascript"></script>


    </body>

</html>
