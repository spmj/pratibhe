<%-- 
    Document   : eventtypecontent
    Created on : Sep 10, 2017, 6:07:37 PM
    Author     : jay
--%>

<table class="table table-bordered bordered table-striped table-condensed datatable" ui-jq="dataTable" ui-options="dataTableOpt">
    <thead>
        <tr>
            <th>Sr. No.</th>
            <th>Event Type Name</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        <tr ng-repeat="data in eventTypeData" ng-dblclick="changeView('eventtype/edit/' + data.EVENTTYPEID)">
            <td>{{$index + 1}}</td>
            <td>{{data.EVENTTYPENAME}}</td>
            <td class="text-center"><a href="#/eventtype/edit/{{data.EVENTTYPEID}}" ><i class="fa fa-edit"></i></a></td>
        </tr>
    </tbody>
</table>