<%-- 
    Document   : eventsadd
    Created on : Aug 25, 2017, 12:22:10 PM
    Author     : jay
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<c:if test="${task eq 'Add'}">
    <div class="ui-content-body" ng-init="loadAdd()">
    </c:if>
    <c:if test="${task eq 'Edit'}">
        <div class="ui-content-body" ng-init="loadEdit()">
        </c:if>

        <div class="ui-container">

            <!--page title and breadcrumb start -->
            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-title"> Events                    
                    </h1>
                </div>
                <div class="col-md-4 text-right">
                    <c:if test="${task eq 'Add'}">
                        <a ng-click="insertDetail()" class="btn btn-success"><i class="fa fa-check"></i> Save</a>
                    </c:if>
                    <c:if test="${task eq 'Edit'}">
                        <a ng-click="updateDetail()" class="btn btn-success"><i class="fa fa-check"></i> Update</a>
                        <a ng-click="deleteDetail()" class="btn btn-danger"><i class="fa fa-times"></i> Delete</a>
                    </c:if>
                    <a href="#/events/view" class="btn btn-default"><i class="fa fa-reply"></i> Cancel</a>
                </div>
            </div>
            <!--page title and breadcrumb end -->

            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <header class="panel-heading panel-border">
                            <c:if test="${task eq 'Add'}">
                                Create Events                    
                            </c:if>
                            <c:if test="${task eq 'Edit'}">
                                Update Events
                            </c:if>
                            <span class="tools pull-right">

                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Title <span class="text-danger"> * </span></label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" maxlength="55"
                                                   id="txtTitle" ng-model="formData['TITLE']">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Summery <span class="text-danger"> * </span></label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" 
                                                   id="txtSummery" ng-model="formData['SUMMERY']">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Detail </label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" 
                                                      ng-model="formData['DETAILS']"></textarea>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Address </label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" 
                                                      ng-model="formData['ADDRESS']"></textarea>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">State <span class="text-danger"> * </span></label>
                                        <div class="col-sm-8">
                                            <select class="form-control" id="cmdState" ng-model="formData['STATEID']" ng-change="getCity(formData.STATEID)">
                                                <option></option>
                                                <option ng-repeat="data in comboData.STATENAME" value="{{data.VALUE}}">{{data.TEXT}}</option>        
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">City <span class="text-danger"> * </span></label>
                                        <div class="col-sm-8">
                                            <select class="form-control" id="cmdCity" ng-model="formData['CITYID']">
                                                <option></option>
                                                <option ng-repeat="data in cityName.CITYNAME" value="{{data.VALUE}}">{{data.TEXT}}</option>        
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Art Form <span class="text-danger"> * </span></label>
                                        <div class="col-sm-8">
                                            <select class="form-control" id="cmdArt" ng-model="formData['ARTID']">
                                                <option></option>
                                                <option ng-repeat="data in comboData.ART" value="{{data.VALUE}}">{{data.TEXT}}</option>        
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Event Type <span class="text-danger"> * </span></label>
                                        <div class="col-sm-8">
                                            <select class="form-control" id="cmdEventType" ng-model="formData['EVENTTYPEID']">
                                                <option></option>
                                                <option ng-repeat="data in comboData.EVENTTYPE" value="{{data.VALUE}}">{{data.TEXT}}</option>        
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Thumbnail <span class="text-danger"> * </span></label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="file" id="fileThumb" name="fileThumb" file-model="formData['THUMBIMG']" 
                                                   filename="formData['FTHUMBNAME']" fileread="" filesize="formData['FTHUMBSIZE']" fileext="formData['FTHUMBEXT']" 
                                                   onchange="angular.element(this).scope().thumbChanged(this);
                                                           angular.element(this).scope().processFiles(this)">
                                            <img id="imgThumbView" ng-src="{{formData.THUMBIMG}}" width="200" height="100" style="border: none;" alt="banner Thumb Image">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Long Banner Image <span class="text-danger"> * </span></label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="file" id="fileBanner" name="fileBanner" file-model="formData['BANNERIMG']" 
                                                   filename="formData['FBANNERNAME']" fileread="" filesize="formData['FBANNERSIZE']" fileext="formData['FBANNEREXT']" 
                                                   onchange="angular.element(this).scope().bannerChanged(this);
                                                           angular.element(this).scope().processFiles(this)">
                                            <img id="imgBannerView" ng-src="{{formData.BANNER}}" width="400" height="200" style="border: none;" alt="banner Original Image">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Start Date <span class="text-danger"> * </span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="txtStartDate" 
                                                   name="txtStartDate" 
                                                   datetime-picker date-format="{{dhxDateFormate}}" date-only
                                                   ng-model="formData['STARTDATE']"
                                                   onblur="expandDate(this.id);" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">End Date <span class="text-danger"> * </span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="txtEndDate" name="txtEndDate" 
                                                   datetime-picker date-format="{{dhxDateFormate}}" date-only
                                                   ng-model="formData['ENDDATE']"
                                                   onblur="expandDate(this.id);" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Contact Name </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" 
                                                   id="txtContactName" ng-model="formData['CONTACTNAME']">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Contact Email </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" 
                                                   id="txtContactEmail" ng-model="formData['CONTACTEMAIL']">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <%-- <c:if test="${task eq 'Edit'}">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Is Approved </label>
                                        <div class="col-sm-8">
                                            <label>
                                                <input type="checkbox" ng-model="formData['ISAPPROVED']">
                                            </label>
                                        </div>
                                    </div>
                                </form>
                            </c:if> --%>
                            <c:if test="${task eq 'Edit'}">
                                <form class="form-horizontal" role="form" ng-if="formData.ISADMIN == 0">
                                    <label class="col-sm-3 control-label">Status</label>
                                    <div class="col-lg-6">
                                        <label class="radio-inline i-checks">
                                            <input name="inlineRadio" id="inlineRadio1" ng-model="formData['ISAPPROVED']" 
                                                   ng-checked="formData['ISAPPROVED']" value="Pending" type="radio">
                                            <i></i> Pending
                                        </label>
                                        <label class="radio-inline i-checks">
                                            <input name="inlineRadio" id="inlineRadio12" ng-model="formData['ISAPPROVED']" 
                                                   ng-checked="formData['ISAPPROVED']" value="Approved" type="radio">
                                            <i></i> Approve
                                        </label>
                                        <label class="radio-inline i-checks">
                                            <input name="inlineRadio" id="inlineRadio13" ng-model="formData['ISAPPROVED']" 
                                                   ng-checked="formData['ISAPPROVED']" value="Rejected" type="radio">
                                            <i></i> Reject
                                        </label>
                                    </div>
                                </form>
                            </c:if>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>