<%-- 
    Document   : eventscontent
    Created on : Sep 10, 2017, 6:07:37 PM
    Author     : jay
--%>

<table class="table table-bordered bordered table-striped table-condensed datatable" ui-jq="dataTable" ui-options="dataTableOpt">
    <thead>
        <tr>
            <th>Sr. No.</th>
            <th>Title</th>
            <th>Art Form</th>
            <th>Event Type</th>
            <th class="text-center">Start Date</th>
            <th class="text-center">End Date</th>
            <!--<th>Contact Name</th>-->
            <!--<th>Mentor Name</th>--> 
            <th class="text-center">Status</th> 
            <th class="text-center">Participant</th> 
            <th class="text-center">Action</th> 
        </tr>
    </thead>
    <tbody>
        <tr ng-repeat="data in eventsData" ng-dblclick="changeView('events/edit/' + data.EVENTID)">
            <td>{{$index + 1}}</td>
            <td>{{data.TITLE}}</td>
            <td>{{data.ARTNAME}}</td>
            <td>{{data.EVENTTYPENAME}}</td>
            <td class="text-center">{{data.STARTDATE}}</td>
            <td class="text-center">{{data.ENDDATE}}</td>
            <!--<td>{{data.CONTACTNAME}}</td>-->
            <!--<td>{{data.NAME}}</td>-->
            <td class="text-center" ng-class="data.ISAPPROVED =='Approved' ? 'approved' : (data.ISAPPROVED == 'Rejected' ? 'rejected' : 'pending')"><b>{{data.ISAPPROVED}}</b></td>
            <td class="text-center"><a data-toggle="modal" data-target=".example-modal-lg" ng-click="getPartcipateData(data.EVENTID)"><i class="fa fa-eye"></i></a></td>
            <td class="text-center"><a href="#/events/edit/{{data.EVENTID}}" ><i class="fa fa-edit"></i></a></td>
        </tr>
    </tbody>
</table>

<div class="modal fade example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">View participants ({{participateData.length}})</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Contact Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-if="participateData.length == 0">
                            <td colspan="4">No Participants Found.</td>
                        </tr>
                        <tr ng-repeat="data in participateData">
                            <td>{{$index + 1}}</td>
                            <td>{{data.NAME}}</td>
                            <td>{{data.EMAIL}}</td>
                            <td>{{data.MOBILE}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>