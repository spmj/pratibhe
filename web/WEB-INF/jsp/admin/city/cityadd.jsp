<%-- 
    Document   : cityadd
    Created on : Aug 25, 2017, 12:22:10 PM
    Author     : jay
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<c:if test="${task eq 'Add'}">
    <div class="ui-content-body" ng-init="loadAdd()">
    </c:if>
    <c:if test="${task eq 'Edit'}">
        <div class="ui-content-body" ng-init="loadEdit()">
        </c:if>

        <div class="ui-container">

            <!--page title and breadcrumb start -->
            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-title"> City                    
                    </h1>
                </div>
                <div class="col-md-4 text-right">
                    <c:if test="${task eq 'Add'}">
                        <a ng-click="insertDetail()" class="btn btn-success"><i class="fa fa-check"></i> Save</a>
                    </c:if>
                    <c:if test="${task eq 'Edit'}">
                        <a ng-click="updateDetail()" class="btn btn-success"><i class="fa fa-check"></i> Update</a>
                        <a ng-click="deleteDetail()" class="btn btn-danger"><i class="fa fa-times"></i> Delete</a>
                    </c:if>
                    <a href="#/city/view" class="btn btn-default"><i class="fa fa-reply"></i> Cancel</a>
                </div>
            </div>
            <!--page title and breadcrumb end -->

            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <header class="panel-heading panel-border">
                            <c:if test="${task eq 'Add'}">
                                Create City                    
                            </c:if>
                            <c:if test="${task eq 'Edit'}">
                                Update City
                            </c:if>
                            <span class="tools pull-right">

                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">State Name <span class="text-danger"> * </span></label>
                                        <div class="col-sm-8">
                                            <select class="form-control" id="cmdStateName" ng-model="formData['STATEID']">
                                                <option ng-repeat="data in comboData.STATENAME" value="{{data.VALUE}}">{{data.TEXT}}</option>        
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">City Name <span class="text-danger"> * </span></label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" 
                                                   id="txtStateName" ng-model="formData['CITYNAME']">
                                        </div>
                                    </div>
                                    <c:if test="${task eq 'Edit'}">
                                        <div class="form-group">
                                            <label class=" col-sm-3 control-label">Is Active </label>
                                            <div class="col-sm-8">
                                                <label>
                                                    <input type="checkbox" ng-model="formData['ISACTIVE']">
                                                </label>
                                            </div>
                                        </div>
                                    </c:if>
                                </form>
                            </div>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>