<%-- 
    Document   : citycontent
    Created on : Sep 10, 2017, 6:07:37 PM
    Author     : jay
--%>

<table class="table table-bordered bordered table-striped table-condensed datatable" ui-jq="dataTable" ui-options="dataTableOpt">
    <thead>
        <tr>
            <th>Sr. No.</th>
            <th>City Name</th>
            <th>State Name</th>
            <th class="text-center">Is Active</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        <tr ng-repeat="data in cityData" ng-dblclick="changeView('city/edit/' + data.CITYID)">
            <td>{{$index + 1}}</td>
            <td>{{data.CITYNAME}}</td>
            <td>{{data.STATENAME}}</td>
            <td class="text-center"><div ng-if="data.ISACTIVE == 1">Yes</div>
                <div ng-if="data.ISACTIVE == 0">No</div></td>
            <td class="text-center"><a href="#/city/edit/{{data.CITYID}}" ><i class="fa fa-edit"></i></a></td>
        </tr>
    </tbody>
</table>