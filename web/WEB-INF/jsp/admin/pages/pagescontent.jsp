<%-- 
    Document   : citycontent
    Created on : Sep 10, 2017, 6:07:37 PM
    Author     : jay
--%>

<table class="table table-bordered bordered table-striped table-condensed datatable" ui-jq="dataTable" ui-options="dataTableOpt">
    <thead>
        <tr>
            <th>Sr. No.</th>
            <th>Page Name</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        <tr ng-repeat="data in pagesData" ng-dblclick="changeView('pages/edit/' + data.PAGEID)">
            <td>{{$index + 1}}</td>
            <td>{{data.PAGENAME}}</td>
            <td class="text-center"><a href="#/pages/edit/{{data.PAGEID}}" ><i class="fa fa-edit"></i></a></td>
        </tr>
    </tbody>
</table>