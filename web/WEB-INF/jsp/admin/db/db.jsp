<%-- 
    Document   : admin db
    Created on : Oct 28, 2017, 7:32:05 PM
    Author     : jay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="ui-content-body" ng-init="getDbData('${TYPE}')">
    <div class="ui-container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel">
                    <header class="panel-heading panel-border">
                        Events with participants
                    </header>
                    <div class="panel-body event-blog">
                        <ul class="task-sum-list" ng-repeat="event in dbData.EVENTPARTICIPANT">
                            <li>
                                <div class="tsk-title">
                                    {{event.TITLE}} <label class="badge badge-info">{{event.NOOFPART}}</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel">
                    <header class="panel-heading panel-border">
                        Event Approval Wise
                    </header>
                    <div class="panel-body event-blog">
                        <ul class="task-sum-list">
                            <li>
                                <div class="tsk-title">
                                    Approved Events  <label class="badge badge-success">{{dbData.EVENTAPPROVAL.Approved}}</label>
                                </div>
                            </li>
                            <li>
                                <div class="tsk-title">
                                    Pending for Approval  <label class="badge badge-info">{{dbData.EVENTAPPROVAL.Pending}}</label>
                                </div>
                            </li>
                            <li>
                                <div class="tsk-title">
                                    Rejected <label class="badge badge-danger">{{dbData.EVENTAPPROVAL.Rejected}}</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel">
                    <header class="panel-heading panel-border">
                        Mentor Approval Wise
                    </header>
                    <div class="panel-body event-blog">
                        <ul class="task-sum-list">
                            <li>
                                <div class="tsk-title">
                                    Approved Mentors <label class="badge badge-success">{{dbData.MENTORAPPROVAL.Approved}}</label>
                                </div>
                            </li>
                            <li>
                                <div class="tsk-title">
                                    Pending for Approval <label class="badge badge-info">{{dbData.MENTORAPPROVAL.Pending}}</label>
                                </div>
                            </li>
                            <li>
                                <div class="tsk-title">
                                    Rejected <label class="badge badge-danger">{{dbData.MENTORAPPROVAL.Rejected}}</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel">
                    <header class="panel-heading panel-border">
                        Number of art work
                    </header>
                    <div class="panel-body event-blog">
                        <ul class="task-sum-list" ng-repeat="art in dbData.ARTCOUNT">
                            <li>
                                <div class="tsk-title">
                                    {{art.ARTNAME}}  <label class="badge badge-warning">{{art.COUNT}}</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
