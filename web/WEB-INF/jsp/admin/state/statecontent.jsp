<%-- 
    Document   : statecontent
    Created on : Sep 10, 2017, 6:07:37 PM
    Author     : jay
--%>

<table class="table table-bordered bordered table-striped table-condensed datatable" ui-jq="dataTable" ui-options="dataTableOpt">
    <thead>
        <tr>
            <th>Sr. No.</th>
            <th>State Name</th>
            <th class="text-center">Is Active</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        <tr ng-repeat="data in stateData" ng-dblclick="changeView('state/edit/' + data.STATEID)">
            <td>{{$index + 1}}</td>
            <td>{{data.STATENAME}}</td>
            <td class="text-center"><div ng-if="data.ISACTIVE == 1">Yes</div>
                <div ng-if="data.ISACTIVE == 0">No</div></td>
            <td class="text-center"><a href="#/state/edit/{{data.STATEID}}" ><i class="fa fa-edit"></i></a></td>
        </tr>
    </tbody>
</table>