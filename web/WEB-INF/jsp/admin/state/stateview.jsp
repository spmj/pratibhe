<%-- 
    Document   : stateview
    Created on : Aug 25, 2017, 12:22:42 PM
    Author     : jay
--%>
<div class="ui-content-body">

    <div class="ui-container">

        <!--page title and breadcrumb start -->
        <div class="row">
            <div class="col-md-8 col-xs-6">
                <h1 class="page-title"> State</h1>
            </div>
            <div class="col-md-4 text-right col-xs-6">
                <a href="#/state/add" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
            </div>
        </div>
        <!--page title and breadcrumb end -->

        <div class="row" ng-init="viewDetail()">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading panel-border">
                        View State
                    </header>
                    <div class="panel-body">
                        <div id="page_content"></div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>