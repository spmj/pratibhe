<%-- 
    Document   : artcontent
    Created on : Sep 10, 2017, 6:07:37 PM
    Author     : jay
--%>

<table class="table table-bordered bordered table-striped table-condensed datatable" ui-jq="dataTable" ui-options="dataTableOpt">
    <thead>
        <tr>
            <th>Sr. No.</th>
            <th>Art Name</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        <tr ng-repeat="data in artData" ng-dblclick="changeView('art/edit/' + data.ARTID)">
            <td>{{$index + 1}}</td>
            <td>{{data.ARTNAME}}</td>
            <td class="text-center"><a href="#/art/edit/{{data.ARTID}}"><i class="fa fa-edit"></i></a></td>
        </tr>
    </tbody>
</table>