<%-- 
    Document   : artadd
    Created on : Aug 25, 2017, 12:22:10 PM
    Author     : jay
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<c:if test="${task eq 'Add'}">
    <div class="ui-content-body" ng-init="loadAdd()">
    </c:if>
    <c:if test="${task eq 'Edit'}">
        <div class="ui-content-body" ng-init="loadEdit()">
        </c:if>

        <div class="ui-container">

            <!--page title and breadcrumb start -->
            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-title"> Art                    
                    </h1>
                </div>
                <div class="col-md-4 text-right">
                    <c:if test="${task eq 'Add'}">
                        <a ng-click="insertDetail()" class="btn btn-success"><i class="fa fa-check"></i> Save</a>
                    </c:if>
                    <c:if test="${task eq 'Edit'}">
                        <a ng-click="updateDetail()" class="btn btn-success"><i class="fa fa-check"></i> Update</a>
                        <a ng-click="deleteDetail()" class="btn btn-danger"><i class="fa fa-times"></i> Delete</a>
                    </c:if>
                    <a href="#/art/view" class="btn btn-default"><i class="fa fa-reply"></i> Cancel</a>
                </div>
            </div>
            <!--page title and breadcrumb end -->

            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <header class="panel-heading panel-border">
                            <c:if test="${task eq 'Add'}">
                                Create Art                    
                            </c:if>
                            <c:if test="${task eq 'Edit'}">
                                Update Art
                            </c:if>
                            <span class="tools pull-right">

                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Art Name <span class="text-danger"> * </span></label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" 
                                                   id="txtArtName" ng-model="formData['ARTNAME']">
                                        </div>
                                    </div>
                                </form>
                            </div>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>