<%-- 
    Document   : cityadd
    Created on : Aug 25, 2017, 12:22:10 PM
    Author     : jay
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%--<c:if test="${task eq 'Edit'}">--%>
<div class="ui-content-body" ng-init="loadAdminWork()">
    <%--</c:if>--%>

    <div class="ui-container work-list">
        <h1 class="page-title"> Work ({{workData.length}}) </h1>
        <div class="row">

            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <!--                        <div class="row">
                                                    <div class="col-md-4">
                                                        {{data.FILE}}
                                                    </div>
                                                    <div class="col-md-4">
                                                        {{data.DESCRIPTION}}
                                                    </div>
                                                    <div class="col-md-4">
                                                     {{data.USERWORKID}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                       
                                                    </div>
                                                    <div class="col-md-4">
                                                        {{data.ARTNAME}}
                                                    </div>
                                                    <div class="col-md-4">
                                                       
                                                    </div>
                                                </div>-->

                        <div ng-if="workData.length == 0">
                            No Work Found.
                        </div>
                        <div class="row" ng-repeat="data in workData">
                            <div class="col-md-2 col-md-offset-1 text-center">
                                <img src="/pratibhe/resource/imgs/products/p1.jpg" alt="" class="img-responsive"/>                                
                            </div>
                            <div class="col-md-5">                                
                                <h3><span class="text-primary text-md text-uppercase">{{data.ARTNAME}} </span></h3> 
                                <p>{{data.DESCRIPTION}}</p>

                            </div>
                            <c:if test="${sessionbean.userType eq 'ADMIN'}">
                                <div class="col-md-3 text-center">                                
                                    <a href="#/admin/user/workdetail/{{data.USERWORKID}}" type="button" class="btn btn-info btn-capsul btn-details">Details</a>
                                </div>
                            </c:if>
                            <c:if test="${sessionbean.userType ne 'ADMIN'}">
                                <div class="col-md-3 text-center">                                
                                    <a href="#/mentor/student/workdetail/{{data.USERWORKID}}" type="button" class="btn btn-info btn-capsul btn-details">Details</a>
                                </div>
                            </c:if>
                            <div class="col-md-10 col-md-offset-1">
                                <div class="hr"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

