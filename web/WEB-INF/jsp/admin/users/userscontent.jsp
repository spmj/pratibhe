<%-- 
    Document   : eventscontent
    Created on : Sep 10, 2017, 6:07:37 PM
    Author     : jay
--%>

<table class="table table-bordered bordered table-striped table-condensed datatable" ui-jq="dataTable" ui-options="dataTableOpt">
    <thead>
        <tr>
            <th>Sr. No.</th>
            <th>Name </th>
            <th ng-if="usertype == 'students'">Mentor Name </th>
            <th>Art </th>
            <th class="text-center">Gender </th>
            <th>Mobile </th>
            <th class="text-center" ng-if="usertype == 'mentors'">Is Approved </th> 
            <th class="text-center">View </th> 
            <th class="text-center">Work </th> 
        </tr>
    </thead>
    <tbody>
        <tr ng-repeat="data in usersData" ng-dblclick="profileView(data.TYPE, data.USERID)">
            <td>{{$index + 1}}</td>
            <td>{{data.NAME}}</td>
            <td ng-if="usertype == 'students'">{{data.MENTORNAME}} </td>
            <td>{{data.ARTNAME}}</td>
            <td class="text-center">{{data.GENDER}}</td>
            <td>{{data.MOBILE}}</td>
            <td class="text-center" ng-if="usertype == 'mentors'" 
                ng-class="data.ISAPPROVED =='Approved' ? 'approved' : 
                            (data.ISAPPROVED == 'Rejected' ? 'rejected' : 'pending')">
                {{data.ISAPPROVED}}</td>
            <td class="text-center" ng-if="data.TYPE == 'MENTOR'"><a href="#mentors/edit/{{data.USERID}}" ><i class="fa fa-eye"></i></a></td>
            <td class="text-center" ng-if="data.TYPE == 'STUDENT'"><a href="#students/edit/{{data.USERID}}" ><i class="fa fa-eye"></i></a></td>
            <td class="text-center" ng-if="data.TYPE == 'MENTOR'"><a href="#admin/user/work/{{data.USERID}}" ><i class="fa fa-tasks"></i></a></td>
            <td class="text-center" ng-if="data.TYPE == 'STUDENT'"><a href="#admin/user/work/{{data.USERID}}" ><i class="fa fa-tasks"></i></a></td>
        </tr>
    </tbody>
</table>