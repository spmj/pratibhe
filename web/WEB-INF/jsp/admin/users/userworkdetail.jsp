<%-- 
    Document   : userworkdetail
    Created on : Nov 5, 2017, 12:29:33 PM
    Author     : SANDIPK
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%--<c:if test="${task eq 'Edit'}">--%>
<div class="ui-content-body" ng-init="getWorkDetail()">
    <%--</c:if>--%>
    <c:set var="contextPath"  value="${pageContext.request.contextPath}"/>
    <div class="ui-container work-list">
        <h1 class="page-title"> Notes </h1>
        <div class="row">

            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <img src="${contextPath}/client/getWorkImg/{{workDetail.USERWORKID}}" alt="" class="img-responsive"/>                                
                                <h2>{{workDetail.ARTNAME}}</h2>

                                <div class="col-md-6">
                                    <p class="text-left"><b>Name : </b>{{workDetail.WORKNAME}}</p>
                                </div>

                                <div class="col-md-6 chk-st">
                                    <input class="chk-star star" type="checkbox" 
                                           ng-model="workDetail.ISFETURED" 
                                           ng-change="setFetureWork()" title="bookmark page"> is featured
                                    <br/><br/>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="text-left"><b>Description : </b>{{workDetail.DESCRIPTION}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" ng-init="getComments()">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="hr"></div>
                                <ul class="chats cool-chat">

                                    <li class="text-center" ng-if="commentData == 0">
                                        No Comments Found.
                                    </li>
                                    <li ng-repeat="comment in commentData" class="{{comment.CLASS}}">
                                        <img ng-show="comment.STUDENTID != null" 
                                             src="${contextPath}/profile/getProfileImage/{{comment.STUDENTID}}" alt="Artist" class="avatar">
                                        <img ng-show="comment.MENTORID != null" 
                                             src="${contextPath}/profile/getProfileImage/{{comment.MENTORID}}" alt="Mentor" class="avatar">
                                        <div class="message">
                                            <span class="arrow"></span>
                                            <span class="name" >{{comment.NAME}}</span>
                                            <span class="datetime">at {{comment.ADDTIME}}</span>
                                            <span class="body">
                                                {{comment.COMMENT}}
                                            </span>
                                        </div>
                                    </li>
                                    <c:if test="${sessionbean.userType ne 'ADMIN'}">
                                        <li class="in">
                                            <img src="${contextPath}/profile/getProfileImage/${sessionbean.userId}" alt="" class="avatar">

                                            <div class="message">
                                                <span class="arrow"></span>
                                                <span class="name" >${sessionbean.userName}</span>
                                                <div class="btn-group  btn-group-sm pull-right">
                                                    <button class="btn btn-default btm-link" type="button"><i class="fa fa-edit"></i></button>
                                                    <button class="btn btn-default btn-link" type="button"><i class="fa fa-refresh"></i></button>                                                       
                                                </div>

                                                <!--<input--> 
                                                <span class="body">
                                                    <textarea class="form-control" rows="3" ng-model="Comment" ></textarea>
                                                    <button type="button" class="btn btn-sm btn-default btn-capsul" ng-click="addComment()">Submit</button>
                                                </span>
                                            </div>
                                        </li>
                                    </c:if>

                            </div>

                        </div>

                    </div>                                   
                </div>
            </div>
        </div>


    </div>
</div>


