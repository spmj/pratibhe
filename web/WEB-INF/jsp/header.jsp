<%-- 
    Document   : header
    Created on : Aug 24, 2017, 10:48:23 PM
    Author     : jay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<header id="header" class="ui-header ui-header--blue text-white">

    <div class="navbar-header">
        <!--logo start-->
        <%-- <c:if test="${TYPE eq 'ADMIN'}">
             <a href="#/db" class="navbar-brand">
                 <span class="logo">Pratibhey</span>
                 <span class="logo-compact">P</span>
             </a>
         </c:if>
         <c:if test="${TYPE eq 'MENTOR'}">
             <a href="#/mentor/db" class="navbar-brand">
                 <span class="logo">Pratibhey</span>
                 <span class="logo-compact">P</span>
             </a>
         </c:if>
         <c:if test="${TYPE eq 'STUDENT'}">
             <a href="#student/db" class="navbar-brand">
                 <span class="logo">Pratibhey</span>
                 <span class="logo-compact">P</span>
             </a>
         </c:if>--%>
        <a href="/pratibhe/" class="navbar-brand" context="${path}">
            <span class="logo">Pratibhey</span>
            <span class="logo-compact">P</span>
        </a>

<!--            <span class="logo"><img src="<c:url value="/resource/imgs/logo-dark.png"/>" alt=""/></span>
    <span class="logo-compact"><img src="<c:url value="/resource/imgs/logo-icon-dark.png"/>" alt=""/></span>-->

        <!--logo end-->
    </div>

    <div class="navbar-collapse nav-responsive-disabled">

        <!--toggle buttons start-->
        <ul class="nav navbar-nav">
            <li>
                <a class="toggle-btn" data-toggle="ui-nav" href="#">
                    <i class="fa fa-bars"></i>
                </a>
            </li>
        </ul>
        <!-- toggle buttons end -->

        <!--notification start-->
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown dropdown-usermenu">
                <a class=" dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <div class="user-avatar"><img src="<c:url value="/profile/getProfileImage/${sessionbean.userId}"/>" alt="..."></div>
                    <span class="hidden-sm hidden-xs">${sessionbean.userName}</span>
                    <!--<i class="fa fa-angle-down"></i>-->
                    <span class="caret hidden-sm hidden-xs"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                    <c:if test="${TYPE eq 'STUDENT' || TYPE eq 'MENTOR'}">
                        <li><a href="#/profile"><i class="fa fa-user-circle"></i>&nbsp;  My Profile</a></li>
                        </c:if>
                    <li><a href="#/changepwd"><i class="fa fa-cogs"></i>&nbsp;  Change Password</a></li>
                    <li><a href="#" ng-click="logout()"><i class="fa fa-sign-out"></i>&nbsp; Log Out</a></li>
                </ul>
            </li>
        </ul>
        <!--notification end-->

    </div>

</header>