<%-- 
    Document   : eventsadd
    Created on : Aug 25, 2017, 12:22:10 PM
    Author     : jay
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<c:if test="${task eq 'Add'}">
    <div class="ui-content-body" ng-init="loadAdd()">
    </c:if>
    <c:if test="${task eq 'Edit'}">
        <div class="ui-content-body" ng-init="loadEdit()">
        </c:if>

        <div class="ui-container">

            <!--page title and breadcrumb start -->
            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-title"> Work                   
                    </h1>
                </div>
                <div class="col-md-4 text-right">
                    <c:if test="${task eq 'Add'}">
                        <a ng-click="insertDetail()" class="btn btn-success"><i class="fa fa-check"></i> Save</a>
                    </c:if>
                    <c:if test="${task eq 'Edit'}">
                        <a ng-click="updateDetail()" class="btn btn-success"><i class="fa fa-check"></i> Update</a>
                        <a ng-click="deleteDetail()" class="btn btn-danger"><i class="fa fa-times"></i> Delete</a>
                    </c:if>
                    <a href="#/student/work/view" class="btn btn-default"><i class="fa fa-reply"></i> Cancel</a>
                </div>
            </div>
            <!--page title and breadcrumb end -->

            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <header class="panel-heading panel-border">
                            <c:if test="${task eq 'Add'}">
                                Work Event                    
                            </c:if>
                            <c:if test="${task eq 'Edit'}">
                                Work Event
                            </c:if>
                            <span class="tools pull-right">

                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Name </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text"  
                                                   maxlength="45"   id="txtName" ng-model="formData['WORKNAME']">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Art Form <span class="text-danger"> * </span></label>
                                        <div class="col-sm-8">
                                            <select class="form-control" id="cmdArt" ng-model="formData['ARTID']">
                                                <option></option>
                                                <option ng-repeat="data in comboData.ART" value="{{data.VALUE}}">{{data.TEXT}}</option>        
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Work <span class="text-danger"> * </span></label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="file" id="fileWork" name="fileWork" file-model="formData['WORKFILE']" 
                                                   filename="formData['FILENAME']" fileread="" filesize="formData['FILESIZE']" fileext="formData['FILEEXT']" >
                                            <a href="{{formData['FILEPATH']}}">{{formData['FILE']}}</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            

                            <div class="col-md-12">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class=" col-sm-3 control-label">Description <span class="text-danger"> * </span></label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" id="txtDescription" name="txtDescription" ng-model="formData['DESCRIPTION']">
                                            </textarea>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>