<%-- 
    Document   : eventscontent
    Created on : Sep 10, 2017, 6:07:37 PM
    Author     : jay
--%>

<table class="table table-bordered bordered table-striped table-condensed datatable" ui-jq="dataTable" ui-options="dataTableOpt">
    <thead>
        <tr>
            <th>Sr. No.</th>
            <th>Name</th>
            <th>Art Form</th>
            <th>File</th>
            <th>Description</th> 
            <th class="text-center">Action</th> 
            <th class="text-center">Work Detail</th> 
        </tr>
    </thead>
    <tbody>
        <tr ng-repeat="data in workData" ng-dblclick="changeView('student/work/edit/' + data.USERWORKID)">
            <td>{{$index + 1}}</td>
            <td>{{data.WORKNAME}}</td>
            <td>{{data.ARTNAME}}</td>
            <td>{{data.FILE}}</td>
            <td>{{data.DESCRIPTION}}</td>
            <td class="text-center"><a href="#/student/work/edit/{{data.USERWORKID}}" ><i class="fa fa-edit"></i></a></td>
            <td class="text-center"><a href="#/student/user/workdetail/{{data.USERWORKID}}" ><i class="fa fa-tasks"></i></a></td>
        </tr>
    </tbody>
</table>