<%-- 
    Document   : happeningscontent
    Created on : Sep 10, 2017, 6:07:37 PM
    Author     : jay
--%>

<table class="table table-bordered bordered table-striped table-condensed datatable" ui-jq="dataTable" ui-options="dataTableOpt">
    <thead>
        <tr>
            <th>Sr. No.</th>
            <th>Title</th>
            <th>Art Form</th>
            <th>Event Type</th>
            <th class="text-center">Start Date</th>
            <th class="text-center">End Date</th>
            <th>Contact Name</th>
            <th class="text-center">View</th> 
            <th class="text-center">Delete</th> 
        </tr>
    </thead>
    <tbody>
        <tr ng-repeat="data in happeningsData" ng-dblclick="changeView('studenthappenings/edit/' + data.HAPPENINGSID)">
            <td>{{$index + 1}}</td>
            <td>{{data.TITLE}}</td>
            <td>{{data.ARTNAME}}</td>
            <td>{{data.EVENTTYPENAME}}</td>
            <td class="text-center">{{data.STARTDATE}}</td>
            <td class="text-center">{{data.ENDDATE}}</td>
            <td>{{data.CONTACTNAME}}</td>
            <td class="text-center"><a href="#studenthappenings/edit/{{data.HAPPENINGSID}}" ><i class="fa fa-eye"></i></a></td>
            <td class="text-center"><a data-toggle="modal" ng-click="deleteDetail(data.HAPPENINGSID)"><i class="fa fa-trash"></i></a></td>
        </tr>
    </tbody>
</table>

