<%-- 
    Document   : Student/db
    Created on : Oct 28, 2017, 7:32:05 PM
    Author     : jay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="ui-content-body" ng-init="getDbData('${TYPE}')">
    <div class="ui-container">
        <div class="row">
<!--            <div class="col-md-6">
                <div class="panel">
                    <header class="panel-heading panel-border">
                        No of events
                    </header>
                    <div class="panel-body event-blog">
                        <ul class="task-sum-list">

                        </ul>
                    </div>
                </div>
            </div>-->
            <div class="col-md-6">
                <div class="panel">
                    <header class="panel-heading panel-border">
                        Work per category
                    </header>
                    <div class="panel-body event-blog">
                        <ul class="task-sum-list"  ng-repeat="data in dbData.WORKCNTART">
                            <li>
                                <div class="tsk-title">
                                    {{data.ARTNAME}}<label class="badge badge-success">{{data.COUNT}}</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
