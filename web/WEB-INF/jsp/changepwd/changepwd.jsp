<%-- 
    Document   : changepwd
    Created on : Aug 28, 2017, 9:33:46 PM
    Author     : jay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 

<div class="ui-content-body">

    <div class="ui-container">

        <!--page title and breadcrumb start -->
        <div class="row">
            <div class="col-md-8">
                <h1 class="page-title"> Change Password                    
                </h1>
            </div>
            <div class="col-md-4 text-right">
                <a ng-click="changePwd()" id="btnchangePwd" class="btn btn-success"><i class="fa fa-check"></i> Change Password</a>
            </div>
        </div>
        <!--page title and breadcrumb end -->

        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <header class="panel-heading panel-border">                        
                        Change Password For ${USERNAME}
                    </header>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Old Password<span style="color: red;"> *</span></label>
                                    <div class="col-sm-8">
                                        <input type="password" id="txtOldPwd" maxlength="50" 
                                               class="form-control" ng-model="formData['OLDPWD']"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">New Password<span style="color: red;"> *</span></label>
                                    <div class="col-sm-8">
                                        <input type="password" id="txtNewPwd" maxlength="50" 
                                               class="form-control" ng-model="formData['NEWPWD']"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class=" col-sm-3 control-label">Confirm Password<span style="color: red;"> *</span></label>
                                    <div class="col-sm-8">
                                        <input type="password" id="txtConfirmPwd" maxlength="50" 
                                               class="form-control" ng-model="formData['CNFPWD']"/>
                                    </div>
                                </div>
                            </form>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
