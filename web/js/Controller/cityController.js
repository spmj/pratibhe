// Project : cityController.js
// Created : 26 Jul, 2017 9:47:55 AM
// Author  : jay

angular.module('PratibheApp').controller('cityController', ['cityService', '$rootScope', '$scope', '$location', '$routeParams', '$q', function (cityService, $rootScope, $scope, $location, $routeParams, $q) {

        $scope.header = "City";
        $scope.infoheader = "City Details";
        $scope.addLink = "/city/add";
        $scope.editLink = "/city/edit";
        $scope.cancelLink = "/city/view";
        $scope.viewLink = "/city/view";

        $scope.formData = {STATEID: "", CITYID: "", CITYNAME: "", ISACTIVE: 1};

        /**
         * load combo fill
         */
        $scope.loadCombo = function () {
            var allCombo = cityService.getAllCombo();
            $rootScope.promise = allCombo.then(function (response) {
                $scope.comboData = response.data;
            });
        };
        /**
         * load city add
         */
        $scope.loadAdd = function () {
            $("#cmdStateName").focus();
            $scope.loadCombo();
        };
        /**
         * load city edit
         */
        $scope.loadEdit = function () {
            $("#cmdStateName").focus();
            $scope.getDataById();
        };

        /**
         * get all city Data
         * @returns {undefined}
         */
        $scope.viewDetail = function () {
            return $q(function (resolve) {
                var getData = cityService.viewDetail();
                $rootScope.promise = getData.then(function (response) {
                    $scope.cityData = response.data.CITYDATA;
                    var getContent = cityService.loadContentPage();
                    getContent.then(function (response) {
                        var $div = $(response.data);
                        $("#page_content").append($div);
                        angular.element(document).injector().invoke(function ($compile) {
                            var scope = angular.element($div).scope();
                            $compile($div)(scope);
                        });
                    });
                });
            });
        };

        /**
         * get city Data
         * @returns {undefined}
         */
        $scope.getDataById = function () {
            $scope.id = $routeParams.id;
            $scope.loadCombo();
            var getData = cityService.getUpdateData($scope.id);
            $rootScope.promise = getData.then(function (response) {
                if (response.data.ISACTIVE == 1) {
                    response.data.ISACTIVE = true;
                } else {
                    response.data.ISACTIVE = false;
                }
                if (response.data.STATEID) {
                    response.data.STATEID = response.data.STATEID.toString();
                }
                $scope.formData = response.data;
                //$scope.updateSelect();
            });
        };

        $scope.updateSelect = function () {
            $('#cmdStateName').val($scope.formData.STATEID).trigger('change.select2');
        };
        /**
         * for category validation
         */
        $scope.validateData = function () {
            if (!validate_combo('cmdStateName', $scope.formData.STATEID, 'State Name')) {
                return false;
            }
            if (!validate_element('txtCityName', $scope.formData.CITYNAME, 'City Name')) {
                return false;
            }
            return true;
        };
        /**
         * for insert city
         */
        $scope.insertDetail = function () {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            var response = cityService.insertDetail($scope.formData);
            $rootScope.promise = response.success(function (data) {
                if (resMsg(data)) {
                    $rootScope.changeView($scope.viewLink);
                }
            });
        };
        /**
         * for update city
         */
        $scope.updateDetail = function () {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            if ($scope.formData.ISACTIVE) {
                $scope.formData.ISACTIVE = 1;
            } else {
                $scope.formData.ISACTIVE = 0;
            }
            var response = cityService.updateDetail($scope.formData.CITYID,$scope.formData);
            $rootScope.promise = response.success(function (data) {
                if (resMsg(data)) {
                    $rootScope.changeView($scope.viewLink);
                }
            });
        };
        /**
         * for delete ciity
         */
        $scope.deleteDetail = function () {
            $rootScope.deleteConfirmBox(function () {
                var response = cityService.deleteDetail($scope.formData.CITYID);
                $rootScope.promise = response.success(function (data) {
                    if (resMsg(data)) {
                        $rootScope.changeView($scope.viewLink);
                    }
                });
            });
        };
    }]);