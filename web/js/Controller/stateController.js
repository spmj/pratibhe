// Project : stateController.js
// Created : 26 Jul, 2017 9:47:55 AM
// Author  : jay

angular.module('PratibheApp').controller('stateController', ['stateService', '$rootScope', '$scope', '$location', '$routeParams', '$q', function (stateService, $rootScope, $scope, $location, $routeParams, $q) {

        $scope.header = "State";
        $scope.infoheader = "State Details";
        $scope.addLink = "/state/add";
        $scope.editLink = "/state/edit";
        $scope.cancelLink = "/state/view";
        $scope.viewLink = "/state/view";

        $scope.formData = {STATEID: "", STATENAME: "", ISACTIVE: 1};

        /**
         * load state add
         */
        $scope.loadAdd = function () {
            $("#txtStateName").focus();
        };

        /**
         * load state edit
         */
        $scope.loadEdit = function () {
            $("#txtStateName").focus();
            $scope.getDataById();
        };


        /**
         * get all state Data
         * @returns {undefined}
         */
        $scope.viewDetail = function () {
            return $q(function (resolve) {
                var getData = stateService.viewDetail();
                $rootScope.promise = getData.then(function (response) {
                    $scope.stateData = response.data.STATEDATA;
                    var getContent = stateService.loadContentPage();
                    getContent.then(function (response) {
                        var $div = $(response.data);
                        $("#page_content").append($div);
                        angular.element(document).injector().invoke(function ($compile) {
                            var scope = angular.element($div).scope();
                            $compile($div)(scope);
                        });
                    });
                });
            });
        };

        /**
         * get state Data
         * @returns {undefined}
         */
        $scope.getDataById = function () {
            $scope.id = $routeParams.id;
            var getData = stateService.getUpdateData($scope.id);
            $rootScope.promise = getData.then(function (response) {
                if (response.data.ISACTIVE == 1) {
                    response.data.ISACTIVE = true;
                } else {
                    response.data.ISACTIVE = false;
                }
                $scope.formData = response.data;
            });
        };

        /**
         * for state validation
         */
        $scope.validateData = function () {
            if (!validate_element('txtStateName', $scope.formData.STATENAME, 'State Name')) {
                return false;
            }
            return true;
        };

        /**
         * for insert state
         */
        $scope.insertDetail = function () {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            var response = stateService.insertDetail($scope.formData);
            $rootScope.promise = response.success(function (data) {
                if (resMsg(data)) {
                    $rootScope.changeView($scope.viewLink);
                }
            });
        };

        /**
         * for update state
         */
        $scope.updateDetail = function () {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            if ($scope.formData.ISACTIVE) {
                $scope.formData.ISACTIVE = 1;
            } else {
                $scope.formData.ISACTIVE = 0;
            }
            var response = stateService.updateDetail($scope.formData.STATEID, $scope.formData);
            $rootScope.promise = response.success(function (data) {
                if (resMsg(data)) {
                    $rootScope.changeView($scope.viewLink);
                }
            });
        };

        /**
         * for delete state
         */
        $scope.deleteDetail = function () {
            $rootScope.deleteConfirmBox(function () {
                var response = stateService.deleteDetail($scope.formData.STATEID);
                $rootScope.promise = response.success(function (data) {
                    if (resMsg(data)) {
                        $rootScope.changeView($scope.viewLink);
                    }
                });
            })
        };
    }]);