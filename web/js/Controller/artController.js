// Project : artController.js
// Created : 26 Jul, 2017 9:47:55 AM
// Author  : jay

angular.module('PratibheApp').controller('artController', ['artService', '$rootScope', '$scope', '$location', '$routeParams', '$q', function (artService, $rootScope, $scope, $location, $routeParams, $q) {

        $scope.header = "Art";
        $scope.infoheader = "Art Details";
        $scope.addLink = "/art/add";
        $scope.editLink = "/art/edit";
        $scope.cancelLink = "/art/view";
        $scope.viewLink = "/art/view";

        $scope.formData = {ARTID: "", ARTNAME: ""};

        /**
         * load art add
         */
        $scope.loadAdd = function () {
            $("#txtArtName").focus();
        };

        /**
         * load art edit
         */
        $scope.loadEdit = function () {
            $("#txtArtName").focus();
            $scope.getDataById();
        };


        /**
         * get all art Data
         * @returns {undefined}
         */
        $scope.viewDetail = function () {
            return $q(function (resolve) {
                var getData = artService.viewDetail();
                $rootScope.promise = getData.then(function (response) {
                    $scope.artData = response.data.ARTDATA;
                    var getContent = artService.loadContentPage();
                    getContent.then(function (response) {
                        var $div = $(response.data);
                        $("#page_content").append($div);
                        angular.element(document).injector().invoke(function ($compile) {
                            var scope = angular.element($div).scope();
                            $compile($div)(scope);
                        });
                    });
                });
            });
        };

        /**
         * get art Data
         * @returns {undefined}
         */
        $scope.getDataById = function () {
            $scope.id = $routeParams.id;
            var getData = artService.getUpdateData($scope.id);
            $rootScope.promise = getData.then(function (response) {
                $scope.formData = response.data;
            });
        };

        /**
         * for art validation
         */
        $scope.validateData = function () {
            if (!validate_element('txtArtName', $scope.formData.ARTNAME, 'Art Name')) {
                return false;
            }
            return true;
        };

        /**
         * for insert art
         */
        $scope.insertDetail = function () {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            var response = artService.insertDetail($scope.formData);
            $rootScope.promise = response.success(function (data) {
                if (resMsg(data)) {
                    $rootScope.changeView($scope.viewLink);
                }
            });
        };

        /**
         * for update art
         */
        $scope.updateDetail = function () {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            var response = artService.updateDetail($scope.formData.ARTID, $scope.formData);
            $rootScope.promise = response.success(function (data) {
                if (resMsg(data)) {
                    $rootScope.changeView($scope.viewLink);
                }
            });
        };

        /**
         * for delete art
         */
        $scope.deleteDetail = function () {
            if($scope.formData.ARTID == '2'){
                return getMessage("You can not delete Music ,Please check ")
            }
            $rootScope.deleteConfirmBox(function () {
                var response = artService.deleteDetail($scope.formData.ARTID);
                $rootScope.promise = response.success(function (data) {
                    if (resMsg(data)) {
                        $rootScope.changeView($scope.viewLink);
                    }
                });
            })
        };
    }]);