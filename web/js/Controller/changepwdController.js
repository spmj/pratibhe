// Project : changepwdController.js
// Created : 26 Jul, 2017 9:47:55 AM
// Author  : jay


angular.module('PratibheApp').controller('changepwdController', ['changepwdService', '$rootScope', '$scope', '$location', '$routeParams', '$q', function (changepwdService, $rootScope, $scope, $location, $routeParams, $q) {

        $scope.formData = {OLDPWD: "", NEWPWD: "", CNFPWD: ""};

        /**
         * for changepwd validation
         */
        $scope.validateData = function () {
            if (!validate_combo('txtOldPwd', $scope.formData.OLDPWD, 'Old Password')) {
                return false;
            }
            if (!validate_element('txtNewPwd', $scope.formData.NEWPWD, 'New Password')) {
                return false;
            }
            if (!validate_element('txtConfirmPwd', $scope.formData.CNFPWD, 'Confirm Password')) {
                return false;
            }
            if ($scope.formData.NEWPWD != $scope.formData.CNFPWD) {
                getMessage("do not match password.", 'danger');
                return false;
            }
            return true;
        };

        /**
         * for change pwd 
         */
        $scope.changePwd = function () {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            var response = changepwdService.changePwd($scope.formData);
            $rootScope.promise = response.success(function (data) {
                if (resMsg(data)) {
                    $scope.formData = {};
                }
            });
        };


    }]);
