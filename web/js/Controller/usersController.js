// Project : artController.js
// Created : 26 Jul, 2017 9:47:55 AM
// Author  : jay

angular.module('PratibheApp').controller('usersController', ['usersService', '$rootScope', '$scope', '$location', '$routeParams', '$q', function(usersService, $rootScope, $scope, $location, $routeParams, $q) {


        $scope.infoheader = "User Details";
        $scope.addLink = "/students/add";
        $scope.editLink = "/students/edit";
        $scope.cancelLink = "/students/view";

        $scope.formData = {USERID: "", TYPE: "", NAME: "", ARTISTNAME: "",
            EMAIL: "", MOBILE: "", AADHARNO: "", GENDER: "", OCCUPATION: "",
            DOB: "", ADDRESS: "", CITY: "", STATE: "", ABOUTME: "", ARTNAME: "",
            OTHER: "", PASSWORD: "", IMAGE: "", ADDTIME: "", EDITTIME: "",
            TYPEOFWORK: "", TENUREOFWORK: "", MENTORID: "", ISACTIVE: "",
            MENTORNAME: "", ISAPPROVED: "", CITYNAME: "", STATENAME: ""};
        
        /**
         * load users edit
         */
        $scope.loadEdit = function() {
            $("#txtAddress").focus();
            $scope.getDataById();
        };

        $scope.usertype = "";
        console.log($location.path() + " ");
        if ($location.path() == "/students/view") {
            $scope.usertype = "students";
            $scope.header = "Students";
            $scope.viewtype = "View Students";
            $scope.viewLink = "#/students/view";
        }
        if ($location.path() == "/mentors/view") {
            $scope.usertype = "mentors";
            $scope.header = "Mentors";
            $scope.viewtype = "View Mentors";
            $scope.viewLink = "#/mentors/view";
        }

        var usl = $location.path();

        if (usl.includes("student")) {
            $scope.header = "Student";
            $scope.viewLink = "#/students/view";
            $scope.viewLinkApproot = "/students/view";

        }
        if (usl.includes("mentor")) {
            $scope.header = "Mentor";
            $scope.viewLink = "#/mentors/view";
            $scope.viewLinkApproot = "/mentors/view";
        }

        console.log($scope.header);

        /**
         * get all users Data
         * @returns {undefined}
         */
        $scope.viewDetail = function() {
            return $q(function(resolve) {
                var getData = usersService.viewDetail($scope.usertype);
                $rootScope.promise = getData.then(function(response) {
                    $scope.usersData = response.data.USERSDATA;
                    var getContent = usersService.loadContentPage();
                    getContent.then(function(response) {
                        var $div = $(response.data);
                        $("#page_content").append($div);
                        angular.element(document).injector().invoke(function($compile) {
                            var scope = angular.element($div).scope();
                            $compile($div)(scope);
                        });
                    });
                });
            });
        };

        /**
         * get users Data
         * @returns {undefined}
         */
        $scope.getDataById = function() {
            $scope.id = $routeParams.id;
            var getData = usersService.getUpdateData($scope.id);
            $rootScope.promise = getData.then(function(response) {
                $scope.formData = response.data;
                $scope
                console.log($scope.formData);
            });
        };

        /**
         * for users validation
         */
        $scope.validateData = function() {
            return true;
        };


        /**
         * for update users
         */
        $scope.updateDetail = function() {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            var response = usersService.updateDetail($scope.formData.USERID, $scope.formData);
            $rootScope.promise = response.success(function(data) {
                if (resMsg(data)) {
                    $rootScope.changeView($scope.viewLinkApproot);
                }
            });
        };

        /**
         * for delete users
         */
        $scope.deleteDetail = function() {
            $rootScope.deleteConfirmBox(function() {
                var response = usersService.deleteDetail($scope.formData.EVENTID);
                $rootScope.promise = response.success(function(data) {
                    if (resMsg(data)) {
                        $rootScope.changeView($scope.viewLinkApproot);
                    }
                });
            });
        };

        /**
         * load users edit
         */
        $scope.loadAdminWork = function() {
            $scope.getWorkById();
        };

        /**
         * get users Data
         * @returns {undefined}
         */
        $scope.getWorkById = function() {
            $scope.id = $routeParams.id;
            console.log($scope.id);
            var getData = usersService.getWorkData($scope.id);
            $rootScope.promise = getData.then(function(response) {
                $scope.workData = response.data;
                console.log($scope.workData);
            });
        };

        $scope.getWorkDetail = function() {
            $scope.id = $routeParams.id;
            var getDetailData = usersService.getWorkDetail($scope.id);
            $rootScope.promise = getDetailData.then(function(response) {
                $scope.workDetail = response.data;
                console.log($scope.workDetail);
            });
        };

        /**
         * for update work
         */
        $scope.setFetureWork = function() {
            console.log($scope.workDetail);
            var response = usersService.setFetureWork($scope.workDetail);
            $rootScope.promise = response.success(function(data) {
                if (resMsg(data)) {
                    
                }
            });
        };

        /**
         * for insert work
         */
        $scope.addComment = function() {
            $scope.data = {"WORKID": $routeParams.id, "COMMENT": $scope.Comment};
            var response = usersService.addComment($scope.data);
            $rootScope.promise = response.success(function(data) {
                if (resMsg(data)) {
                    $scope.Comment = "";
                    $scope.getComments();
                }
            });
        };

        $scope.getComments = function() {
            $scope.id = $routeParams.id;
            var getCommentData = usersService.getCommentDetail($scope.id);
            $rootScope.promise = getCommentData.then(function(response) {
                $scope.commentData = response.data;
            });
        };
    }]);