// Project : artController.js
// Created : 26 Jul, 2017 9:47:55 AM
// Author  : jay

angular.module('PratibheApp').controller('studentHappeningsController', ['studentHappeningsSercvice', '$rootScope', '$scope', '$location', '$routeParams', '$q', function(studentHappeningsSercvice, $rootScope, $scope, $location, $routeParams, $q) {

        $scope.header = "Happenings";
        $scope.infoheader = "Happenings Details";
        $scope.addLink = "student/happenings/add";
        $scope.editLink = "studenthappenings/edit";
        $scope.cancelLink = "studnenthappenings/view";
        $scope.viewLink = "studnenthappenings/view";

        $scope.formData = {EVENTID: "", TITLE: "", SUMMERY: "", DETAILS: "", ADDRESS: "", STATEID: "", CITYID: "",
            ARTID: "", EVENTTYPEID: "", THUMBIMG: "", BANNERIMG: "", STARTDATE: "", ENDDATE: "", CONTACTNAME: "",
            CONTACTEMAIL: "", NOOFSLOT: ""};



        /**
         * load happenings edit
         */
        $scope.loadEdit = function() {
            $("#txtAddress").focus();
            $scope.getDataById();
        };


        /**
         * get all happenings Data
         * @returns {undefined}
         */
        $scope.viewDetail = function() {
            return $q(function(resolve) {
                var getData = studentHappeningsSercvice.viewDetail();
                $rootScope.promise = getData.then(function(response) {
                    $scope.happeningsData = response.data.HAPPENINGSSDATA;
                    var getContent = studentHappeningsSercvice.loadContentPage();
                    getContent.then(function(response) {
                        var $div = $(response.data);
                        $("#page_content").append($div);
                        angular.element(document).injector().invoke(function($compile) {
                            var scope = angular.element($div).scope();
                            $compile($div)(scope);
                        });
                    });
                });
            });
        };

        /**
         * get happenings Data
         * @returns {undefined}
         */
        $scope.getDataById = function() {
            $scope.id = $routeParams.id;
            var getData = studentHappeningsSercvice.getUpdateData($scope.id);
            $rootScope.promise = getData.then(function(response) {
                $scope.formData = response.data;
                console.log($scope.formData);
            });
        };


        /**
         * for happenings validation
         */
        $scope.validateData = function() {
//            if (!validate_element('txtArtName', $scope.formData.ARTNAME, 'Art Name')) {
//                return false;
//            }
            return true;
        };




        /**
         * for update happenings
         */
        $scope.updateDetail = function() {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            var response = studentHappeningsSercvice.updateDetail($scope.formData);
            $rootScope.promise = response.success(function(data) {
                if (resMsg(data)) {
                    $rootScope.changeView($scope.viewLink);
                }
            });
        };

        /**
         * for delete happenings
         */
        $scope.deleteDetail = function(id) {
            $rootScope.deleteConfirmBox(function() {
                var response = studentHappeningsSercvice.deleteDetail(id);
                $rootScope.promise = response.success(function(data) {
                    if (resMsg(data)) {
                        console.log("inside");
                        $rootScope.changeView($scope.viewLink);
                    }
                });
            });
        };
    }]);