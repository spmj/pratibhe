// Project : artController.js
// Created : 26 Jul, 2017 9:47:55 AM
// Author  : jay

angular.module('PratibheApp').controller('happeningsController', ['happeningsService', '$rootScope', '$scope', '$location', '$routeParams', '$q', function (happeningsService, $rootScope, $scope, $location, $routeParams, $q) {

        $scope.header = "Happenings";
        $scope.infoheader = "Happenings Details";
        $scope.addLink = "/happenings/add";
        $scope.editLink = "/happenings/edit";
        $scope.cancelLink = "/happenings/view";
        $scope.viewLink = "/happenings/view";

        $scope.formData = {EVENTID: "", TITLE: "", SUMMERY: "", DETAILS: "", ADDRESS: "", STATEID: "", CITYID: "",
            ARTID: "", EVENTTYPEID: "", THUMBIMG: "", BANNERIMG: "", STARTDATE: "", ENDDATE: "", CONTACTNAME: "",
            CONTACTEMAIL: "", NOOFSLOT: ""};

        /**
         * load combo fill
         */
        $scope.loadCombo = function () {
            var allCombo = happeningsService.getAllCombo();
            $rootScope.promise = allCombo.then(function (response) {
                $scope.comboData = response.data;
            });
        };

        /**
         * load combo fill
         */
        $scope.getCity = function (stateid) {
            var city = happeningsService.getCity(stateid);
            city.then(function (response) {
                $scope.cityName = response.data;
            })
        };

        /**
         * load happenings add
         */
        $scope.loadAdd = function () {
            $("#txtAddress").focus();
            $scope.loadCombo();
        };

        /**
         * load happenings edit
         */
        $scope.loadEdit = function () {
            $("#txtAddress").focus();
            $scope.getDataById();
        };


        /**
         * get all happenings Data
         * @returns {undefined}
         */
        $scope.viewDetail = function () {
            return $q(function (resolve) {
                var getData = happeningsService.viewDetail();
                $rootScope.promise = getData.then(function (response) {
                    $scope.happeningsData = response.data.HAPPENINGSSDATA;
                    var getContent = happeningsService.loadContentPage();
                    getContent.then(function (response) {
                        var $div = $(response.data);
                        $("#page_content").append($div);
                        angular.element(document).injector().invoke(function ($compile) {
                            var scope = angular.element($div).scope();
                            $compile($div)(scope);
                        });
                    });
                });
            });
        };

        /**
         * get happenings Data
         * @returns {undefined}
         */
        $scope.getDataById = function () {
            $scope.id = $routeParams.id;
            $scope.loadCombo();
            var getData = happeningsService.getUpdateData($scope.id);
            $rootScope.promise = getData.then(function (response) {
                $scope.getCity(response.data.STATEID);
                if (response.data.STATEID) {
                    response.data.STATEID = response.data.STATEID.toString();
                }
                if (response.data.CITYID) {
                    response.data.CITYID = response.data.CITYID.toString();
                }
                if (response.data.ARTID) {
                    response.data.ARTID = response.data.ARTID.toString();
                }
                if (response.data.EVENTTYPEID) {
                    response.data.EVENTTYPEID = response.data.EVENTTYPEID.toString();
                }
                $scope.formData = response.data;

                $scope.updateSelect();
            });
        };

        $scope.updateSelect = function () {
            $('#cmdState').val($scope.formData.STATEID).trigger('change.select2');
            $('#cmdCity').val($scope.formData.CITYID).trigger('change.select2');
            $('#cmdArt').val($scope.formData.ARTID).trigger('change.select2');
            $('#cmdEventType').val($scope.formData.EVENTTYPEID).trigger('change.select2');
        };

        $scope.thumbChanged = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imgThumbView')
                            .attr('src', e.target.result)
                            .width(200)
                            .height(100);
                };
                reader.readAsDataURL(input.files[0]);
            }
        };

        $scope.bannerChanged = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imgBannerView')
                            .attr('src', e.target.result)
                            .width(400)
                            .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }
        };

        /**
         * for happenings validation
         */
        $scope.validateData = function () {
            if (!validate_element('txtTitle', $scope.formData.TITLE, 'Title')) {
                return false;
            }
            if (!validate_element('txtSummery', $scope.formData.SUMMERY, 'Summery')) {
                return false;
            }            
            if (!validate_combo('cmdState', $scope.formData.STATEID, 'State')) {
                return false;
            }
            if (!validate_combo('cmdCity', $scope.formData.CITYID, 'City')) {
                return false;
            }
            if (!validate_combo('cmdArt', $scope.formData.ARTID, 'Art Form')) {
                return false;
            }
            if (!validate_combo('cmdEventType', $scope.formData.EVENTTYPEID, 'Event Type')) {
                return false;
            }
            if (!validate_file('fileThumb', $scope.formData.THUMBIMG, 'Thumbnail')) {
                return false;
            }
            if (!validate_file('fileBanner', $scope.formData.BANNERIMG, 'Long Banner Image')) {
                return false;
            }
            if (!validate_element('txtStartDate', $scope.formData.STARTDATE, 'Start Date')) {
                return false;
            }
            if (!validate_element_regex('txtStartDate', $scope.formData.STARTDATE, REG_DATE, MSG_DATE + 'Start Date')) {
                return false;
            }
            if (!validate_element('txtEndDate', $scope.formData.ENDDATE, 'End Date')) {
                return false;
            }
            if (!validate_element_regex('txtEndDate', $scope.formData.ENDDATE, REG_DATE, MSG_DATE + 'End Date')) {
                return false;
            }

            var fromdate = $rootScope.stringToDate($scope.formData.STARTDATE, "dd-mmm-yyyy", "-");
            var todate = $rootScope.stringToDate($scope.formData.ENDDATE, "dd-mmm-yyyy", "-");

            if (todate < fromdate) {
                getMessage("Start date must be less than End date", 'danger');
                return false;
            }
            if (!validate_element('txtNoofSolt', $scope.formData.NOOFSLOT, 'No Of Slot')) {
                return false;
            }
            return true;
        };

        /**
         * for insert happenings
         */
        $scope.insertDetail = function () {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            var response = happeningsService.insertDetail($scope.formData);
            $rootScope.promise = response.success(function (data) {
                if (resMsg(data)) {
                    $rootScope.changeView($scope.viewLink);
                }
            });
        };

        /**
         * for update happenings
         */
        $scope.updateDetail = function () {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            var response = happeningsService.updateDetail($scope.formData);
            $rootScope.promise = response.success(function (data) {
                if (resMsg(data)) {
                    $rootScope.changeView($scope.viewLink);
                }
            });
        };

        /**
         * for delete happenings
         */
        $scope.deleteDetail = function () {
            $rootScope.deleteConfirmBox(function () {
                var response = happeningsService.deleteDetail($scope.formData.HAPPENINGSID);
                $rootScope.promise = response.success(function (data) {
                    if (resMsg(data)) {
                        $rootScope.changeView($scope.viewLink);
                    }
                });
            })
        };

        /**
         * for delete happenings
         */
        $scope.getPartcipateData = function (id) {
            var participateData = happeningsService.getPartcipateData(id);
            participateData.then(function (response) {
                $scope.participateData = response.data.PARTCIPATEDATA;
            })
        };
    }]);