// Project : cityController.js
// Created : 26 Jul, 2017 9:47:55 AM
// Author  : jay

angular.module('PratibheApp').controller('inquiryController', ['inquiryService', '$rootScope', '$scope', '$location', '$routeParams', '$q', function (inquiryService, $rootScope, $scope, $location, $routeParams, $q) {

        $scope.header = "Inquiries";
        $scope.infoheader = "Inquiry";
        $scope.addLink = "/inquiry/add";
        $scope.editLink = "/inquiry/edit";
        $scope.cancelLink = "#/users/inquiry/view";
        $scope.viewLink = "#/users/inquiry/view";

        $scope.formData = {INQUIRYID: "",USERID:"", NAME: "", EMAIL: "",MOBILE:"", MESSAGE: ""};

        /**
         * load inquiry edit
         */
        $scope.loadEdit = function () {
            $scope.getDataById();
        };

        /**
         * get all inquiry Data
         * @returns {undefined}
         */
        $scope.viewDetail = function () {
            return $q(function (resolve) {
                var getData = inquiryService.viewDetail();
                $rootScope.promise = getData.then(function (response) {
                    $scope.inquiryData = response.data.INQUIRYSDATA;
                    var getContent = inquiryService.loadContentPage();
                    getContent.then(function (response) {
                        var $div = $(response.data);
                        $("#page_content").append($div);
                        angular.element(document).injector().invoke(function ($compile) {
                            var scope = angular.element($div).scope();
                            $compile($div)(scope);
                        });
                    });
                });
            });
        };

        /**
         * get inquiry Data
         * @returns {undefined}
         */
        $scope.getDataById = function () {
            console.log($routeParams.id);
            $scope.id = $routeParams.id;
            var getData = inquiryService.getUpdateData($scope.id);
            $rootScope.promise = getData.then(function (response) {
                $scope.formData = response.data;
            });
        };


        /**
         * for delete ciity
         */
        $scope.deleteDetail = function () {
            $rootScope.deleteConfirmBox(function () {
                var response = inquiryService.deleteDetail($scope.formData.INQUIRYID);
                $rootScope.promise = response.success(function (data) {
                    if (resMsg(data)) {
                        $rootScope.changeView($scope.viewLink);
                    }
                });
            });
        };
    }]);