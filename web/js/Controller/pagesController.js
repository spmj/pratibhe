// Project : pagesController.js
// Created : 26 Jul, 2017 9:47:55 AM
// Author  : jay

angular.module('PratibheApp').controller('pagesController', ['pagesService', '$rootScope', '$scope', '$location', '$routeParams', '$q', '$window',
    function(pagesService, $rootScope, $scope, $location, $routeParams, $q, $window) {

        $scope.header = "Pages";
        $scope.infoheader = "Pages Details";
        $scope.addLink = "/pages/add";
        $scope.editLink = "/pages/edit";
        $scope.cancelLink = "/pages/view";
        $scope.viewLink = "/pages/view";

        $scope.content = "<p> this is custom directive </p>";
        $scope.formData = {PAGEID: "", PAGENAME: "", CONTENT: ""};
        /**
         * load pages add
         */
        $scope.loadAdd = function() {
//            $("#cmdStateName").focus();
//            $scope.loadCombo();
        };

//        /**
//         * load page content
//         */
//        $scope.getPageContent = function(stateid) {
//            var page = pagesService.getCity(stateid);
//            page.then(function(response) {
//                $scope.pageContent = response.data;
//            })
//        };

        /**
         * load pages edit
         */
        $scope.loadEdit = function() {
            $("#cmdPageName").focus();
            $scope.getDataById();
        };

        /**
         * get all pages Data
         * @returns {undefined}
         */
        $scope.viewDetail = function() {
            return $q(function(resolve) {
                var getData = pagesService.viewDetail();
                $rootScope.promise = getData.then(function(response) {
                    $scope.pagesData = response.data.PAGESDATA;
                    var getContent = pagesService.loadContentPage();
                    getContent.then(function(response) {
                        var $div = $(response.data);
                        $("#page_content").append($div);
                        angular.element(document).injector().invoke(function($compile) {
                            var scope = angular.element($div).scope();
                            $compile($div)(scope);
                        });
                    });
                });
            });
        };

        $scope.cancel = function() {
            $rootScope.changeView($scope.viewLink);
            $window.location.reload();
        };

        /**
         * get pages Data
         * @returns {undefined}
         */
        $scope.getDataById = function() {
            $scope.id = $routeParams.id;
            var getData = pagesService.getUpdateData($scope.id);
            $rootScope.promise = getData.then(function(response) {

                if (response.data.PAGEID) {
                    response.data.PAGEID = response.data.PAGEID.toString();
                }
                $scope.formData = response.data;
            });
        };

        /**
         * for category validation
         */
        $scope.validateData = function() {
            if (!validate_combo('cmdPageName', $scope.formData.STATEID, 'Page Name')) {
                return false;
            }
//            if (!validate_element('txtPagesName', $scope.formData.CITYNAME, 'Pages C')) {
//                return false;
//            }
            return true;
        };
//        /**
//         * for insert pages
//         */
//        $scope.insertDetail = function() {
//            if (!$scope.validateData()) {//check validation
//                return false;
//            }
//            var response = pagesService.insertDetail($scope.formData);
//            $rootScope.promise = response.success(function(data) {
//                if (resMsg(data)) {
//                    $rootScope.changeView($scope.viewLink);
//                }
//            });
//        };
        /**
         * for update pages
         */
        $scope.updateDetail = function() {
//            if (!$scope.validateData()) {//check validation
//                return false;
//            }
            console.log($scope.formData);
            var response = pagesService.updateDetail($scope.formData.PAGEID, $scope.formData);
            $rootScope.promise = response.success(function(data) {
                if (resMsg(data)) {
                    $scope.cancel();
                }
            });
        };
        /**
         * for delete ciity
         */
        $scope.deleteDetail = function() {
            $rootScope.deleteConfirmBox(function() {
                var response = pagesService.deleteDetail($scope.formData.PAGEID);
                $rootScope.promise = response.success(function(data) {
                    if (resMsg(data)) {
                        $rootScope.changeView($scope.viewLink);
                    }
                });
            });
        };
    }]);
