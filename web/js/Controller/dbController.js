// Project : homeController.js
// Created : 26 Jul, 2017 9:47:55 AM
// Author  : jay

angular.module('PratibheApp').controller('dbController', ['$rootScope', '$scope', '$location', '$routeParams', 'dbService', function ($rootScope, $scope, $location, $routeParams, dbService) {

        $scope.getDbData = function (type) {
            if (type == "ADMIN") {
                var data = dbService.getAdminDbData();
                $rootScope.promise = data.then(function (response) {
                    $scope.dbData = response.data;
                });
            }
            else if (type == "MENTOR") {
                var data = dbService.getMentorDbData();
                $rootScope.promise = data.then(function (response) {
                    $scope.dbData = response.data;
                });
            }
            else {
                var data = dbService.getStudentDbData();
                $rootScope.promise = data.then(function (response) {
                    $scope.dbData = response.data;
                });
            }
        };
    }]);