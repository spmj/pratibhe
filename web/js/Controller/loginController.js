// Project : loginController.js
// Created : 8 Jun, 2017 5:31:52 PM
// Author  : jay


angular.module('PratibheApp').controller('loginController', ['loginService', '$scope', '$rootScope', '$window', '$location', function(loginService, $scope, $rootScope, $window, $location) {
        $scope.formData = {"USERNAME": "", PWD: ""};

        $scope.createUser = function() {
            $('#signin').modal('hide');
            $("#signup").modal();
        };

        $scope.validateData = function() {
            if (!validate_element('txtUsername', $("#txtUsername").val(), 'User name')) {
                return false;
            }
            if (!validate_element('txtPassword', $("#txtPassword").val(), 'Password')) {
                return false;
            }
            return true;
        };

        $scope.signIn = function() {
            if (!$scope.validateData()) {//check validation
                return false;
            }

            //code for login
            var login = loginService.signIn($scope.formData);
            login.then(function(response) {
                if (resMsg(response.data)) {
                    window.location = "/pratibhe/";
                }
            });
        };

        $scope.checkUser = function() {
            if (!validate_element('txtEmail', $("#txtEmail").val(), 'Email')) {
                return false;
            }

            //code for login
            var check = loginService.checkUser($scope.formData);
            check.then(function(response) {
                if (resMsg(response.data)) {
                    $scope.formData["EMAIL"] = "";
                }
            });
        };

        $scope.changefgpwd = function() {
            console.log($scope.formData);
            if (!validate_element('txtNewPwd', $("#txtNewPwd").val(), 'New Password')) {
                return false;
            }
            if (!validate_element('txtConfPwd', $("#txtConfPwd").val(), 'Confirm Password')) {
                return false;
            }

            if ($scope.formData.NEWPWD !== $scope.formData.CONFPWD) {
                getMessage("Password is not matched.", 'danger');
                return false;
            }

            //code for login
            var change = loginService.changefgpwd($scope.formData);
            change.then(function(response) {
                if (resMsg(response.data)) {
                    location.href = getContextPath() + "/login";
                }
            });
        };

        function getContextPath()
        {
            return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
        }
        ;

    }]);