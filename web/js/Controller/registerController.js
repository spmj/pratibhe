/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//angular.module('PratibheApp', ['checklist-model']);
angular.module('PratibheApp').controller('registerController', ['registerService', '$rootScope', '$scope', '$location', '$q', function (registerService, $rootScope, $scope, $location, $q) {
        $scope.formData = {TYPE: "", NAME: "", ARTISTNAME: "", EMAIL: "", MOBILE: "", AADHARNO: "", GENDER: "MALE",
            OCCUPATION: "", DOB: "", ADDRESS: "", CITYID: "", STATEID: "", ABOUTME: "", ARTID: [], OTHER: "", PASSWORD: "", WEBSITE: "",
            IMAGE: "", TYPEOFWORK: "", TENUREOFWORK: "", ISACTIVE: "", HEARABOUT: []};

        $scope.ARTDATA = {};

        $scope.thumbChanged = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imgProfile')
                            .attr('src', e.target.result)
                            .width(200)
                            .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }
        };

        /**
         * for register validation
         */
        $scope.validateData = function (type) {
            if (!validate_element('txtName', $scope.formData.NAME, 'Name')) {
                return false;
            }
            if (!validate_element('txtEmail', $scope.formData.EMAIL, 'Email')) {
                return false;
            }
            if (!validate_element_regex('txtEmail', $scope.formData.EMAIL, REG_EMAIL, MSG_EMAIL + ' Email')) {
                return false;
            }

            if (!validate_element('txtDob', $scope.formData.DOB, 'Date of Birth')) {
                return false;
            }
            if (!validate_element_regex('txtDob', $scope.formData.DOB, REG_DATE, MSG_DATE + 'Date of Birth')) {
                return false;
            }

            if (!validate_element('txtAadhar', $scope.formData.AADHARNO, 'AADHAR Number')) {
                return false;
            }

            if ($("#txtAadhar").val().length < 12) {
                return getMessage("Please enter valid Aadhar Number !!");
            }

            console.log($("#cmdArt").val());
            if ($("#cmdArt").val() == null) {
                return getMessage("Please select atleast one Art !!");
            }

            if (!validate_element('txtMobile', $scope.formData.MOBILE, 'Mobile')) {
                return false;
            }

            if (!validate_element_regex('txtMobile', $scope.formData.MOBILE, REG_MOBILENO, MSG_MOBILENO + ' Mobile')) {
                return false;
            }

            if (!validate_element('cmdState', $scope.formData.STATEID, 'State')) {
                return false;
            }

            if (!validate_element('cmdCity', $scope.formData.CITYID, 'City')) {
                return false;
            }


            if (!validate_element('fileProfile', $scope.formData.IMAGE, 'Profile Picture')) {
                return false;
            }

//            if (type == 'MENTOR') {
//                if (!validate_element('txtTOW', $scope.formData.TYPEOFWORK, 'Type of work')) {
//                    return false;
//                }
//            }
//
//            if (type == 'MENTOR') {
//                if (!validate_element('txtTEOW', $scope.formData.TENUREOFWORK, 'Tenure of work')) {
//                    return false;
//                }
//            }

            if (!validate_element('txtPassword', $scope.formData.PASSWORD, 'Password')) {
                return false;
            }
            if (!validate_element('txtConfirmation', $scope.formData.REPASSWORD, 'Confirm Password')) {
                return false;
            }
            if ($scope.formData.PASSWORD != $scope.formData.REPASSWORD) {
                getMessage("Confirm Password is not matched with password.", 'danger');
                return false;
            }

            return true;
        };

        /**
         * load combo fill
         */
        $scope.getCity = function (stateid) {
            var city = registerService.getCity(stateid);
            city.then(function (response) {
                $scope.cityName = response.data;
            })
        };

        /**
         * for insert register
         */
        $scope.insertDetail = function (type) {
            if (!$scope.validateData(type)) {//check validation
                return false;
            }
            $scope.formData.TYPE = type;
            var response = registerService.insertDetail($scope.formData);
            $rootScope.promise = response.success(function (data) {
                if (resMsg(data)) {
                    location.href = getContextPath() + "/login";
//                    $rootScope.changeView($scope.viewLink);
                }
            });
        };

        function getContextPath()
        {
            return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
        }
        ;
    }]);
