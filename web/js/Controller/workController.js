// Project : artController.js
// Created : 26 Jul, 2017 9:47:55 AM
// Author  : jay

angular.module('PratibheApp').controller('workController', ['workService', '$rootScope', '$scope', '$location', '$routeParams', '$q', function(workService, $rootScope, $scope, $location, $routeParams, $q) {

        $scope.header = "Work";
        $scope.infoheader = "Work Details";

        var usl = $location.path();

        if (usl.includes("student")) {
            $scope.type = "STUDENT";
            $scope.viewLink = "student/work/view";
        }
        if (usl.includes("mentor")) {
            $scope.type = "MENTOR";
            $scope.viewLink = "mentor/work/view";
        }

        $scope.formData = {USERWORKID: "",  USERID: "", ARTID: "", FILE: "",
            WORKNAME: "",WORKFILE:"", DESCRIPTION: "", ISFETURED: ""};

        /**
         * load combo fill
         */
        $scope.loadCombo = function() {
            var allCombo = workService.getAllCombo();
            $rootScope.promise = allCombo.then(function(response) {
                $scope.comboData = response.data;
            });
        };

        /**
         * load work add
         */
        $scope.loadAdd = function() {
            $("#cmdArt").focus();
            $scope.loadCombo();
        };

        /**
         * load work edit
         */
        $scope.loadEdit = function() {
            $("#cmdArt").focus();
            $scope.getDataById();
        };


        /**
         * get all work Data
         * @returns {undefined}
         */
        $scope.viewDetail = function() {
            return $q(function(resolve) {
                var getData = workService.viewDetail();
                $rootScope.promise = getData.then(function(response) {
                    $scope.workData = response.data.WORKDATA;
                    var getContent = workService.loadContentPage($scope.type);
                    getContent.then(function(response) {
                        var $div = $(response.data);
                        $("#page_content").append($div);
                        angular.element(document).injector().invoke(function($compile) {
                            var scope = angular.element($div).scope();
                            $compile($div)(scope);
                        });
                    });
                });
            });
        };

        /**
         * get work Data
         * @returns {undefined}
         */
        $scope.getDataById = function() {
            $scope.id = $routeParams.id;
            $scope.loadCombo();
            var getData = workService.getUpdateData($scope.id);
            $rootScope.promise = getData.then(function(response) {
                if (response.data.ARTID) {
                    response.data.ARTID = response.data.ARTID.toString();
                }
                $scope.formData = response.data;
                $scope.updateSelect();
            });
        };

        $scope.updateSelect = function() {
            $('#cmdArt').val($scope.formData.ARTID).trigger('change.select2');
        };

        /**
         * for work validation
         */
        $scope.validateData = function() {
            if (!validate_combo('txtName', $scope.formData.WORKNAME, 'Name')) {
                return false;
            }
            if (!validate_combo('cmdArt', $scope.formData.ARTID, 'Art Form')) {
                return false;
            }
            if (!validate_element('txtDescription', $scope.formData.DESCRIPTION, 'Description')) {
                return false;
            }
            return true;
        };

        /**
         * for insert work
         */
        $scope.insertDetail = function() {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            var response = workService.insertDetail($scope.formData);
            $rootScope.promise = response.success(function(data) {
                if (resMsg(data)) {
                    $rootScope.changeView($scope.viewLink);
                }
            });
        };

        /**
         * for update work
         */
        $scope.updateDetail = function() {
            
            if (!$scope.validateData()) {//check validation
                return false;
            }
            var response = workService.updateDetail($scope.formData);
            $rootScope.promise = response.success(function(data) {
                if (resMsg(data)) {
                    $rootScope.changeView($scope.viewLink);
                }
            });
        };

        /**
         * for delete work
         */
        $scope.deleteDetail = function() {
            $rootScope.deleteConfirmBox(function() {
                var response = workService.deleteDetail($scope.formData.USERWORKID);
                $rootScope.promise = response.success(function(data) {
                    if (resMsg(data)) {
                        $rootScope.changeView($scope.viewLink);
                    }
                });
            })
        };
        
        
    }]);