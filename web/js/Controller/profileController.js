// Project : artController.js
// Created : 26 Jul, 2017 9:47:55 AM
// Author  : jay

angular.module('PratibheApp').controller('profileController', ['profileService', '$rootScope', '$scope', '$location', '$routeParams', '$q', function (profileService, $rootScope, $scope, $location, $routeParams, $q) {

        $scope.header = "My Profile";
        $scope.infoheader = "Profile Details";

        /**
         * load combo fill
         */
        $scope.loadCombo = function () {
            var allCombo = profileService.getAllCombo();
            $rootScope.promise = allCombo.then(function (response) {
                $scope.comboData = response.data;
            });
        };

        /**
         * load combo fill
         */
        $scope.getCity = function (stateid) {
            var city = profileService.getCity(stateid);
            city.then(function (response) {
                $scope.cityName = response.data;
            })
        };

        /**
         * load users edit
         */
        $scope.loadEdit = function () {
            $("#txtAddress").focus();
            $scope.getDataById();
        };

        /**
         * get users Data
         * @returns {undefined}
         */
        $scope.getDataById = function () {
            $scope.loadCombo();
            var getData = profileService.getProfileData();
            $rootScope.promise = getData.then(function (response) {
                $scope.getCity(response.data.STATEID);
                if (response.data.STATEID) {
                    response.data.STATEID = response.data.STATEID.toString();
                }
                if (response.data.CITYID) {
                    response.data.CITYID = response.data.CITYID.toString();
                }
                $scope.formData = response.data;
            });
        };

        /**
         * for profile validation
         */
        $scope.validateData = function () {

            return true;
        };
        
        $scope.thumbChanged = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imgProfile')
                            .attr('src', e.target.result)
                            .width(200)
                            .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }   
        };

        /**
         * for profile
         */
        $scope.updateProfile = function () {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            var response = profileService.updateProfile($scope.formData);
            $rootScope.promise = response.success(function (data) {
                if (resMsg(data)) {
                }
            });
        };
    }]);