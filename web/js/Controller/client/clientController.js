// Project : artController.js
// Created : 26 Jul, 2017 9:47:55 AM
// Author  : jay
//angular.module('PratibheApp', ['ui.bootstrap','angularjs-datetime-picker','checklist-model']);
angular.module('PratibheApp').controller('clientController', ['clientService', '$rootScope', '$scope', '$location', '$q', '$timeout',
    function(clientService, $rootScope, $scope, $location, $q, $timeout) {



        /**
         * load combo fill
         */
//        $scope.clientData = {"EVENTBNRDATA":[{"TITLE":"EVENT4","DETAILS":"asdfasdf","THUMBIMG":"5_thumb.jpg","BANNER":"http://localhost:8084/pratibhe/events/getBanner/4","STARTDATE":"10-Nov-2017","ENDDATE":"12-Mar-2020","ISAPPROVED":"Approved"},{"TITLE":"EVENT3","DETAILS":"dfdgfdg","THUMBIMG":"4_thumb.jpg","BANNER":"http://localhost:8084/pratibhe/events/getBanner/3","STARTDATE":"12-Nov-2017","ENDDATE":"12-Dec-2016","ISAPPROVED":"Approved"}],"FETWORKDATA":[]};
//        $scope.bannerlength = $scope.clientData.EVENTBNRDATA.length;

        $scope.loadClientDetail = function() {
            var allClientData = clientService.getAllClientDetails();
            $rootScope.promise = allClientData.then(function(response) {
//                $scope.clientData = response.data;
//                $scope.bannerlength = $scope.clientData.EVENTBNRDATA.length;
            });
        };

        /**
         * load clients edit
         */
        $scope.loadEdit = function() {
            $("#txtAddress").focus();
            $scope.getDataById();
        };


        /**
         * get all clients Data
         * @returns {undefined}
         */
        $scope.viewDetail = function() {
            return $q(function(resolve) {
                var getData = clientService.viewDetail();
                $rootScope.promise = getData.then(function(response) {
                    $scope.clientsData = response.data.EVENTSDATA;
                    var getContent = clientService.loadContentPage();
                    getContent.then(function(response) {
                        var $div = $(response.data);
                        $("#page_content").append($div);
                        angular.element(document).injector().invoke(function($compile) {
                            var scope = angular.element($div).scope();
                            $compile($div)(scope);
                        });
                    });
                });
            });
        };



        /**
         * get clients Data
         * @returns {undefined}
         */
        $scope.getDataById = function() {
            $scope.id = $routeParams.id;
            $scope.loadCombo();
            var getData = clientService.getUpdateData($scope.id);
            $rootScope.promise = getData.then(function(response) {
                $scope.getCity(response.data.STATEID);
                if (response.data.STATEID) {
                    response.data.STATEID = response.data.STATEID.toString();
                }
                if (response.data.CITYID) {
                    response.data.CITYID = response.data.CITYID.toString();
                }
                if (response.data.ARTID) {
                    response.data.ARTID = response.data.ARTID.toString();
                }
                if (response.data.EVENTTYPEID) {
                    response.data.EVENTTYPEID = response.data.EVENTTYPEID.toString();
                }
                $scope.formData = response.data;

                $scope.updateSelect();
            });
        };

        /**
         * get all clients Data
         * @returns {undefined}
         */
        $scope.HAPPFILDATA = {YEAR: "", MONTH: "", EVNTYPE: "", ARTID: ""};
        $scope.getFilteredHappenings = function() {
            $scope.HAPPFILDATA.LIMIT = 9;
            $scope.HAPPFILDATA.SLIMIT = 9;
            console.log("inside");
            return $q(function(resolve) {
                var getData = clientService.getFilteredHappenings($scope.HAPPFILDATA);
                $rootScope.promise = getData.then(function(response) {
                    $scope.HAPPFILDATA.currentPage = 1;
                    $scope.HAPPFILDATA.itemsPerPage = 9;
                    $scope.HAPPFILDATA.maxSize = 5;
                    $scope.HAPPFILDATA.totalItems = response.data.HAPPENINGSSDATACNT;
                    $scope.HAPPDATA = response.data.HAPPENINGSSDATA;
                });
            });
        };


        $scope.insertPartHap = function(happId) {
            var partHap = clientService.insertPartHap(happId);
            partHap.then(function(data) {
                if (data.data.responseCode == 500) {
                    getMessage(data.data.responseMessage, "danger");
                }
                if (data.data.responseCode == 104) {
                    getMessage(data.data.responseMessage, "danger");
                }
                if (data.data.responseCode == 200) {
                    getMessage(data.data.responseMessage, "success");
                }
                if (data.data.responseCode == 401) {
                    location.href = getContextPath() + "/login";
                }
                ;
            });
        };

        $scope.eventpart = {NAME: "", EMAIL: "", MOBILE: "", EVENTID: ""};
        $scope.insertPartEvent = function(eventId) {
            $scope.eventpart.EVENTID = eventId;
            if (!validate_element('txtName', $scope.eventpart.NAME, 'Name')) {
                return false;
            }
            if (!validate_element('txtEmail', $scope.eventpart.EMAIL, 'Email')) {
                return false;
            }
            if (!validate_element_regex('txtEmail', $scope.eventpart.EMAIL, REG_EMAIL, MSG_EMAIL + 'Email')) {
                return false;
            }
            if (!validate_element('txtMobile', $scope.eventpart.MOBILE, 'Mobile No')) {
                return false;
            }
            if (!validate_element_regex('txtMobile', $scope.eventpart.MOBILE, REG_MOBILENO, MSG_MOBILENO + 'Mobile No')) {
                return false;
            }
            var partEvent = clientService.insertPartEvent($scope.eventpart);
            partEvent.then(function(data) {
                if (data.data.responseCode == 104) {
                    getMessage(data.data.responseMessage, "danger");
                }
                if (data.data.responseCode == 200) {
                    $("#eventModal").modal('hide');
                    getMessage(data.data.responseMessage, "success");
                }

            });
        };


        /**
         * get all clients Data
         * @returns {undefined}
         */
        $scope.getFilterEventDetails = function() {
            $scope.HAPPFILDATA.LIMIT = 5;
            $scope.HAPPFILDATA.SLIMIT = 5;
            return $q(function(resolve) {
                var getData = clientService.getFilteredEvents($scope.HAPPFILDATA);
                $rootScope.promise = getData.then(function(response) {
                    $scope.HAPPFILDATA.currentPage = 1;
                    $scope.HAPPFILDATA.itemsPerPage = 5;
                    $scope.HAPPFILDATA.maxSize = 5;
                    $scope.HAPPFILDATA.totalItems = response.data.EVENTSDATACNT;
                    $scope.FILEVENTSDATA = response.data.EVENTSDATA;
                });
            });
        };

        //For Pagination Data Function
        $scope.getPageData = function(currentPage) {
            return $q(function(resolve) {
                var record = currentPage * 9;
                record = record - 9;
                $scope.HAPPFILDATA["LIMIT"] = record;
                $scope.HAPPFILDATA["SLIMIT"] = 0;
                var getData = clientService.getFilteredHappenings($scope.HAPPFILDATA);
                $rootScope.promise = getData.then(function(response) {
                    $scope.HAPPDATA = response.data.HAPPENINGSSDATA;
                });
            });
        };

        //For Pagination Data Function
        $scope.getEventPageData = function(currentPage) {
            return $q(function(resolve) {
                var record = currentPage * 5;
                record = record - 5;
                $scope.HAPPFILDATA["LIMIT"] = record;
                $scope.HAPPFILDATA["SLIMIT"] = 0;
                var getData = clientService.getFilteredEvents($scope.HAPPFILDATA);
                $rootScope.promise = getData.then(function(response) {
                    $scope.FILEVENTSDATA = response.data.EVENTSDATA;
                });
            });
        };


        function getContextPath()
        {
            return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
        }
        ;

        $scope.ARTISTSDATA = {LIMIT: 18, SLIMIT: 18};
        $scope.getArtist = function() {
            return $q(function(resolve) {
                var getData = clientService.getArtist($scope.ARTISTSDATA);
                $rootScope.promise = getData.then(function(response) {
                    $scope.ARTISTSDATA.currentPage = 1;
                    $scope.ARTISTSDATA.itemsPerPage = 18;
                    $scope.ARTISTSDATA.maxSize = 5;
                    $scope.ARTISTSDATA.totalItems = response.data.ARTISTSDATACNT;
                    $scope.ARTISTDATAS = response.data.ARTISTSDATA;
                });
            });
        };

        //For Pagination Data Function
        $scope.getArtPageData = function(currentPage) {
            return $q(function(resolve) {
                var record = currentPage * 18;
                record = record - 18;
                $scope.ARTISTSDATA["LIMIT"] = record;
                $scope.ARTISTSDATA["SLIMIT"] = 0;
                var getData = clientService.getArtist($scope.ARTISTSDATA);
                $rootScope.promise = getData.then(function(response) {
                    $scope.ARTISTDATAS = response.data.ARTISTSDATA;
                });
            });
        };

        $scope.artistInq = {NAME: "", EMAIL: "", MOBILE: "", INQUIRYID: "", MESSAGE: "", USERID: ""};
        $scope.insertArtistInquiry = function(inquiryId) {
            console.log("in inq");
            $scope.artistInq.USERID = inquiryId;
            if (!validate_element('txtName', $scope.artistInq.NAME, 'Name')) {
                return false;
            }
            if (!validate_element('txtEmail', $scope.artistInq.EMAIL, 'Email')) {
                return false;
            }
            if (!validate_element_regex('txtEmail', $scope.artistInq.EMAIL, REG_EMAIL, MSG_EMAIL + 'Email')) {
                return false;
            }

            if (!validate_element_regex('txtMobile', $scope.artistInq.MOBILE, REG_MOBILENO, MSG_MOBILENO + 'Mobile No')) {
                return false;
            }
            if (!validate_element('txtMessage', $scope.artistInq.MESSAGE, 'Message')) {
                return false;
            }
            var artInq = clientService.insertArtistInquiry($scope.artistInq);
            artInq.then(function(data) {
                if (data.data.responseCode == 104) {
                    getMessage(data.data.responseMessage, "danger");
                }
                if (data.data.responseCode == 200) {
//                    $('#btnMdlClose').trigger('click');
//                    $("#inqModal").modal('hide');
                    getMessage(data.data.responseMessage, "success");
                }

            });
        };

        $scope.GALLERYDATA = {USERID: "", ARTID: ""};






        $scope.getData = function() {
            if ($scope.GALLERYDATA.ARTID == '2') {
                $scope.getGalleryMusicData();
                $scope.GALLERYDATAS = {};
                $scope.GALLERYDATAS.length = 0;

            } else if($scope.GALLERYDATA.ARTID != ""){
                $scope.getGalleryData();
                $scope.GALLERYMUSICDATAS = {};
                $scope.GALLERYMUSICDATAS.length = 0;
            }else{
                $scope.getGalleryMusicData();
                $scope.getGalleryData();
            }
        };

        $scope.getGalleryData = function() {
            $scope.GALLERYDATA.LIMIT = 9;
            $scope.GALLERYDATA.SLIMIT = 9;
            return $q(function(resolve) {
                var getData = clientService.getGalleryData($scope.GALLERYDATA);
                $rootScope.promise = getData.then(function(response) {
                    $scope.GALLERYDATA.currentPage = 1;
                    $scope.GALLERYDATA.itemsPerPage = 9;
                    $scope.GALLERYDATA.maxSize = 5;
                    $scope.GALLERYDATA.totalItems = response.data.GALLERYDATACNT;
                    $scope.GALLERYDATAS = response.data.GALLERYDATA;
                });
            });
        };

        $scope.GALLERYMUSICDATA = {USERID: "", ARTID: ""};
        $scope.getGalleryMusicData = function() {
            //gallary music data
            $scope.GALLERYMUSICDATA.LIMIT = 4;
            $scope.GALLERYMUSICDATA.SLIMIT = 4;
            $scope.GALLERYMUSICDATA.USERID = $scope.GALLERYDATA.USERID;
            $scope.GALLERYMUSICDATA.ARTID = $scope.GALLERYDATA.ARTID;
            return $q(function(resolve) {
                var getData = clientService.getGalleryMusicData($scope.GALLERYMUSICDATA);
                $rootScope.promise = getData.then(function(response) {
                    $scope.GALLERYMUSICDATA.currentPage = 1;
                    $scope.GALLERYMUSICDATA.itemsPerPage = 4;
                    $scope.GALLERYMUSICDATA.maxSize = 5;
                    $scope.GALLERYMUSICDATA.totalItems = response.data.GALLERYMUSICDATACNT;
                    $scope.GALLERYMUSICDATAS = response.data.GALLERYMUSICDATA;
                });
            });
        };

        //For Pagination Data Function
        $scope.getGalleryPageData = function(currentPage) {
            return $q(function(resolve) {
                var record = currentPage * 9;
                record = record - 9;
                $scope.GALLERYDATA["LIMIT"] = record;
                $scope.GALLERYDATA["SLIMIT"] = 0;
                var getData = clientService.getGalleryData($scope.GALLERYDATA);
                $rootScope.promise = getData.then(function(response) {
                    $scope.GALLERYDATAS = response.data.GALLERYDATA;
                });
            });
        };

        //For Pagination Data Function
        $scope.getGalleryPageData = function(currentPage) {
            return $q(function(resolve) {
                var record = currentPage * 4;
                record = record - 4;
                $scope.GALLERYMUSICDATA["LIMIT"] = record;
                $scope.GALLERYMUSICDATA["SLIMIT"] = 0;
                var getData = clientService.getGalleryMusicData($scope.GALLERYMUSICDATA);
                $rootScope.promise = getData.then(function(response) {
                    $scope.GALLERYMUSICDATAS = response.data.GALLERYMUSICDATA;
                });
            });
        };

    }]);
