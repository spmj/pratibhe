// Project : artController.js
// Created : 26 Jul, 2017 9:47:55 AM
// Author  : jay

angular.module('PratibheApp').controller('mappingController', ['mappingService', '$rootScope', '$scope', '$location', '$routeParams', '$q', function (mappingService, $rootScope, $scope, $location, $routeParams, $q) {

        /**
         * get all mapping Data
         * @returns {undefined}
         */
        $scope.viewDetail = function () {
            return $q(function (resolve) {
                var getData = mappingService.viewDetail();
                $rootScope.promise = getData.then(function (response) {
                    $scope.temp = response.data.MAPPINGDATA;
                    for (var i = 0; i < $scope.temp.length; i++) {
                        if ($scope.temp[i].MENTORID == 1) {
                            $scope.temp[i].MENTORID = true;
                        } else {
                            $scope.temp[i].MENTORID = false;
                        }
                    }
                    $scope.mappingData = $scope.temp;
                    var getContent = mappingService.loadContentPage();
                    getContent.then(function (response) {
                        var $div = $(response.data);
                        $("#page_content").append($div);
                        angular.element(document).injector().invoke(function ($compile) {
                            var scope = angular.element($div).scope();
                            $compile($div)(scope);
                        });
                    });
                });
            });
        };


        /**
         * for art validation
         */
        $scope.validateData = function () {
//            if (!validate_element('txtArtName', $scope.formData.ARTNAME, 'Art Name')) {
//                return false;
//            }
            return true;
        };

        /**
         * for update art
         */
        $scope.updateDetail = function () {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            $scope.studId1 = "";
            $scope.studId0 = "";
            for (var i = 0; i < $scope.mappingData.length; i++)
            {
                if ($scope.mappingData[i].MENTORID == true)
                {
                    $scope.studId1 += $scope.mappingData[i].USERID + ",";
                } else {
                    $scope.studId0 += $scope.mappingData[i].USERID + ",";
                }
            }
            $scope.studId1 = $scope.studId1.substring(0, $scope.studId1.length - 1);
            $scope.studId0 = $scope.studId0.substring(0, $scope.studId0.length - 1);
            $scope.json = {STUDID1: $scope.studId1, STUDID0: $scope.studId0};
            var response = mappingService.updateDetail($scope.json);
            $rootScope.promise = response.success(function (data) {
                if (resMsg(data)) {
                    $rootScope.changeView($scope.viewLink);
                }
            });
        };

    }]);