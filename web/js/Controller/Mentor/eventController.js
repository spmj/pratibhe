// Project : artController.js
// Created : 26 Jul, 2017 9:47:55 AM
// Author  : jay

angular.module('PratibheApp').controller('eventController', ['eventService', '$rootScope', '$scope', '$location', '$routeParams', '$q', function (eventService, $rootScope, $scope, $location, $routeParams, $q) {

        $scope.header = "Event";
        $scope.infoheader = "Event Details";
        $scope.addLink = "mentor/event/add";
        $scope.editLink = "mentor/event/edit";
        $scope.cancelLink = "mentor/event/view";
        $scope.viewLink = "mentor/event/view";

        $scope.formData = {EVENTID: "", TITLE: "", SUMMERY: "", DETAILS: "", ADDRESS: "", STATEID: "", CITYID: "",
            ARTID: "", EVENTTYPEID: "", THUMBIMG: "", BANNERIMG: "", STARTDATE: "", ENDDATE: "", CONTACTNAME: "",
            CONTACTEMAIL: ""};

        /**
         * load combo fill
         */
        $scope.loadCombo = function () {
            var allCombo = eventService.getAllCombo();
            $rootScope.promise = allCombo.then(function (response) {
                $scope.comboData = response.data;
            });
        };

        /**
         * load combo fill
         */
        $scope.getCity = function (stateid) {
            var city = eventService.getCity(stateid);
            city.then(function (response) {
                $scope.cityName = response.data;
            })
        };

        /**
         * load events add
         */
        $scope.loadAdd = function () {
            $("#txtAddress").focus();
            $scope.loadCombo();
        };

        /**
         * load events edit
         */
        $scope.loadEdit = function () {
            $("#txtAddress").focus();
            $scope.getDataById();
        };


        /**
         * get all events Data
         * @returns {undefined}
         */
        $scope.viewDetail = function () {
            return $q(function (resolve) {
                var getData = eventService.viewDetail();
                $rootScope.promise = getData.then(function (response) {
                    $scope.eventsData = response.data.EVENTSDATA;
                    var getContent = eventService.loadContentPage();
                    getContent.then(function (response) {
                        var $div = $(response.data);
                        $("#page_content").append($div);
                        angular.element(document).injector().invoke(function ($compile) {
                            var scope = angular.element($div).scope();
                            $compile($div)(scope);
                        });
                    });
                });
            });
        };

        $scope.thumbChanged = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imgThumbView')
                            .attr('src', e.target.result)
                            .width(200)
                            .height(100);
                };
                reader.readAsDataURL(input.files[0]);
            }
        };

        $scope.bannerChanged = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imgBannerView')
                            .attr('src', e.target.result)
                            .width(400)
                            .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }
        };

        /**
         * get events Data
         * @returns {undefined}
         */
        $scope.getDataById = function () {
            $scope.id = $routeParams.id;
            $scope.loadCombo();
            var getData = eventService.getUpdateData($scope.id);
            $rootScope.promise = getData.then(function (response) {
                $scope.getCity(response.data.STATEID);
                if (response.data.STATEID) {
                    response.data.STATEID = response.data.STATEID.toString();
                }
                if (response.data.CITYID) {
                    response.data.CITYID = response.data.CITYID.toString();
                }
                if (response.data.ARTID) {
                    response.data.ARTID = response.data.ARTID.toString();
                }
                if (response.data.EVENTTYPEID) {
                    response.data.EVENTTYPEID = response.data.EVENTTYPEID.toString();
                }
                $scope.formData = response.data;

                $scope.updateSelect();
            });
        };

        $scope.updateSelect = function () {
            $('#cmdState').val($scope.formData.STATEID).trigger('change.select2');
            $('#cmdCity').val($scope.formData.CITYID).trigger('change.select2');
            $('#cmdArt').val($scope.formData.ARTID).trigger('change.select2');
            $('#cmdEventType').val($scope.formData.EVENTTYPEID).trigger('change.select2');
        };

        /**
         * for events validation
         */
        $scope.validateData = function () {
            if (!validate_element('txtTitle', $scope.formData.TITLE, 'Title')) {
                return false;
            }
            if (!validate_element('txtSummery', $scope.formData.SUMMERY, 'Summery')) {
                return false;
            }
            if (!validate_combo('cmdState', $scope.formData.STATEID, 'State')) {
                return false;
            }
            if (!validate_combo('cmdCity', $scope.formData.CITYID, 'City')) {
                return false;
            }
            if (!validate_combo('cmdArt', $scope.formData.ARTID, 'Art Form')) {
                return false;
            }
            if (!validate_combo('cmdEventType', $scope.formData.EVENTTYPEID, 'Event Type')) {
                return false;
            }
            if (!validate_file('fileThumb', $scope.formData.THUMBIMG, 'Thumbnail')) {
                return false;
            }
            if (!validate_file('fileBanner', $scope.formData.BANNERIMG, 'Long Banner Image')) {
                return false;
            }
            if (!validate_element('txtStartDate', $scope.formData.STARTDATE, 'Start Date')) {
                return false;
            }
            if (!validate_element_regex('txtStartDate', $scope.formData.STARTDATE, REG_DATE, MSG_DATE + 'Start Date')) {
                return false;
            }
            if (!validate_element('txtEndDate', $scope.formData.ENDDATE, 'End Date')) {
                return false;
            }
            if (!validate_element_regex('txtEndDate', $scope.formData.ENDDATE, REG_DATE, MSG_DATE + 'End Date')) {
                return false;
            }

            var fromdate = $rootScope.stringToDate($scope.formData.STARTDATE, "dd-mmm-yyyy", "-");
            var todate = $rootScope.stringToDate($scope.formData.ENDDATE, "dd-mmm-yyyy", "-");

            if (todate < fromdate) {
                getMessage("Start date must be less than End date", 'danger');
                return false;
            }
            return true;
        };

        /**
         * for insert events
         */
        $scope.insertDetail = function () {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            var response = eventService.insertDetail($scope.formData);
            $rootScope.promise = response.success(function (data) {
                if (resMsg(data)) {
                    $rootScope.changeView($scope.viewLink);
                }
            });
        };

        /**
         * for update events
         */
        $scope.updateDetail = function () {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            var response = eventService.updateDetail($scope.formData);
            $rootScope.promise = response.success(function (data) {
                if (resMsg(data)) {
                    $rootScope.changeView($scope.viewLink);
                }
            });
        };

        /**
         * for delete events
         */
        $scope.deleteDetail = function () {
            $rootScope.deleteConfirmBox(function () {
                var response = eventService.deleteDetail($scope.formData.EVENTID);
                $rootScope.promise = response.success(function (data) {
                    if (resMsg(data)) {
                        $rootScope.changeView($scope.viewLink);
                    }
                });
            })
        };

        /**
         * for delete events
         */
        $scope.getPartcipateData = function (id) {
            var participateData = eventService.getPartcipateData(id);
            participateData.then(function (response) {
                $scope.participateData = response.data.PARTCIPATEDATA;
            })
        };
    }]);