// Project : artController.js
// Created : 26 Jul, 2017 9:47:55 AM
// Author  : jay

angular.module('PratibheApp').controller('studentController', ['studentService', '$rootScope', '$scope', '$location', '$routeParams', '$q', function (studentService, $rootScope, $scope, $location, $routeParams, $q) {


        $scope.infoheader = "Student Details";
        $scope.addLink = "mentor/student/add";
        $scope.editLink = "mentor/student/edit";
        $scope.cancelLink = "mentor/student/view";
        $scope.viewLink="mentor/student/view";

        $scope.formData = {USERID: "", TYPE: "", NAME: "", ARTISTNAME: "",
            EMAIL: "", MOBILE: "", AADHARNO: "", GENDER: "", OCCUPATION: "",
            DOB: "", ADDRESS: "", CITY: "", STATE: "", ABOUTME: "", ARTNAME: "",
            OTHER: "", PASSWORD: "", IMAGE: "", ADDTIME: "", EDITTIME: "",
            TYPEOFWORK: "", TENUREOFWORK: "", MENTORID: "", ISACTIVE: "",
            MENTORNAME: "", ISAPPROVED: "", CITYNAME: "", STATENAME: ""};

        /**
         * load users edit
         */
        $scope.loadEdit = function () {
            $("#txtAddress").focus();
            $scope.getDataById();
        };

        
        /**
         * get all users Data
         * @returns {undefined}
         */
        $scope.viewDetail = function () {
            return $q(function (resolve) {
                var getData = studentService.viewDetail();
                $rootScope.promise = getData.then(function (response) {
                    $scope.usersData = response.data.USERSDATA;
                    var getContent = studentService.loadContentPage();
                    getContent.then(function (response) {
                        var $div = $(response.data);
                        $("#page_content").append($div);
                        angular.element(document).injector().invoke(function ($compile) {
                            var scope = angular.element($div).scope();
                            $compile($div)(scope);
                        });
                    });
                });
            });
        };

        /**
         * get users Data
         * @returns {undefined}
         */
        $scope.getDataById = function () {
            $scope.id = $routeParams.id;
            var getData = studentService.getUpdateData($scope.id);
            $rootScope.promise = getData.then(function (response) {
                $scope.formData = response.data;
                console.log($scope.formData);
            });
        };

        /**
         * for users validation
         */
        $scope.validateData = function () {
            return true;
        };


        /**
         * for update users
         */
        $scope.updateDetail = function () {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            var response = studentService.updateDetail($scope.formData.USERID, $scope.formData);
            $rootScope.promise = response.success(function (data) {
                if (resMsg(data)) {
                    $rootScope.changeView($scope.viewLink);
                }
            });
        };

        /**
         * for delete users
         */
        $scope.deleteDetail = function () {
            $rootScope.deleteConfirmBox(function () {
                var response = studentService.deleteDetail($scope.formData.EVENTID);
                $rootScope.promise = response.success(function (data) {
                    if (resMsg(data)) {
                        $rootScope.changeView($scope.viewLink);
                    }
                });
            });
        };
    }]);