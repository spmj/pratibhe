// Project : artController.js
// Created : 26 Jul, 2017 9:47:55 AM
// Author  : jay

angular.module('PratibheApp').controller('mentorHappeningsController', ['mentorHappningService', '$rootScope', '$scope', '$location', '$routeParams', '$q', function (mentorHappningService, $rootScope, $scope, $location, $routeParams, $q) {

        $scope.header = "Happenings";
        $scope.infoheader = "Happenings Details";
        $scope.addLink = "mentorhappenings/add";
        $scope.editLink = "mentorhappenings/edit";
        $scope.cancelLink = "mentorhappenings/view";
        $scope.viewLink = "mentorhappenings/view";
        
       

        $scope.formData = {EVENTID: "", TITLE: "", SUMMERY: "", DETAILS: "", ADDRESS: "", STATEID: "", CITYID: "",
            ARTID: "", EVENTTYPEID: "", THUMBIMG: "", BANNERIMG: "", STARTDATE: "", ENDDATE: "", CONTACTNAME: "",
            CONTACTEMAIL: "", NOOFSLOT: ""};



        /**
         * load happenings edit
         */
        $scope.loadEdit = function () {
            $("#txtAddress").focus();
            $scope.getDataById();
        };


        /**
         * get all happenings Data
         * @returns {undefined}
         */
        $scope.viewDetail = function () {
            return $q(function (resolve) {
                var getData = mentorHappningService.viewDetail();
                $rootScope.promise = getData.then(function (response) {
                    $scope.happeningsData = response.data.HAPPENINGSSDATA;
                    var getContent = mentorHappningService.loadContentPage();
                    getContent.then(function (response) {
                        var $div = $(response.data);
                        $("#page_content").append($div);
                        angular.element(document).injector().invoke(function ($compile) {
                            var scope = angular.element($div).scope();
                            $compile($div)(scope);
                        });
                    });
                });
            });
        };

        /**
         * get happenings Data
         * @returns {undefined}
         */
        $scope.getDataById = function () {
            $scope.id = $routeParams.id;
            var getData = mentorHappningService.getUpdateData($scope.id);
            $rootScope.promise = getData.then(function (response) {
                $scope.formData = response.data;
            });
        };


        /**
         * for happenings validation
         */
        $scope.validateData = function () {
            return true;
        };

        /**
         * for delete happenings
         */
        $scope.deleteDetail = function () {
            $rootScope.deleteConfirmBox(function () {
                var response = mentorHappningService.deleteDetail($scope.formData.HAPPENINGSID);
                $rootScope.promise = response.success(function (data) {
                    if (resMsg(data)) {
                        $rootScope.changeView($scope.viewLink);
                    }
                });
            });
        };
    }]);