// Project : eventtypeController.js
// Created : 26 Jul, 2017 9:47:55 AM
// Author  : jay

angular.module('PratibheApp').controller('eventtypeController', ['eventtypeService', '$rootScope', '$scope', '$location', '$routeParams', '$q', function (eventtypeService, $rootScope, $scope, $location, $routeParams, $q) {

        $scope.header = "State";
        $scope.infoheader = "State Details";
        $scope.addLink = "/eventtype/add";
        $scope.editLink = "/eventtype/edit";
        $scope.cancelLink = "/eventtype/view";
        $scope.viewLink = "/eventtype/view";

        $scope.formData = {EVENTTYPEID: "", EVENTTYPENAME: ""};

        /**
         * load eventtype add
         */
        $scope.loadAdd = function () {
            $("#txtEventTypeName").focus();
        };

        /**
         * load eventtype edit
         */
        $scope.loadEdit = function () {
            $("#txtEventTypeName").focus();
            $scope.getDataById();
        };


        /**
         * get all eventtype Data
         * @returns {undefined}
         */
        $scope.viewDetail = function () {
            return $q(function (resolve) {
                var getData = eventtypeService.viewDetail();
                $rootScope.promise = getData.then(function (response) {
                    $scope.eventTypeData = response.data.EVENTTYPEDATA;
                    var getContent = eventtypeService.loadContentPage();
                    getContent.then(function (response) {
                        var $div = $(response.data);
                        $("#page_content").append($div);
                        angular.element(document).injector().invoke(function ($compile) {
                            var scope = angular.element($div).scope();
                            $compile($div)(scope);
                        });
                    });
                });
            });
        };

        /**
         * get eventtype Data
         * @returns {undefined}
         */
        $scope.getDataById = function () {
            $scope.id = $routeParams.id;
            var getData = eventtypeService.getUpdateData($scope.id);
            $rootScope.promise = getData.then(function (response) {
                $scope.formData = response.data;
            });
        };

        /**
         * for eventtype validation
         */
        $scope.validateData = function () {
            if (!validate_element('txtEventTypeName', $scope.formData.EVENTTYPENAME, 'Event Type Name')) {
                return false;
            }
            return true;
        };

        /**
         * for insert eventtype
         */
        $scope.insertDetail = function () {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            var response = eventtypeService.insertDetail($scope.formData);
            $rootScope.promise = response.success(function (data) {
                if (resMsg(data)) {
                    $rootScope.changeView($scope.viewLink);
                }
            });
        };

        /**
         * for update eventtype
         */
        $scope.updateDetail = function () {
            if (!$scope.validateData()) {//check validation
                return false;
            }
            var response = eventtypeService.updateDetail($scope.formData.EVENTTYPEID, $scope.formData);
            $rootScope.promise = response.success(function (data) {
                if (resMsg(data)) {
                    $rootScope.changeView($scope.viewLink);
                }
            });
        };

        /**
         * for delete eventtype
         */
        $scope.deleteDetail = function () {
            $rootScope.deleteConfirmBox(function () {
                var response = eventtypeService.deleteDetail($scope.formData.EVENTTYPEID);
                $rootScope.promise = response.success(function (data) {
                    if (resMsg(data)) {
                        $rootScope.changeView($scope.viewLink);
                    }
                });
            })
        };
    }]);