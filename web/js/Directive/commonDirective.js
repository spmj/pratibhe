// Project : ecomadmin commonDirective.js
// Created : 9 Sep, 2017 12:03:13 PM
// Author  : Sandip Kotadiya

angular.module('PratibheApp').directive("fileread", [function() {
        return {
            scope: {
                fileread: "=",
                filesize: "=",
                filename: "=",
                fileext: "="
            },
            link: function(scope, element, attributes) {
                element.bind("change", function(changeEvent) {
                    var reader = new FileReader();
                    reader.onload = function(loadEvent) {
                        scope.$apply(function() {
                            scope.fileread = loadEvent.target.result;//get base64
                            scope.filesize = changeEvent.target.files[0].size;//get filesize
                            scope.filename = changeEvent.target.files[0].name;//get filename

                            var filename_length = scope.filename.length;
                            var pos = scope.filename.lastIndexOf('.') + 1;
                            var ext = scope.filename.substring(pos, filename_length);
                            scope.fileext = ext.toLowerCase();

                            console.log(scope.fileext);
                        });
                    };
                    reader.readAsDataURL(changeEvent.target.files[0]);
                });
            }
        };
    }]);

//http://f1angular.com/angular-js/file-upload-using-angularjs-and-spring-mvc-example/
angular.module('PratibheApp').directive('fileModel', ['$parse', function($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var isMultiple = attrs.multiple;
                var modelSetter = model.assign;

                element.bind('change', function() {
                    scope.$apply(function() {
                        if (isMultiple) {
                            modelSetter(scope, element[0].files);
                        } else {
                            modelSetter(scope, element[0].files[0]);
                        }
                    });
                });
            }
        };
    }]);

angular.module('PratibheApp').directive('catFile', ['$parse', function($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.catFile);
                var isMultiple = attrs.multiple;
                var modelSetter = model.assign;

                element.bind('change', function() {
                    scope.$apply(function() {
                        if (isMultiple) {
                            modelSetter(scope, element[0].files);
                        } else {
                            modelSetter(scope, element[0].files[0]);
                        }
                    });
                });
            }
        };
    }]);

angular.module('PratibheApp').directive('fileDropzone', function() {
    return {
        restrict: 'A',
        scope: {
            file: '=',
            fileName: '='
        },
        link: function(scope, element, attrs) {
            var checkSize,
                    isTypeValid,
                    processDragOverOrEnter,
                    validMimeTypes;

            processDragOverOrEnter = function(event) {
                if (event != null) {
                    event.preventDefault();
                }
                event.dataTransfer.effectAllowed = 'copy';
                return false;
            };

            validMimeTypes = attrs.fileDropzone;

            checkSize = function(size) {
                var _ref;
                if (((_ref = attrs.maxFileSize) === (void 0) || _ref === '') || (size / 1024) / 1024 < attrs.maxFileSize) {
                    return true;
                } else {
                    alert("File must be smaller than " + attrs.maxFileSize + " MB");
                    return false;
                }
            };

            isTypeValid = function(type) {
                if ((validMimeTypes === (void 0) || validMimeTypes === '') || validMimeTypes.indexOf(type) > -1) {
                    return true;
                } else {
                    alert("Invalid file type.  File must be one of following types " + validMimeTypes);
                    return false;
                }
            };

            element.bind('dragover', processDragOverOrEnter);
            element.bind('dragenter', processDragOverOrEnter);

            return element.bind('drop', function(event) {
                var file, name, reader, size, type;
                if (event != null) {
                    event.preventDefault();
                }
                reader = new FileReader();
                reader.onload = function(evt) {
                    if (checkSize(size) && isTypeValid(type)) {
                        return scope.$apply(function() {
                            scope.file = evt.target.result;
                            if (angular.isString(scope.fileName)) {
                                return scope.fileName = name;
                            }
                        });
                    }
                };
                file = event.dataTransfer.files[0];
                name = file.name;
                type = file.type;
                size = file.size;
                reader.readAsDataURL(file);
                return false;
            });
        }
    };
})

//angular.module('PratibheApp').directive('ckEditor', ['$parse', function($parse) {
//        return {
//            require: '?ngModel',
//            link: function(scope, elm, attr, ngModel) {
//                //http://ckeditor.com/apps/ckeditor/4.4.0/samples/plugins/toolbar/toolbar.html
//
//                var toolBarSetting;
//                if (attr.editor == 'minimal') {
//                    toolBarSetting = {
//                        toolbar: [
//                            {name: 'document', items: ['Source', '-', 'NewPage', 'Preview', '-', 'Templates']}, // Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
//                            ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'], // Defines toolbar group without name.
//                            '/', // Line break - next group will be placed in new line.
//                            {name: 'basicstyles', items: ['Bold', 'Italic']}
//                        ]
//                    }, {
//                        toolbarGroups: [
//                            {name: 'document', groups: ['mode', 'document']}, // Displays document group with its two subgroups.
//                            {name: 'clipboard', groups: ['clipboard', 'undo']}, // Group's name will be used to create voice label.
//                            '/', // Line break - next group will be placed in new line.
//                            {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
//                            {name: 'links'}
//                        ]
//                    };
//                } else if (attr.editor == 'maximum') {
//                    toolBarSetting = {
//                        toolbar: [
//                            {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat']},
//                            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']},
//                            {name: 'styles', items: ['Styles', 'Format']},
//                            {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
//                            {name: 'editing', groups: ['find', 'selection', 'spellchecker'], items: ['Scayt']},
//                            {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
//                            {name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'SpecialChar']},
//                            {name: 'tools', items: ['Maximize']},
//                            {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source']},
//                            {name: 'others', items: ['-']}
//                        ]   
//                    }, {
//                        toolbarGroups: [
//                            {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
//                            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi']},
//                            {name: 'styles'},
//                            {name: 'colors'},
//                            {name: 'clipboard', groups: ['clipboard', 'undo']},
//                            {name: 'editing', groups: ['find', 'selection', 'spellchecker']},
//                            {name: 'links'},
//                            {name: 'insert'},
//                            {name: 'forms'},
//                            {name: 'tools'},
//                            {name: 'document', groups: ['mode', 'document', 'doctools']},
//                            {name: 'others'}
//
//                        ]
//                    };
//
//                } else {
//                    toolBarSetting = null;
//                }
//
//                var ck = CKEDITOR.replace(elm[0], toolBarSetting);
//
////                CKEDITOR.instances.editor.readOnly(attr.ndisable);
////                CKEDITOR.instances.editor1.readOnly(false);
//                CKEDITOR.config.readOnly = attr.ndisable;
////                if (attr.isEdit == false) {
////                    alert(attr.isDisable);
////                    CKEDITOR.config.readOnly = false;
////                } else {
////                    alert(attr.isDisable);
////                    CKEDITOR.config.readOnly = true;
////                }
//
//                if (!ngModel)
//                    return;
//                ck.on('instanceReady', function() {
//                    ck.setData(ngModel.$viewValue);
//                });
//                function updateModel() {
//                    scope.$apply(function() {
//                        ngModel.$setViewValue(ck.getData());
//                    });
//                }
//                ck.on('change', updateModel);
//                ck.on('key', updateModel);
//                ck.on('dataReady', updateModel);
//
//                ngModel.$render = function(value) {
//                    ck.setData(ngModel.$viewValue);
//                };
//            }
//        }
//        ;
//    }]);
//


//https://naveensingh.net/how-to-use-ckeditor-in-angularjs-with-custom-directive/
angular.module('PratibheApp').directive('ckEditor', function() {
    return {
        require: '?ngModel',
        link: function(scope, elm, attr, ngModel) {
            var ck = CKEDITOR.replace(elm[0]);
            if (!ngModel)
                return;
            ck.on('instanceReady', function() {
                ck.setData(ngModel.$viewValue);
            });
            function updateModel() {
                scope.$apply(function() {
                    ngModel.$setViewValue(ck.getData());
                });
            }
            ck.on('change', updateModel);
            ck.on('key', updateModel);
            ck.on('dataReady', updateModel);

            ngModel.$render = function(value) {
                ck.setData(ngModel.$viewValue);
            };
        }
    };
});
//
