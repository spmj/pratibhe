// Project : bzcrm approot.js
// Created : 10 Dec, 2016 11:23:54 AM
// Author  : Sandip Kotadiya

angular.module('PratibheApp').run(["$rootScope", "$route", "$http", "$location", "commonService", "$window", "$routeParams"
            , function($rootScope, $route, $http, $location, commonService, $window, $routeParams) {
                $http.get("js/data/appFiles.json?t=" + (new Date()).getTime()).success(function(response) {
                    $ocLazyLoadReference.config(response);
                });

                $rootScope.promise = $http.get("js/data/routes.json?t=" + (new Date()).getTime()).success(function(data) {
                    var iLoop = 0, currentRoute;

                    for (iLoop = 0; iLoop < data.roots.length; iLoop++) {
                        currentRoute = data.roots[iLoop];
                        if (currentRoute.Controller == "") {
                            var routeName = "/" + currentRoute.KeyName;
                            $routeProviderReference.when(routeName, {
                                templateUrl: currentRoute.PageUrls,
                                data: {
                                    private: false
                                }
                            });
                        } else {
                            var routeName = "/" + currentRoute.KeyName;
                            $routeProviderReference.when(routeName, {
                                //jsp
                                templateUrl: currentRoute.PageUrls,
                                controller: currentRoute.Controller,
                                //js
                                resolve: eval(currentRoute.resolveTmp),
                                data: {
                                    private: false
                                }
                            });
                        }
                    }
                    $route.reload();
                });

                $rootScope.dhxDateFormate = "dd-MMM-yyyy"; //dhx date fromate

                $rootScope.back = function() {
                    var prevUrl = history.length > 1 ? history.splice(-2)[0] : "/";
                    $location.path(prevUrl);
                };

                $rootScope.logout = function() {
                    var getData = commonService.getLogOut();
                    $rootScope.promise = getData.then(function(response) {
                        if (response) {
                            window.location = "/pratibhe";
                        }
                    });
                };

                $rootScope.deleteConfirmBox = function(successCallBack) {
                    bootbox.confirm({
                        title: 'Confirm Delete',
                        message: MSG_DELETE,
                        closeButton: false,
                        buttons: {
                            cancel: {
                                label: 'Cancel',
                                className: 'btn-default'
                            },
                            confirm: {
                                label: 'Delete',
                                className: 'btn-danger'
                            }
                        },
                        callback: function(result) {
                            if (result) {
                                successCallBack();
                            }
                        }
                    });
                };

                $rootScope.loadDefault = function(type) {
                    if ($location.path() == "") {
                        if (type == 'ADMIN') {
                            $location.path("db"); // path not hash
                        }
                        if (type == 'MENTOR') {
                            $location.path("mentor/db"); // path not hash
                        }
                        if (type == 'STUDENT') {
                            $location.path("student/db"); // path not hash
                        }
                    }
                };

                $rootScope.loadProfile = function() {
                    $location.path("profile");
                };

                $rootScope.loadSignin = function() {
                    $location.path("signin");
                };

                $rootScope.changeView = function(view) {
                    $location.path(view); // path not hash 

                };

                $rootScope.profileView = function(type,id) {
                    if (type == 'MENTOR') {
                        $location.path('mentors/edit/' + id); // path not hash 
                    } else {
                        $location.path('students/edit/' + id); // path not hash 
                    }
                };



                $rootScope.dataTableOpt = {
                    //custom datatable options 
                    // or load data through ajax call also
                    "aLengthMenu": [[10, 20, 30, 40, 50, 100, -1], [10, 20, 30, 40, 50, 100, 'All']],
                };

                $rootScope.stringToDate = function(_date, _format, _delimiter)
                {
                    var formatLowerCase = _format;
                    var formatItems = formatLowerCase.split(_delimiter);
                    var dateItems = _date.split(_delimiter);
                    var monthIndex = formatItems.indexOf("mmm");
                    var dayIndex = formatItems.indexOf("dd");
                    var yearIndex = formatItems.indexOf("yyyy");
                    var month;
                    if (dateItems[monthIndex] === "Jan") {
                        month = 1;
                    }
                    if (dateItems[monthIndex] === "Feb") {
                        month = 2;
                    }
                    if (dateItems[monthIndex] === "Mar") {
                        month = 3;
                    }
                    if (dateItems[monthIndex] === "Apr") {
                        month = 4;
                    }
                    if (dateItems[monthIndex] === "May") {
                        month = 5;
                    }
                    if (dateItems[monthIndex] === "Jun") {
                        month = 6;
                    }
                    if (dateItems[monthIndex] === "Jul") {
                        month = 7;
                    }
                    if (dateItems[monthIndex] === "Aug") {
                        month = 8;
                    }
                    if (dateItems[monthIndex] === "Sup") {
                        month = 9;
                    }
                    if (dateItems[monthIndex] === "Oct") {
                        month = 10;
                    }
                    if (dateItems[monthIndex] === "Nov") {
                        month = 11;
                    }
                    if (dateItems[monthIndex] === "Dec") {
                        month = 12;
                    }
                    month -= 1;
//                    console.log(dateItems[dayIndex]+1+" "+dayIndex);
                    var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
                    return formatedDate;
                };

                $rootScope.getContextPath = function()
                {
                    return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
                };
            }]);


