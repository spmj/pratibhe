var PratibheApp = angular.module('PratibheApp', ['ngRoute','angularjs-datetime-picker', 'oc.lazyLoad',
    'ui.bootstrap','ui.utils','checklist-model','ngCkeditor']);
var $routeProviderReference;
var $ocLazyLoadReference;
PratibheApp.config(['$routeProvider', '$ocLazyLoadProvider', '$locationProvider',
    function ($routeProvider, $ocLazyLoadProvider, $locationProvider) {
//        $locationProvider.html5Mode(true);
        $routeProviderReference = $routeProvider;
        $ocLazyLoadReference = $ocLazyLoadProvider;
    }
]);