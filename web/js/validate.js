var REG_NAME = /^[a-zA-Z0-9\b \(\)\&\.\,\-]+$/;
var MSG_NAME = "Please enter valid value(alpha-numeric and (,),.,-) for ";

var REG_WEBSITE = /^(http[s]?:\/\/){0,1}?(www.){1}(([0-9A-Za-z]+\.)([0-9A-Za-z]+\.)|([0-9A-Za-z]+\.))(com|org|net|mil|edu|ca|co.uk|com.au|co.in|gov|in)+$/;
var MSG_WEBSITE = "Please enter valid value for ";

var REG_ALPHA_NUM = /^[a-zA-Z0-9\b]+$/;
var MSG_ALPHA_NUM = "Please enter valid value (alpha-numeric) for ";

var REG_MOBILENO = /^\+?[0-9\(\)\,\-\+\ ]{10,}$/;
var MSG_MOBILENO = "Please enter valid value(at least 10 digit and (,),+,-) for ";

var REG_TELEPHONE = /^[0-9\b\(\)\-\+\, ]+$/;
var MSG_TELEPHONE = "Please enter valid value(numeric,(,),+,-) and , for ";

var REG_EMAIL = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var MSG_EMAIL = "Please enter valid value for ";

var REG_ALPHA_SPACE = /^[a-zA-Z\b ]+$/;
var MSG_ALPHA_SPACE = "Please enter valid value (alphabets) for ";

var REG_NAME_MODELPORT = /^[a-zA-Z0-9\b \(\)\&\.\-]+$/;
var MSG_NAME_MODELPORT = "Please enter valid value(alpha-numeric and (,),.,&,-) for ";

var REG_NAME_FAQNO = /^[a-zA-Z0-9\b \.\-\:\_]+$/;
var MSG_NAME_FAQNO = "Please enter valid value(alpha-numeric and .,:,-,_ for ";

var REG_NUM = /^[0-9\b]+$/;
var MSG_NUM = "Please enter valid value(numeric) for ";

var REG_ALPHA_NUM_SPACE = /^[a-zA-Z0-9\b ]+$/;
var MSG_ALPHA_NUM_SPACE = "Please enter valid value (alpha-numeric) for ";

var REG_NUM_DOT = /^([0-9]*[.]{0,1}[0-9]+)$/;
var MSG_NUM_DOT = "Please enter valid value(digit and single .) for ";

var REG_FAX = /^\+?[0-9\b\(\)\-\+\ ]{6,}$/;
var MSG_FAX = "Please enter valid value(at least 6 digit and (,),+,-) for ";

var REG_ALPHA_SPACE_COMMA = /^[a-zA-Z\b\, ]+$/;
var MSG_ALPHA_SPACE_COMMA = "Please enter valid value (alphabets and ,) for ";

var REG_DEC_TWO = /^(?:\d*\.\d{1,2}|\d+)$/;
var MSG_DEC_TWO = "Please enter valid value(up to 2 decimal) for ";

var REG_DEC_ONE = /^(?:\d*\.\d{1,1}|\d+)$/;
var MSG_DEC_ONE = "Please enter valid value(up to 1 decimal) for ";

var REG_DEC_THREE = /^(?:\d*\.\d{1,3}|\d+)$/;
var MSG_DEC_THREE = "Please enter valid value(up to 3 decimal) for ";

var REG_DEC_FOUR = /^(?:\d*\.\d{1,4}|\d+)$/;
var MSG_DEC_FOUR = "Please enter valid value(up to 4 decimal) for ";

var REG_DEC_4_2 = /^([0-9]{1,2})(\.[0-9]{1,2})?$/;
var MSG_DEC_4_2 = "Please enter valid value(max two digit before dot like ## or ##.# or ##.##) for ";

var REG_DEC_5_2 = /^([0-9]{1,3})(\.[0-9]{1,2})?$/;
var MSG_DEC_5_2 = "Please enter valid value(max two three before dot like ### or  ###.## or  ##.##) for ";

var REG_DEC_12_2 = /^([0-9]{1,10})(\.[0-9]{1,2})?$/;
var MSG_DEC_12_2 = "Please enter valid value(max ten digit before dot like ##########.##) for ";

var REG_DEC_14_4 = /^([0-9]{1,10})(\.[0-9]{1,4})?$/;
var MSG_DEC_14_4 = "Please enter valid value(max 14 digit before dot like ############.####) for ";

var REG_DEC_8_3 = /^([0-9]{1,5})(\.[0-9]{1,3})?$/;
var MSG_DEC_8_3 = "Please enter valid value(max 5 digit before dot like #####.###) for ";

var REG_SOME_SPECIAL = /^[a-zA-Z0-9\b\(\)\&\/.\_ ]+$/;
var MSG_SOME_SPECIAL = "Please enter valid value (alpha-numeric) for ";

var REG_ALPHA_SLASH = /^[a-zA-Z\b \/]+$/;
var MSG_ALPHA_SLASH = "Please enter valid value (alphabets and /) for ";

var REG_DOMAIN = /^[A-Za-z0-9]+(\-)*[A-Za-z0-9]+(\.[A-Za-z0-9\-]+(\-)*[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$/;
var MSG_DOMAIN = "Please enter valid value (alpha-numeric and -,.) for ";

var REG_SMTP_POP = /^[A-Za-z0-9]+(\.[A-Za-z0-9]+)*(\.[A-Za-z0-9]{2,})$/; //SMTP->SMTP.example.com , POP->mail.SMTP.example.com
var MSG_SMTP_POP = "Please enter valid value (alpha-numeric and .) for ";

var REG_IFSC = /^[^\s]{4}(0)[A-Z|a-z|0-9]{6}$/;
var MSG_IFSC = "Please enter valid value(4 chars + 0(Zero) + 6 alpha-numeric) for ";

var REG_DATE = /^([0-9]{2})\-([A-Z|a-z]{3})\-([0-9]{4})$/;
var MSG_DATE = "Please enter valid value for ";

var REG_DATETIME = /^([0-9]{2})\-([A-Z|a-z]{3})\-([0-9]{4}) (\d{2}):(\d{2}):(\d{2})$/;
var MSG_DATETIME = "Please enter valid value for ";

var VAL_UNSIGN_SMALLINT = 65535;
var VAL_UNSIGN_INT = 4294967295;

var DATE_FORMAT = "%d-%M-%Y"; //30-AUG-2014
var TIME_FORMAT = "%H:%i:%s"; //15:30:45
var DATETIME_FORMAT = "%d-%M-%Y %H:%i:%s"; //30-AUG-2014 15:30:45

//var REG_TIME = /([01]?[0-9]|2[0-3]):[0-5][0-9]/;
var REG_TIME = /^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/;
var MSG_TIME = "Please enter valid time for ";

var MSG_NOACTION = "There is no any changes.";
var MSG_DELETE = "Are you sure want to delete record?";

var DRIVE_DEVLOPER_KEY = "AIzaSyAqVqYm4DG_43dq_NbIkYH4P6Ij_VxQTs";
var DRIVE_CLIENT_ID = "103950605131-gmi4e4e9400in1dadhk7h5ppfimtebsa.apps.googleusercontent.com";

//    08 - backspace
//    09 - tab
//    13 - enter
//    45 - insert
//    46 - delete
//    35 - end
//    36 - home
//    37 - left arrow
//    39 - right arrow
//    16 - shift
function Trim(TRIM_VALUE)
{
    if (TRIM_VALUE.length < 1)
        return "";
    TRIM_VALUE = LTrim(TRIM_VALUE);
    TRIM_VALUE = RTrim(TRIM_VALUE);
    if (TRIM_VALUE === "")
        return "";
    else
        return TRIM_VALUE;
}
function RTrim(VALUE)
{
    var w_space = String.fromCharCode(32);
    var w_enter = String.fromCharCode(10);
    var v_length = VALUE.length;
    var strTemp = "";
    if (v_length < 0)
        return"";
    var iTemp = v_length - 1;
    while (iTemp > -1)
    {
        if (VALUE.charAt(iTemp) === w_space || VALUE.charAt(iTemp) === w_enter)
            iTemp = iTemp - 1;
        else
            break;
    }
    strTemp = VALUE.substring(0, iTemp + 1);
    return strTemp;
}
function LTrim(VALUE)
{
    var w_space = String.fromCharCode(32);
    var w_enter = String.fromCharCode(10);
    if (v_length < 1)
        return"";
    var v_length = VALUE.length;
    var strTemp = "";
    var iTemp = 0;
    while (iTemp < v_length)
    {
        if (VALUE.charAt(iTemp) === w_space || VALUE.charAt(iTemp) === w_enter)
            iTemp = iTemp + 1;
        else
            break;
    }
    strTemp = VALUE.substring(iTemp, v_length);
    return strTemp;
}

function validate_alphanumber(event) {
    var keyRE = /^[a-zA-Z0-9]+$/;
    if ((typeof (event.keyCode) !== 'undefined' && event.keyCode > 0 &&
            String.fromCharCode(event.keyCode).search(keyRE) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode > 0 &&
                    String.fromCharCode(event.charCode).search(keyRE) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode !== event.keyCode &&
                    typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13|45|46|35|36|37|39)$/) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode === event.keyCode &&
                    typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13)$/) !== (-1))) {
        return true;
    } else {
        return false;
    }
}
function validate_alphanumber_space(event) {
    var keyRE = /^[a-zA-Z0-9\b ]+$/;
    if ((typeof (event.keyCode) !== 'undefined' && event.keyCode > 0 &&
            String.fromCharCode(event.keyCode).search(keyRE) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode > 0 &&
                    String.fromCharCode(event.charCode).search(keyRE) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode !== event.keyCode &&
                    typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|45|46|35|36|37|39)$/) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode === event.keyCode &&
                    typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13)$/) !== (-1))) {
        return true;
    } else {
        return false;
    }
}

function validate_alpha(event) {
    var keyRE = /^[a-zA-Z\b]+$/;
    if ((typeof (event.keyCode) !== 'undefined' && event.keyCode > 0 &&
            String.fromCharCode(event.keyCode).search(keyRE) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode > 0 &&
                    String.fromCharCode(event.charCode).search(keyRE) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode !== event.keyCode &&
                    typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13|45|46|35|36|37|39)$/) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode === event.keyCode &&
                    typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13)$/) !== (-1))) {
        return true;
    } else {
        return false;
    }
}

function validate_alpha_space(event) {
    var keyRE = /^[a-zA-Z\b ]+$/;
    if ((typeof (event.keyCode) !== 'undefined' && event.keyCode > 0 &&
            String.fromCharCode(event.keyCode).search(keyRE) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode > 0 &&
                    String.fromCharCode(event.charCode).search(keyRE) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode !== event.keyCode &&
                    typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13|45|46|35|36|37|39)$/) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode === event.keyCode &&
                    typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13)$/) !== (-1))) {
        return true;
    } else {
        return false;
    }
}

function validate_number_phone_mobi_fax(event) {
    var keyRE = /^[0-9\b\(\)\,\-\+\ ]+$/;
    if ((typeof (event.keyCode) !== 'undefined' && event.keyCode > 0 &&
            String.fromCharCode(event.keyCode).search(keyRE) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode > 0 &&
                    String.fromCharCode(event.charCode).search(keyRE) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode !== event.keyCode &&
                    typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13|45|46|35|36|37|39)$/) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode === event.keyCode &&
                    typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13)$/) !== (-1))) {
        return true;
    } else {
        return false;
    }
}

function validate_alpha_website(event) {
    var keyRE = /^[a-zA-Z0-9\b\.\?\&\=\:\/\-\_]+$/;
    if ((typeof (event.keyCode) !== 'undefined' && event.keyCode > 0 &&
            String.fromCharCode(event.keyCode).search(keyRE) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode > 0 &&
                    String.fromCharCode(event.charCode).search(keyRE) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode !== event.keyCode &&
                    typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13|45|46|35|36|37|39)$/) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode === event.keyCode &&
                    typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13)$/) !== (-1))) {
        return true;
    } else {
        return false;
    }
}

function validate_number_percent(event) {
    var keyRE = /^[0-9\.\%]+$/;
    if ((typeof (event.keyCode) !== 'undefined' && event.keyCode > 0 &&
            String.fromCharCode(event.keyCode).search(keyRE) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode > 0 &&
                    String.fromCharCode(event.charCode).search(keyRE) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode !== event.keyCode &&
                    typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13|45|46|35|36|37|39)$/) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode === event.keyCode &&
                    typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13)$/) !== (-1))) {
        return true;
    } else {
        return false;
    }
}

function validate_email(event) {
    var keyRE = /^[a-zA-Z0-9\b\.\@\_]+$/;
    if ((typeof (event.keyCode) !== 'undefined' && event.keyCode > 0 &&
            String.fromCharCode(event.keyCode).search(keyRE) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode > 0 &&
                    String.fromCharCode(event.charCode).search(keyRE) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode !== event.keyCode &&
                    typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13|45|46|35|36|37|39)$/) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode === event.keyCode &&
                    typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13)$/) !== (-1))) {
        return true;
    } else {
        return false;
    }
}

function validate_Plus_Minus(event) {
    var keyRE = /^[\-\+\ ]+$/;
    if ((typeof (event.keyCode) !== 'undefined' && event.keyCode > 0 &&
            String.fromCharCode(event.keyCode).search(keyRE) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode > 0 &&
                    String.fromCharCode(event.charCode).search(keyRE) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode !== event.keyCode &&
                    typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13|45|46|35|36|37|39)$/) !== (-1)) ||
            (typeof (event.charCode) !== 'undefined' && event.charCode === event.keyCode &&
                    typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13)$/) !== (-1))) {
        return true;
    } else {
        return false;
    }
}

function validate_alphaNumeric(event)
{
    var charCode = (event.which) ? event.which : window.event.keyCode;
    var keyChar = String.fromCharCode(charCode);
    var re = /^[a-zA-Z0-9\t\b]+$/;
    return re.test(keyChar);
}

function validate_number(event) {
// Allow: backspace, delete, tab and escape
    if (event.keyCode === 46 || event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 27 ||
            // Allow: Ctrl+A
                    (event.keyCode === 65 && event.ctrlKey === true) ||
                    (event.keyCode === 67 && event.ctrlKey === true) ||
                    (event.keyCode === 86 && event.ctrlKey === true) ||
                    // Allow: home, end, left, right
                            (event.keyCode >= 35 && event.keyCode <= 39)) {
                // let it happen, don't do anything
                return true;
            } else {
                // Ensure that it is a number and stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                    event.preventDefault();
                }
            }
            return true;
        }
        function validate_time(event) {
            var keyRE = /([01]?[0-9]|2[0-3]):[0-5][0-9]/;
            if ((typeof (event.keyCode) !== 'undefined' && event.keyCode > 0 &&
                    String.fromCharCode(event.keyCode).search(keyRE) !== (-1)) ||
                    (typeof (event.charCode) !== 'undefined' && event.charCode > 0 &&
                            String.fromCharCode(event.charCode).search(keyRE) !== (-1)) ||
                    (typeof (event.charCode) !== 'undefined' && event.charCode !== event.keyCode &&
                            typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13|45|46|35|36|37|39)$/) !== (-1)) ||
                    (typeof (event.charCode) !== 'undefined' && event.charCode === event.keyCode &&
                            typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13)$/) !== (-1))) {
                return true;
            } else {
                return false;
            }
        }

        function validate_faqno(event) {
            var keyRE = /^[a-zA-Z0-9\b \.\-\:\_]+$/;
            if ((typeof (event.keyCode) !== 'undefined' && event.keyCode > 0 &&
                    String.fromCharCode(event.keyCode).search(keyRE) !== (-1)) ||
                    (typeof (event.charCode) !== 'undefined' && event.charCode > 0 &&
                            String.fromCharCode(event.charCode).search(keyRE) !== (-1)) ||
                    (typeof (event.charCode) !== 'undefined' && event.charCode !== event.keyCode &&
                            typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|45|46|35|36|37|39)$/) !== (-1)) ||
                    (typeof (event.charCode) !== 'undefined' && event.charCode === event.keyCode &&
                            typeof (event.keyCode) !== 'undefined' && event.keyCode.toString().search(/^(8|9|13)$/) !== (-1))) {
                return true;
            } else {
                return false;
            }
        }

        function validate_decimal(val, event) {
            var v = val.value;
            if ((event.keyCode === 190) || (event.keyCode === 110))
            {
                if (v.indexOf(".") !== -1)
                {
                    return false;
                }
            }
            if (event.keyCode === 46 || event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 27 ||
                    // Allow: Ctrl+A
                            (event.keyCode === 86 && event.ctrlKey === true) || (event.keyCode === 67 && event.ctrlKey === true) || (event.keyCode === 65 && event.ctrlKey === true) || (event.keyCode === 190) || (event.keyCode === 110) ||
                            // Allow: home, end, left, right
                                    (event.keyCode >= 35 && event.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return true;
                    } else {
                        // Ensure that it is a number and stop the keypress
                        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                            event.preventDefault();
                        }
                    }
                    return true;
                }


                function str_custom(a, b, order) {// custom sorting in dhtmlx grid on string (uppercase+lowercase).
                    if (order === "asc")
                        return (a.toLowerCase() > b.toLowerCase() ? 1 : -1);
                    else
                        return (a.toLowerCase() > b.toLowerCase() ? -1 : 1);
                }//ok
                function date_custom(a, b, order) { // custom sorting in dhtmlx grid on date.
                    a = a.split("-");
                    b = b.split("-");
                    if (a[2] === b[2]) {
                        if (a[1] === b[1])
                            return (a[0] > b[0] ? 1 : -1) * (order === "asc" ? 1 : -1);
                        else
                            return (a[1] > b[1] ? 1 : -1) * (order === "asc" ? 1 : -1);
                    } else
                        return (a[2] > b[2] ? 1 : -1) * (order === "asc" ? 1 : -1);
                }

                function getMessage(Message, Type) {
                    setTimeout(function() {
                        new PNotify({
                            text: Message,
                            type: Type,
                            allow_dismiss: false,
                            showDuration: 40
                        });
                    }, 10);
                }

                function resMsg(data) {
                    if (data.responseCode === 200) {
                        getMessage(data.responseMessage, 'success');
                        return true;
                    } else {
                        getMessage(data.responseMessage, 'danger');
                        return false;
                    }
                }
                ;

                // textbox
                function validate_element(elementname, value, msg) {
                    if (!value || value === null || value.trim() === "") {
                        getMessage("Please enter value for " + msg + ".", 'danger');
                        $("#" + elementname).select();
                        return false;
                    }

                    return true;
                }
                function validate_element_regex(elementname, value, regex, msg) {
                    if (value !== null && value !== "") {
                        if (!value.match(regex)) {
                            getMessage(msg, 'danger');
                            $("#" + elementname).select();
                            return false;
                        }
                    }
                    return true;
                }
                // for combo
                function validate_combo(elementname, value, msg) {
                    if (value === null || value === "") {
                        getMessage("Please make selection for " + msg + ".", 'danger');
                        $("#" + elementname).select();
                        return false;
                    }
                    return true;
                }

                function validate_file(elementname, value, msg) {
                    if (value === null || value === "") {
                        getMessage("Please make selection for " + msg + ".", 'danger');
                        $("#" + elementname).select();
                        return false;
                    }
                    return true;
                }

                // for multi combo --> Added by Dixita Rana
                function validate_multi_combo(elementname, value, msg) {
                    if (value === null || value === "" || value.length == 0) {
                        getMessage("Please make selection for " + msg + ".", 'danger');
                        $("#" + elementname).select();
                        return false;
                    }
                    return true;
                }
                // save time regex check
                function validate_save(elementname, value, reg, regmsg, msg) {
                    if (value != null && value != "") {
                        if (!value.match(reg)) {
                            alertValidate(elementname, regmsg + msg + ".");
                            return false;
                        }
                    }
                    return true;
                }
                function alertValidate(elenament, msg) {
                    dhtmlx.alert({
                        id: "alert",
                        title: "Validation",
                        type: "alert-error",
                        text: msg,
                        ok: "OK",
                        callback: function() {
                            $("#" + elenament).select();
                        }
                    });
                }
                function alertCombo(elenament, msg) {
                    dhtmlx.alert({
                        id: "alert",
                        title: "Validation",
                        type: "alert-error",
                        text: msg,
                        ok: "OK",
                        callback: function() {
                            $("#" + elenament).trigger('chosen:activate');
                        }
                    });
                }
                // Red alert
                function alertError(msg) {
                    dhtmlx.alert({
                        id: "alert",
                        title: "Error",
                        type: "alert-error",
                        text: msg,
                        ok: "OK"
                    });
                }
                // Red alert
                function alertValidateMsg(msg) {
                    dhtmlx.alert({
                        id: "alert",
                        title: "Validation",
                        type: "alert-error",
                        text: msg,
                        ok: "OK"
                    });
                }
                //string map to object and check status.
                function messageLX(mapStr) {
                    var map = (mapStr.replace(/=/gi, "\"= \"").replace(/,\s*/gi, "\",\"").replace(/=/gi, ":"));
                    map = "{\"" + map.substring(1, (map.length) - 1) + "\"}";
                    var mObj = eval("(" + map + ")");
                    if (mObj.STATUS > 0) {// normal
                        dhtmlx.message(mObj.MSG);
                    } else {//error
                        alertError(mObj.MSG);
                    }
                }
                function conformLX(msg, callbackfunc) {
                    dhtmlx.message({
                        title: "Confirmation",
                        type: "confirm-warning",
                        text: msg,
                        callback: function(result) {
                            if (result === true) {
                                callbackfunc();
                            }
                        }
                    });

                }

                // Simple priviledges
                function alertSimple(msg, callbackfunc) {
                    dhtmlx.alert({
                        id: "alert",
                        title: "Validation",
                        type: "alert-error",
                        text: msg,
                        ok: "OK",
                        callback: function(result) {
                            $("#" + callbackfunc).select();
                        }
                    });
                }

// Return Date obj to dd/mm/yyyy string
                function getDMY(dt) {
                    var dd = dt.getDate();
                    var mm = dt.getMonth(); //January is 0!
                    var yyyy = dt.getFullYear();
                    if (dd < 10) {
                        dd = '0' + dd;
                    }
                    var m3 = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun",
                            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
                    return (dd + '-' + m3[mm] + '-' + yyyy);
                }

                function expandDate(id) {
                    var str = $("#" + id).val();
                    if (str.length > 8) {
                        return false;
                    }
                    var dt = new Date();
                    var dd = dt.getDate();
                    var mm = dt.getMonth() + 1;
                    var yy = dt.getFullYear();

                    if (str.length === 8) {
                        dd = parseInt(str.substring(0, 2));
                        mm = parseInt(str.substring(2, 4));
                        yy = parseInt(str.substring(4, 8));
                    } else if (str.length === 6) {
                        dd = parseInt(str.substring(0, 2));
                        mm = parseInt(str.substring(2, 4));
                        yy = parseInt(yy.toString().substring(0, 2) + str.substring(4, 6));
                    } else if (str.length === 4) {
                        dd = parseInt(str.substring(0, 2));
                        mm = parseInt(str.substring(2, 4));
                    } else if (str.length === 2) {
                        dd = parseInt(str.substring(0, 2));
                    }
                    var noofdays = new Date(yy, mm, 0).getDate();
                    if (dd > noofdays || dd === 0) {
                        return false;
                    } else if (mm > 12 || mm === 0) {
                        return false;
                    }
                    if (dd < 10) {
                        dd = '0' + dd;
                    }
                    if (mm < 10) {
                        mm = '0' + mm;
                    }
                    var d = new dhtmlXCalendarObject().getFormatedDate(DATE_FORMAT, new Date(yy, mm - 1, dd));
                    $("#" + id).val(d);
                }

                function expandDate(id, blank) {
                    var str = $("#" + id).val();

                    if (blank != undefined) {
                        if (blank.trim() === 'YES')
                        {
                            return false;
                        }
                    }

                    if (str.trim() === "") {
                        return false;
                    }
                    if (str.length > 8) {
                        return false;
                    }
                    var dt = new Date();
                    var dd = dt.getDate();
                    var mm = dt.getMonth() + 1;
                    var yy = dt.getFullYear();

                    if (str.length === 8) {
                        dd = parseInt(str.substring(0, 2));
                        mm = parseInt(str.substring(2, 4));
                        yy = parseInt(str.substring(4, 8));
                    } else if (str.length === 6) {
                        dd = parseInt(str.substring(0, 2));
                        mm = parseInt(str.substring(2, 4));
                        yy = parseInt(yy.toString().substring(0, 2) + str.substring(4, 6));
                    } else if (str.length === 4) {
                        dd = parseInt(str.substring(0, 2));
                        mm = parseInt(str.substring(2, 4));
                    } else if (str.length === 2) {
                        dd = parseInt(str.substring(0, 2));
                    }
                    var noofdays = new Date(yy, mm, 0).getDate();
                    if (dd > noofdays || dd === 0) {
                        return false;
                    } else if (mm > 12 || mm === 0) {
                        return false;
                    }
                    if (dd < 10) {
                        dd = '0' + dd;
                    }
                    if (mm < 10) {
                        mm = '0' + mm;
                    }
                    var d = new dhtmlXCalendarObject().getFormatedDate(DATE_FORMAT, new Date(yy, mm - 1, dd));
                    $("#" + id).val(d);
                }

//-----------added by m agrawal --23-09-2014-----
                function roundNumber(rnum, rlength) { // Arguments: number to round, number of decimal places

                    var newnumber = Math.round(rnum * Math.pow(10, rlength)) / Math.pow(10, rlength);
                    return newnumber;
                }

//-------------------added by m agrawal---14-10-2014----for enter key press-- no error
                $(function() {
                    var keyStop = {
                        8: ":not(input:text, textarea, input:file, input:password)", // stop backspace = back
                        13: "input:text, input:password", // stop enter = submit 

                        end: null
                    };
                    $(document).bind("keydown", function(event) {
                        var selector = keyStop[event.which];

                        if (selector !== undefined && $(event.target).is(selector)) {
                            event.preventDefault(); //stop event
                        }
                        return true;
                    });
                });
/// Cookies kk@30-10-2014////
                function getCookie(c_name) {
                    var i, x, y, ARRcookies = document.cookie.split(";");
                    for (i = 0; i < ARRcookies.length; i++) {
                        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
                        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
                        x = x.replace(/^\s+|\s+$/g, "");
                        if (x == c_name) {
                            return unescape(y);
                        }
                    }
                    return "";
                }
                function setCookie(c_name, value, exdays) {
                    var exdate = new Date();
                    exdate.setDate(exdate.getDate() + exdays);
                    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
                    document.cookie = c_name + "=" + c_value;
                }

                //SANDIP@10-12-2014
                var entity_table = {
                    34: "&quot;", // Quotation mark. Not required
                    38: "&amp;", // Ampersand. Applied before everything else in the application
                    160: "&nbsp;" // Non-breaking space
                };

                function entityToHtml(string) {
                    for (var i in entity_table) {
                        if (i !== 38) {
                            string = string.replace(new RegExp(entity_table[i], "g"), String.fromCharCode(i));
                        }
                    }
                    //string = string.replace(new RegExp("&#(x?)(\\d+);", "g"), String.fromCharCode(((p1 == 'x') ? parseInt(p2, 16) : p2)));
                    string = string.replace(new RegExp(entity_table[38], "g"), String.fromCharCode(38));
                    return string;
                }
                function setRowIdKK(obj, index) {
                    try {
                        for (var i = 1, vLen = obj.getRowsNum(); i <= vLen; i++) {
                            obj.changeRowId(i, "k" + i);
                        }
                        for (i = 1, vLen = obj.getRowsNum(); i <= vLen; i++) {
                            obj.changeRowId("k" + i, obj.cells("k" + i, index).getValue());
                        }
                    } catch (err) {

                    }
                }
                function checkDateForFY(id) {
                    var strDate = $("#" + id).val();
                    var fy = getDataValid("login.lx?act=getFinYear", null);
                    var date = fy.split("|");

                    var m3 = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun",
                            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

                    var dateStr = strDate.split("-");
                    var day = dateStr[0];
                    var month = dateStr[1];
                    var year = dateStr[2];

                    var mm = m3.indexOf(month) + 1;
                    if (mm < 10) {
                        mm = '0' + mm;
                    }
                    var edate = year + "-" + mm + "-" + day;

                    var entrydate = new Date(edate);
                    var startdate = new Date(date[0]);
                    var enddate = new Date(date[1]);

                    if (startdate <= entrydate && enddate >= entrydate) {
                    } else {
                        alertSimple("Date must be fall in financial year.", id);
                        $("#" + id).val("");
                        return false;
                    }
                    return true;
                }

                function checkedClickDate(Obj, field) {
                    Obj.attachEvent("onClick", function(date) {
                        if (!checkDateForFY(field))
                            return false;
                    });
                }

                function disableNonFinYearDate(Obj) {
                    var fy = getDataValid("login.lx?act=getFinYear", null);
                    var date = fy.split("|");
                    Obj.setSensitiveRange("01-Apr-" + date[0].substr(0, 4), "31-Mar-" + date[1].substr(0, 4));
                }



