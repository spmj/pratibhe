/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('eventService', ['$http', function ($http) {

        this.getAllCombo = function () {
            return $http.get('mentor/event/getAllCombo');
        };

        this.getCity = function (id) {
            return $http.get('mentor/event/getCity/' + id);
        };

        this.viewDetail = function () {
            return $http.get('mentor/event/viewDetail');
        };

        this.loadContentPage = function () {
            return $http.get('mentor/event/getContentPage');
        };

//        this.insertDetail = function (data) {
//            return $http.post('event/', JSON.stringify(data));
//        };

        this.insertDetail = function (data) {
            var file_thumb = data.THUMBIMG;
            var file_banner = data.BANNERIMG;
            var uploadUrl = "event/";
            var fd = new FormData();
            fd.append('file_thumb', file_thumb);
            fd.append('file_banner', file_banner);
            fd.append('formData', JSON.stringify(data));
            return $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            });
        };

//        this.updateDetail = function (id, data) {
//            return $http.put('event/' + id, JSON.stringify(data));
//        };

        this.updateDetail = function (data) {
            var file_thumb = data.THUMBIMG;
            var file_banner = data.BANNERIMG;
            var uploadUrl = "event/updateEvent";
            var fd = new FormData();
            fd.append('file_thumb', file_thumb);
            fd.append('file_banner', file_banner);
            fd.append('formData', JSON.stringify(data));
            return $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            });
        };

        this.deleteDetail = function (id) {
            return $http.delete('mentor/event/' + id);
        };

        this.getUpdateData = function (id) {
            return $http.get('mentor/event/' + id);
        };

        this.getPartcipateData = function (id) {
            return $http.get('mentor/event/getPartcipateData/' + id);
        };
    }]);
