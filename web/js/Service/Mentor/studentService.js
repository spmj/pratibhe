/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('studentService', ['$http', function ($http) {

        this.getAllCombo = function () {
            return $http.get('mentor/student/getAllCombo');
        };

        this.getCity = function (id) {
            return $http.get('mentor/student/getCity/' + id);
        };

        this.viewDetail = function () {
            return $http.get('mentor/student/viewStudentsDetail');
        };

        this.loadContentPage = function () {
            return $http.get('mentor/student/getContentPage');
        };

        this.insertDetail = function (data) {
            return $http.post('mentor/student/', JSON.stringify(data));
        };

        this.updateDetail = function (id, data) {
            return $http.put('mentor/student/' + id, JSON.stringify(data));
        };

        this.deleteDetail = function (id) {
            return $http.delete('mentor/student/' + id);
        };

        this.getUpdateData = function (id) {
            return $http.get('mentor/student/' + id);
        };
    }]);
