/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('mentorHappningService', ['$http', function ($http) {


        this.viewDetail = function () {
            return $http.get('mentorhappenings/viewDetail');
        };

        this.loadContentPage = function () {
            return $http.get('mentor/happenings/getContentPage');
        };

        this.deleteDetail = function (id) {
            return $http.delete('mentorhappenings/' + id);
        };

        this.getUpdateData = function (id) {
            return $http.get('mentorhappenings/' + id);
        };

     
    }]);
