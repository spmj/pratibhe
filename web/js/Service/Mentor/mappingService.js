/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('mappingService', ['$http', function ($http) {

        this.viewDetail = function () {
            return $http.get('mapping/viewDetail');
        };

        this.loadContentPage = function () {
            return $http.get('mentor/mapping/getContentPage');
        };

        this.updateDetail = function (data) {
            return $http.post('mapping/updateDetail/', JSON.stringify(data));
        };
    }]);
