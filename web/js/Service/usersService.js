/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('usersService', ['$http', function ($http) {

        this.getAllCombo = function () {
            return $http.get('users/getAllCombo');
        };

        this.getCity = function (id) {
            return $http.get('users/getCity/' + id);
        };

        this.viewDetail = function (type) {
            if (type == 'students') {
                return $http.get('users/viewStudentsDetail');
            }
            if (type == 'mentors') {
                return $http.get('users/viewMentorsDetail');
            }
        };

        this.loadContentPage = function () {
            return $http.get('admin/users/getContentPage');
        };

        this.insertDetail = function (data) {
            return $http.post('users/', JSON.stringify(data));
        };

        this.updateDetail = function (id, data) {
            return $http.put('users/' + id, JSON.stringify(data));
        };

        this.deleteDetail = function (id) {
            return $http.delete('users/' + id);
        };

        this.getUpdateData = function (id) {
            return $http.get('users/' + id);
        };
        
        this.getWorkData = function (id) {
            return $http.get('users/work/' + id);
        };
        
        this.getWorkDetail = function (id) {
            return $http.get('users/workdetail/' + id);
        }; 
        
        this.getCommentDetail= function (id) {
            return $http.get('users/work/comments/' + id);
        }; 
                
        this.addComment = function (data) {
            return $http.post('users/comment', JSON.stringify(data));
        };
        
         this.setFetureWork = function(data) {
            return $http.put('work/featurework',JSON.stringify(data));
        };

    }]);
