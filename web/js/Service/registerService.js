/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('registerService', ['$http', function ($http) {

//        this.insertDetail = function (data) {
//            return $http.post('signup/', JSON.stringify(data));
//        };
        
         this.getCity = function (id) {
            return $http.get('profile/getCity/' + id);
        };
        
        this.insertDetail = function (data) {
            var file_profile = data.IMAGE;
            var uploadUrl = "signup/";
            var fd = new FormData();
            fd.append('file_profile', file_profile);
            fd.append('formData', JSON.stringify(data));
            return $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            });
        };
        
//        this.signIn = function (data) {
//            return $http.post('signin/', JSON.stringify(data));
//        };
    }]);
