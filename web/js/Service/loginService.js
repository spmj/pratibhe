// Project : pariweb loginService.js
// Created : 8 Jun, 2017 5:32:19 PM
// Author  : Sandip Kotadiya

angular.module('PratibheApp').service('loginService', ['$http', function ($http) {
        this.signIn = function (data) {
            return $http.post('signin/', JSON.stringify(data));
        };
        
        this.checkUser = function (data) {
            return $http.post('checkuser/', JSON.stringify(data));
        };
        
        this.changefgpwd= function (data) {
            return $http.post('../../changefgpwd/', JSON.stringify(data));
        };
    }]);