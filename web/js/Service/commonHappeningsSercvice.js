/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('commonHappeningsSercvice', ['$http', function ($http) {


        this.viewDetail = function () {
            return $http.get('happenings/viewDetail');
        };

        this.loadContentPage = function () {
            return $http.get('student/happenings/getContentPage');
        };

        this.updateDetail = function (data) {
            var file_thumb = data.THUMBIMG;
            var file_banner = data.BANNERIMG;
            var uploadUrl = "happenings/updateHappenings";
            var fd = new FormData();
            fd.append('file_thumb', file_thumb);
            fd.append('file_banner', file_banner);
            fd.append('formData', JSON.stringify(data));
            return $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            });
        };

        this.deleteDetail = function (id) {
            return $http.delete('happenings/' + id);
        };

        this.getUpdateData = function (id) {
            return $http.get('happenings/' + id);
        };

        this.getPartcipateData = function (id) {
            return $http.get('happenings/getPartcipateData/' + id);
        };
    }]);
