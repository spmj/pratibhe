/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('workService', ['$http', function($http) {

        this.getAllCombo = function() {
            return $http.get('work/getAllCombo');
        };

        this.viewDetail = function() {
            return $http.get('work/viewDetail');
        };

        this.loadContentPage = function(type) {
            if (type == "MENTOR") {
                return $http.get('mentor/work/getContentPage');
            } else {
                return $http.get('student/work/getContentPage');
            }
        };

//        this.insertDetail = function (data) {
//            return $http.post('work/', JSON.stringify(data));
//        };


        this.insertDetail = function(data) {
            var workfile = data.WORKFILE;
            var uploadUrl = "work/";
            var fd = new FormData();
            fd.append('file', workfile);
            fd.append('formData', JSON.stringify(data));
            return $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            });
        };


//        this.updateDetail = function (id, data) {
//            return $http.put('work/' + id, JSON.stringify(data));
//        };

        this.updateDetail = function(data) {
            var file = data.WORKFILE;
            var uploadUrl = "work/updateWork";
            var fd = new FormData();
            fd.append('file', file);
            fd.append('formData', JSON.stringify(data));
            return $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            });
        };

        this.deleteDetail = function(id) {
            return $http.delete('work/' + id);
        };

        this.getUpdateData = function(id) {
            return $http.get('work/' + id);
        };

    }]);
