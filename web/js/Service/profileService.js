/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('profileService', ['$http', function ($http) {

        this.getAllCombo = function () {
            return $http.get('profile/getAllCombo');
        };

        this.getCity = function (id) {
            return $http.get('profile/getCity/' + id);
        };

        this.getProfileData = function () {
            return $http.get('profile/getProfileData');
        };

//        this.updateProfile = function (id, data) {
//            return $http.put('profile/' + id, JSON.stringify(data));
//        };
        
        this.updateProfile = function (data) {
            var file_profile = data.IMAGE;
            var uploadUrl = "profile/updateProfile";
            var fd = new FormData();
            fd.append('file_profile', file_profile);
            fd.append('formData', JSON.stringify(data));
            return $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            });
        };

    }]);
