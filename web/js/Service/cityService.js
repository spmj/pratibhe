/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('cityService', ['$http', function ($http) {

        this.getAllCombo = function () {
            return $http.get('city/getAllCombo');
        };

        this.viewDetail = function () {
            return $http.get('city/viewDetail');
        };

        this.loadContentPage = function () {
            return $http.get('admin/city/getContentPage');
        };

        this.insertDetail = function (data) {
            return $http.post('city/', JSON.stringify(data));
        };

        this.updateDetail = function (id, data) {
            return $http.put('city/' + id, JSON.stringify(data));
        };

        this.deleteDetail = function (id) {
            return $http.delete('city/' + id);
        };

        this.getUpdateData = function (id) {
            return $http.get('city/' + id);
        };
    }]);
