/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('inquiryService', ['$http', function ($http) {


        this.viewDetail = function () {
            return $http.get('inquiry/viewDetail');
        };

        this.loadContentPage = function () {
            return $http.get('users/inquiry/getContentPage');
        };

        this.deleteDetail = function (id) {
            return $http.delete('inquiry/' + id);
        };

        this.getUpdateData = function (id) {
            return $http.get('inquiry/' + id);
        };
    }]);
        