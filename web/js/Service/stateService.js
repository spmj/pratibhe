/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('stateService', ['$http', function ($http) {

        this.viewDetail = function () {
            return $http.get('state/viewDetail');
        };

        this.loadContentPage = function () {
            return $http.get('admin/state/getContentPage');
        };

        this.insertDetail = function (data) {
            return $http.post('state/', JSON.stringify(data));
        };

        this.updateDetail = function (id, data) {
            return $http.put('state/' + id, JSON.stringify(data));
        };

        this.deleteDetail = function (id) {
            return $http.delete('state/' + id);
        };

        this.getUpdateData = function (id) {
            return $http.get('state/' + id);
        };
    }]);
