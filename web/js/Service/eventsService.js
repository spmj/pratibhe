/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('eventsService', ['$http', function ($http) {

        this.getAllCombo = function () {
            return $http.get('events/getAllCombo');
        };

        this.getCity = function (id) {
            return $http.get('events/getCity/' + id);
        };

        this.viewDetail = function () {
            return $http.get('events/viewDetail');
        };

        this.loadContentPage = function () {
            return $http.get('admin/events/getContentPage');
        };

//        this.insertDetail = function (data) {
//            return $http.post('events/', JSON.stringify(data));
//        };

        this.insertDetail = function (data) {
            var file_thumb = data.THUMBIMG;
            var file_banner = data.BANNERIMG;
            var uploadUrl = "events/";
            var fd = new FormData();
            fd.append('file_thumb', file_thumb);
            fd.append('file_banner', file_banner);
            fd.append('formData', JSON.stringify(data));
            return $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            });
        };

//        this.updateDetail = function (id, data) {
//            return $http.put('events/' + id, JSON.stringify(data));
//        };

        this.updateDetail = function (data) {
            var file_thumb = data.THUMBIMG;
            var file_banner = data.BANNERIMG;
            var uploadUrl = "events/updateEvent";
            var fd = new FormData();
            fd.append('file_thumb', file_thumb);
            fd.append('file_banner', file_banner);
            fd.append('formData', JSON.stringify(data));
            return $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            });
        };

        this.deleteDetail = function (id) {
            return $http.delete('events/' + id);
        };

        this.getUpdateData = function (id) {
            return $http.get('events/' + id);
        };

        this.getPartcipateData = function (id) {
            return $http.get('events/getPartcipateData/' + id);
        };
    }]);
