/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('eventtypeService', ['$http', function ($http) {

        this.viewDetail = function () {
            return $http.get('eventtype/viewDetail');
        };

        this.loadContentPage = function () {
            return $http.get('admin/eventtype/getContentPage');
        };

        this.insertDetail = function (data) {
            return $http.post('eventtype/', JSON.stringify(data));
        };

        this.updateDetail = function (id, data) {
            return $http.put('eventtype/' + id, JSON.stringify(data));
        };

        this.deleteDetail = function (id) {
            return $http.delete('eventtype/' + id);
        };

        this.getUpdateData = function (id) {
            return $http.get('eventtype/' + id);
        };
    }]);
