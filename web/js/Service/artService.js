/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('artService', ['$http', function ($http) {

        this.viewDetail = function () {
            return $http.get('art/viewDetail');
        };

        this.loadContentPage = function () {
            return $http.get('admin/art/getContentPage');
        };

        this.insertDetail = function (data) {
            return $http.post('art/', JSON.stringify(data));
        };

        this.updateDetail = function (id, data) {
            return $http.put('art/' + id, JSON.stringify(data));
        };

        this.deleteDetail = function (id) {
            return $http.delete('art/' + id);
        };

        this.getUpdateData = function (id) {
            return $http.get('art/' + id);
        };
    }]);
