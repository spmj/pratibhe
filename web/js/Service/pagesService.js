/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('pagesService', ['$http', function ($http) {

        this.getAllCombo = function () {
            return $http.get('pages/getAllCombo');
        };

        this.viewDetail = function () {
            return $http.get('pages/viewDetail');
        };

        this.loadContentPage = function () {
            return $http.get('admin/pages/getContentPage');
        };

        this.insertDetail = function (data) {
            return $http.post('pages/', JSON.stringify(data));
        };

        this.updateDetail = function (id, data) {
            return $http.put('pages/' + id, JSON.stringify(data));
        };

        this.deleteDetail = function (id) {
            return $http.delete('pages/' + id);
        };

        this.getUpdateData = function (id) {
            return $http.get('pages/' + id);
        };
    }]);
