// Project : ecomadmin commonService.js
// Created : 23 Aug, 2017 8:45:11 PM
// Author  : Sandip Kotadiya



angular.module('PratibheApp').service('commonService', ['$http', function ($http) {
        this.getLogOut = function () {
            return $http.get('logout');
        };
        
        this.getProfile = function (remotemobile) {
            return $http.get('profile/getRemoteProfile/'+remotemobile);
        };
    }]);
