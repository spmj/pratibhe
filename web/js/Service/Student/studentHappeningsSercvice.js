/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('studentHappeningsSercvice', ['$http', function ($http) {


        this.viewDetail = function () {
            return $http.get('studenthappenings/viewDetail');
        };

        this.loadContentPage = function () {
            return $http.get('student/happenings/getContentPage');
        };

        this.deleteDetail = function (id) {
            return $http.delete('studenthappenings/' + id);
        };

        this.getUpdateData = function (id) {
            return $http.get('studenthappenings/' + id);
        };

        this.getPartcipateData = function (id) {
            return $http.get('happenings/getPartcipateData/' + id);
        };
    }]);
