/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('changepwdService', ['$http', function ($http) {

        this.changePwd = function (data) {
            return $http.post('changepwd/updateChangepwd', JSON.stringify(data));
        };

    }]);
