/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('dbService', ['$http', function ($http) {

        this.getAdminDbData = function () {
            return $http.get('admin/getDbData');
        };
        this.getMentorDbData = function () {
            return $http.get('mentor/getDbData');
        };
        this.getStudentDbData = function () {
            return $http.get('student/getDbData');
        };

    }]);
