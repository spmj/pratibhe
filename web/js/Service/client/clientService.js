/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('PratibheApp').service('clientService', ['$http', function ($http) {

        this.getAllClientDetails = function () {
            return $http.get('client/getAllClientDetails');
        };

        this.getCity = function (id) {
            return $http.get('client/getCity/' + id);
        };

        this.viewDetail = function () {
            return $http.get('client/viewDetail');
        };

        this.loadContentPage = function () {
            return $http.get('admin/client/getContentPage');
        };



        this.deleteDetail = function (id) {
            return $http.delete('client/' + id);
        };

        this.getUpdateData = function (id) {
            return $http.get('client/' + id);
        };

        this.getPartcipateData = function (id) {
            return $http.get('client/getPartcipateData/' + id);
        };

        this.getFilteredHappenings = function (data) {
            return $http.post('client/getFilteredHappenings/', JSON.stringify(data));
        };
        
        this.getArtist = function (data) {
            return $http.post('client/getArtist/', JSON.stringify(data));
        };
        
         this.getFilteredEvents = function (data) {
            return $http.post('client/getFilteredEvents/', JSON.stringify(data));
        };
        

//         this.insertDetail = function (data) {
//            return $http.post('client/getFilteredHappenings/', JSON.stringify(data));
//        };

        this.insertPartHap = function (id) {
            return $http.get('client/insertPartHap/' + id);
        };
        
        this.insertPartEvent = function (data) {
            return $http.post('client/insertPartEvent/', JSON.stringify(data));
        };
        
        this.insertArtistInquiry = function (data) {
            return $http.post('client/insertArtInq/', JSON.stringify(data));
        };
        
        this.getGalleryData = function (data) {
            return $http.post('client/getGalleryData/', JSON.stringify(data));
        };
        
        this.getGalleryMusicData = function (data) {
            return $http.post('client/getGalleryMusicData/', JSON.stringify(data));
        };
    }]);
