/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.mentor.happning;

import com.pratibhe.common.Const;
import com.pratibhe.common.SessionBean;
import com.pratibhe.util.TranSqlServices;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jay
 */
@Repository
public class MentorHappeningsDataManager {

     @Autowired
    TranSqlServices objSQL;

    @Autowired
    SessionBean sBean;

    /**
     * get all events
     *
     * @return
     * @throws Exception
     */
    public List viewDetail() throws Exception {
        Map param = new HashMap();
        param.put("USERID", sBean.getUserId());
        String sQuery = "SELECT H.HAPPENINGSID, TITLE, SUMMERY, DETAILS, H.ADDRESS, S.STATENAME, "
                + "C.CITYNAME, A.ARTNAME,  ET.EVENTTYPENAME, \n"
                + "DATE_FORMAT(STARTDATE,'%d-%b-%Y') STARTDATE, "
                + "DATE_FORMAT(ENDDATE,'%d-%b-%Y') ENDDATE,"
                + "CONTACTNAME, CONTACTEMAIL \n"
                + "FROM HAPPENINGS H  \n"
                + "INNER JOIN STATE S ON S.STATEID = H.STATEID  \n"
                + "INNER JOIN CITY C ON C.CITYID = H.CITYID  \n"
                + "INNER JOIN ART A ON A.ARTID = H.ARTID  \n"
                + "INNER JOIN EVENTTYPE ET ON ET.EVENTTYPEID = H.EVENTTYPEID  \n"
                + "INNER JOIN USER_HAPPENINGS UH ON H.HAPPENINGSID = UH.HAPPENINGSID\n"
                + "WHERE UH.USERID=:USERID\n"
                + "ORDER BY H.HAPPENINGSID DESC ";
        return objSQL.getList(sQuery, param);
    }

    /**
     * get events
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getUpdateData(Map param) throws Exception {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        param.put("USERID", sBean.getUserId());
        String sQuery = "SELECT H.HAPPENINGSID, TITLE, SUMMERY, DETAILS, H.ADDRESS, "
                + "S.STATENAME, C.CITYNAME, A.ARTNAME,  ET.EVENTTYPENAME, \n"
                + "DATE_FORMAT(STARTDATE,'%d-%b-%Y') STARTDATE,DATE_FORMAT(ENDDATE,'%d-%b-%Y') ENDDATE, "
                + "CONTACTNAME, CONTACTEMAIL \n"
                + "FROM HAPPENINGS H  \n"
                + "INNER JOIN STATE S ON S.STATEID = H.STATEID  \n"
                + "INNER JOIN CITY C ON C.CITYID = H.CITYID  \n"
                + "INNER JOIN ART A ON A.ARTID = H.ARTID  \n"
                + "INNER JOIN EVENTTYPE ET ON ET.EVENTTYPEID = H.EVENTTYPEID  \n"
                + "INNER JOIN USER_HAPPENINGS UH ON H.HAPPENINGSID = UH.HAPPENINGSID\n"
                + "WHERE H.HAPPENINGSID=:HAPPENINGSID AND USERID=:USERID\n"
                + "ORDER BY H.HAPPENINGSID DESC ";
        return objSQL.getMap(sQuery, param);
    }

    /**
     * for update events
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateDetail(Map param) throws Exception {
        String sQuery = "UPDATE HAPPENINGS "
                + " SET "
                + " TITLE = :TITLE, SUMMERY = :SUMMERY, DETAILS = :DETAILS, "
                + " ADDRESS = :ADDRESS, STATEID = :STATEID, CITYID = :CITYID, "
                + " ARTID = :ARTID, EVENTTYPEID = :EVENTTYPEID, "
                + " STARTDATE = :STARTDATE, ENDDATE = :ENDDATE, "
                + " CONTACTNAME = :CONTACTNAME, CONTACTEMAIL = :CONTACTEMAIL, "
                + " NOOFSLOT = :NOOFSLOT, CREATERID = :CREATERID, "
                + " EDITTIME = NOW() "
                + " WHERE HAPPENINGSID = :HAPPENINGSID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * for delete events
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int deleteDetail(Map param) throws Exception {
        param.put("USERID", sBean.getUserId());
        String sQuery = "DELETE FROM USER_HAPPENINGS WHERE USERID=:USERID AND HAPPENINGSID = :HAPPENINGSID";
        return objSQL.persist(sQuery, param);
    }
}
