/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.mentor.mapping;

import com.pratibhe.util.TranSqlServices;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jay
 */
@Repository
public class MappingDataManager {

    @Autowired
    TranSqlServices objSQL;

    /**
     * get art ID
     *
     * @param param
     * @return
     * @throws Exception
     */
    public String getArtId(Map param) throws Exception {
        String sQuery = " SELECT GROUP_CONCAT(ARTID) ARTID FROM USER_ART WHERE USERID = :USERID ";
        System.out.println("mapping art query " + sQuery);
        return objSQL.getString(sQuery, param);
    }

    /**
     * get mapping data
     *
     * @param param
     * @return
     * @throws Exception
     */
    public List viewDetail(Map param) throws Exception {
        String sQuery = "SELECT R.USERID,GROUP_CONCAT(ART.ARTNAME) ARTNAME,R.NAME,R.ARTISTNAME,R.EMAIL,"
                + " R.ABOUTME,S.STATENAME,C.CITYNAME,(CASE WHEN (R.MENTORID IS NULL) THEN 0 ELSE 1 END) MENTORID "
                + " FROM REGISTRATION R "
                + " LEFT JOIN  USER_ART UA ON UA.USERID = R.USERID  "
                + " INNER JOIN ART ON ART.ARTID = UA.ARTID "
                + " LEFT JOIN STATE S ON S.STATEID = R.STATEID "
                + " LEFT JOIN CITY C ON C.CITYID = R.CITYID "
                + " WHERE UA.ARTID IN (" + param.get("ARTID").toString() + ") AND R.TYPE = 'STUDENT' AND "
                + " (R.MENTORID IS NULL OR R.MENTORID = :USERID) "
                + " GROUP BY R.USERID ";
        System.out.println("view detail of mapping detail " + sQuery);
        return objSQL.getList(sQuery, param);
    }

    /**
     * update student
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int updateDetail(Map param) throws Exception {
        String sQuery = "UPDATE REGISTRATION "
                + " SET "
                + " MENTORID = :MENTORID,"
                + " EDITTIME = NOW() "
                + " WHERE USERID IN (:USERID)";
        return objSQL.persist(sQuery, param);
    }
}
