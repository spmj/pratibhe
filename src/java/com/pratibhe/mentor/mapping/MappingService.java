/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.mentor.mapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jay
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class MappingService {

    @Autowired
    MappingDataManager datamanager;

    /**
     * get All mapping Data
     *
     * @param param
     * @return
     * @throws Exception
     */
    public List viewDetail(Map param) throws Exception {
        param.put("ARTID", datamanager.getArtId(param));
        return datamanager.viewDetail(param);
    }

    /**
     * for update student
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map updateDetail(Map param) throws Exception {
        Map map = new HashMap();
        String[] studId1 = param.get("STUDID1").toString().split(",");
        if (!param.get("STUDID1").equals("")) {
            for (int i = 0; i < studId1.length; i++) {
                param.put("USERID", studId1[i]);
                map.put("RESULT", datamanager.updateDetail(param));
            }
        }
        if (!param.get("STUDID0").equals("")) {
            String[] studId0 = param.get("STUDID0").toString().split(",");
            for (int i = 0; i < studId0.length; i++) {
                param.put("USERID", studId0[i]);
                param.put("MENTORID", null);
                map.put("RESULT", datamanager.updateDetail(param));
            }
        }
        return map;
    }

}
