/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.mentor.mapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pratibhe.common.Const;
import com.pratibhe.common.SessionBean;
import com.pratibhe.common.Status;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author jay
 */
@Controller
@RequestMapping("/mapping")
public class MappingController {

    private static final Logger LOG = Logger.getLogger(MappingController.class);

    @Autowired
    MappingService service;

    @Autowired
    SessionBean sBean;

    /**
     * Get All mapping Data.
     *
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/viewDetail")
    public ResponseEntity<Map> viewDetail() throws Exception {
        Map param = new HashMap();
        param.put("USERID", sBean.getUserId());
        Map map = new HashMap();
        map.put("MAPPINGDATA", service.viewDetail(param));
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    /**
     * update student
     *
     * @param jObj
     * @return
     * @throws java.lang.Exception
     */
    @PostMapping(value = "/updateDetail")
    public ResponseEntity<Status> updateDetail(@RequestBody String jObj) throws Exception {
        Status s = new Status();
        ResponseEntity<Status> retResEnt;
        Map param = new ObjectMapper().readValue(jObj, Map.class);
        param.put("MENTORID", sBean.getUserId());
        Map map = service.updateDetail(param);
        if (Integer.parseInt(map.get("RESULT").toString()) > 0) {
            s.setResponseCode(Const.MSG_SUCCESS_CODE);
            s.setResponseMessage("Mentor Id Assign Successfully..");
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        } else {
            s.setResponseCode(Const.MSG_FAIL_CODE);
            s.setResponseMessage(Const.MSG_ERROR);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        }
        return retResEnt;
    }
}
