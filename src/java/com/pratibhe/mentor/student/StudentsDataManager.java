/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.mentor.student;

import com.pratibhe.util.TranSqlServices;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jay
 */
@Repository
public class StudentsDataManager {

    @Autowired
    TranSqlServices objSQL;

    /**
     * get all events
     *
     * @return
     * @throws Exception
     */
    public List viewStudentsDetail(Map param) throws Exception {
        String sQuery = "SELECT RG.USERID,GROUP_CONCAT(T.ARTNAME) ARTNAME, RG.TYPE, RG.NAME, RG.ARTISTNAME, RG.EMAIL, RG.MOBILE, RG.AADHARNO, \n"
                + " RG.GENDER, RG.OCCUPATION, RG.DOB, RG.ADDRESS, RG.CITYID, CT.CITYNAME, ST.STATENAME, RG.ABOUTME, RG.OTHER, \n"
                + " RG.PASSWORD, RG.IMAGE, RG.ADDTIME, RG.EDITTIME, RG.TYPEOFWORK, RG.TENUREOFWORK, RG.MENTORID, RG.ISACTIVE, MT.NAME MENTORNAME\n"
                + " FROM REGISTRATION RG\n"
                + " LEFT JOIN CITY CT ON  CT.CITYID = RG.CITYID\n"
                + " LEFT JOIN STATE ST ON RG.STATEID = ST.STATEID\n"
                + " LEFT JOIN USER_ART UA ON RG.USERID = UA.USERID \n"
                + " LEFT JOIN ART T ON UA.ARTID = T.ARTID\n"
                + " LEFT JOIN REGISTRATION MT ON MT.USERID = RG.MENTORID \n"
                + " WHERE RG.TYPE='STUDENT' AND RG.MENTORID = :MENTORID\n"
                + " GROUP BY RG.USERID ORDER BY RG.NAME";
        System.out.println("student list " + sQuery);
        return objSQL.getList(sQuery, param);
    }

    /**
     * get events
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getUpdateData(Map param) throws Exception {
        String sQuery = "SELECT RG.USERID,GROUP_CONCAT(T.ARTNAME) ARTNAME, RG.TYPE, "
                + " RG.NAME, RG.ARTISTNAME, RG.EMAIL, RG.MOBILE, RG.AADHARNO, \n"
                + " RG.GENDER, RG.OCCUPATION,DATE_FORMAT(RG.DOB,'%d-%b-%Y') DOB, RG.ADDRESS, RG.CITYID, "
                + " CT.CITYNAME, ST.STATENAME, RG.ABOUTME, RG.OTHER, \n"
                + " RG.PASSWORD, RG.IMAGE, RG.ADDTIME, RG.EDITTIME, RG.TYPEOFWORK, "
                + " RG.TENUREOFWORK, RG.MENTORID, RG.ISACTIVE,MT.NAME MENTORNAME,\n"
                + " RG.ISAPPROVED,RG.ISACTIVE\n"
                + " FROM REGISTRATION RG\n"
                + " LEFT JOIN CITY CT ON  CT.CITYID = RG.CITYID\n"
                + " LEFT JOIN STATE ST ON RG.STATEID = ST.STATEID\n"
                + " LEFT JOIN USER_ART UA ON RG.USERID = UA.USERID \n"
                + " LEFT JOIN ART T ON UA.ARTID = T.ARTID\n"
                + " LEFT JOIN REGISTRATION MT ON MT.USERID = RG.MENTORID \n"
                + " WHERE  RG.USERID=:USERID  \n"
                + " GROUP BY UA.USERID";
        System.out.println("get Update Date of userDataManager " + sQuery);
        return objSQL.getMap(sQuery, param);
    }

    /**
     * for update events
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateDetail(Map param) throws Exception {
        String sQuery = "UPDATE REGISTRATION "
                + " SET "
                + " ISAPPROVED = :ISAPPROVED"
                + " WHERE USERID = :USERID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * for delete events
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int deleteDetail(Map param) throws Exception {
        String sQuery = "DELETE FROM REGISTRATION WHERE USERID = :USERID";
        return objSQL.persist(sQuery, param);
    }
}
