/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.city;

import com.pratibhe.util.TranSqlServices;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jay
 */
@Repository
public class CityDataManager {

    @Autowired
    TranSqlServices objSQL;

    /**
     * TO GET State NAME
     *
     * @return
     * @throws Exception
     */
    public List getStateName() throws Exception {
        String sQuery = "SELECT STATEID VALUE, STATENAME TEXT"
                + " FROM STATE"
                + " WHERE ISACTIVE = 1"
                + " ORDER BY STATENAME";
        return objSQL.getList(sQuery);
    }

    /**
     * get all City
     *
     * @return
     * @throws Exception
     */
    public List viewDetail() throws Exception { 
        String sQuery = "SELECT CITY.CITYID, CITY.CITYNAME, CITY.ISACTIVE, CITY.ADDTIME, CITY.EDITTIME, "
                + " STATE.STATEID, STATE.STATENAME"
                + " FROM CITY"
                + " INNER JOIN STATE ON STATE.STATEID = CITY.STATEID"
                + " ORDER BY STATE.STATENAME,CITY.CITYNAME ";
        return objSQL.getList(sQuery);
    }

    /**
     * get City
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getUpdateData(Map param) throws Exception {
        String sQuery = "SELECT CITYID, STATEID, CITYNAME, ISACTIVE"
                + " FROM CITY"
                + " WHERE CITYID = :CITYID";
        return objSQL.getMap(sQuery, param);
    }

    /**
     * check City name exists or not
     *
     * @param param
     * @return
     */
    public Map availabilityInsert(Map param) {
        String sQuery;
        sQuery = "SELECT COUNT(*)\n"
                + " FROM CITY\n"
                + " WHERE CITYNAME = :CITYNAME";
        param.put("RSLTCITYNAME", objSQL.getInteger(sQuery, param));

        return param;
    }

    /**
     * for insert Category
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int insertDetail(Map param) throws Exception {

        String sQuery = "INSERT INTO CITY ("
                + " STATEID, CITYNAME, ISACTIVE, ADDTIME, EDITTIME\n"
                + ") VALUES (\n"
                + " :STATEID, :CITYNAME, :ISACTIVE, NOW(), NOW()\n"
                + ")";
        return objSQL.persistKey(sQuery, param);
    }

    /**
     * check City name exists or not
     *
     * @param param
     * @return
     */
    public Map availabilityUpdate(Map param) {
        String sBNameQuery;
        sBNameQuery = "SELECT COUNT(*)\n"
                + " FROM CITY\n"
                + " WHERE CITYNAME = :CITYNAME AND CITYID != :CITYID";
        param.put("RSLTCITYNAME", objSQL.getInteger(sBNameQuery, param));
        return param;
    }

    /**
     * for update City
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateDetail(Map param) throws Exception {
        String sQuery = "UPDATE CITY "
                + " SET "
                + " STATEID = :STATEID, CITYNAME = :CITYNAME,"
                + " ISACTIVE = :ISACTIVE, EDITTIME = NOW() "
                + " WHERE CITYID = :CITYID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * for delete City
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int deleteDetail(Map param) throws Exception {
        String sQuery = "DELETE FROM CITY WHERE CITYID = :CITYID";
        return objSQL.persist(sQuery, param);
    }
}
