/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.art;

import com.pratibhe.util.TranSqlServices;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jay
 */
@Repository
public class ArtDataManager {

    @Autowired
    TranSqlServices objSQL; 

    /**
     * get all art
     *
     * @return
     * @throws Exception
     */
    public List viewDetail() throws Exception {
        String sQuery = "SELECT ARTID, ARTNAME, ADDTIME, EDITTIME"
                + " FROM ART"
                + " ORDER BY ARTID DESC";
        return objSQL.getList(sQuery);
    }

    /**
     * get art
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getUpdateData(Map param) throws Exception {
        String sQuery = "SELECT ARTID, ARTNAME"
                + " FROM ART"
                + " WHERE ARTID = :ARTID";
        return objSQL.getMap(sQuery, param);
    }

    /**
     * check art name exists or not
     *
     * @param param
     * @return
     */
    public Map availabilityInsert(Map param) {
        String sQuery;
        sQuery = "SELECT COUNT(*)\n"
                + " FROM ART\n"
                + " WHERE ARTNAME = :ARTNAME";
        param.put("RSLTARTNAME", objSQL.getInteger(sQuery, param));

        return param;
    }

    /**
     * for insert art
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int insertDetail(Map param) throws Exception {

        String sQuery = "INSERT INTO ART("
                + " ARTNAME, ADDTIME, EDITTIME\n"
                + ") VALUES (\n"
                + " :ARTNAME, NOW(), NOW()\n"
                + ")";
        return objSQL.persistKey(sQuery, param);
    }

    /**
     * check art name exists or not
     *
     * @param param
     * @return
     */
    public Map availabilityUpdate(Map param) {
        String sBNameQuery;
        sBNameQuery = "SELECT COUNT(*)\n"
                + " FROM ART\n"
                + " WHERE ARTNAME = :ARTNAME AND ARTID != :ARTID";
        param.put("RSLTARTNAME", objSQL.getInteger(sBNameQuery, param));
        return param;
    }

    /**
     * for update art
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateDetail(Map param) throws Exception {
        String sQuery = "UPDATE ART "
                + " SET "
                + " ARTNAME = :ARTNAME,"
                + " EDITTIME = NOW() "
                + " WHERE ARTID = :ARTID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * for delete art
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int deleteDetail(Map param) throws Exception {
        String sQuery = "DELETE FROM ART WHERE ARTID = :ARTID";
        return objSQL.persist(sQuery, param);
    }
}
