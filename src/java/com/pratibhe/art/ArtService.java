/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.art;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jay
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArtService {

    @Autowired
    ArtDataManager datamanager;

    /**
     * get All art Data
     *
     * @return
     * @throws Exception
     */
    public List viewDetail() throws Exception {
        return datamanager.viewDetail();
    }

    /**
     * get art
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getUpdateData(Map param) throws Exception {
        return datamanager.getUpdateData(param);
    }

    /**
     * for insert art
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map insertDetail(Map param) throws Exception {
        Map map = new HashMap();
        map.put("ARTNAME", String.valueOf(param.get("ARTNAME")));
        map = datamanager.availabilityInsert(map);
        if (Integer.parseInt(map.get("RSLTARTNAME").toString()) == 0) {
            Iterator it = param.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                param.replace(pair.getKey(), "", null);
            }
            int refId = datamanager.insertDetail(param);
            map.put("RESULT", refId);
        } else {
            map.put("RESULT", 0);
        }
        return map;
    }

    /**
     * for update art
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map updateDetail(Map param) throws Exception {
        Map map = new HashMap();
        map.put("ARTID", String.valueOf(param.get("ARTID")));
        map.put("ARTNAME", String.valueOf(param.get("ARTNAME")));
        map = datamanager.availabilityUpdate(map);
        if (Integer.parseInt(map.get("RSLTARTNAME").toString()) == 0) {
            Iterator it = param.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                param.replace(pair.getKey(), "", null);
            }
            map.put("RESULT", datamanager.updateDetail(param));

        } else {
            map.put("RESULT", 0);
        }
        return map;
    }

    /**
     * for delete art
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int deleteDetail(Map param) throws Exception {
        return datamanager.deleteDetail(param);
    }
}
