// Project : ecomadmin Status.java
// Created : 6 Jun, 2017 5:43:02 PM
// Author  : Sandip Kotadiya
package com.pratibhe.common;

import java.util.Map;

/**
 *
 * @author Sandip kotadiya
 */
public class Status {
    
    private int responseCode;
    private String responseMessage;
    private Map responseData;

    public Status() {
        responseCode = 500;
        responseMessage = "Something went Wrong...";
        responseData = null;
    }

    public Status(int responseCode, String message) {
        this.responseCode = responseCode;
        this.responseMessage = message;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public Map getResponseData() {
        return responseData;
    }

    public void setResponseData(Map responseData) {
        this.responseData = responseData;
    }
}
