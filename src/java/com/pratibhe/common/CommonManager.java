// Project : CommonManager.java
// Created : 19 Apr, 2017 4:56:19 PM
// Author  : Sandip Kotadiya
package com.pratibhe.common;

//import javax.mail.*;
//import javax.mail.internet.*;
import static java.lang.ProcessBuilder.Redirect.to;
import java.util.Map;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sandip Kotadiya
 */
@Repository
public class CommonManager {

    /**
     * send Email
     *
     * @param param
     * @throws Exception
     */
    public static void SendMail(Map param) throws Exception {

        final String username = "info.mayurm@gmail.com";
        final String password = "mayur992599";

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
//        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        session.setDebug(true);
        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("info.mayurm@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(param.get("EMAIL").toString()));
            if (param.containsKey("FORGOTPWD")) {

                message.setSubject("Your request for password change.");
                message.setText("Dear," + param.get("NAME") + ""
                        + "\n\n We have received a password reset request from you.\n\n"
                        + "\n\n You may click on the link below and reset the password on your own."
                        + "\n\n http://localhost:8084/pratibhe/changepassword/" + param.get("USERID") + "/" + param.get("ACTIVATIONCODE") + "\n\n");
                
            } else {
                message.setSubject("To activate your subscription, please follow the link below.");
                message.setText("Dear," + param.get("NAME") + ""
                        + "\n\n To activate your account, please clieck the link below.\n\n"
                        + "\n\n http://localhost:8084/pratibhe/activate/" + param.get("USERID") + "/" + param.get("ACTIVATIONCODE") + "\n\n");
            }

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

}
