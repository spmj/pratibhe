// Project : Const.java
// Created : 18 Apr, 2017 10:26:20 AM
// Author  : Sandip Kotadiya
package com.pratibhe.common;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author Sandip kotadiya
 */
public class Const {

    public static String AppPath = "";

    public static String getAppPath() {
        return AppPath;
    }

    public static void setAppPath(String AppPath) {
        Const.AppPath = AppPath;
    }

    /* messages */
    public static final int MSG_SUCCESS_CODE = 200;
    public static final int MSG_EXISTS_CODE = 104;
    public static final int MSG_FAIL_CODE = 500;
    public static final int MSG_UNAUTHORIZED_CODE = 401;
    public static final String MSG = "MSG";
    public static final String MSG_ERROR = "Error occured.";
    public static final String MSG_ADD_SUCESS = "Record has been added successfully.";
    public static final String MSG_UPDATE_SUCESS = "Record has been updated successfully.";
    public static final String MSG_FLD_UPDATE_SUCESS = "Field has been updated successfully.";
    public static final String MSG_UPDATE_FAIL = "Record has not updated.";
    public static final String MSG_DELETE_ALERT = "Delete not allowed because reference exists.";
    public static final String MSG_DELETE_SUCESS = "Record has been deleted successfully.";
    public static final String MSG_DELETE_FAIL = "Record has not deleted.";
    public static final String MSG_EXISTS = " already exists !";
    public static final String MSG_STATUS = "STATUS";
    public static final String MSG_SYSTEM_GROUP_DELETE = "System Group can not be Deleted.";
    public static final String MSG_SYSTEM_GROUP_UPDATE = "System Group can not be Updated.";

    //Login Msg
    public static final String MSG_LOGIN_SUCCESS = "Login Successfully.";
    public static final String MSG_LOGIN_FAIL = "Login Fail.";
    public static final String MSG_LOGIN_FIRST = "Please Login First.";

    //Change password Msg
    public static final String MSG_INVALID_OLDPWD = "Invalid old password.";

    //Cart Msg
    public static final String MSG_ADD_CART = "Item add into cart.";
    public static final String MSG_REMOVE_CART = "Item remove from cart.";

    /* task */
    public static final String TASK = "task";

    public static final String TASK_ADD = "Add";
    public static final String TASK_EDIT = "Edit";

    public static String testQuery(String str, Map map) {
        Iterator entries = map.entrySet().iterator();
        while (entries.hasNext()) {
            Entry thisEntry = (Entry) entries.next();
            Object key = thisEntry.getKey();
            Object value = thisEntry.getValue();
            str = str.replace(":" + key + "", value.toString());
        }
        return str;
    }
}
