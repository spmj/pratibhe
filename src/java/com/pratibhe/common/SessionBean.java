// Project : SessionBean.java
// Created : 18 Apr, 2017 10:12:00 AM
// Author  : Sandip Kotadiya
package com.pratibhe.common;

/**
 *
 * @author Sandip kotadiya
 */
public class SessionBean implements java.io.Serializable {
    //venderid,vendername,mobile,email,gstnumber

    private String userId;
    private String userName;
    private String userType;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {
        return "SessionBean{" + "userId=" + userId + ", userName=" + userName + ", userType=" + userType + '}';
    }
}
