/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.events;

import com.pratibhe.common.Const;
import com.pratibhe.util.TranSqlServices;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jay
 */
@Repository
public class EventsDataManager {

    @Autowired
    TranSqlServices objSQL;

    /**
     * TO GET State NAME
     *
     * @return
     * @throws Exception
     */
    public List getStateName() throws Exception {
        String sQuery = "SELECT STATEID VALUE, STATENAME TEXT"
                + " FROM STATE"
                + " WHERE ISACTIVE = 1"
                + " ORDER BY STATENAME";
        return objSQL.getList(sQuery);
    }

    /**
     * get Item Base On Item Group Name
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public List getCity(Map param) throws Exception {
        String sQuery = "SELECT CITYID VALUE,CITYNAME TEXT\n"
                + " FROM CITY\n"
                + " WHERE STATEID = :STATEID AND ISACTIVE = 1\n"
                + " ORDER BY CITYNAME";
        return objSQL.getList(sQuery, param);
    }

    /**
     * get Art Form
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public List getArt(Map param) throws Exception {
        String sQuery = "SELECT ARTID VALUE,ARTNAME TEXT\n"
                + " FROM ART\n"
                + " ORDER BY ARTNAME";
        return objSQL.getList(sQuery, param);
    }

    /**
     * get Event Type
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public List getEventType(Map param) throws Exception {
        String sQuery = "SELECT EVENTTYPEID VALUE,EVENTTYPENAME TEXT\n"
                + " FROM EVENTTYPE\n"
                + " ORDER BY EVENTTYPENAME";
        return objSQL.getList(sQuery, param);
    }

    /**
     * get all events
     *
     * @return
     * @throws Exception
     */
    public List viewDetail() throws Exception {
        String sQuery = "SELECT EVENTID, TITLE, SUMMERY, DETAILS, E.ADDRESS, S.STATENAME, C.CITYNAME, A.ARTNAME, "
                + " ET.EVENTTYPENAME, THUMBIMG, BANNER, DATE_FORMAT(STARTDATE,'%d-%b-%Y') STARTDATE, "
                + " DATE_FORMAT(ENDDATE,'%d-%b-%Y') ENDDATE, CONTACTNAME, CONTACTEMAIL, "
                + " R.NAME, APPID, E.ADDTIME, E.EDITTIME, ISBANNER, ISADMIN, E.ISAPPROVED "
                + " FROM EVENT E "
                + " INNER JOIN STATE S ON S.STATEID = E.STATEID "
                + " INNER JOIN CITY C ON C.CITYID = E.CITYID "
                + " INNER JOIN ART A ON A.ARTID = E.ARTID "
                + " INNER JOIN EVENTTYPE ET ON ET.EVENTTYPEID = E.EVENTTYPEID "
                + " INNER JOIN REGISTRATION R ON R.USERID = E.CREATERID "
                + " ORDER BY EVENTID DESC ";
        return objSQL.getList(sQuery);
    }

    /**
     * get events
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getUpdateData(Map param) throws Exception {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sQuery = "SELECT EVENTID, TITLE, SUMMERY, DETAILS, ADDRESS, STATEID, CITYID, ARTID, "
                + " EVENTTYPEID,CONCAT('" + prop.getProperty("DOMAIN") + "','/events/getBannerThumb/',"
                + " EVENTID) THUMBIMG,"
                + " CONCAT('" + prop.getProperty("DOMAIN") + "','/events/getBanner/',EVENTID) BANNER, "
                + " DATE_FORMAT(STARTDATE,'%d-%b-%Y') STARTDATE, DATE_FORMAT(ENDDATE,'%d-%b-%Y') ENDDATE, "
                + " CONTACTNAME, CONTACTEMAIL, ISAPPROVED, ISADMIN "
                + " FROM EVENT "
                + " WHERE EVENTID = :EVENTID ";
        return objSQL.getMap(sQuery, param);
    }

    /**
     * get banner Image.
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getBannerImages(Map param) throws Exception {
        String sQuery = "SELECT EVENTID,THUMBIMG,BANNER FROM EVENT WHERE EVENTID=:EVENTID";
        return objSQL.getMap(sQuery, param);
    }

    /**
     * update thumb and banner Image
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateEventBannersImage(Map param) throws Exception {
        String sQuery = "UPDATE EVENT"
                + " SET "
                + " THUMBIMG = :THUMBIMG, BANNER=:BANNERIMG"
                + " WHERE EVENTID = :EVENTID";
        return objSQL.persist(sQuery, param);
    }
    
    /**
     * update thumb Image
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateThumbImage(Map param) throws Exception {
        String sQuery = "UPDATE EVENT"
                + " SET "
                + " THUMBIMG = :THUMBIMG"
                + " WHERE EVENTID = :EVENTID";
        return objSQL.persist(sQuery, param);
    }
    
    /**
     * update thumb Image
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateBannerImage(Map param) throws Exception {
        String sQuery = "UPDATE EVENT"
                + " SET "
                + " BANNER = :BANNERIMG"
                + " WHERE EVENTID = :EVENTID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * for insert events
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int insertDetail(Map param) throws Exception {

        String sQuery = "INSERT INTO EVENT("
                + " TITLE, SUMMERY, DETAILS, ADDRESS, STATEID, CITYID, ARTID,\n"
                + " EVENTTYPEID, THUMBIMG, BANNER, STARTDATE, ENDDATE, CONTACTNAME, \n"
                + " CONTACTEMAIL, CREATERID, ISAPPROVED, ISADMIN, ADDTIME, EDITTIME\n"
                + ") VALUES (\n"
                + " :TITLE, :SUMMERY, :DETAILS, :ADDRESS, :STATEID, :CITYID, :ARTID,\n"
                + " :EVENTTYPEID, 'asd', 'asd', STR_TO_DATE(:STARTDATE,'%d-%b-%Y'),"
                + " STR_TO_DATE(:ENDDATE,'%d-%b-%Y') , :CONTACTNAME, \n"
                + " :CONTACTEMAIL, :CREATERID, 'Approved', 1, NOW(), NOW()\n"
                + ")";
        return objSQL.persistKey(sQuery, param);
    }

    /**
     * for update events
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateDetail(Map param) throws Exception {
        String sQuery = "UPDATE EVENT "
                + " SET "
                + " TITLE = :TITLE, SUMMERY = :SUMMERY, DETAILS = :DETAILS, "
                + " ADDRESS = :ADDRESS, STATEID = :STATEID, CITYID = :CITYID, "
                + " ARTID = :ARTID, EVENTTYPEID = :EVENTTYPEID, "
                + " STARTDATE = STR_TO_DATE(:STARTDATE,'%d-%b-%Y'), "
                + " ENDDATE = STR_TO_DATE(:ENDDATE,'%d-%b-%Y'), "
                + " CONTACTNAME = :CONTACTNAME, CONTACTEMAIL = :CONTACTEMAIL, "
                + " CREATERID = :CREATERID, ISAPPROVED = :ISAPPROVED,"
                + " EDITTIME = NOW() "
                + " WHERE EVENTID = :EVENTID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * for delete events
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int deleteDetail(Map param) throws Exception {
        String sQuery = "DELETE FROM EVENT WHERE EVENTID = :EVENTID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * get Item Base On Item Group Name
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public List getPartcipateData(Map param) throws Exception {
        String sQuery = "SELECT EVENTPARTID, EVENTID, NAME, EMAIL, MOBILE, ADDTIME\n"
                + " FROM EVENTPARTICIPANT\n"
                + " WHERE EVENTID = :EVENTID\n"
                + " ORDER BY NAME";
        return objSQL.getList(sQuery, param);
    }

    /**
     * get File Path
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getFilePath(Map param) throws Exception {
        String sQuery = "SELECT THUMBIMG,BANNER FROM EVENT WHERE EVENTID=:EVENTID";
        return objSQL.getMap(sQuery, param);
    }

}
