/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.events;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pratibhe.common.Const;
import com.pratibhe.common.SessionBean;
import com.pratibhe.common.Status;
import com.pratibhe.util.FileUpload;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jay
 */
@Controller
@RequestMapping("/events")
public class EventsController {

    private static final Logger LOG = Logger.getLogger(EventsController.class);

    @Autowired
    SessionBean sBean;

    @Autowired
    EventsService service;

    /**
     * For All Combo Load Data
     *
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/getAllCombo")
    public ResponseEntity<Map> getAllCombo() throws Exception {
        Map mCombos = service.getAllCombo();
        return new ResponseEntity<>(mCombos, HttpStatus.OK);
    }

    /**
     * get City
     *
     * @param id
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/getCity/{id}")
    public ResponseEntity<Map> getCity(@PathVariable int id) throws Exception {
        Map param = new HashMap();
        param.put("STATEID", String.valueOf(id));
        return new ResponseEntity<>(service.getCity(param), HttpStatus.OK);
    }

    /**
     * Get All events Data.
     *
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/viewDetail")
    public ResponseEntity<Map> viewDetail() throws Exception {
        Map map = new HashMap();
        map.put("EVENTSDATA", service.viewDetail());
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    /**
     * Get events Data For Update.
     *
     * @param id
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<Map> getUpdateData(@PathVariable int id) throws Exception {
        Map param = new HashMap();
        param.put("EVENTID", String.valueOf(id));
        return new ResponseEntity<>(service.getUpdateData(param), HttpStatus.OK);
    }

    /**
     * Insert events Detail.
     *
     * @param file_thumb
     * @param file_banner
     * @param request
     * @param jObj
     * @return
     * @throws java.lang.Exception
     */
    @PostMapping(value = "/")
    public ResponseEntity<Status> insertDetail(@RequestParam(value = "file_thumb") MultipartFile file_thumb, @RequestParam(value = "file_banner") MultipartFile file_banner, HttpServletRequest request, @RequestParam(value = "formData") String jObj) throws Exception {
        Status s = new Status();
        ResponseEntity<Status> retResEnt;
        Map param = new ObjectMapper().readValue(jObj, Map.class);

        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sLocation = prop.getProperty("STORAGEBOX") + prop.getProperty("EVENT_PATH");

        param.put("LOCATION", sLocation);
        param.put("FILE_THUMB", file_thumb);
        param.put("FILE_BANNER", file_banner);

        param.put("CREATERID", sBean.getUserId());
        Map map = service.insertDetail(param);
        if (Integer.parseInt(map.get("RESULT").toString()) > 0) {
            s.setResponseCode(Const.MSG_SUCCESS_CODE);
            s.setResponseMessage(Const.MSG_ADD_SUCESS);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        } else {
            s.setResponseCode(Const.MSG_FAIL_CODE);
            s.setResponseMessage(Const.MSG_ERROR);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        }
        return retResEnt;
    }

    /**
     * Download Thumb File.
     *
     * @param id
     * @param res
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/getBannerThumb/{id}")
    public void getBannerThumb(@PathVariable("id") String id, HttpServletResponse res) throws Exception {
        try {
            InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
            Properties prop = new Properties();
            prop.load(input);
            Map param = new HashMap();
            param.put("EVENTID", id);
            Map mImgDetail = service.getBannerImages(param);
            String sLocation = prop.getProperty("STORAGEBOX") + prop.getProperty("EVENT_PATH") + "/" + mImgDetail.get("THUMBIMG").toString();
            FileUpload.downloadFileFromDisk(sLocation, res);
        } catch (Exception ex) {
            System.out.println("ex=" + ex.getMessage());
        }
    }

    /**
     * Download Thumb File.
     *
     * @param id
     * @param res
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/getBanner/{id}")
    public void getBanner(@PathVariable("id") String id, HttpServletResponse res) throws Exception {
        try {
            InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
            Properties prop = new Properties();
            prop.load(input);
            Map param = new HashMap();
            param.put("EVENTID", id);
            Map mImgDetail = service.getBannerImages(param);
            String sLocation = prop.getProperty("STORAGEBOX") + prop.getProperty("EVENT_PATH") + "/" + mImgDetail.get("BANNER").toString();
            FileUpload.downloadFileFromDisk(sLocation, res);
        } catch (Exception ex) {
            System.out.println("ex=" + ex.getMessage());
        }
    }

    /**
     * Update events Detail.
     *
     * @param file_thumb
     * @param file_banner
     * @param request
     * @param id
     * @param jObj
     * @return
     * @throws java.lang.Exception
     */
    @PostMapping(value = "/updateEvent")
    public ResponseEntity<Status> updateDetail(@RequestParam(value = "file_thumb", required = false) MultipartFile file_thumb, @RequestParam(value = "file_banner", required = false) MultipartFile file_banner, HttpServletRequest request, @RequestParam(value = "formData") String jObj) throws Exception {
        Status s = new Status();
        ResponseEntity<Status> retResEnt;
        Map param = new ObjectMapper().readValue(jObj, Map.class);

        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sLocation = prop.getProperty("STORAGEBOX") + prop.getProperty("EVENT_PATH");

        param.put("LOCATION", sLocation);
        param.put("FILE_THUMB", file_thumb);
        param.put("FILE_BANNER", file_banner);

        param.put("CREATERID", sBean.getUserId());
        Map map = service.updateDetail(param);
        if (Integer.parseInt(map.get("RESULT").toString()) > 0) {
            s.setResponseCode(Const.MSG_SUCCESS_CODE);
            s.setResponseMessage(Const.MSG_UPDATE_SUCESS);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        } else {
            s.setResponseCode(Const.MSG_FAIL_CODE);
            s.setResponseMessage(Const.MSG_ERROR);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        }
        return retResEnt;
    }

    /**
     * Delete events detail
     *
     * @param id
     * @return
     * @throws java.lang.Exception
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Status> deleteDetail(@PathVariable int id) throws Exception {
        Status s = new Status();
        ResponseEntity<Status> retResEnt;
        Map param = new HashMap();
        param.put("EVENTID", id);
        
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sLocation = prop.getProperty("STORAGEBOX") + prop.getProperty("EVENT_PATH");
        param.put("LOCATION", sLocation);
        
        int iRes = service.deleteDetail(param);
        if (iRes > 0) {
            s.setResponseCode(Const.MSG_SUCCESS_CODE);
            s.setResponseMessage(Const.MSG_DELETE_SUCESS);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        } else {
            s.setResponseCode(Const.MSG_FAIL_CODE);
            s.setResponseMessage(Const.MSG_DELETE_ALERT);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        }
        return retResEnt;
    }

    /**
     * get Participants Data
     *
     * @param id
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/getPartcipateData/{id}")
    public ResponseEntity<Map> getPartcipateData(@PathVariable int id) throws Exception {
        Map param = new HashMap();
        param.put("EVENTID", String.valueOf(id));
        return new ResponseEntity<>(service.getPartcipateData(param), HttpStatus.OK);
    }

    /**
     * Catch all Controller exception
     *
     * @param req
     * @param ex
     * @return
     */
    @ExceptionHandler({Exception.class})
    public ResponseEntity<Status> exceptionHandler(HttpServletRequest req, Exception ex) {
        Status error = new Status(500, ex.getMessage());
        LOG.error("Request: " + req.getRequestURL() + " raised " + ex);
        return new ResponseEntity<>(error, HttpStatus.OK);
    }
}
