/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.state;

import com.pratibhe.util.TranSqlServices;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jay
 */
@Repository
public class StateDataManager {

    @Autowired
    TranSqlServices objSQL;

    /**
     * get all State
     *
     * @return
     * @throws Exception
     */
    public List viewDetail() throws Exception {
        String sQuery = "SELECT STATEID, STATENAME, ISACTIVE, ADDTIME, EDITTIME"
                + " FROM STATE"
                + " ORDER BY STATENAME ";
        return objSQL.getList(sQuery);
    }

    /**
     * get State
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getUpdateData(Map param) throws Exception {
        String sQuery = "SELECT STATEID, STATENAME, ISACTIVE"
                + " FROM STATE"
                + " WHERE STATEID = :STATEID";
        return objSQL.getMap(sQuery, param);
    }

    /**
     * check State name exists or not
     *
     * @param param
     * @return
     */
    public Map availabilityInsert(Map param) {
        String sQuery;
        sQuery = "SELECT COUNT(*)\n"
                + " FROM STATE\n"
                + " WHERE STATENAME = :STATENAME";
        param.put("RSLTSTATENAME", objSQL.getInteger(sQuery, param));

        return param;
    }

    /**
     * for insert State
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int insertDetail(Map param) throws Exception {

        String sQuery = "INSERT INTO STATE("
                + " STATENAME, ISACTIVE, ADDTIME, EDITTIME\n"
                + ") VALUES (\n"
                + " :STATENAME, :ISACTIVE, NOW(), NOW()\n"
                + ")";
        return objSQL.persistKey(sQuery, param);
    }

    /**
     * check State name exists or not
     *
     * @param param
     * @return
     */
    public Map availabilityUpdate(Map param) {
        String sBNameQuery;
        sBNameQuery = "SELECT COUNT(*)\n"
                + " FROM STATE\n"
                + " WHERE STATENAME = :STATENAME AND STATEID != :STATEID";
        param.put("RSLTSTATENAME", objSQL.getInteger(sBNameQuery, param));
        return param;
    }

    /**
     * for update State
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateDetail(Map param) throws Exception {
        String sQuery = "UPDATE STATE "
                + " SET "
                + " STATENAME = :STATENAME,"
                + " ISACTIVE = :ISACTIVE, EDITTIME = NOW() "
                + " WHERE STATEID = :STATEID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * for delete State
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int deleteDetail(Map param) throws Exception {
        String sQuery = "DELETE FROM STATE WHERE STATEID = :STATEID";
        return objSQL.persist(sQuery, param);
    }
}
