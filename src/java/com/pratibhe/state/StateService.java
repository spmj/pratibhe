/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.state;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jay
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class StateService {

    @Autowired
    StateDataManager datamanager;

    /**
     * get All State Data
     *
     * @return
     * @throws Exception
     */
    public List viewDetail() throws Exception {
        return datamanager.viewDetail();
    }

    /**
     * get State
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getUpdateData(Map param) throws Exception {
        return datamanager.getUpdateData(param);
    }

    /**
     * for insert State
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map insertDetail(Map param) throws Exception {
        Map map = new HashMap();
        map.put("STATENAME", String.valueOf(param.get("STATENAME")));
        map = datamanager.availabilityInsert(map);
        if (Integer.parseInt(map.get("RSLTSTATENAME").toString()) == 0) {
            Iterator it = param.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                param.replace(pair.getKey(), "", null);
            }
            int refId = datamanager.insertDetail(param);
            map.put("RESULT", refId);
        } else {
            map.put("RESULT", 0);
        }
        return map;
    }

    /**
     * for update State
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map updateDetail(Map param) throws Exception {
        Map map = new HashMap();
        map.put("STATEID", String.valueOf(param.get("STATEID")));
        map.put("STATENAME", String.valueOf(param.get("STATENAME")));
        map = datamanager.availabilityUpdate(map);
        if (Integer.parseInt(map.get("RSLTSTATENAME").toString()) == 0) {
            Iterator it = param.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                param.replace(pair.getKey(), "", null);
            }
            map.put("RESULT", datamanager.updateDetail(param));

        } else {
            map.put("RESULT", 0);
        }
        return map;
    }

    /**
     * for delete State
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int deleteDetail(Map param) throws Exception {
        return datamanager.deleteDetail(param);
    }
}
