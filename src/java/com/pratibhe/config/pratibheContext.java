/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.config;

import com.pratibhe.common.Const;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author SANDIPK
 */
public class pratibheContext implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        // On Application Startup, pleaseâ€¦
        System.out.println("-- -- App started -- --");

        WebApplicationContextUtils.getRequiredWebApplicationContext(servletContextEvent.getServletContext())
                .getAutowireCapableBeanFactory().autowireBean(this);

        Const.setAppPath(servletContextEvent.getServletContext().getRealPath("/"));

        //Const.SBOX = servletContextEvent.getServletContext().getInitParameter("SBOX") + "{TENANTID}/";
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        // On Application Shutdown, pleaseâ€¦
        System.out.println("-- -- App Shutting down -- --");
        // ... First close any background tasks which may be using the DB ...
        // ... Then close any DB connection pools ...

        // Now deregister JDBC drivers in this context's ClassLoader:
        // Get the webapp's ClassLoader
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        // Loop through all drivers
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            if (driver.getClass().getClassLoader() == cl) {
                // This driver was registered by the webapp's ClassLoader, so deregister it:
                try {
                    //log.info("Deregistering JDBC driver {}", driver);
                    System.out.println("Deregistering JDBC driver {}" + driver);
                    DriverManager.deregisterDriver(driver);
                } catch (SQLException ex) {
                    //log.error("Error deregistering JDBC driver {}", driver, ex);
                    System.out.println("Error deregistering JDBC driver {}" + driver + ex);
                }
            } else {
                // driver was not registered by the webapp's ClassLoader and may be in use elsewhere
                //log.trace("Not deregistering JDBC driver {} as it does not belong to this webapp's ClassLoader", driver);
                System.out.println("Not deregistering JDBC driver {} as it does not belong to this webapp's ClassLoader" + driver);
            }
        }
        System.out.println("-- -- App Shut down -- --");
    }

}
