/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.home;

import javax.servlet.http.HttpServletRequest;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author sandipk
 */
@Controller
@RequestMapping("/home")
public class HomeController {
    @Autowired
    HomeService service;

    @GetMapping(value = "")
    public String loadIndex(ModelMap model, HttpServletRequest request) throws JSONException {
        return "index";
    }
    
    @GetMapping(value = "home")
    public String loadHome(ModelMap model, HttpServletRequest request) throws JSONException {
        return "home";
    }
}
