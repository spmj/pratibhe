/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.profile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pratibhe.common.Const;
import com.pratibhe.common.SessionBean;
import com.pratibhe.common.Status;
import com.pratibhe.util.FileUpload;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jay
 */
@Controller
@RequestMapping("/profile")
public class ProfileController {

    private static final Logger LOG = Logger.getLogger(ProfileController.class);

    @Autowired
    ProfileService service;

    @Autowired
    SessionBean session;

    @GetMapping(value = "")
    public String loadAdd(ModelMap model, HttpServletRequest request) throws JSONException {
        model.addAttribute(Const.TASK, Const.TASK_ADD);
        return "profile/profile";
    }

    /**
     * For All Combo Load Data
     *
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/getAllCombo")
    public ResponseEntity<Map> getAllCombo() throws Exception {
        Map mCombos = service.getAllCombo();
        return new ResponseEntity<>(mCombos, HttpStatus.OK);
    }

    /**
     * get City
     *
     * @param id
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/getCity/{id}")
    public ResponseEntity<Map> getCity(@PathVariable int id) throws Exception {
        Map param = new HashMap();
        param.put("STATEID", String.valueOf(id));
        return new ResponseEntity<>(service.getCity(param), HttpStatus.OK);
    }

    /**
     * get Profile Data
     *
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/getProfileData")
    public ResponseEntity<Map> getProfileData() throws Exception {
        Map param = new HashMap();
        param.put("USERID", session.getUserId());
        param.put("TYPE", session.getUserType());
        return new ResponseEntity<>(service.getProfileData(param), HttpStatus.OK);
    }

    /**
     * Change Password
     *
     * @param id
     * @param jObj
     * @return
     * @throws java.lang.Exception
     */
    @PostMapping(value = "/updateProfile")
    public ResponseEntity<Status> updateProfile(@RequestParam(value = "file_profile", required = false) MultipartFile file_profile, HttpServletRequest request, @RequestParam(value = "formData") String jObj) throws Exception {
        Status s = new Status();
        ResponseEntity<Status> retResEnt;
        Map param = new ObjectMapper().readValue(jObj, Map.class);

        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sLocation = prop.getProperty("STORAGEBOX") + prop.getProperty("PROFILE_PATH");
        
        param.put("LOCATION", sLocation);
        param.put("FILE_PROFILE", file_profile);

        param.put("TYPE", session.getUserType());
        Map map = service.updateProfile(param);
        if (Integer.parseInt(map.get("RESULT").toString()) > 0) {
            s.setResponseCode(Const.MSG_SUCCESS_CODE);
            s.setResponseMessage(Const.MSG_UPDATE_SUCESS);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        } else {
            s.setResponseCode(Const.MSG_EXISTS_CODE);
            if (Integer.parseInt(map.get("RSLTEMAIL").toString()) > 0) {
                s.setResponseMessage("Email " + Const.MSG_EXISTS);
            }
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        }
        return retResEnt;
    }
    
    /**
     * Download Thumb File.
     *
     * @param id
     * @param res
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/getProfileImage/{id}")
    public void getProfileImage(@PathVariable("id") String id, HttpServletResponse res) throws Exception {
        try {
            InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
            Properties prop = new Properties();
            prop.load(input);
            Map param = new HashMap();
            param.put("USERID", id);
            Map mFileDetail = service.getFile(param);
            String sLocation = prop.getProperty("STORAGEBOX") + prop.getProperty("PROFILE_PATH") + "/" + mFileDetail.get("IMAGE").toString();
            FileUpload.downloadFileFromDisk(sLocation, res);
        } catch (Exception ex) {
            System.out.println("ex=" + ex.getMessage());
        }
    }


//    /**
//     * Change Password
//     *
//     * @param id
//     * @param jObj
//     * @return
//     * @throws java.lang.Exception
//     */
//    @PutMapping(value = "/{id}")
//    public ResponseEntity<Status> updateProfile(@PathVariable int id, @RequestBody String jObj) throws Exception {
//        Status s = new Status();
//        ResponseEntity<Status> retResEnt;
//        Map param = new ObjectMapper().readValue(jObj, Map.class);
//        param.put("TYPE", session.getUserType());
//        Map map = service.updateProfile(param);
//        if (Integer.parseInt(map.get("RESULT").toString()) > 0) {
//            s.setResponseCode(Const.MSG_SUCCESS_CODE);
//            s.setResponseMessage(Const.MSG_UPDATE_SUCESS);
//            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
//        } else {
//            s.setResponseCode(Const.MSG_EXISTS_CODE);
//            if (Integer.parseInt(map.get("RSLTEMAIL").toString()) > 0) {
//                s.setResponseMessage("Email " + Const.MSG_EXISTS);
//            }
//            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
//        }
//        return retResEnt;
//    }
    /**
     * Catch all Controller exception
     *
     * @param req
     * @param ex
     * @return
     */
    @ExceptionHandler({Exception.class})
    public ResponseEntity<Status> exceptionHandler(HttpServletRequest req, Exception ex) {
        Status error = new Status(500, ex.getMessage());
        LOG.error("Request: " + req.getRequestURL() + " raised " + ex);
        return new ResponseEntity<>(error, HttpStatus.OK);
    }
}
