/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.profile;

import com.pratibhe.common.Const;
import com.pratibhe.util.TranSqlServices;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jay
 */
@Repository
public class ProfileDataManager {

    @Autowired
    TranSqlServices objSQL;

    /**
     * TO GET State NAME
     *
     * @return
     * @throws Exception
     */
    public List getStateName() throws Exception {
        String sQuery = "SELECT STATEID VALUE, STATENAME TEXT"
                + " FROM STATE"
                + " WHERE ISACTIVE = 1"
                + " ORDER BY STATENAME";
        return objSQL.getList(sQuery);
    }

    /**
     * get Item Base On Item Group Name
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public List getCity(Map param) throws Exception {
        String sQuery = "SELECT CITYID VALUE,CITYNAME TEXT\n"
                + " FROM CITY\n"
                + " WHERE STATEID = :STATEID AND ISACTIVE = 1\n"
                + " ORDER BY CITYNAME";
        return objSQL.getList(sQuery, param);
    }

    /**
     * get Profile Data
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getProfileData(Map param) throws Exception {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT USERID, TYPE, NAME, ARTISTNAME, EMAIL, ");
        sb.append(" CONCAT('" + prop.getProperty("DOMAIN") + "','/profile/getProfileImage/',USERID) IMAGE, ");
        sb.append(" MOBILE, AADHARNO, GENDER, OCCUPATION, DOB, ");
        if (param.get("TYPE").equals("MENTOR")) {
            sb.append(" TYPEOFWORK, TENUREOFWORK,");
        }
        sb.append(" ADDRESS, CITYID, STATEID, ABOUTME, OTHER");
        sb.append(" FROM REGISTRATION ");
        sb.append(" WHERE  USERID = :USERID");
        return objSQL.getMap(sb.toString(), param);
    }
    
    /**
     * check Email exists or not
     *
     * @param param
     * @return
     */
    public Map availabilityUpdate(Map param) {
        String sBNameQuery;
        sBNameQuery = "SELECT COUNT(*)\n"
                + " FROM REGISTRATION\n"
                + " WHERE EMAIL = :EMAIL AND USERID != :USERID";
        param.put("RSLTEMAIL", objSQL.getInteger(sBNameQuery, param));
        return param;
    }

    /**
     * for update profile
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateProfile(Map param) throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append(" UPDATE REGISTRATION ");
        sb.append(" SET ");
        sb.append(" TYPE = :TYPE, NAME = :NAME, ARTISTNAME = :ARTISTNAME, ");
        sb.append(" EMAIL = :EMAIL, MOBILE = :MOBILE, AADHARNO = :AADHARNO, ");
        sb.append(" GENDER = :GENDER, OCCUPATION = :OCCUPATION, DOB = :DOB, ");
        sb.append(" ADDRESS = :ADDRESS, CITYID = :CITYID, STATEID = :STATEID, ");
        sb.append(" ABOUTME = :ABOUTME, OTHER = :OTHER, ");
        if (param.get("TYPE").equals("MENTOR")) {
            sb.append(" TYPEOFWORK = :TYPEOFWORK, TENUREOFWORK = :TENUREOFWORK, ");
        }
        sb.append(" EDITTIME = NOW() ");
        sb.append(" WHERE USERID = :USERID ");
        return objSQL.persist(sb.toString(), param);
    }
    
    
    /**
     * update profile Image
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateProfileImage(Map param) throws Exception {
        String sQuery = "UPDATE REGISTRATION"
                + " SET "
                + " IMAGE = :IMAGE"
                + " WHERE USERID = :USERID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * get File Path
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getFilePath(Map param) throws Exception {
        String sQuery = "SELECT USERID,IMAGE FROM REGISTRATION WHERE USERID=:USERID";
        return objSQL.getMap(sQuery, param);
    }
    

}
