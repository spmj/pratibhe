/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.profile;

import com.pratibhe.util.FileUpload;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jay
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ProfileService {

    @Autowired
    ProfileDataManager datamanager;

    /**
     * get All Combo
     *
     * @return
     * @throws Exception
     */
    public Map getAllCombo() throws Exception {
        Map mCombos = new HashMap();
        mCombos.put("STATENAME", datamanager.getStateName());
        return mCombos;
    }

    /**
     * get City
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getCity(Map param) throws Exception {
        Map mCombos = new HashMap();
        mCombos.put("CITYNAME", datamanager.getCity(param));
        return mCombos;
    }

    /**
     * get Profile Data
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getProfileData(Map param) throws Exception {
        return datamanager.getProfileData(param);
    }

    /**
     * for update profile
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map updateProfile(Map param) throws Exception {
        Map map = new HashMap();

        MultipartFile file_profile = (MultipartFile) param.get("FILE_PROFILE");

        map.put("USERID", String.valueOf(param.get("USERID")));
        map.put("EMAIL", String.valueOf(param.get("EMAIL")));
        map = datamanager.availabilityUpdate(map);
        if (Integer.parseInt(map.get("RSLTEMAIL").toString()) == 0) {
            Iterator it = param.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                param.replace(pair.getKey(), "", null);
            }

            int iResult = datamanager.updateProfile(param);
            if (iResult > 0) {

                Map sPath = datamanager.getFilePath(param);
                String iUserId = String.valueOf(param.get("USERID"));
                String extension = file_profile.getOriginalFilename().substring(file_profile.getOriginalFilename().lastIndexOf("."), file_profile.getOriginalFilename().length());
                String sThumbImage = iUserId + extension;
                param.replace("IMAGE", sThumbImage);
                param.put("USERID", iUserId);
                int iRes = datamanager.updateProfileImage(param);
                if (iRes > 0) {
                    String sThumbImg = param.get("LOCATION").toString() + sPath.get("IMAGE").toString();
                    FileUpload.deleteFileFromDisk(sThumbImg);
                    FileUpload.uploadFile(param.get("LOCATION").toString(), String.valueOf(iUserId), file_profile, true);
                }
            }
            map.put("RESULT", 1);

        } else {
            map.put("RESULT", 0);
        }
        return map;
    }

    public Map getFile(Map param) throws Exception {
        return datamanager.getFilePath(param);
    }
}
