/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.views;

import com.pratibhe.common.Const;
import com.pratibhe.common.SessionBean;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author SANDIPK
 */
@Controller
@RequestMapping("/mentor")
public class MentorController {

    @Autowired
    SessionBean sBean;

    @GetMapping(value = "/db")
    public String loadDb(ModelMap model, HttpServletRequest req) {
        model.addAttribute("TYPE", "MENTOR");
        return getPage("mentor/db/db");
    }

    @GetMapping(value = "/mapping")
    public String loadView(ModelMap model, HttpServletRequest request) throws JSONException {
        return getPage("mentor/mapping/mappingview");
    }

    @GetMapping(value = "/mapping/getContentPage")
    public String loadContentPage(ModelMap model, HttpServletRequest req) {
        return getPage("mentor/mapping/mappingcontent");
    }

    @GetMapping(value = "/student/add")
    public String loadUserAdd(ModelMap model, HttpServletRequest request) throws JSONException {
        model.addAttribute(Const.TASK, Const.TASK_ADD);
        return getPage("mentor/student/studentadd");
    }

    @GetMapping(value = "/student/edit")
    public String loadUserEdit(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("mentor/student/studentadd");
    }

    @GetMapping(value = "/student/view")
    public String loadUserView(ModelMap model, HttpServletRequest request) throws JSONException {
        return getPage("mentor/student/studentview");
    }

    @GetMapping(value = "/student/getContentPage")
    public String loadUserContentPage(ModelMap model, HttpServletRequest req) {
        return getPage("mentor/student/studentcontent");
    }

    @GetMapping(value = "/event/add")
    public String loadEventAdd(ModelMap model, HttpServletRequest request) throws JSONException {
        model.addAttribute(Const.TASK, Const.TASK_ADD);
        return getPage("mentor/event/eventadd");
    }

    @GetMapping(value = "/event/edit")
    public String loadEventEdit(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("mentor/event/eventadd");
    }

    @GetMapping(value = "/event/view")
    public String loadEventView(ModelMap model, HttpServletRequest request) throws JSONException {
        return getPage("mentor/event/eventview");
    }

    @GetMapping(value = "/event/getContentPage")
    public String loadEventContentPage(ModelMap model, HttpServletRequest req) {
        return getPage("mentor/event/eventcontent");
    }

    @GetMapping(value = "/work/add")
    public String loadWorkAdd(ModelMap model, HttpServletRequest request) throws JSONException {
        model.addAttribute(Const.TASK, Const.TASK_ADD);
        return getPage("mentor/work/workadd");
    }

    @GetMapping(value = "/work/edit")
    public String loadWorkEdit(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("mentor/work/workadd");
    }

    @GetMapping(value = "/work/view")
    public String loadWorkView(ModelMap model, HttpServletRequest request) throws JSONException {
        return getPage("mentor/work/workview");
    }

    @GetMapping(value = "/work/getContentPage")
    public String loadWorkContentPage(ModelMap model, HttpServletRequest req) {
        return getPage("mentor/work/workcontent");
    }

    @GetMapping(value = "/happenings/view")
    public String loadHappeningsView(ModelMap model, HttpServletRequest request) throws JSONException {
        return getPage("mentor/happenings/happeningsview");
    }

    @GetMapping(value = "/happenings/edit")
    public String loadHappeningsEdit(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("mentor/happenings/happeningsadd");
    }

    @GetMapping(value = "/happenings/getContentPage")
    public String loadHappeningsContentPage(ModelMap model, HttpServletRequest req) {
        return getPage("mentor/happenings/happeningscontent");
    }
    
    @GetMapping(value = "/user/work")
    public String loadUserWork(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("admin/users/userwork");
    }
    
    @GetMapping(value = "/user/workdetail")
    public String loadUserWorkDetail(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("admin/users/userworkdetail");
    }
    
    @GetMapping(value = "/inquiry/view")
    public String loadInquiryView(ModelMap model, HttpServletRequest request) throws JSONException {
        return getPage("student/inquiry/inquiryview");
    }

    public String getPage(String sPage) {
        if (sBean.getUserType() != null && sBean.getUserType().equals("MENTOR")) {
            return sPage;
        } else {
            return "404";
        }
    }

}
