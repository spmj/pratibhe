/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.views;

import com.pratibhe.common.Const;
import com.pratibhe.common.SessionBean;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author SANDIPK
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    SessionBean sBean;

    @GetMapping(value = "/db")
    public String loadDb(ModelMap model, HttpServletRequest req) {
        model.addAttribute("TYPE", "ADMIN");
        return getPage("admin/db/db");
    }

    @GetMapping(value = "/art/add")
    public String loadArtAdd(ModelMap model, HttpServletRequest request) throws JSONException {
        model.addAttribute(Const.TASK, Const.TASK_ADD);
        return getPage("admin/art/artadd");
    }

    @GetMapping(value = "/art/edit")
    public String loadArtEdit(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("admin/art/artadd");
    }

    @GetMapping(value = "/art/view")
    public String loadArtView(ModelMap model, HttpServletRequest request) throws JSONException {
        return getPage("admin/art/artview");
    }

    @GetMapping(value = "/art/getContentPage")
    public String loadArtContentPage(ModelMap model, HttpServletRequest req) {
        return getPage("admin/art/artcontent");
    }

    @GetMapping(value = "/state/add")
    public String loadStateAdd(ModelMap model, HttpServletRequest request) throws JSONException {
        model.addAttribute(Const.TASK, Const.TASK_ADD);
        return getPage("admin/state/stateadd");
    }

    @GetMapping(value = "/state/edit")
    public String loadStateEdit(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("admin/state/stateadd");
    }

    @GetMapping(value = "/state/view")
    public String loadStateView(ModelMap model, HttpServletRequest request) throws JSONException {
        return getPage("admin/state/stateview");
    }

    @GetMapping(value = "/state/getContentPage")
    public String loadStateContentPage(ModelMap model, HttpServletRequest req) {
        return getPage("admin/state/statecontent");
    }

    public String getPage(String sPage) {
        if (sBean.getUserType() != null && sBean.getUserType().equals("ADMIN")) {
            return sPage;
        } else {
            return "404";
        }
    }

    @GetMapping(value = "/users/add")
    public String loadUserAdd(ModelMap model, HttpServletRequest request) throws JSONException {
        model.addAttribute(Const.TASK, Const.TASK_ADD);
        return getPage("admin/users/usersadd");
    }

    @GetMapping(value = "/users/edit")
    public String loadUserEdit(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("admin/users/usersadd");
    }

    @GetMapping(value = "/users/view")
    public String loadUserView(ModelMap model, HttpServletRequest request) throws JSONException {
        return getPage("admin/users/usersview");
    }

    @GetMapping(value = "/users/getContentPage")
    public String loadUserContentPage(ModelMap model, HttpServletRequest req) {
        return getPage("admin/users/userscontent");
    }

    @GetMapping(value = "/eventtype/add")
    public String loadEventtypeAdd(ModelMap model, HttpServletRequest request) throws JSONException {
        model.addAttribute(Const.TASK, Const.TASK_ADD);
        return getPage("admin/eventtype/eventtypeadd");
    }

    @GetMapping(value = "/eventtype/edit")
    public String loadEventtypeEdit(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("admin/eventtype/eventtypeadd");
    }

    @GetMapping(value = "/eventtype/view")
    public String loadEventtypeView(ModelMap model, HttpServletRequest request) throws JSONException {
        return getPage("admin/eventtype/eventtypeview");
    }

    @GetMapping(value = "/eventtype/getContentPage")
    public String loadEventtypeContentPage(ModelMap model, HttpServletRequest req) {
        return getPage("admin/eventtype/eventtypecontent");
    }

    @GetMapping(value = "/events/add")
    public String loadEventsAdd(ModelMap model, HttpServletRequest request) throws JSONException {
        model.addAttribute(Const.TASK, Const.TASK_ADD);
        return getPage("admin/events/eventsadd");
    }

    @GetMapping(value = "/events/edit")
    public String loadEventsEdit(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("admin/events/eventsadd");
    }

    @GetMapping(value = "/events/view")
    public String loadEventsView(ModelMap model, HttpServletRequest request) throws JSONException {
        return getPage("admin/events/eventsview");
    }

    @GetMapping(value = "/events/getContentPage")
    public String loadEventsContentPage(ModelMap model, HttpServletRequest req) {
        return getPage("admin/events/eventscontent");
    }

    @GetMapping(value = "/happenings/add")
    public String loadHappeningsAdd(ModelMap model, HttpServletRequest request) throws JSONException {
        model.addAttribute(Const.TASK, Const.TASK_ADD);
        return getPage("admin/happenings/happeningsadd");
    }

    @GetMapping(value = "/happenings/edit")
    public String loadHappeningsEdit(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("admin/happenings/happeningsadd");
    }

    @GetMapping(value = "/happenings/view")
    public String loadHappeningsView(ModelMap model, HttpServletRequest request) throws JSONException {
        return getPage("admin/happenings/happeningsview");
    }

    @GetMapping(value = "/happenings/getContentPage")
    public String loadHappeningsContentPage(ModelMap model, HttpServletRequest req) {
        return getPage("admin/happenings/happeningscontent");
    }

    @GetMapping(value = "/city/add")
    public String loadCityAdd(ModelMap model, HttpServletRequest request) throws JSONException {
        model.addAttribute(Const.TASK, Const.TASK_ADD);
        return getPage("admin/city/cityadd");
    }

    @GetMapping(value = "/city/edit")
    public String loadCityEdit(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("admin/city/cityadd");
    }

    @GetMapping(value = "/city/view")
    public String loadCityView(ModelMap model, HttpServletRequest request) throws JSONException {
        return getPage("admin/city/cityview");

    }

    @GetMapping(value = "/city/getContentPage")
    public String loadCityContentPage(ModelMap model, HttpServletRequest req) {
        return getPage("admin/city/citycontent");
    }

    @GetMapping(value = "/user/work")
    public String loadUserWork(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("admin/users/userwork");
    }

    @GetMapping(value = "/user/workdetail")
    public String loadUserWorkDetail(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("admin/users/userworkdetail");
    }

    @GetMapping(value = "/pages/add")
    public String loadPagesAdd(ModelMap model, HttpServletRequest request) throws JSONException {
        model.addAttribute(Const.TASK, Const.TASK_ADD);
        return getPage("admin/pages/pagesadd");
    }

    @GetMapping(value = "/pages/edit")
    public String loadPagesEdit(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("admin/pages/pagesadd");
    }

    @GetMapping(value = "/pages/view")
    public String loadPagesView(ModelMap model, HttpServletRequest request) throws JSONException {
        return getPage("admin/pages/pagesview");

    }

    @GetMapping(value = "/pages/getContentPage")
    public String loadPagesContentPage(ModelMap model, HttpServletRequest req) {
        return getPage("admin/pages/pagescontent");
    }
}
