/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.views;

import com.pratibhe.common.Const;
import com.pratibhe.common.SessionBean;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author SANDIPK
 */
@Controller
@RequestMapping("/student")
public class StudentController {

    @Autowired
    SessionBean sBean;

    @GetMapping(value = "/db")
    public String loadDb(ModelMap model, HttpServletRequest req) {
        model.addAttribute("TYPE", "STUDENT");
        return getPage("student/db/db");
    }

    @GetMapping(value = "/happenings/view")
    public String loadHappeningsView(ModelMap model, HttpServletRequest request) throws JSONException {
        return getPage("student/happenings/happeningsview");
    }

    @GetMapping(value = "/happenings/edit")
    public String loadHappeningsEdit(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("student/happenings/happeningsadd");
    }

    @GetMapping(value = "/happenings/getContentPage")
    public String loadHappeningsContentPage(ModelMap model, HttpServletRequest req) {
        return getPage("student/happenings/happeningscontent");
    }

    @GetMapping(value = "/work/add")
    public String loadWorkAdd(ModelMap model, HttpServletRequest request) throws JSONException {
        model.addAttribute(Const.TASK, Const.TASK_ADD);
        return getPage("student/work/workadd");
    }

    @GetMapping(value = "/work/edit")
    public String loadWorkEdit(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("student/work/workadd");
    }

    @GetMapping(value = "/work/view")
    public String loadWorkView(ModelMap model, HttpServletRequest request) throws JSONException {
        return getPage("student/work/workview");
    }

    @GetMapping(value = "/work/getContentPage")
    public String loadWorkContentPage(ModelMap model, HttpServletRequest req) {
        return getPage("student/work/workcontent");
    }

    @GetMapping(value = "/user/workdetail")
    public String loadUserWorkDetail(ModelMap model, HttpServletRequest req) {
        model.addAttribute(Const.TASK, Const.TASK_EDIT);
        return getPage("admin/users/userworkdetail");
    }
    
    public String getPage(String sPage) {
        if (sBean.getUserType() != null && sBean.getUserType().equals("STUDENT")) {
            return sPage;
        } else {
            return "404";
        }
    }
}
