/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.views;

import com.pratibhe.client.ClientService;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Mayur
 */
@Controller
@RequestMapping("/")
public class ClientsController {

    @Autowired
    ClientService service;

    @GetMapping(value = "")
    public String loadIndex(ModelMap model, HttpServletRequest req) throws Exception {
        model.addAttribute("EVENTBNRDATA", service.getEventBnr());
        Map param = new HashMap();
        param.put("SLIMIT", 14);
        model.addAttribute("ARTISTSDATA", service.getArtistsData(param));
        model.addAttribute("HOMEHAPPENINGS", service.getHomePageHappenings());
        model.addAttribute("FETWORKDATA", service.getFeaturedWork());
        model.addAttribute("FETMUSICDATA", service.getFeaturedMusic());
        return "client/index";
    }

    @GetMapping(value = "/client/happenings")
    public String loadHappenings(ModelMap model, HttpServletRequest req) throws Exception {
        model.addAttribute("ARTFILTER", service.artFilterData());
        model.addAttribute("EVENTTYPEFILTER", service.evetntFilterData());
        return "client/happenings";
    }

    @GetMapping(value = "/client/programmes")
    public String loadProgrammes(ModelMap model, HttpServletRequest req) throws Exception {
        model.addAttribute("EVENTBNRDATA", service.getEventBnr());
        model.addAttribute("ARTFILTER", service.artFilterData());
        model.addAttribute("EVENTTYPEFILTER", service.evetntFilterData());
        return "client/programmes";
    }

    @GetMapping(value = "/mentorship")
    public String loadMentorship(ModelMap model, HttpServletRequest req) throws Exception {
        return "client/mentorship";
    }

    @GetMapping(value = "/artist")
    public String loadArtistPage(ModelMap model, HttpServletRequest req) throws Exception {
        return "client/artist";
    }
    
    @GetMapping(value = "/gallery")
    public String loadGalleryPage(ModelMap model, HttpServletRequest req) throws Exception {
        model.addAttribute("ARTFILTER", service.galleryArtFilterData());
        model.addAttribute("ARTISTFILTER", service.galleryArtistFilterData());
        return "client/gallery";
    }

    @GetMapping(value = "/error404")
    public String error404(ModelMap model, HttpServletRequest req) throws Exception {
        return "error404";
    }

}
