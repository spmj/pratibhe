// Project : LoginDataManager.java
// Created : 8 Jun, 2017 5:34:12 PM
// Author  : Sandip Kotadiya
package com.pratibhe.login;

import com.pratibhe.util.TranSqlServices;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sandip kotadiya
 */
@Repository
public class LoginDataManager {

    @Autowired
    TranSqlServices objSQL;

    /**
     * SignIn
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map signIn(Map param) throws Exception {
        System.out.println("param" + param.toString());
        //Query for check credential.
        String sQuery = "SELECT COUNT(*) FROM REGISTRATION WHERE ( MOBILE=:USERNAME OR EMAIL=:USERNAME ) AND PASSWORD=:PWD AND ISACTIVE=1";
        int iCnt = objSQL.getInteger(sQuery, param);
        System.out.println("iCnt=" + iCnt);
        if (iCnt > 0) {//if credential authenticate then record found
            sQuery = "SELECT USERID,NAME,EMAIL,TYPE FROM REGISTRATION WHERE ( MOBILE=:USERNAME OR EMAIL=:USERNAME ) AND PASSWORD=:PWD AND ISACTIVE=1";
            param.putAll(objSQL.getMap(sQuery, param));
            param.put("RESULT", iCnt);
        } else {
            param.put("RESULT", 0);
        }
        return param;
    }

    /**
     * check user.
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map checkUser(Map param) throws Exception {
        String sQuery = "SELECT COUNT(*) FROM REGISTRATION WHERE EMAIL=:EMAIL AND ISACTIVE=1";
        int i = objSQL.getInteger(sQuery, param);
        if (i > 0) {
            sQuery = "SELECT COUNT(*) RESULT,USERID,NAME,EMAIL FROM REGISTRATION WHERE EMAIL=:EMAIL AND ISACTIVE=1";
            param = objSQL.getMap(sQuery, param);
        } else {
            param.put("RESULT", 0);
        }
        return param;
    }

    /**
     * refresh activation code
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int refreshCode(Map param) throws Exception {
        String sQuery = "UPDATE REGISTRATION"
                + " SET "
                + " ACTIVATIONCODE = :ACTIVATIONCODE"
                + " WHERE EMAIL=:EMAIL AND ISACTIVE=1";
        return objSQL.persist(sQuery, param);
    }

    /**
     * verify user for change password.
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int verifyForCP(Map param) throws Exception {
        String sQuery = "SELECT COUNT(*) FROM REGISTRATION WHERE ACTIVATIONCODE=:ACTIVATIONCODE AND USERID=:USERID";
        return objSQL.getInteger(sQuery, param);
    }

    /**
     * change forgot password
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int changeFgPwd(Map param) throws Exception {
        String sQuery = "UPDATE REGISTRATION"
                + " SET "
                + " PASSWORD = :NEWPWD"
                + " WHERE ACTIVATIONCODE=:ACTIVATIONCODE AND USERID=:USERID";
        return objSQL.persist(sQuery, param);
    }

}
