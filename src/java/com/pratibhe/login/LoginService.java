// Project : LoginService.java
// Created : 8 Jun, 2017 5:34:02 PM
// Author  : Sandip Kotadiya
package com.pratibhe.login;

import com.pratibhe.common.CommonManager;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sandip kotadiya
 */
@Service
@Transactional
public class LoginService {

    @Autowired
    LoginDataManager datamanager;

    @Autowired
    CommonManager commonmanager;

    public Map signIn(Map param) throws Exception {
        return datamanager.signIn(param);
    }

    public Map checkUser(Map param) throws Exception {
        return datamanager.checkUser(param);
    }
    
    public int sendMail(Map param) throws Exception{
        param.put("ACTIVATIONCODE", UUID.randomUUID().toString());
        datamanager.refreshCode(param);
        CommonManager.SendMail(param);
        //code for send mail
        return 1;
    }

    public int verifyForCP(Map param) throws Exception {
        return datamanager.verifyForCP(param);
    }
    
    public int changeFgPwd(Map param) throws Exception{
        return datamanager.changeFgPwd(param);
    }
    
    

}
