// Project : LoginController.java
// Created : 8 Jun, 2017 5:33:49 PM
// Author  : Sandip Kotadiya
package com.pratibhe.login;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pratibhe.common.Const;
import com.pratibhe.common.SessionBean;
import com.pratibhe.common.Status;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Sandip kotadiya
 */
@Controller
@RequestMapping("/")
public class LoginController {

    @Autowired
    LoginService service;

    @Autowired
    SessionBean sBean;

    @GetMapping(value = "login")
    public String loadLogin(ModelMap model) {
        return "index";
    }

    @GetMapping(value = "dashboard")
    public String loadIndex(ModelMap model) {
        if (sBean != null) {
            if (sBean.getUserName() != null && !"".equals(sBean.getUserName())) {
                model.addAttribute("TYPE", sBean.getUserType());
                return "home"; // send to main page
            }
        }
        return "redirect:login"; // return to home page
    }

//    private static final Logger LOG = Logger.getLogger(LoginController.class);
    @GetMapping(value = "/page")
    public String loadView(ModelMap model, HttpServletRequest req) {
        return "profile/loginpage";
    }

    @GetMapping(value = "/forgotpassword")
    public String loadForgotPwd(ModelMap model, HttpServletRequest req) {
        return "forgotpwd";
    }

    @PostMapping(value = "/signin")
    public ResponseEntity<Status> signIn(@RequestBody String jObj) throws Exception {
        try {
            Status s = new Status();
            ResponseEntity<Status> retResEnt;
            Map param = new ObjectMapper().readValue(jObj, Map.class);
            int i;
            param = service.signIn(param);
            i = Integer.parseInt(param.get("RESULT").toString());
            if (i == 1) {
                s.setResponseCode(Const.MSG_SUCCESS_CODE);
                s.setResponseMessage(Const.MSG_LOGIN_SUCCESS);

                sBean.setUserId(param.get("USERID").toString());
                sBean.setUserName(param.get("NAME").toString());
                sBean.setUserType(param.get("TYPE").toString());
//                sBean.setUserId(param.get("USERID").toString());
//                sBean.setUserName(param.get("USERNAME").toString());

                retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
            } else {
                s.setResponseCode(Const.MSG_FAIL_CODE);
                s.setResponseMessage(Const.MSG_LOGIN_FAIL);
                retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
            }
            System.out.println("session " + sBean.toString());
            return retResEnt;
        } catch (Exception ex) {
            System.out.println("ex=" + ex.toString());
            return null;
        }
    }

    /**
     * activate user.
     *
     * @param userid
     * @param activationcode
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/changepassword/{userid}/{activationcode}")
    public String activateuser(@PathVariable String userid, @PathVariable String activationcode, ModelMap model) throws Exception {
        Map param = new HashMap();
        param.put("USERID", userid);
        param.put("ACTIVATIONCODE", activationcode);
        model.addAttribute("activationcode", activationcode);
        model.addAttribute("userid", userid);
        int isVerify = service.verifyForCP(param);
        if (isVerify > 0) {
            return "changefgpwd";
        }
        return "home";
    }

    /**
     * activate user.
     *
     * @param jObj
     * @return
     * @throws java.lang.Exception
     */
    @PostMapping(value = "/changefgpwd")
    public ResponseEntity<Status> changefgpwd(@RequestBody String jObj) throws Exception {
        Status s = new Status();
        ResponseEntity<Status> retResEnt;
        Map param = new ObjectMapper().readValue(jObj, Map.class);
        int iRes = service.changeFgPwd(param);
        if (iRes > 0) {
            s.setResponseCode(Const.MSG_SUCCESS_CODE);
            s.setResponseMessage("Password Change Succesfully");
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        } else {
            s.setResponseCode(Const.MSG_FAIL_CODE);
            s.setResponseMessage("Password not changed");
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        }
        return retResEnt;
    }

    @PostMapping(value = "/checkuser")
    public ResponseEntity<Status> checkUser(@RequestBody String jObj) throws Exception {
        try {
            Status s = new Status();
            ResponseEntity<Status> retResEnt;
            Map param = new ObjectMapper().readValue(jObj, Map.class);
            param = service.checkUser(param);
            if (Integer.parseInt(param.get("RESULT").toString()) > 0) {
                param.put("FORGOTPWD", true);
                if (service.sendMail(param) > 0) {
                    s.setResponseCode(Const.MSG_SUCCESS_CODE);
                    s.setResponseMessage("Link has been send to your registered mail id");
                    retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
                } else {
                    s.setResponseCode(Const.MSG_FAIL_CODE);
                    s.setResponseMessage("Something went wrong.");
                    retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
                }
            } else {
                s.setResponseCode(Const.MSG_FAIL_CODE);
                s.setResponseMessage("User not registered.");
                retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
            }
            return retResEnt;
        } catch (Exception ex) {
            System.out.println("ex=" + ex.toString());
            return null;
        }
    }

    /**
     * Logout
     *
     * @param request
     * @param model
     * @return
     */
    @GetMapping(value = "/logout")
    public ResponseEntity<Boolean> logout(HttpServletRequest request, ModelMap model) {
        HttpSession session = request.getSession(false);
        Boolean logoutFlag = false;
        if (session != null) {
            session.invalidate();
            logoutFlag = true;
        }
        return new ResponseEntity<>(logoutFlag, HttpStatus.OK);
    }

    /**
     * Catch all Controller exception
     *
     * @param req
     * @param ex
     * @return
     */
    @ExceptionHandler({Exception.class})
    public ResponseEntity<Status> exceptionHandler(HttpServletRequest req, Exception ex) {
        Status error = new Status(500, Const.MSG_ERROR);
        System.out.println("Exception : " + ex.toString());
//        LOG.error("Request: " + req.getRequestURL() + " raised " + ex);
        return new ResponseEntity<>(error, HttpStatus.OK);
    }

}
