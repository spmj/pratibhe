/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.client;

import com.pratibhe.profile.ProfileDataManager;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jay
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ClientService {

    @Autowired
    ClientDataManager datamanager;

    @Autowired
    ProfileDataManager profiledatamanager;

    public List getFeaturedWork() throws Exception {
        return datamanager.getFeaturedWork();
    }

    public List getFeaturedMusic() throws Exception {
        return datamanager.getFeaturedMusic();
    }

    public List getEventBnr() throws Exception {
        return datamanager.getEventBnr();
    }

    public List getHapBnr() throws Exception {
        return datamanager.getHapBnr();
    }

    public List getArtistsData(Map param) throws Exception {
        return datamanager.getArtistsData(param);
    }

    /**
     * get register image.
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getRegImg(Map param) throws Exception {
        return datamanager.getRegImg(param);
    }

    /**
     * get FEATURE WORK image.
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getWorkImg(Map param) throws Exception {
        return datamanager.getWorkImg(param);
    }

    public List getHomePageHappenings() throws Exception {
        return datamanager.getHomePageHappenings();
    }

    public Map getHomePageHappeningsDetail(Map param) throws Exception {
        param = datamanager.getHomePageHappeningsDetail(param);
        param.put("MOREHAPPENINGS", datamanager.moreHappeningDetails(param));
//        System.out.println("param = > "+param);
        return param;
    }

    public Map getHomePageEventDetails(Map param) throws Exception {
        param = datamanager.getHomePageEventDetails(param);
        param.put("MOREEVENTS", datamanager.moreEventDetails(param));
        return param;
    }

    public Map getHomePageArtistsDetails(Map param) throws Exception {
        Map map = datamanager.getHomePageArtistsDetails(param);
        map.put("ARTDATA", datamanager.getSubArtOfArtistDetail(param));
        return map;
    }

    public Map getHomePageArtworkDetails(Map param) throws Exception {
        return datamanager.getHomePageArtworkDetails(param);
    }

    public List artFilterData() {
        return datamanager.artFilterData();
    }

    public List evetntFilterData() {
        return datamanager.evetntFilterData();
    }

    public List getFilterHappeningDetails(Map param) throws FileNotFoundException, IOException {
        return datamanager.getFilterHappeningDetails(param);
    }

    public int getTotalBDData(Map param) throws FileNotFoundException, IOException, Exception {
        return datamanager.getTotalBDData(param);
    }

    public int getTotalArtistData(Map param) throws FileNotFoundException, IOException, Exception {
        return datamanager.getTotalArtistData(param);
    }

    /**
     * insert happings part
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map insertDetail(Map param) throws Exception {
        Map map = new HashMap();
        map.put("HAPPENINGSID", String.valueOf(param.get("HAPPENINGSID")));
        map.put("USERID", String.valueOf(param.get("USERID")));
        map = datamanager.checkNoOfSlot(map);
        map = datamanager.availabilityInsert(map);
        if (Integer.parseInt(map.get("RSLTCNTPART").toString()) == 0) {
            map.put("RESULTCNT", 0);
            return map;
        }
        if (Integer.parseInt(map.get("RSLTPART").toString()) == 0) {
            int refId = datamanager.insertDetail(param);
            map.put("RESULT", refId);
        } else {
            map.put("RESULT", 0);
        }

        return map;
    }

    public List getFilterEventDetails(Map param) throws FileNotFoundException, IOException {
        return datamanager.getFilterEventDetails(param);
    }

    public int getTotalEventData(Map param) throws FileNotFoundException, IOException, Exception {
        return datamanager.getTotalEventData(param);
    }
    
    public int getTotalGalleryData(Map param) throws FileNotFoundException, IOException, Exception {
        return datamanager.getTotalGalleryData(param);
    }

    /**
     * insert events part
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map insertPartEvent(Map param) throws Exception {
        Map map = new HashMap();
        map = datamanager.availabilityInsertPartEvent(map);
        if (Integer.parseInt(map.get("RSLTPART").toString()) == 0) {
            int refId = datamanager.insertPartEvent(param);
            map.put("RESULT", refId);
        } else {
            map.put("RESULT", 0);
        }

        return map;
    }

    public List getCity(Map param) throws Exception {
        return profiledatamanager.getCity(param);
    }

    public List getStateName() throws Exception {
        return profiledatamanager.getStateName();
    }

    /**
     * insert events part
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map insertArtistInquiry(Map param) throws Exception {
        Iterator it = param.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            param.replace(pair.getKey(), "", null);
        }
        int refId = datamanager.insertArtistInquiry(param);
        if (refId > 0) {
            param.put("RESULT", refId);
        } else {
            param.put("RESULT", 0);
        }
        return param;
    }

    public List getGalleryData(Map param) throws Exception {
        return datamanager.getGalleryData(param);
    }

    public List getSubFeaturedWork(Map param) throws Exception {
        return datamanager.getSubFeaturedWork(param);
    }

    public String getAboutUs() throws Exception {
        return datamanager.getAboutUs();
    }
    
    public String getPartners() throws Exception {
        return datamanager.getPartners();
    }
    
    public List galleryArtFilterData() {
        return datamanager.galleryArtFilterData();
    }
    
    public List galleryArtistFilterData() {
        return datamanager.galleryArtistFilterData();
    }
}
