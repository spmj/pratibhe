/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.client;

import com.pratibhe.common.Const;
import com.pratibhe.util.TranSqlServices;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jay
 */
@Repository
public class ClientDataManager {

    @Autowired
    TranSqlServices objSQL;

    public List getFeaturedWork() throws Exception {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sQuery = " SELECT CONCAT('" + prop.getProperty("DOMAIN") + "','/client/getWorkImg/',USERWORKID) FILE,"
                + " UW.USERWORKID, UW.DESCRIPTION,UW.ADDTIME,UW.ISFETURED,A.ARTNAME,R.NAME  "
                + " FROM USERWORK UW "
                + " INNER JOIN ART A ON A.ARTID = UW.ARTID "
                + " INNER JOIN REGISTRATION R ON R.USERID = UW.USERID "
                + " WHERE UW.ISFETURED = 1 AND A.ARTID NOT IN (2)"
                + " LIMIT 8";
        System.out.println("getFeaturedWork => " + Const.testQuery(sQuery, new HashMap()));
        return objSQL.getList(sQuery);
    }

    public List getFeaturedMusic() throws Exception {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sQuery = " SELECT CONCAT('" + prop.getProperty("DOMAIN") + "','/client/getWorkImg/',USERWORKID) FILE,UW.WORKNAME,"
                + " UW.USERWORKID, UW.DESCRIPTION,UW.ADDTIME,UW.ISFETURED,A.ARTNAME,R.NAME  "
                + " FROM USERWORK UW "
                + " INNER JOIN ART A ON A.ARTID = UW.ARTID "
                + " INNER JOIN REGISTRATION R ON R.USERID = UW.USERID "
                + " WHERE UW.ISFETURED = 1 AND A.ARTID IN (2)"
                + " LIMIT 8";
        System.out.println("getFeaturedMusic => " + Const.testQuery(sQuery, new HashMap()));
        return objSQL.getList(sQuery);
    }

    public List getEventBnr() throws Exception {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sQuery = "SELECT EVENTID,TITLE,DETAILS,THUMBIMG, CONCAT('" + prop.getProperty("DOMAIN") + "','/events/getBanner/',EVENTID) BANNER, "
                + " DATE_FORMAT(STARTDATE,'%d %b') STARTDATE, "
                + " DATE_FORMAT(ENDDATE,'%d %b') ENDDATE,ISAPPROVED\n"
                + " FROM EVENT "
                + " WHERE ISAPPROVED='Approved' AND (STARTDATE >= CURDATE() OR ENDDATE >= CURDATE())"
                + " ORDER   BY STARTDATE DESC LIMIT 3";
        return objSQL.getList(sQuery);
    }

    public List getHapBnr() throws Exception {
        String sQuery = " SELECT TITLE,DETAILS,THUMBIMG, BANNER, STARTDATE, ENDDATE  "
                + " FROM HAPPENINGS "
                + " WHERE ISBANNER = 1 ";
        return objSQL.getList(sQuery);
    }

    public List getArtistsData(Map param) throws Exception {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sQuery = "SELECT USERID,TYPE,NAME,ARTISTNAME, "
                + " CONCAT('" + prop.getProperty("DOMAIN") + "','/client/getRegImg/',USERID) IMAGE, "
                + " ISFETURED,ISAPPROVED\n"
                + " FROM REGISTRATION "
                + " WHERE ISFETURED = 1 AND ISAPPROVED='Approved' ";
        if (param.get("LIMIT") != null && !param.get("LIMIT").toString().equalsIgnoreCase("")) {
            if ((Integer) param.get("SLIMIT") == 18) {
                sQuery += " LIMIT " + param.get("SLIMIT") + "";
            } else {
                sQuery += " LIMIT " + param.get("LIMIT") + ", 18";
            }
        } else {
            sQuery += " LIMIT " + param.get("SLIMIT") + "";
        }
        System.out.println("getArtistData => " + Const.testQuery(sQuery, param));
        return objSQL.getList(sQuery);
    }

    /**
     * get register Image.
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getRegImg(Map param) throws Exception {
        String sQuery = "SELECT USERID,IMAGE FROM REGISTRATION WHERE USERID=:USERID";
        return objSQL.getMap(sQuery, param);
    }

    /**
     * get FEATURE WORK Image.
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getWorkImg(Map param) throws Exception {
        String sQuery = "SELECT USERWORKID,FILE FROM USERWORK WHERE USERWORKID=:USERWORKID";
        return objSQL.getMap(sQuery, param);
    }

    public List getHomePageHappenings() throws Exception {

        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sQuery = "SELECT HAPPENINGSID,TITLE, "
                + " CONCAT('" + prop.getProperty("DOMAIN") + "','/happenings/getBannerThumb/',HAPPENINGSID) THUMBIMG,"
                + " BANNER, DATE_FORMAT(STARTDATE,'%d %b') STARTDATE, ENDDATE \n"
                + " FROM HAPPENINGS  ORDER   BY STARTDATE DESC LIMIT 3";
        return objSQL.getList(sQuery);
    }

    /**
     * get events
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getHomePageHappeningsDetail(Map param) throws Exception {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);

        String sQuery = "SELECT HAPPENINGSID, TITLE, SUMMERY, DETAILS, ADDRESS, "
                + " STATEID, CITYID, ARTID, "
                + " EVENTTYPEID, CONCAT('" + prop.getProperty("DOMAIN") + "','/happenings/getBannerThumb/',HAPPENINGSID) THUMBIMG,"
                + " CONCAT('" + prop.getProperty("DOMAIN") + "','/happenings/getBanner/',HAPPENINGSID) BANNER, "
                + " DATE_FORMAT(STARTDATE,'%d-%b-%Y') STARTDATE, DATE_FORMAT(ENDDATE,'%d-%b-%Y') ENDDATE, "
                + " CONTACTNAME, CONTACTEMAIL, NOOFSLOT "
                + " FROM HAPPENINGS "
                + " WHERE HAPPENINGSID = :HAPPENINGSID ";
        System.out.println("");
        return objSQL.getMap(sQuery, param);
    }

    /**
     * get events
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getHomePageEventDetails(Map param) throws Exception {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sQuery = "SELECT EVENTID, TITLE, SUMMERY, DETAILS, ADDRESS,  ARTID, C.CITYNAME, S.STATENAME,"
                + " EVENTTYPEID,CONCAT('" + prop.getProperty("DOMAIN") + "','/events/getBannerThumb/',"
                + " EVENTID) THUMBIMG,"
                + " CONCAT('" + prop.getProperty("DOMAIN") + "','/events/getBanner/',EVENTID) BANNER, "
                + " DATE_FORMAT(STARTDATE,'%d-%b-%Y') STARTDATE, DATE_FORMAT(ENDDATE,'%d-%b-%Y') ENDDATE, "
                + " CONTACTNAME, CONTACTEMAIL, ISAPPROVED, ISADMIN "
                + " FROM EVENT E"
                + " INNER JOIN CITY C ON C.CITYID = E.CITYID"
                + " INNER JOIN STATE S ON S.STATEID = E.STATEID"
                + " WHERE EVENTID = :EVENTID ";
        return objSQL.getMap(sQuery, param);
    }

    /**
     * get artists
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getHomePageArtistsDetails(Map param) throws Exception {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sQuery = "SELECT USERID,TYPE,NAME,ARTISTNAME,EMAIL,MOBILE,AADHARNO,GENDER,OCCUPATION,DOB,"
                + " ADDRESS, C.CITYNAME, S.STATENAME,R.ABOUTME,"
                + " CONCAT('" + prop.getProperty("DOMAIN") + "','/client/getRegImg/',USERID) IMAGE "
                + " FROM REGISTRATION R"
                + " INNER JOIN CITY C ON C.CITYID = R.CITYID"
                + " INNER JOIN STATE S ON S.STATEID = R.STATEID"
                + " WHERE USERID = :USERID ";
        System.out.println("getHomePageArtistsDetails => " + sQuery);
        return objSQL.getMap(sQuery, param);
    }

    /**
     * get artwork detail
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getHomePageArtworkDetails(Map param) throws Exception {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sQuery = "SELECT CONCAT('" + prop.getProperty("DOMAIN") + "','/client/getWorkImg/',USERWORKID) FILE,"
                + " UW.USERWORKID,UW.WORKNAME, UW.DESCRIPTION,UW.ADDTIME,UW.ISFETURED,A.ARTNAME,R.NAME,A.ARTID,R.USERID  "
                + " FROM USERWORK UW "
                + " INNER JOIN ART A ON A.ARTID = UW.ARTID "
                + " INNER JOIN REGISTRATION R ON R.USERID = UW.USERID "
                + " WHERE UW.USERWORKID = :USERWORKID ";
        System.out.println("Art details " + Const.testQuery(sQuery, param));
        return objSQL.getMap(sQuery, param);
    }

    public List moreHappeningDetails(Map param) throws FileNotFoundException, IOException {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);

        String sQuery = "SELECT HAPPENINGSID, TITLE, SUMMERY, DETAILS, ADDRESS, "
                + " STATEID, CITYID, ARTID, "
                + " EVENTTYPEID, CONCAT('" + prop.getProperty("DOMAIN") + "','/happenings/getBannerThumb/',HAPPENINGSID) THUMBIMG,"
                + " CONCAT('" + prop.getProperty("DOMAIN") + "','/happenings/getBanner/',HAPPENINGSID) BANNER, "
                + " DATE_FORMAT(STARTDATE,'%d-%b-%Y') STARTDATE, DATE_FORMAT(ENDDATE,'%d-%b-%Y') ENDDATE, "
                + " CONTACTNAME, CONTACTEMAIL, NOOFSLOT "
                + " FROM HAPPENINGS "
                + " WHERE ARTID = :ARTID AND HAPPENINGSID != :HAPPENINGSID ORDER BY STARTDATE DESC LIMIT 3 ";
//        System.out.println(param.get("HAPPENINGSID")+"  "+param.get("ARTID")+" "+"sQuery moreHappeningDetails => "+sQuery);
        return objSQL.getList(sQuery, param);
    }

    public List moreEventDetails(Map param) throws FileNotFoundException, IOException {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);

        String sQuery = "SELECT EVENTID, TITLE, SUMMERY, DETAILS, ADDRESS, "
                + " STATEID, CITYID, ARTID, "
                + " EVENTTYPEID, CONCAT('" + prop.getProperty("DOMAIN") + "','/events/getBannerThumb/',EVENTID) THUMBIMG,"
                + " CONCAT('" + prop.getProperty("DOMAIN") + "','/events/getBanner/',EVENTID) BANNER, "
                + " DATE_FORMAT(STARTDATE,'%d-%b-%Y') STARTDATE, DATE_FORMAT(ENDDATE,'%d-%b-%Y') ENDDATE, "
                + " CONTACTNAME, CONTACTEMAIL "
                + " FROM EVENT "
                + " WHERE ARTID = :ARTID AND EVENTID != :EVENTID AND ENDDATE > NOW() ORDER BY STARTDATE DESC LIMIT 3 ";
        return objSQL.getList(sQuery, param);
    }

    public List artFilterData() {
        String sQuery = "SELECT ARTID,ARTNAME FROM ART ORDER BY ARTNAME  ";
        return objSQL.getList(sQuery);
    }

    public List evetntFilterData() {
        String sQuery = " SELECT EVENTTYPEID,EVENTTYPENAME FROM EVENTTYPE ";
        return objSQL.getList(sQuery);
    }

    public List getFilterHappeningDetails(Map param) throws FileNotFoundException, IOException {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);

        String sQuery = "SELECT IF(@CNT>8,@CNT:=1,@CNT:=@CNT+1) CNT,HAPPENINGSID, TITLE, SUMMERY, DETAILS, ADDRESS, "
                + " STATEID, CITYID, ARTID, "
                + " EVENTTYPEID, CONCAT('" + prop.getProperty("DOMAIN") + "','/happenings/getBannerThumb/',HAPPENINGSID) THUMBIMG,"
                + " CONCAT('" + prop.getProperty("DOMAIN") + "','/happenings/getBanner/',HAPPENINGSID) BANNER, "
                + " DATE_FORMAT(STARTDATE,'%d-%b-%Y') STARTDATE, DATE_FORMAT(ENDDATE,'%d-%b-%Y') ENDDATE, "
                + " CONTACTNAME, CONTACTEMAIL, NOOFSLOT "
                + " FROM HAPPENINGS,(SELECT @CNT:= 0) AS CNT WHERE 1=1 ";

        if (param.get("ARTID") != null && !param.get("ARTID").toString().equalsIgnoreCase("")) {
            sQuery += " AND ARTID = '" + param.get("ARTID") + "'";
        }
        if (param.get("EVNTYPE") != null && !param.get("EVNTYPE").toString().equalsIgnoreCase("")) {
            sQuery += " AND EVENTTYPEID = '" + param.get("EVNTYPE") + "'";
        }
        if (param.get("MONTH") != null && !param.get("MONTH").toString().equalsIgnoreCase("")) {
            sQuery += " AND MONTH(STARTDATE) = '" + param.get("MONTH") + "'";
        }
        if (param.get("YEAR") != null && !param.get("YEAR").toString().equalsIgnoreCase("")) {
            sQuery += " AND YEAR(STARTDATE) = '" + param.get("YEAR") + "'";
        }
        sQuery += " ORDER BY HAPPENINGSID DESC ";
        if (param.get("LIMIT") != null && !param.get("LIMIT").toString().equalsIgnoreCase("")) {
            if ((Integer) param.get("SLIMIT") == 9) {
                sQuery += " LIMIT " + param.get("SLIMIT") + "";
            } else {
                sQuery += " LIMIT " + param.get("LIMIT") + ", 9";
            }
        }

        System.out.println(param.get("HAPPENINGSID") + "  " + param.get("ARTID") + " " + "sQuery moreHappeningDetails => " + sQuery);
        return objSQL.getList(sQuery);
    }

    /**
     * get happenings Data Total Count
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int getTotalBDData(Map param) throws Exception {
        String sQuery = "SELECT COUNT(*) LENGTH FROM HAPPENINGS WHERE 1=1";
        //                + map.get("FROM").toString() + " "
        if (param.get("ARTID") != null && !param.get("ARTID").toString().equalsIgnoreCase("")) {
            sQuery += " AND ARTID = '" + param.get("ARTID") + "'";
        }
        if (param.get("EVNTYPE") != null && !param.get("EVNTYPE").toString().equalsIgnoreCase("")) {
            sQuery += " AND EVENTTYPEID = '" + param.get("EVNTYPE") + "'";
        }
        if (param.get("MONTH") != null && !param.get("MONTH").toString().equalsIgnoreCase("")) {
            sQuery += " AND MONTH(STARTDATE) = '" + param.get("MONTH") + "'";
        }
        if (param.get("YEAR") != null && !param.get("YEAR").toString().equalsIgnoreCase("")) {
            sQuery += " AND YEAR(STARTDATE) = '" + param.get("YEAR") + "'";
        }
        return objSQL.getInteger(sQuery);
    }

    public List getFilterEventDetails(Map param) throws FileNotFoundException, IOException {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);

        String sQuery = "SELECT IF(@CNT>3,@CNT:=1,@CNT:=@CNT+1) CNT,E.EVENTID, E.TITLE, E.SUMMERY, E.DETAILS, E.ADDRESS, S.STATENAME, C.CITYNAME, A.ARTNAME, "
                + " E.EVENTTYPEID,CONCAT('" + prop.getProperty("DOMAIN") + "','/events/getBannerThumb/',"
                + " E.EVENTID) THUMBIMG,"
                + " CONCAT('" + prop.getProperty("DOMAIN") + "','/events/getBanner/',E.EVENTID) BANNER, "
                + " DATE_FORMAT(E.STARTDATE,'%d-%b-%Y') STARTDATE, DATE_FORMAT(E.ENDDATE,'%d-%b-%Y') ENDDATE, "
                + " E.CONTACTNAME, E.CONTACTEMAIL "
                + " FROM (SELECT @CNT:= 0) AS CNT,EVENT E "
                + " INNER JOIN STATE S ON S.STATEID = E.STATEID "
                + " INNER JOIN CITY C ON C.CITYID = E.CITYID "
                + " INNER JOIN ART A ON A.ARTID = E.ARTID "
                + " WHERE ISAPPROVED='Approved'  ";

        if (param.get("ARTID") != null && !param.get("ARTID").toString().equalsIgnoreCase("")) {
            sQuery += " AND ARTID = '" + param.get("ARTID") + "'";
        }
        if (param.get("EVNTYPE") != null && !param.get("EVNTYPE").toString().equalsIgnoreCase("")) {
            sQuery += " AND EVENTTYPEID = '" + param.get("EVNTYPE") + "'";
        }
        if (param.get("MONTH") != null && !param.get("MONTH").toString().equalsIgnoreCase("")) {
            sQuery += " AND MONTH(STARTDATE) = '" + param.get("MONTH") + "'";
        }
        if (param.get("YEAR") != null && !param.get("YEAR").toString().equalsIgnoreCase("")) {
            sQuery += " AND YEAR(STARTDATE) = '" + param.get("YEAR") + "'";
        }

        if (param.get("LIMIT") != null && !param.get("LIMIT").toString().equalsIgnoreCase("")) {
            if ((Integer) param.get("SLIMIT") == 5) {
                sQuery += " LIMIT " + param.get("SLIMIT") + "";
            } else {
                sQuery += " LIMIT " + param.get("LIMIT") + ", 5";
            }
        }

        System.out.println(param.get("EVENTID") + "  " + param.get("ARTID") + " " + "sQuery moreeventDetails => " + sQuery);
        return objSQL.getList(sQuery);
    }

    /**
     * get event Data Total Count
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int getTotalEventData(Map param) throws Exception {
        String sQuery = "SELECT COUNT(*) LENGTH FROM EVENT WHERE ISAPPROVED='Approved'";
        //                + map.get("FROM").toString() + " "
        if (param.get("ARTID") != null && !param.get("ARTID").toString().equalsIgnoreCase("")) {
            sQuery += " AND ARTID = '" + param.get("ARTID") + "'";
        }
        if (param.get("EVNTYPE") != null && !param.get("EVNTYPE").toString().equalsIgnoreCase("")) {
            sQuery += " AND EVENTTYPEID = '" + param.get("EVNTYPE") + "'";
        }
        if (param.get("MONTH") != null && !param.get("MONTH").toString().equalsIgnoreCase("")) {
            sQuery += " AND MONTH(STARTDATE) = '" + param.get("MONTH") + "'";
        }
        if (param.get("YEAR") != null && !param.get("YEAR").toString().equalsIgnoreCase("")) {
            sQuery += " AND YEAR(STARTDATE) = '" + param.get("YEAR") + "'";
        }
        return objSQL.getInteger(sQuery);
    }

    /**
     * check happings no of slot
     *
     * @param param
     * @return
     */
    public Map checkNoOfSlot(Map param) {
        String sQuery;

        sQuery = "SELECT COUNT(*)\n"
                + " FROM USER_HAPPENINGS\n"
                + " WHERE HAPPENINGSID = :HAPPENINGSID";
        String cntSlot = objSQL.getString(sQuery, param);
        sQuery = "";
        sQuery = "SELECT COUNT(*)\n"
                + " FROM HAPPENINGS \n"
                + " WHERE NOOFSLOT >= " + cntSlot + "";
        param.put("RSLTCNTPART", objSQL.getInteger(sQuery));

        return param;
    }

    /**
     * check happings part exists or not
     *
     * @param param
     * @return
     */
    public Map availabilityInsert(Map param) {
        String sQuery;
        sQuery = "SELECT COUNT(*)\n"
                + " FROM USER_HAPPENINGS\n"
                + " WHERE HAPPENINGSID = :HAPPENINGSID AND USERID = :USERID";
        param.put("RSLTPART", objSQL.getInteger(sQuery, param));

        return param;
    }

    /**
     * insert happings part
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int insertDetail(Map param) throws Exception {
        String sQuery = "INSERT INTO USER_HAPPENINGS ("
                + " HAPPENINGSID, USERID "
                + " ) VALUES ( "
                + " :HAPPENINGSID, :USERID "
                + " )";
        return objSQL.persist(sQuery, param);
    }

    /**
     * check EVENTS part exists or not
     *
     * @param param
     * @return
     */
    public Map availabilityInsertPartEvent(Map param) {
        String sQuery;
        sQuery = "SELECT COUNT(*)\n"
                + " FROM EVENTPARTICIPANT\n"
                + " WHERE EVENTID = :EVENTID AND NAME = :NAME AND EMAIL = :EMAIL AND MOBILE = :MOBILE";
        param.put("RSLTPART", objSQL.getInteger(sQuery, param));

        return param;
    }

    /**
     * insert events part
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int insertPartEvent(Map param) throws Exception {
        String sQuery = "INSERT INTO EVENTPARTICIPANT ("
                + " EVENTID, NAME, EMAIL, MOBILE, ADDTIME "
                + " ) VALUES ( "
                + " :EVENTID, :NAME, :EMAIL, :MOBILE, NOW()"
                + " )";
        return objSQL.persist(sQuery, param);
    }

    /**
     * get artist total
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int getTotalArtistData(Map param) throws Exception {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sQuery = "SELECT COUNT(*)\n"
                + " FROM REGISTRATION "
                + " WHERE ISFETURED = 1 AND ISAPPROVED='Approved' ";
        return objSQL.getInteger(sQuery);
    }

    /**
     * insert events part
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int insertArtistInquiry(Map param) throws Exception {
        String sQuery = "INSERT INTO INQUIRY ("
                + " USERID, NAME, EMAIL, MOBILE,MESSAGE, CREATEDDATE "
                + " ) VALUES ( "
                + " :USERID, :NAME, :EMAIL, :MOBILE,:MESSAGE, NOW()"
                + " )";
        return objSQL.persist(sQuery, param);
    }

    public List getGalleryData(Map param) throws Exception {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sQuery = " SELECT CONCAT('" + prop.getProperty("DOMAIN") + "','/client/getWorkImg/',USERWORKID) FILE,UW.WORKNAME,"
                + " UW.USERWORKID, UW.DESCRIPTION,UW.ADDTIME,UW.ISFETURED,A.ARTNAME,R.NAME  "
                + " FROM USERWORK UW "
                + " INNER JOIN ART A ON A.ARTID = UW.ARTID "
                + " INNER JOIN REGISTRATION R ON R.USERID = UW.USERID "
                + " WHERE UW.ISFETURED = 1 ";
        if (param.get("MUSIC") != null && !param.get("MUSIC").toString().equalsIgnoreCase("")) {
            if ((Integer) param.get("MUSIC") == 0) {
                sQuery += " AND A.ARTID NOT IN (2) ";
            } else {
                sQuery += " AND A.ARTID IN (2) ";
            }
        }
        if (param.get("USERID") != null && !param.get("USERID").equals("")) {
            sQuery += " AND R.USERID=:USERID";
        }
        if (param.get("ARTID") != null && !param.get("ARTID").equals("")) {
            sQuery += " AND A.ARTID=:ARTID";
        }
        if (param.get("LIMIT") != null && !param.get("LIMIT").toString().equalsIgnoreCase("")) {
            if ((Integer) param.get("MUSIC") == 0) {
                if ((Integer) param.get("SLIMIT") == 9) {
                    sQuery += " LIMIT " + param.get("SLIMIT") + "";
                } else {
                    sQuery += " LIMIT " + param.get("LIMIT") + ", 9";
                }
            } else {
                if ((Integer) param.get("SLIMIT") == 4) {
                    sQuery += " LIMIT " + param.get("SLIMIT") + "";
                } else {
                    sQuery += " LIMIT " + param.get("LIMIT") + ", 4";
                }
            }
        }
        System.out.println("gallary dat1a" + Const.testQuery(sQuery, param));
        return objSQL.getList(sQuery, param);
    }

    /**
     * get gallery Data Total Count
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int getTotalGalleryData(Map param) throws Exception {
        String sQuery = " SELECT COUNT(*) LENGTH  "
                + " FROM USERWORK UW "
                + " INNER JOIN ART A ON A.ARTID = UW.ARTID "
                + " INNER JOIN REGISTRATION R ON R.USERID = UW.USERID "
                + " WHERE UW.ISFETURED = 1 ";
        if (param.get("MUSIC") != null && !param.get("MUSIC").equals("")) {
            if ((Integer) param.get("MUSIC") == 0) {
                sQuery += " AND A.ARTID NOT IN (2) ";
            } else {
                sQuery += " AND A.ARTID IN (2) ";
            }
        }
        if (param.get("USERID") != null && !param.get("USERID").equals("")) {
            sQuery += " AND R.USERID=:USERID";
        }
        if (param.get("ARTID") != null && !param.get("ARTID").equals("")) {
            sQuery += " AND A.ARTID=:ARTID";
        }
        System.out.println("gallary data" + Const.testQuery(sQuery, param));
        return objSQL.getInteger(sQuery,param);
    }

    public List getSubFeaturedWork(Map param) throws Exception {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sQuery = " SELECT CONCAT('" + prop.getProperty("DOMAIN") + "','/client/getWorkImg/',USERWORKID) FILE,"
                + " UW.USERWORKID, UW.DESCRIPTION,UW.ADDTIME,UW.ISFETURED,A.ARTNAME,R.NAME  "
                + " FROM USERWORK UW "
                + " INNER JOIN ART A ON A.ARTID = UW.ARTID "
                + " INNER JOIN REGISTRATION R ON R.USERID = UW.USERID "
                + " WHERE UW.ISFETURED = 1 AND A.ARTID NOT IN (2) AND A.ARTID IN (:ARTID)"
                + "  ORDER BY USERWORKID DESC LIMIT 9";
//        System.out.println("getSubFeaturedWork => " + Const.testQuery(sQuery, param));
        return objSQL.getList(sQuery, param);
    }

    /**
     * get artwork detail
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public List getSubArtOfArtistDetail(Map param) throws Exception {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sQuery = "SELECT CONCAT('" + prop.getProperty("DOMAIN") + "','/client/getWorkImg/',USERWORKID) FILE,"
                + " UW.USERWORKID,UW.WORKNAME, UW.DESCRIPTION,UW.ADDTIME,UW.ISFETURED,A.ARTNAME,R.NAME,A.ARTID,R.USERID  "
                + " FROM USERWORK UW "
                + " INNER JOIN ART A ON A.ARTID = UW.ARTID "
                + " INNER JOIN REGISTRATION R ON R.USERID = UW.USERID "
                + " WHERE UW.USERID = :USERID ORDER BY USERWORKID DESC";
        System.out.println("Art details " + Const.testQuery(sQuery, param));
        return objSQL.getList(sQuery, param);
    }

    /**
     * get getAboutUs.
     *
     * @return
     * @throws java.lang.Exception
     */
    public String getAboutUs() throws Exception {
        String sQuery = "SELECT CONTENT FROM PAGES WHERE PAGEID='1'";
        return objSQL.getString(sQuery);
    }

    /**
     * get getAboutUs.
     *
     * @return
     * @throws java.lang.Exception
     */
    public String getPartners() throws Exception {
        String sQuery = "SELECT CONTENT FROM PAGES WHERE PAGEID='2'";
        return objSQL.getString(sQuery);
    }

    public List galleryArtFilterData() {
        String sQuery = "SELECT ARTID,ARTNAME FROM ART ORDER BY ARTNAME  ";
        return objSQL.getList(sQuery);
    }

    public List galleryArtistFilterData() {
        String sQuery = "SELECT USERID,NAME FROM REGISTRATION WHERE TYPE != 'ADMIN' ";
        return objSQL.getList(sQuery);
    }
}
