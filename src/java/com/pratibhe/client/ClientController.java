/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pratibhe.common.Const;
import com.pratibhe.common.SessionBean;
import com.pratibhe.common.Status;
import com.pratibhe.util.FileUpload;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author jay
 */
@Controller
@RequestMapping("/client")
public class ClientController {

    @Autowired
    SessionBean sBean;

    @Autowired
    ClientService service;

    /**
     * Get Featured Work.
     *
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/getAllClientDetails")
    public ResponseEntity<Map> getAllClientDetails() throws Exception {
        Map map = new HashMap();
        map.put("EVENTBNRDATA", service.getEventBnr());
        map.put("FETWORKDATA", service.getFeaturedWork());
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    /**
     * Get Featured Work.
     *
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/getFeaturedWork")
    public ResponseEntity<Map> getFeaturedWork() throws Exception {
        Map map = new HashMap();
        map.put("FETWORKDATA", service.getFeaturedWork());
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    /**
     * Get Event Banner.
     *
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/getEventBnr")
    public ResponseEntity<Map> getEventBnr() throws Exception {
        Map map = new HashMap();
        map.put("EVENTBNRDATA", service.getEventBnr());
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    /**
     * Get Happenings Banner.
     *
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/getHapBnr")
    public ResponseEntity<Map> getHapBnr() throws Exception {
        Map map = new HashMap();
        map.put("HAPBNRDATA", service.getHapBnr());
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    /**
     * get registration image
     *
     * @param id
     * @param res
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/getRegImg/{id}")
    public void getRegImg(@PathVariable("id") String id, HttpServletResponse res) throws Exception {
        try {
            InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
            Properties prop = new Properties();
            prop.load(input);
            Map param = new HashMap();
            param.put("USERID", id);
            Map mImgDetail = service.getRegImg(param);
            String sLocation = prop.getProperty("STORAGEBOX") + prop.getProperty("REGISTRATION_PATH") + "/" + mImgDetail.get("IMAGE").toString();
            FileUpload.downloadFileFromDisk(sLocation, res);
        } catch (Exception ex) {
            System.out.println("ex=" + ex.getMessage());
        }
    }

    /**
     * get FEATURE WORK image
     *
     * @param id
     * @param res
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/getWorkImg/{id}")
    public void getWorkImg(@PathVariable("id") String id, HttpServletResponse res) throws Exception {
        try {
            InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
            Properties prop = new Properties();
            prop.load(input);
            Map param = new HashMap();
            param.put("USERWORKID", id);
            Map mImgDetail = service.getWorkImg(param);
            String sLocation = prop.getProperty("STORAGEBOX") + prop.getProperty("WORK_PATH") + "/" + mImgDetail.get("FILE").toString();
            FileUpload.downloadFileFromDisk(sLocation, res);
        } catch (Exception ex) {
            System.out.println("ex=" + ex.getMessage());
        }
    }

    /**
     * Get Happenings Detail.
     *
     * @param id
     * @param model
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/happenings/{id}")
    public String getHappeningDetails(@PathVariable String id, ModelMap model) throws Exception {
        Map map = new HashMap();
        map.put("HAPPENINGSID", id);
        model.addAllAttributes(service.getHomePageHappeningsDetail(map));
        return "client/happeningsdetail";
    }

    /**
     * Get Events Details.
     *
     * @param id
     * @param model
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/event/{id}")
    public String getEventDetails(@PathVariable String id, ModelMap model) throws Exception {
        Map map = new HashMap();
        map.put("EVENTID", id);
        model.addAllAttributes(service.getHomePageEventDetails(map));
        return "client/eventdetail";
    }

    /**
     * Get Artists Details.
     *
     * @param id
     * @param model
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/artists/{id}")
    public String getArtistsDetails(@PathVariable String id, ModelMap model) throws Exception {
        Map map = new HashMap();
        map.put("USERID", id);
        model.addAllAttributes(service.getHomePageArtistsDetails(map));
        return "client/artistsdetail";
    }

    /**
     * Get artwork Details.
     *
     * @param id
     * @param model
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/artwork/{id}")
    public String getArtworkDetails(@PathVariable String id, ModelMap model) throws Exception {
        Map map = new HashMap();
        map.put("USERWORKID", id);
        map=service.getHomePageArtworkDetails(map);
        map.put("FETWORKDATA", service.getSubFeaturedWork(map));
        model.addAllAttributes(map);
        return "client/artworkdetail";
    }

    /**
     * Get All events Data.
     *
     * @return
     * @throws java.lang.Exception
     */
    @PostMapping(value = "client/getFilteredHappenings")
    public ResponseEntity<Map> getFilterHappeningDetails(@RequestBody String jObj) throws Exception {
        Map param = new ObjectMapper().readValue(jObj, Map.class);
        param.put("HAPPENINGSSDATA", service.getFilterHappeningDetails(param));
        param.put("HAPPENINGSSDATACNT", service.getTotalBDData(param));
        return new ResponseEntity<>(param, HttpStatus.OK);
    }

    /**
     * Get artist Data.
     *
     * @return
     * @throws java.lang.Exception
     */
    @PostMapping(value = "getArtist")
    public ResponseEntity<Map> getArtist(@RequestBody String jObj) throws Exception {
        Map param = new ObjectMapper().readValue(jObj, Map.class);
        param.put("ARTISTSDATA", service.getArtistsData(param));
        param.put("ARTISTSDATACNT", service.getTotalArtistData(param));
        return new ResponseEntity<>(param, HttpStatus.OK);
    }

    /**
     * insert happings part
     *
     * @param id
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/happenings/client/insertPartHap/{id}")
    public ResponseEntity<Status> insertPartHap(@PathVariable int id) throws Exception {
        Status s = new Status();
        ResponseEntity<Status> retResEnt;
        Map param = new HashMap();
        param.put("HAPPENINGSID", String.valueOf(id));
        System.out.println("session bean is  " + sBean);
        if (sBean.getUserId() == null) {
            s.setResponseCode(Const.MSG_UNAUTHORIZED_CODE);
            s.setResponseMessage("Please Login or Sign Up to participated in Happenings !!! ");
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
            return retResEnt;
        }

        param.put("USERID", sBean.getUserId());
        Map map = service.insertDetail(param);
        if (map.containsKey("RESULTCNT")) {
            s.setResponseCode(Const.MSG_FAIL_CODE);
            s.setResponseMessage("Available spots are already filled , Please try again sometime!!!");
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
            return retResEnt;
        }
        if (Integer.parseInt(map.get("RESULT").toString()) > 0) {
            s.setResponseCode(Const.MSG_SUCCESS_CODE);
            s.setResponseMessage("You have been successfully participate !!");
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        } else {
            s.setResponseCode(Const.MSG_EXISTS_CODE);
            if (Integer.parseInt(map.get("RSLTPART").toString()) > 0) {
                s.setResponseMessage("It seems like you already participated in this Happenings !!!");
            }
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        }
        return retResEnt;
    }

    /**
     * Get All events Data.
     *
     * @return
     * @throws java.lang.Exception
     */
    @PostMapping(value = "client/getFilteredEvents")
    public ResponseEntity<Map> getFilterEventDetails(@RequestBody String jObj) throws Exception {
        Map param = new ObjectMapper().readValue(jObj, Map.class);

        param.put("EVENTSDATA", service.getFilterEventDetails(param));
        param.put("EVENTSDATACNT", service.getTotalEventData(param));
        return new ResponseEntity<>(param, HttpStatus.OK);
    }

    /**
     * insert events part
     *
     * @param jObj
     * @return
     * @throws java.lang.Exception
     */
    @PostMapping(value = "/event/client/insertPartEvent")
    public ResponseEntity<Status> insertPartEvent(@RequestBody String jObj) throws Exception {
        Status s = new Status();
        ResponseEntity<Status> retResEnt;
        Map param = new ObjectMapper().readValue(jObj, Map.class);
        Map map = service.insertPartEvent(param);
        if (Integer.parseInt(map.get("RESULT").toString()) > 0) {
            s.setResponseCode(Const.MSG_SUCCESS_CODE);
            s.setResponseMessage("You have been successfully participate !!");
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        } else {
            s.setResponseCode(Const.MSG_EXISTS_CODE);
            if (Integer.parseInt(map.get("RSLTPART").toString()) > 0) {
                s.setResponseMessage("It seems like you already participated in this Events !!!");
            }
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        }
        return retResEnt;
    }

    /**
     * Get Events Details.
     *
     * @param model
     * @ * @param model
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/aboutus")
    public String getAboutUs(ModelMap model) throws Exception {
        model.addAttribute("ABOUTUS", service.getAboutUs());
        return "client/about_us";
    }

    /**
     * Get Events Details.
     *
     * @param model
     * @ * @param model
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/ourpartners")
    public String getOurPartner(ModelMap model) throws Exception {
         model.addAttribute("PARTNERS", service.getPartners());
        return "client/our_partners";
    }

    /**
     * insert artist inquiry
     *
     * @param jObj
     * @return
     * @throws java.lang.Exception
     */
    @PostMapping(value = "/artists/client/insertArtInq")
    public ResponseEntity<Status> insertArtistInquiry(@RequestBody String jObj) throws Exception {
        Status s = new Status();
        ResponseEntity<Status> retResEnt;
        Map param = new ObjectMapper().readValue(jObj, Map.class);
        Map map = service.insertArtistInquiry(param);
        if (Integer.parseInt(map.get("RESULT").toString()) > 0) {
            s.setResponseCode(Const.MSG_SUCCESS_CODE);
            s.setResponseMessage("Your inqury has been successfully sent !!");
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        } else {
            s.setResponseCode(Const.MSG_EXISTS_CODE);
            if (Integer.parseInt(map.get("RSLTPART").toString()) > 0) {
                s.setResponseMessage("It seems some error please try again later !!!");
            }
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        }
        return retResEnt;
    }

    /**
     * Get gallery Data.
     *
     * @param jObj
     * @return
     * @throws java.lang.Exception
     */
    @PostMapping(value = "getGalleryData")
    public ResponseEntity<Map> getGalleryData(@RequestBody String jObj) throws Exception {
        Map param = new ObjectMapper().readValue(jObj, Map.class);
        param.put("MUSIC", 0);
        param.put("GALLERYDATA", service.getGalleryData(param));
        param.put("GALLERYDATACNT", service.getTotalGalleryData(param));
        return new ResponseEntity<>(param, HttpStatus.OK);
    }
    
    /**
     * Get gallery Music Data.
     *
     * @param jObj
     * @return
     * @throws java.lang.Exception
     */
    @PostMapping(value = "getGalleryMusicData")
    public ResponseEntity<Map> getGalleryMusicData(@RequestBody String jObj) throws Exception {
        Map param = new ObjectMapper().readValue(jObj, Map.class);
        param.put("MUSIC", 1);
        param.put("GALLERYMUSICDATA", service.getGalleryData(param));
        param.put("GALLERYMUSICDATACNT", service.getTotalGalleryData(param));
        return new ResponseEntity<>(param, HttpStatus.OK);
    }
}
