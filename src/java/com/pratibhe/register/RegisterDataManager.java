/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.register;

import com.pratibhe.util.TranSqlServices;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jay
 */
@Repository
public class RegisterDataManager {

    @Autowired
    TranSqlServices objSQL;

    /**
     * check Email exists or not
     *
     * @param param
     * @return
     */
    public Map availabilityInsert(Map param) {
        String sQuery;
        sQuery = "SELECT COUNT(*)\n"
                + " FROM REGISTRATION\n"
                + " WHERE EMAIL = :EMAIL";
        param.put("RSLTEMAIL", objSQL.getInteger(sQuery, param));
        sQuery = "SELECT COUNT(*)\n"
                + " FROM REGISTRATION\n"
                + " WHERE MOBILE = :MOBILE";
        param.put("RSLTMOBILE", objSQL.getInteger(sQuery, param));
//        System.out.println(param+" avai inse "+sQuery);
        return param;
    }

    /**
     * for insert registration
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int insertDetail(Map param) throws Exception {
        System.out.println("param=" + param.toString());
        List lstHearAbout = (List) param.get("HEARABOUT");
        String listString = "";
        for (int i = 0; i < lstHearAbout.size(); i++) {
            String str = String.valueOf(lstHearAbout.get(i));
            listString += str + ",";
        }
        if (!listString.equals("") && listString.length() > 0) {
            listString = listString.substring(0, listString.length() - 1);
        }
        param.replace("HEARABOUT", listString);
        String sQuery = "INSERT INTO REGISTRATION ("
                + "     TYPE, NAME, ARTISTNAME, EMAIL, MOBILE, AADHARNO, GENDER, OCCUPATION, DOB, ADDRESS, "
                + "     CITYID, STATEID, ABOUTME, OTHER, PASSWORD,  ADDTIME, EDITTIME, TYPEOFWORK, TENUREOFWORK, ACTIVATIONCODE, HEARABOUT "
                + ") VALUES (\n"
                + "     :TYPE, :NAME, :ARTISTNAME, :EMAIL, :MOBILE, :AADHARNO, :GENDER, :OCCUPATION, STR_TO_DATE(:DOB,'%d-%b-%Y'), :ADDRESS, "
                + "     :CITYID, :STATEID, :ABOUTME, :OTHER, :PASSWORD,NOW(), NOW(), :TYPEOFWORK,"
                + "     :TENUREOFWORK, :ACTIVATIONCODE, :HEARABOUT\n"
                + ")";
        return objSQL.persistKey(sQuery, param);
    }

    /**
     * Activate User
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int activateUser(Map param) throws Exception {
        String sQuery = "UPDATE REGISTRATION"
                + " SET "
                + " ISACTIVE = 1"
                + " WHERE ACTIVATIONCODE = :ACTIVATIONCODE AND USERID=:USERID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * update profile pic
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateProfileImage(Map param) throws Exception {
        String sQuery = "UPDATE REGISTRATION"
                + " SET "
                + " IMAGE = :IMAGE"
                + " WHERE USERID = :USERID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * insert art detail
     * 
     * @param param
     * @return
     * @throws Exception 
     */
    public int insertArtDetail(Map param) throws Exception {
        System.out.println("param=" + param.toString());
        String sQuery = "INSERT INTO USER_ART ("
                + "     USERID, ARTID "
                + ") VALUES (\n"
                + "     :USERID, :ARTID "
                + ")";
        return objSQL.persist(sQuery, param);
    }

}
