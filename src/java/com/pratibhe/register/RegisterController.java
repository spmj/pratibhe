/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.register;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pratibhe.client.ClientService;
import com.pratibhe.common.Const;
import com.pratibhe.common.Status;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jay
 */
@Controller
@RequestMapping("/")
public class RegisterController {

    @Autowired
    RegisterService service;

    @Autowired
    ClientService clientService;

    @GetMapping(value = "register")
    public String loadIndex(ModelMap model, HttpServletRequest request) throws JSONException {
        return "register";
    }

    /**
     * Get Events Details.
     *
     * @param model
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/studentregistration")
    public String getStudentRegistration(ModelMap model) throws Exception {
        model.addAttribute("ART", clientService.artFilterData());
        model.addAttribute("STATE", clientService.getStateName());
        return "client/studentregistration";
    }

    /**
     * Get Events Details.
     *
     * @param model
     * @ * @param model
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/mentorregistration")
    public String getMentorRegistration(ModelMap model) throws Exception {
        model.addAttribute("ART", clientService.artFilterData());
        model.addAttribute("STATE", clientService.getStateName());
        return "client/mentorregistration";
    }

    /**
     * Insert register Detail.
     *
     * @param file_profile
     * @param request
     * @param jObj
     * @return
     * @throws java.lang.Exception
     */
    @PostMapping(value = "/signup")
    public ResponseEntity<Status> insertDetail(@RequestParam(value = "file_profile") MultipartFile file_profile, HttpServletRequest request, @RequestParam(value = "formData") String jObj) throws Exception {
        Status s = new Status();
        ResponseEntity<Status> retResEnt;
        Map param = new ObjectMapper().readValue(jObj, Map.class);

        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sLocation = prop.getProperty("STORAGEBOX") + prop.getProperty("PROFILE_PATH");

        param.put("LOCATION", sLocation);
        param.put("FILE_PROFILE", file_profile);

        Map map = service.insertDetail(param);
        if (Integer.parseInt(map.get("RESULT").toString()) > 0) {
            s.setResponseCode(Const.MSG_SUCCESS_CODE);
            s.setResponseMessage(Const.MSG_ADD_SUCESS);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        } else {
            s.setResponseCode(Const.MSG_EXISTS_CODE);
            if (Integer.parseInt(map.get("RSLTEMAIL").toString()) > 0
                    && Integer.parseInt(map.get("RSLTMOBILE").toString()) > 0) {
                s.setResponseMessage("Email and Mobile " + Const.MSG_EXISTS);
            } else if (Integer.parseInt(map.get("RSLTMOBILE").toString()) > 0) {
                s.setResponseMessage("Mobile Number " + Const.MSG_EXISTS);
            } else if (Integer.parseInt(map.get("RSLTEMAIL").toString()) > 0) {
                s.setResponseMessage("Email " + Const.MSG_EXISTS);
            }
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        }
        return retResEnt;
    }

    /**
     * activate user.
     *
     * @param userid
     * @param activationcode
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/activate/{userid}/{activationcode}")
    public String activateuser(@PathVariable String userid, @PathVariable String activationcode) throws Exception {
        Map param = new HashMap();
        param.put("USERID", userid);
        param.put("ACTIVATIONCODE",activationcode);
        int isActivate = service.activateUser(param);
        if(isActivate>0){
            return "index";
        }
        return "home";
    }

//    /**
//     * Insert register Detail.
//     *
//     * @param jObj
//     * @return
//     * @throws java.lang.Exception
//     */
//    @PostMapping(value = "/signup")
//    public ResponseEntity<Status> insertDetail(@RequestBody String jObj) throws Exception {
//        Status s = new Status();
//        ResponseEntity<Status> retResEnt;
//        Map param = new ObjectMapper().readValue(jObj, Map.class);
//        Map map = service.insertDetail(param);
//        if (Integer.parseInt(map.get("RESULT").toString()) > 0) {
//            s.setResponseCode(Const.MSG_SUCCESS_CODE);
//            s.setResponseMessage(Const.MSG_ADD_SUCESS);
//            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
//        } else {
//            s.setResponseCode(Const.MSG_EXISTS_CODE);
//            if (Integer.parseInt(map.get("RSLTEMAIL").toString()) > 0
//                    && Integer.parseInt(map.get("RSLTMOBILE").toString()) > 0) {
//                s.setResponseMessage("Email and Mobile " + Const.MSG_EXISTS);
//            } else if (Integer.parseInt(map.get("RSLTMOBILE").toString()) > 0) {
//                s.setResponseMessage("Mobile Number " + Const.MSG_EXISTS);
//            } else if (Integer.parseInt(map.get("RSLTEMAIL").toString()) > 0) {
//                s.setResponseMessage("Email " + Const.MSG_EXISTS);
//            }
//            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
//        }
//        return retResEnt;
//    }
}
