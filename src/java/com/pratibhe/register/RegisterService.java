/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.register;

import com.pratibhe.common.CommonManager;
import com.pratibhe.util.FileUpload;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jay
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class RegisterService {

    @Autowired
    RegisterDataManager datamanager;

    @Autowired
    CommonManager commonmanager;

    /**
     * for insert Category
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map insertDetail(Map param) throws Exception {
        Map map = datamanager.availabilityInsert(param);

        MultipartFile file_profile = (MultipartFile) param.get("FILE_PROFILE");

        System.out.println("map => " + map);
        if (Integer.parseInt(map.get("RSLTEMAIL").toString()) == 0
                && Integer.parseInt(map.get("RSLTMOBILE").toString()) == 0) {
            Iterator it = param.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                param.replace(pair.getKey(), "", null);
            }
            param.put("ACTIVATIONCODE", UUID.randomUUID().toString());
            int userId = datamanager.insertDetail(param);
            if (userId > 0) {
                param.put("USERID", userId);

                List lstArtIds = (List) param.get("ARTID");
                for (int i = 0; i < lstArtIds.size(); i++) {
                    String sArtId = String.valueOf(lstArtIds.get(i));
                    param.put("ARTID", sArtId);
                    datamanager.insertArtDetail(param);
                }

                String extension = file_profile.getOriginalFilename().substring(file_profile.getOriginalFilename().lastIndexOf("."), file_profile.getOriginalFilename().length());
                String sThumbImage = userId + extension;
                param.replace("IMAGE", sThumbImage);
                param.put("USERID", userId);
                int iRes = datamanager.updateProfileImage(param);
                if (iRes > 0) {
                    FileUpload.uploadFile(param.get("LOCATION").toString(), String.valueOf(userId), file_profile, true);
                }
                CommonManager.SendMail(param);
            }

            map.put("RESULT", userId);
        } else {
            map.put("RESULT", 0);
        }
        return map;
    }

    public int activateUser(Map param) throws Exception {
        return datamanager.activateUser(param);
    }

}
