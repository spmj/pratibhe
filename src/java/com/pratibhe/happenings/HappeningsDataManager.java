/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.happenings;

import com.pratibhe.common.Const;
import com.pratibhe.util.TranSqlServices;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jay
 */
@Repository
public class HappeningsDataManager {

    @Autowired
    TranSqlServices objSQL;

    /**
     * TO GET State NAME
     *
     * @return
     * @throws Exception
     */
    public List getStateName() throws Exception {
        String sQuery = "SELECT STATEID VALUE, STATENAME TEXT"
                + " FROM STATE"
                + " WHERE ISACTIVE = 1"
                + " ORDER BY STATENAME";
        return objSQL.getList(sQuery);
    }

    /**
     * get Item Base On Item Group Name
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public List getCity(Map param) throws Exception {
        String sQuery = "SELECT CITYID VALUE,CITYNAME TEXT\n"
                + " FROM CITY\n"
                + " WHERE STATEID = :STATEID AND ISACTIVE = 1\n"
                + " ORDER BY CITYNAME";
        return objSQL.getList(sQuery, param);
    }

    /**
     * get Art Form
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public List getArt(Map param) throws Exception {
        String sQuery = "SELECT ARTID VALUE,ARTNAME TEXT\n"
                + " FROM ART\n"
                + " ORDER BY ARTNAME";
        return objSQL.getList(sQuery, param);
    }

    /**
     * get Event Type
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public List getEventType(Map param) throws Exception {
        String sQuery = "SELECT EVENTTYPEID VALUE,EVENTTYPENAME TEXT\n"
                + " FROM EVENTTYPE\n"
                + " ORDER BY EVENTTYPENAME";
        return objSQL.getList(sQuery, param);
    }

    /**
     * get all events
     *
     * @return
     * @throws Exception
     */
    public List viewDetail() throws Exception {
        String sQuery = "SELECT HAPPENINGSID, TITLE, SUMMERY, DETAILS, H.ADDRESS, S.STATENAME, "
                + " C.CITYNAME, A.ARTNAME, "
                + " ET.EVENTTYPENAME, THUMBIMG, BANNER, DATE_FORMAT(STARTDATE,'%d-%b-%Y') STARTDATE, "
                + " DATE_FORMAT(ENDDATE,'%d-%b-%Y') ENDDATE, "
                + " CONTACTNAME, CONTACTEMAIL, NOOFSLOT,"
                + " R.NAME, APPID, H.ADDTIME, H.EDITTIME, ISBANNER, ISADMIN "
                + " FROM HAPPENINGS H "
                + " INNER JOIN STATE S ON S.STATEID = H.STATEID "
                + " INNER JOIN CITY C ON C.CITYID = H.CITYID "
                + " INNER JOIN ART A ON A.ARTID = H.ARTID "
                + " INNER JOIN EVENTTYPE ET ON ET.EVENTTYPEID = H.EVENTTYPEID "
                + " INNER JOIN REGISTRATION R ON R.USERID = H.CREATERID "
                + " ORDER BY H.HAPPENINGSID DESC ";
        System.out.println("sQuery for happening " + sQuery);
        return objSQL.getList(sQuery);
    }

    /**
     * get events
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getUpdateData(Map param) throws Exception {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);

        String sQuery = "SELECT HAPPENINGSID, TITLE, SUMMERY, DETAILS, ADDRESS, "
                + " STATEID, CITYID, ARTID, "
                + " EVENTTYPEID, CONCAT('" + prop.getProperty("DOMAIN") + "','/happenings/getBannerThumb/',HAPPENINGSID) THUMBIMG,"
                + " CONCAT('" + prop.getProperty("DOMAIN") + "','/happenings/getBanner/',HAPPENINGSID) BANNER, "
                + " DATE_FORMAT(STARTDATE,'%d-%b-%Y') STARTDATE, DATE_FORMAT(ENDDATE,'%d-%b-%Y') ENDDATE, "
                + " CONTACTNAME, CONTACTEMAIL, NOOFSLOT "
                + " FROM HAPPENINGS "
                + " WHERE HAPPENINGSID = :HAPPENINGSID ";
        System.out.println("");
        return objSQL.getMap(sQuery, param);
    }

    /**
     * for insert events
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int insertDetail(Map param) throws Exception {

        String sQuery = "INSERT INTO HAPPENINGS("
                + " TITLE, SUMMERY, DETAILS, ADDRESS, STATEID, CITYID, ARTID,\n"
                + " EVENTTYPEID,STARTDATE, "
                + " ENDDATE , CONTACTNAME, \n"
                + " CONTACTEMAIL, NOOFSLOT, CREATERID, ADDTIME, EDITTIME\n"
                + ") VALUES (\n"
                + " :TITLE, :SUMMERY, :DETAILS, :ADDRESS, :STATEID, :CITYID, :ARTID,\n"
                + " :EVENTTYPEID, STR_TO_DATE(:STARTDATE,'%d-%b-%Y'), STR_TO_DATE(:ENDDATE,'%d-%b-%Y'), :CONTACTNAME, \n"
                + " :CONTACTEMAIL, :NOOFSLOT, :CREATERID, NOW(), NOW()\n"
                + ")";
        System.out.println("insert query of happenings => " + sQuery);
        return objSQL.persistKey(sQuery, param);
    }

    /**
     * for update events
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateDetail(Map param) throws Exception {
        String sQuery = "UPDATE HAPPENINGS "
                + " SET "
                + " TITLE = :TITLE, SUMMERY = :SUMMERY, DETAILS = :DETAILS, "
                + " ADDRESS = :ADDRESS, STATEID = :STATEID, CITYID = :CITYID, "
                + " ARTID = :ARTID, EVENTTYPEID = :EVENTTYPEID, "
                + " STARTDATE = STR_TO_DATE(:STARTDATE,'%d-%b-%Y'), "
                + " ENDDATE = STR_TO_DATE(:ENDDATE,'%d-%b-%Y'), "
                + " CONTACTNAME = :CONTACTNAME, CONTACTEMAIL = :CONTACTEMAIL, "
                + " NOOFSLOT = :NOOFSLOT, CREATERID = :CREATERID, "
                + " EDITTIME = NOW() "
                + " WHERE HAPPENINGSID = :HAPPENINGSID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * for delete events
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int deleteDetail(Map param) throws Exception {
        String sQuery = "DELETE FROM HAPPENINGS WHERE HAPPENINGSID = :HAPPENINGSID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * get banner Image.
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getBannerImages(Map param) throws Exception {
        String sQuery = "SELECT HAPPENINGSID,THUMBIMG,BANNER "
                + " FROM HAPPENINGS "
                + " WHERE HAPPENINGSID=:HAPPENINGSID";
        return objSQL.getMap(sQuery, param);
    }

    /**
     * get Item Base On Item Group Name
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public List getPartcipateData(Map param) throws Exception {
        String sQuery = "SELECT RG.USERID, TYPE, NAME, ARTISTNAME, EMAIL, MOBILE,  GENDER, OCCUPATION, S.STATENAME, C.CITYNAME\n"
                + "FROM\n"
                + "REGISTRATION RG\n"
                + "INNER JOIN USER_HAPPENINGS UH ON RG.USERID = UH.USERID\n"
                + "INNER JOIN STATE S ON S.STATEID = RG.STATEID  \n"
                + "INNER JOIN CITY C ON C.CITYID = RG.CITYID  \n"
                + "WHERE HAPPENINGSID=:HAPPENINGSID ORDER BY RG.NAME ASC";
        return objSQL.getList(sQuery, param);
    }

    /**
     * update thumb and banner Image
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateHappeningBannersImage(Map param) throws Exception {
        String sQuery = "UPDATE HAPPENINGS"
                + " SET "
                + " THUMBIMG = :THUMBIMG, BANNER=:BANNERIMG "
                + " WHERE HAPPENINGSID = :HAPPENINGSID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * update thumb Image
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateThumbImage(Map param) throws Exception {
        String sQuery = "UPDATE HAPPENINGS"
                + " SET "
                + " THUMBIMG = :THUMBIMG"
                + " WHERE HAPPENINGSID = :HAPPENINGSID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * update banner Image
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateBannerImage(Map param) throws Exception {
        String sQuery = "UPDATE HAPPENINGS"
                + " SET "
                + " BANNER = :BANNERIMG"
                + " WHERE HAPPENINGSID = :HAPPENINGSID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * get File Path
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getFilePath(Map param) throws Exception {
        String sQuery = "SELECT THUMBIMG,BANNER FROM HAPPENINGS WHERE HAPPENINGSID=:HAPPENINGSID";
        return objSQL.getMap(sQuery, param);
    }

}
