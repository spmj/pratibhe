/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.happenings;

import com.pratibhe.util.FileUpload;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jay
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class HappeningsService {

    @Autowired
    HappeningsDataManager datamanager;

    /**
     * get All Combo
     *
     * @return
     * @throws Exception
     */
    public Map getAllCombo() throws Exception {
        Map mCombos = new HashMap();
        mCombos.put("STATENAME", datamanager.getStateName());
        mCombos.put("ART", datamanager.getArt(mCombos));
        mCombos.put("EVENTTYPE", datamanager.getEventType(mCombos));
        return mCombos;
    }

    /**
     * get happenings
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getCity(Map param) throws Exception {
        Map mCombos = new HashMap();
        mCombos.put("CITYNAME", datamanager.getCity(param));
        return mCombos;
    }

    /**
     * get All happenings Data
     *
     * @return
     * @throws Exception
     */
    public List viewDetail() throws Exception {
        return datamanager.viewDetail();
    }

    /**
     * get happenings
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getUpdateData(Map param) throws Exception {
        return datamanager.getUpdateData(param);
    }

    /**
     * for insert happenings
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map insertDetail(Map param) throws Exception {
        Map map = new HashMap();

        MultipartFile file_thumb = (MultipartFile) param.get("FILE_THUMB");
        MultipartFile file_banner = (MultipartFile) param.get("FILE_BANNER");

        Iterator it = param.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            param.replace(pair.getKey(), "", null);
        }
        int iHappeningId = datamanager.insertDetail(param);
        if (iHappeningId > 0) {
            String extension = file_thumb.getOriginalFilename().substring(file_thumb.getOriginalFilename().lastIndexOf("."), file_thumb.getOriginalFilename().length());
            String bannerext = file_banner.getOriginalFilename().substring(file_banner.getOriginalFilename().lastIndexOf("."), file_banner.getOriginalFilename().length());
            String sThumbImage = iHappeningId + "_thumb" + extension;
            String sBannerImage = iHappeningId + bannerext;
            param.replace("THUMBIMG", sThumbImage);
            param.replace("BANNERIMG", sBannerImage);
            param.put("HAPPENINGSID", iHappeningId);
            int iRes = datamanager.updateHappeningBannersImage(param);
            if (iRes > 0) {
                FileUpload.uploadFile(param.get("LOCATION").toString(), String.valueOf(iHappeningId) + "_thumb", file_thumb, true);
                FileUpload.uploadFile(param.get("LOCATION").toString(), String.valueOf(iHappeningId), file_banner, true);
            }
        }
        map.put("RESULT", 1);
        return map;
    }

    /**
     * for update happenings
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map updateDetail(Map param) throws Exception {
        Map map = new HashMap();

        MultipartFile file_thumb = (MultipartFile) param.get("FILE_THUMB");
        MultipartFile file_banner = (MultipartFile) param.get("FILE_BANNER");

        Iterator it = param.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            param.replace(pair.getKey(), "", null);
        }
        int iResult = datamanager.updateDetail(param);
        if (iResult > 0) {
            Map sPath = datamanager.getFilePath(param);
            String iHappeningsId = String.valueOf(param.get("HAPPENINGSID"));
            if (file_thumb != null) {
                String extension = file_thumb.getOriginalFilename().substring(file_thumb.getOriginalFilename().lastIndexOf("."), file_thumb.getOriginalFilename().length());
                String sThumbImage = iHappeningsId + "_thumb" + extension;
                param.replace("THUMBIMG", sThumbImage);
                param.put("HAPPENINGSID", iHappeningsId);
                int iRes = datamanager.updateThumbImage(param);
                if (iRes > 0) {
                    String sThumbImg = param.get("LOCATION").toString() + sPath.get("THUMBIMG").toString();
                    FileUpload.deleteFileFromDisk(sThumbImg);
                    FileUpload.uploadFile(param.get("LOCATION").toString(), String.valueOf(iHappeningsId) + "_thumb", file_thumb, true);
                }
            }
            if (file_banner != null) {
                String bannerext = file_banner.getOriginalFilename().substring(file_banner.getOriginalFilename().lastIndexOf("."), file_banner.getOriginalFilename().length());
                String sBannerImage = iHappeningsId + bannerext;
                param.replace("BANNERIMG", sBannerImage);
                param.put("HAPPENINGSID", iHappeningsId);
                int iRes = datamanager.updateBannerImage(param);
                if (iRes > 0) {
                    String sBannerImg = param.get("LOCATION").toString() + sPath.get("BANNER").toString();
                    FileUpload.deleteFileFromDisk(sBannerImg);
                    FileUpload.uploadFile(param.get("LOCATION").toString(), String.valueOf(iHappeningsId), file_banner, true);
                }
            }

        }
        map.put("RESULT", 1);
        return map;
    }

    /**
     * for delete happenings
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int deleteDetail(Map param) throws Exception {
        int iResult = 0;
        Map sPath = datamanager.getFilePath(param);
        int iDelRslt = datamanager.deleteDetail(param);
        if (iDelRslt > 0) {
            String sThumbImg = param.get("LOCATION").toString() + sPath.get("THUMBIMG").toString();
            String sBannerImg = param.get("LOCATION").toString() + sPath.get("BANNER").toString();
            FileUpload.deleteFileFromDisk(sThumbImg);
            FileUpload.deleteFileFromDisk(sBannerImg);
            iResult = 1;
        }
        return iResult;
    }

    /**
     * get banner image.
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getBannerImages(Map param) throws Exception {
        return datamanager.getBannerImages(param);
    }

    /**
     * get Participants Data
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getPartcipateData(Map param) throws Exception {
        Map mCombos = new HashMap();
        mCombos.put("PARTCIPATEDATA", datamanager.getPartcipateData(param));
        return mCombos;
    }
}
