/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.changepwd;

import com.pratibhe.util.TranSqlServices;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jay
 */
@Repository
public class ChangePwdDataManager {

    @Autowired
    TranSqlServices objSQL;

    /**
     * check Category name exists or not
     *
     * @param param
     * @return
     */
    public Map getOldPwd(Map param) {
        String sQuery;
        sQuery = "SELECT PASSWORD\n"
                + " FROM REGISTRATION\n"
                + " WHERE USERID = :USERID";
        param.put("PASSWORD", objSQL.getString(sQuery, param));
        return param;
    }

    /**
     * for update password
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updatePwd(Map param) throws Exception {
        String sQuery = "UPDATE REGISTRATION "
                + " SET "
                + " PASSWORD = :PASSWORD"
                + " WHERE USERID = :USERID";
        return objSQL.persist(sQuery, param);
    }

}
