/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.changepwd;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jay
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChangePwdService {

    @Autowired
    ChangePwdDataManager datamanager;

    /**
     * get old password
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getOldPwd(Map param) throws Exception {
        return datamanager.getOldPwd(param);
    }

    /**
     * for update password
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map updatePwd(Map param) throws Exception {
        Map map = new HashMap();
        map.put("RESULT", datamanager.updatePwd(param));
        return map;
    }

}
