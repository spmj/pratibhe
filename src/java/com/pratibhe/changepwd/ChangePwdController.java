/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.changepwd;

import com.pratibhe.common.Const;
import com.pratibhe.common.SessionBean;
import com.pratibhe.common.Status;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author jay
 */
@Controller
@RequestMapping("/changepwd")
public class ChangePwdController {

    private static final Logger LOG = Logger.getLogger(ChangePwdController.class);

    @Autowired
    ChangePwdService service;
    
    @Autowired
    SessionBean session;

    @GetMapping(value = "")
    public String loadAdd(ModelMap model, HttpServletRequest request) throws JSONException {
        model.addAttribute(Const.TASK, Const.TASK_ADD);
        model.addAttribute("USERNAME", session.getUserName());
        return "changepwd/changepwd";
    }

    /**
     * Change Password
     *
     * @param jObj
     * @return
     * @throws java.lang.Exception
     */
    @PostMapping(value = "/updateChangepwd") 
    public ResponseEntity<Status> changepwd(@RequestBody String jObj) throws Exception {
        Status s = new Status();
        ResponseEntity<Status> retResEnt;
        Map param = new ObjectMapper().readValue(jObj, Map.class);
        param.put("USERID", session.getUserId());
        Map map = service.getOldPwd(param);
        if (!map.get("PASSWORD").toString().equals(param.get("OLDPWD"))) {
            s.setResponseCode(Const.MSG_FAIL_CODE);
            s.setResponseMessage("old password wrong");
            return retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        }
        Map upmap = service.updatePwd(param);
        if (Integer.parseInt(upmap.get("RESULT").toString()) > 0) {
            s.setResponseCode(Const.MSG_SUCCESS_CODE);
            s.setResponseMessage("SuccessFully Password change.");
            return retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        } else {
            s.setResponseCode(Const.MSG_FAIL_CODE);
            s.setResponseMessage("Not SuccessFully Password change");
            return retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        }
    }

    /**
     * Catch all Controller exception
     *
     * @param req
     * @param ex
     * @return
     */
    @ExceptionHandler({Exception.class})
    public ResponseEntity<Status> exceptionHandler(HttpServletRequest req, Exception ex) {
        Status error = new Status(500, ex.getMessage());
        LOG.error("Request: " + req.getRequestURL() + " raised " + ex);
        return new ResponseEntity<>(error, HttpStatus.OK);
    }
}
