/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.users;

import com.pratibhe.util.TranSqlServices;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jay
 */
@Repository
public class UsersDataManager {

    @Autowired
    TranSqlServices objSQL;

    /**
     * get all events
     *
     * @return
     * @throws Exception
     */
    public List viewStudentsDetail() throws Exception {
        String sQuery = "SELECT RG.USERID,GROUP_CONCAT(T.ARTNAME) ARTNAME, RG.TYPE, RG.NAME, RG.ARTISTNAME, RG.EMAIL, RG.MOBILE, RG.AADHARNO, \n"
                + " RG.GENDER, RG.OCCUPATION, RG.DOB, RG.ADDRESS, RG.CITYID, CT.CITYNAME, ST.STATENAME, RG.ABOUTME, RG.OTHER, \n"
                + " RG.PASSWORD, RG.IMAGE, RG.ADDTIME, RG.EDITTIME, RG.TYPEOFWORK, RG.TENUREOFWORK, RG.MENTORID, RG.ISACTIVE, MT.NAME MENTORNAME\n"
                + " FROM REGISTRATION RG\n"
                + " LEFT JOIN CITY CT ON  CT.CITYID = RG.CITYID\n"
                + " LEFT JOIN STATE ST ON RG.STATEID = ST.STATEID\n"
                + " LEFT JOIN USER_ART UA ON RG.USERID = UA.USERID \n"
                + " LEFT JOIN ART T ON UA.ARTID = T.ARTID\n"
                + " LEFT JOIN REGISTRATION MT ON MT.USERID = RG.MENTORID \n"
                + " WHERE RG.TYPE='STUDENT'\n"
                + " GROUP BY UA.USERID ";
        System.out.println("student list " + sQuery);
        return objSQL.getList(sQuery);
    }

    /**
     * get all events
     *
     * @return
     * @throws Exception
     */
    public List viewMentorsDetail() throws Exception {
        String sQuery = "SELECT RG.USERID,GROUP_CONCAT(T.ARTNAME) ARTNAME, RG.TYPE, RG.NAME, RG.ARTISTNAME, RG.EMAIL, RG.MOBILE, RG.AADHARNO, \n"
                + "RG.GENDER, RG.OCCUPATION, RG.DOB, RG.ADDRESS, RG.CITYID, CT.CITYNAME, ST.STATENAME, RG.ABOUTME, RG.OTHER, \n"
                + "RG.PASSWORD, RG.IMAGE, RG.ADDTIME, RG.EDITTIME, RG.TYPEOFWORK, TENUREOFWORK, MENTORID, RG.ISACTIVE, RG.ISAPPROVED\n"
                + "FROM REGISTRATION RG\n"
                + "LEFT JOIN CITY CT ON  CT.CITYID = RG.CITYID\n"
                + "LEFT JOIN STATE ST ON RG.STATEID = ST.STATEID\n"
                + "LEFT JOIN USER_ART UA ON RG.USERID = UA.USERID \n"
                + "LEFT JOIN ART T ON UA.ARTID = T.ARTID\n"
                + "WHERE RG.TYPE='MENTOR'\n"
                + "GROUP BY UA.USERID ";
        return objSQL.getList(sQuery);
    }

    /**
     * get events
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getUpdateData(Map param) throws Exception {
        String sQuery = "SELECT RG.USERID,GROUP_CONCAT(T.ARTNAME) ARTNAME, RG.TYPE, "
                + " RG.NAME, RG.ARTISTNAME, RG.EMAIL, RG.MOBILE, RG.AADHARNO, \n"
                + " RG.GENDER, RG.OCCUPATION,DATE_FORMAT(RG.DOB,'%d-%b-%Y') DOB, RG.ADDRESS, RG.CITYID, "
                + " CT.CITYNAME, ST.STATENAME, RG.ABOUTME, RG.OTHER, \n"
                + " RG.PASSWORD, RG.IMAGE, RG.ADDTIME, RG.EDITTIME, RG.TYPEOFWORK, "
                + " RG.TENUREOFWORK, RG.MENTORID, RG.ISACTIVE,MT.NAME MENTORNAME,\n"
                + " RG.ISAPPROVED,RG.ISACTIVE,RG.HEARABOUT\n"
                + " FROM REGISTRATION RG\n"
                + " LEFT JOIN CITY CT ON  CT.CITYID = RG.CITYID\n"
                + " LEFT JOIN STATE ST ON RG.STATEID = ST.STATEID\n"
                + " LEFT JOIN USER_ART UA ON RG.USERID = UA.USERID \n"
                + " LEFT JOIN ART T ON UA.ARTID = T.ARTID\n"
                + " LEFT JOIN REGISTRATION MT ON MT.USERID = RG.MENTORID \n"
                + " WHERE  RG.USERID=:USERID  \n"
                + " GROUP BY UA.USERID";
        System.out.println("get Update Date of userDataManager " + sQuery);
        return objSQL.getMap(sQuery, param);
    }

    /**
     * for update events
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateDetail(Map param) throws Exception {
        String sQuery = "UPDATE REGISTRATION "
                + " SET "
                + " ISACTIVE = :ISACTIVE,"
                + " ISAPPROVED = :ISAPPROVED"
                + " WHERE USERID = :USERID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * for delete events
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int deleteDetail(Map param) throws Exception {
        String sQuery = "DELETE FROM REGISTRATION WHERE USERID = :USERID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * get events
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public List getUserWorkData(Map param) throws Exception {
        String sQuery = "SELECT UW.USERWORKID ,UW.FILE,UW.DESCRIPTION,ART.ARTNAME\n"
                + "FROM USERWORK UW\n"
                + "LEFT JOIN ART ART ON ART.ARTID = UW.ARTID \n"
                + "WHERE UW.USERID=:USERID";
        System.out.println("get Update Date of userDataManager " + sQuery);
        return objSQL.getList(sQuery, param);
    }

    /**
     * get user work detail.
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getUserWorkDetail(Map param) throws Exception {
        String sQuery = "SELECT UW.USERWORKID,UW.WORKNAME ,UW.FILE,UW.DESCRIPTION,ART.ARTNAME,UW.ISFETURED\n"
                + " FROM USERWORK UW\n"
                + " LEFT JOIN ART ART ON ART.ARTID = UW.ARTID \n"
                + " WHERE UW.USERWORKID=:WORKID";
        return objSQL.getMap(sQuery, param);
    }

    /**
     * get user comment detail.
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public List getCommentDetail(Map param) throws Exception {
        String sQuery = "SELECT STUDENTID ,NAME ,WORKCOMMENT.MENTORID ,COMMENT ,DATE_FORMAT(WORKCOMMENT.ADDTIME,'%M %d, %Y %H:%i') ADDTIME, IF(STUDENTID IS NOT NULL,'out','in') CLASS\n"
                + " FROM WORKCOMMENT \n"
                + " INNER JOIN REGISTRATION REG ON if(STUDENTID IS NOT NULL,REG.USERID=WORKCOMMENT.STUDENTID,REG.USERID=WORKCOMMENT.MENTORID)"
                + " WHERE WORKID=:WORKID ORDER BY ID ";
        if (param.get("USERTYPE").equals("STUDENT")) {
            sQuery = "SELECT STUDENTID ,NAME ,WORKCOMMENT.MENTORID ,COMMENT ,DATE_FORMAT(WORKCOMMENT.ADDTIME,'%M %d, %Y %H:%i') ADDTIME, IF(STUDENTID IS NOT NULL,'in','out') CLASS\n"
                    + " FROM WORKCOMMENT \n"
                    + " INNER JOIN REGISTRATION REG ON if(STUDENTID IS NOT NULL,REG.USERID=WORKCOMMENT.STUDENTID,REG.USERID=WORKCOMMENT.MENTORID)"
                    + " WHERE WORKID=:WORKID ORDER BY ID ";
        }
        return objSQL.getList(sQuery, param);
    }
    
     /**
     * for add comment
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int insertComment(Map param) throws Exception {
        String sQuery = "INSERT INTO WORKCOMMENT ("
                + " WORKID, STUDENTID, MENTORID, COMMENT, ADDTIME\n"
                + ") VALUES (\n"
                + " :WORKID, :STUDENTID, :MENTORID, :COMMENT, NOW()\n"
                + ")";
        return objSQL.persistKey(sQuery, param);
    }

}
