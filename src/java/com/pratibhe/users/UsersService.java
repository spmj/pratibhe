/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.users;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jay
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UsersService {

    @Autowired
    UsersDataManager datamanager;

    /**
     * get All events Data
     *
     * @return
     * @throws Exception
     */
    public List viewStudentsDetail() throws Exception {
        return datamanager.viewStudentsDetail();
    }

    /**
     * get All events Data
     *
     * @return
     * @throws Exception
     */
    public List viewMentorsDetail() throws Exception {
        return datamanager.viewMentorsDetail();
    }

    /**
     * get events
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getUpdateData(Map param) throws Exception {
        return datamanager.getUpdateData(param);
    }

    /**
     * for update events
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map updateDetail(Map param) throws Exception {
        Map map = new HashMap();
        Iterator it = param.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            param.replace(pair.getKey(), "", null);
        }
        map.put("RESULT", datamanager.updateDetail(param));
        return map;
    }

    /**
     * for user work data
     * 
     * @param param
     * @return
     * @throws Exception 
     */
    public List getUserWorkData(Map param) throws Exception {
        return datamanager.getUserWorkData(param);
    }

    /**
     * for user work detail
     * 
     * @param param
     * @return
     * @throws Exception 
     */
    public Map getUserWorkDetail(Map param) throws Exception {
        return datamanager.getUserWorkDetail(param);
    }

    /**
     * for comment detail
     * 
     * @param param
     * @return
     * @throws Exception 
     */
    public List getCommentDetail(Map param) throws Exception {
        return datamanager.getCommentDetail(param);
    }
    
     /**
     * for insert Work
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map insertComment(Map param) throws Exception {
        Map map = new HashMap();
        Iterator it = param.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            param.replace(pair.getKey(), "", null);
        }
        int refId = datamanager.insertComment(param);
        map.put("RESULT", refId);
        return map;
    }

    /**
     * for delete events
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int deleteDetail(Map param) throws Exception {
        return datamanager.deleteDetail(param);
    }
}
