/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.inquiry;

import com.pratibhe.common.SessionBean;
import com.pratibhe.util.TranSqlServices;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jay
 */
@Repository
public class InquiryDataManager {

    @Autowired
    TranSqlServices objSQL;

    @Autowired
    SessionBean sBean;

    /**
     * get all Inquiry
     *
     * @return
     * @throws Exception
     */
    public List viewDetail() throws Exception {
        Map param = new HashMap();
        param.put("USERID", sBean.getUserId());
        String sQuery = "SELECT INQUIRYID,USERID,NAME,EMAIL,MOBILE,"
                + "MESSAGE,DATE_FORMAT(CREATEDDATE,'%d-%b-%Y %H:%i:%s') CREATEDDATE FROM INQUIRY\n"
                + "WHERE USERID=:USERID ORDER BY CREATEDDATE ";
        return objSQL.getList(sQuery, param);
    }

    /**
     * get Inquiry
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getUpdateData(Map param) throws Exception {
        param.put("USERID", sBean.getUserId());
        String sQuery = "SELECT INQUIRYID,USERID,NAME,EMAIL,MOBILE,MESSAGE,CREATEDDATE FROM INQUIRY\n"
                + "WHERE INQUIRYID=:INQUIRYID AND USERID=:USERID\n";
        return objSQL.getMap(sQuery, param);
    }

    /**
     * for delete Inquiry
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int deleteDetail(Map param) throws Exception {
        param.put("USERID", sBean.getUserId());
        String sQuery = "DELETE FROM INQUIRY WHERE USERID=:USERID AND INQUIRYID = :INQUIRYID";
        return objSQL.persist(sQuery, param);
    }
}
