/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.db;

import com.pratibhe.common.SessionBean;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author jay
 */
@Controller
@RequestMapping("")
public class DbController {

    @Autowired
    DbService service;

    @Autowired
    SessionBean sBean;

    /**
     * get Db Data for admin
     *
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/admin/getDbData")
    public ResponseEntity<Map> getAdminDbData() throws Exception {
        Map map = service.getAdminDbData();
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    /**
     * get Db Data for mentor
     *
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/mentor/getDbData")
    public ResponseEntity<Map> getMentorDbData() throws Exception {
        Map map = service.getMentorDbData();
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    /**
     * get Db Data for student
     *
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/student/getDbData")
    public ResponseEntity<Map> getStudentDbData() throws Exception {
        Map map = service.getStudentDbData();
        return new ResponseEntity<>(map, HttpStatus.OK);
    }
}
