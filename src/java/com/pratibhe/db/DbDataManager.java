/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.db;

import com.pratibhe.common.SessionBean;
import com.pratibhe.util.TranSqlServices;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jay
 */
@Repository
public class DbDataManager {

    @Autowired
    TranSqlServices objSQL;

    @Autowired
    SessionBean sBean;

    /**
     * get Approved Events count
     *
     * @return
     * @throws Exception
     */
    public Map getEventsApprovalWise() throws Exception {
        String sQuery = "SELECT SUM(ISAPPROVED = 'Approved') AS Approved,\n"
                + "       SUM(ISAPPROVED = 'Pending') AS Pending,\n"
                + "       SUM(ISAPPROVED = 'Rejected') AS Rejected\n"
                + "       FROM EVENT;";
        return objSQL.getMap(sQuery);
    }

    /**
     * get mentor approval wise
     *
     * @return
     * @throws Exception
     */
    public Map getMentorApprovalWise() throws Exception {
        String sQuery = "SELECT SUM(ISAPPROVED = 'Approved') AS Approved,\n"
                + "       SUM(ISAPPROVED = 'Pending') AS Pending,\n"
                + "       SUM(ISAPPROVED = 'Rejected') AS Rejected\n"
                + "       FROM REGISTRATION WHERE TYPE='MENTOR';\n";
        return objSQL.getMap(sQuery);
    }

    /**
     * get last 30 last 30 days events with participants
     *
     * @return
     * @throws Exception
     */
    public List getEventParticipantWise() throws Exception {
        String sQuery = "SELECT E.TITLE  ,COUNT(EP.EVENTPARTID) NOOFPART\n"
                + "FROM EVENT E\n"
                + "LEFT JOIN EVENTPARTICIPANT EP ON E.EVENTID = EP.EVENTID\n"
                + "WHERE E.STARTDATE BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()\n"
                + " GROUP BY E.EVENTID;\n";
        return objSQL.getList(sQuery);
    }

    /**
     * get last 30 last 30 days events with participants
     *
     * @return
     * @throws Exception
     */
    public List getArtCount() throws Exception {
        String sQuery = "SELECT A.ARTNAME,COUNT(USERWORKID) COUNT \n"
                + " FROM ART A\n"
                + " LEFT JOIN USERWORK UW ON UW.ARTID=A.ARTID\n"
                + " GROUP BY A.ARTID";
        return objSQL.getList(sQuery);
    }

    /**
     * get Pending for Approval
     *
     * @return
     * @throws Exception
     */
    public int getPendingEvents() throws Exception {
        String sQuery = "SELECT COUNT(*) FROM event where ISAPPROVED = 0";
        return objSQL.getInteger(sQuery);
    }

    /**
     * get last 30 last 30 days events with participants
     *
     * @return
     * @throws Exception
     */
    public List getMentorArtCount() throws Exception {
        Map param = new HashMap();
        param.put("MENTORID", sBean.getUserId());
        String sQuery = "SELECT MENTORART.ARTID,ARTNAME,TOTALWORK COUNT FROM\n"
                + "(SELECT ART.ARTID,ARTNAME FROM ART\n"
                + "INNER JOIN USER_ART ON USER_ART.ARTID=ART.ARTID\n"
                + "INNER JOIN REGISTRATION REG ON REG.USERID=USER_ART.USERID AND REG.TYPE='MENTOR' AND REG.USERID=:MENTORID) MENTORART\n"
                + "INNER JOIN (SELECT COUNT(*) TOTALWORK, ARTID FROM REGISTRATION REGSTUDENT\n"
                + "INNER JOIN USERWORK ON USERWORK.USERID=REGSTUDENT.USERID\n"
                + "WHERE MENTORID=:MENTORID GROUP BY ARTID) STUDENTART ON MENTORART.ARTID=STUDENTART.ARTID";
//        String sQuery = "SELECT \n"
//                + "    COUNT(UW.ARTID) COUNT, A.ARTNAME\n"
//                + "FROM\n"
//                + "    USERWORK UW\n"
//                + "        LEFT JOIN\n"
//                + "    ART A ON UW.ARTID = A.ARTID\n"
//                + "WHERE\n"
//                + "    USERID IN (SELECT \n"
//                + "            RG.USERID\n"
//                + "        FROM\n"
//                + "            REGISTRATION RG\n"
//                + "                LEFT JOIN\n"
//                + "            USERWORK UW ON UW.USERID = RG.USERID\n"
//                + "        WHERE\n"
//                + "            RG.MENTORID = :MENTORID\n"
//                + "        GROUP BY RG.USERID)\n"
//                + "        AND UW.ARTID IN (SELECT \n"
//                + "            ARTID\n"
//                + "        FROM\n"
//                + "            USER_ART UA\n"
//                + "                LEFT JOIN\n"
//                + "            REGISTRATION RG ON RG.USERID = UA.USERID\n"
//                + "        WHERE\n"
//                + "            RG.USERID = :MENTORID)\n"
//                + "GROUP BY UW.ARTID";
        return objSQL.getList(sQuery, param);
    }

    /**
     * get last 30 last 30 days events with participants of mentor and status is
     * approved
     *
     * @return
     * @throws Exception
     */
    public List getMentorEventParticipantWise() throws Exception {
        Map param = new HashMap();
        param.put("MENTORID", sBean.getUserId());
        String sQuery = "SELECT E.TITLE  ,COUNT(EP.EVENTPARTID) NOOFPART\n"
                + " FROM EVENT E\n"
                + " LEFT JOIN EVENTPARTICIPANT EP ON E.EVENTID = EP.EVENTID\n"
                + " WHERE E.STARTDATE BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()\n"
                + " AND CREATERID = :MENTORID AND ISAPPROVED='Approved'  GROUP BY E.EVENTID;\n";
        System.out.println("getMentorEventParticipantWise " + sQuery);
        return objSQL.getList(sQuery, param);
    }

    /**
     * get last 30 last 30 days events with participants of mentor
     *
     * @return
     * @throws Exception
     */
    public List getStudentWorkWiseForMentor() throws Exception {
        Map param = new HashMap();
        param.put("MENTORID", sBean.getUserId());
        String sQuery = "SELECT RG.NAME  ,COUNT(UW.USERWORKID) COUNT\n"
                + "FROM REGISTRATION RG\n"
                + "LEFT JOIN USERWORK UW ON RG.USERID=UW.USERID AND ARTID IN (SELECT ARTID FROM USER_ART WHERE USERID=:MENTORID)\n"
                + "WHERE RG.MENTORID=:MENTORID\n"
                + "GROUP BY RG.USERID ORDER BY NAME";
        return objSQL.getList(sQuery, param);
    }

    /**
     * get fetured student count
     *
     * @return
     * @throws Exception
     */
    public int getFeaturedStudentCount() throws Exception {
        Map param = new HashMap();
        param.put("MENTORID", sBean.getUserId());
        String sQuery = " SELECT COUNT(*) FEATUREDSTUDENT "
                + " FROM REGISTRATION "
                + " WHERE MENTORID=MENTORID AND ISFETURED='1'";
        return objSQL.getInteger(sQuery, param);
    }

    /**
     * get selected student count
     *
     * @return
     * @throws Exception
     */
    public int getSelectedStudentCount() throws Exception {
        Map param = new HashMap();
        param.put("MENTORID", sBean.getUserId());
        String sQuery = " SELECT COUNT(*) FROM REGISTRATION WHERE MENTORID=:MENTORID;";
        return objSQL.getInteger(sQuery, param);
    }

    /**
     * get selected student count
     *
     * @return
     * @throws Exception
     */
    public int getAvailableStudentCount() throws Exception {
        Map param = new HashMap();
        param.put("MENTORID", sBean.getUserId());
        String sQuery = " SELECT COUNT(*) FROM REGISTRATION WHERE MENTORID=:MENTORID;";
        return objSQL.getInteger(sQuery, param);
    }

    /**
     * get last 30 last 30 days events with participants of mentor
     *
     * @return
     * @throws Exception
     */
    public List getWorkCountByArt() throws Exception {
        Map param = new HashMap();
        param.put("USERID", sBean.getUserId());
        String sQuery = "SELECT ART.ARTNAME,COUNT(*) COUNT FROM REGISTRATION REG\n"
                + " LEFT JOIN USER_ART UA ON UA.USERID=REG.USERID\n"
                + " LEFT JOIN USERWORK UW ON UW.USERID=REG.USERID AND UA.ARTID=UW.ARTID\n"
                + " INNER JOIN ART ON ART.ARTID=UA.ARTID\n"
                + " WHERE TYPE='STUDENT' AND REG.USERID=:USERID GROUP BY ART.ARTID";
        return objSQL.getList(sQuery, param);
    }

}
