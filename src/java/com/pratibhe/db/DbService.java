/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.db;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jay
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DbService {

    @Autowired
    DbDataManager datamanager;

    /**
     * get Db Data for admin
     *
     * @return
     * @throws Exception
     */
    public Map getAdminDbData() throws Exception {
        Map map = new HashMap();
        map.put("EVENTPARTICIPANT", datamanager.getEventParticipantWise());
        map.put("MENTORAPPROVAL", datamanager.getMentorApprovalWise());
        map.put("EVENTAPPROVAL", datamanager.getEventsApprovalWise());
        map.put("ARTCOUNT", datamanager.getArtCount());
        return map;
    }

    /**
     * get Db Data for mentor
     *
     * @return
     * @throws Exception
     */
    public Map getMentorDbData() throws Exception {
        Map map = new HashMap();
        map.put("EVENTPARTICIPANT", datamanager.getMentorEventParticipantWise());
        map.put("ARTCOUNT", datamanager.getMentorArtCount());
        map.put("STUDENTWORK", datamanager.getStudentWorkWiseForMentor());
        map.put("NOOFSELECTEDSTU",datamanager.getSelectedStudentCount());
        map.put("NOOFAVAILSTU",datamanager.getAvailableStudentCount());
        map.put("NOOFFETUREDSTU",datamanager.getFeaturedStudentCount());
        return map;
    }

    /**
     * get Db Data for student
     *
     * @return
     * @throws Exception
     */
    public Map getStudentDbData() throws Exception {
        Map map = new HashMap();
        map.put("WORKCNTART",datamanager.getWorkCountByArt());
        return map;
    }
}
