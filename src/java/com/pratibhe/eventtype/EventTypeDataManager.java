/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.eventtype;

import com.pratibhe.util.TranSqlServices;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jay
 */
@Repository
public class EventTypeDataManager {

    @Autowired
    TranSqlServices objSQL;

    /**
     * get all EventType
     *
     * @return
     * @throws Exception
     */
    public List viewDetail() throws Exception {
        String sQuery = "SELECT EVENTTYPEID, EVENTTYPENAME, ADDTIME, EDITTIME"
                + " FROM EVENTTYPE"
                + " ORDER BY EVENTTYPEID DESC";
        return objSQL.getList(sQuery);
    }

    /**
     * get EventType
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getUpdateData(Map param) throws Exception {
        String sQuery = "SELECT EVENTTYPEID, EVENTTYPENAME"
                + " FROM EVENTTYPE"
                + " WHERE EVENTTYPEID = :EVENTTYPEID";
        return objSQL.getMap(sQuery, param);
    }

    /**
     * check EventType name exists or not
     *
     * @param param
     * @return
     */
    public Map availabilityInsert(Map param) {
        String sQuery;
        sQuery = "SELECT COUNT(*)\n"
                + " FROM EVENTTYPE\n"
                + " WHERE EVENTTYPENAME = :EVENTTYPENAME";
        param.put("RSLTEVENTTYPENAME", objSQL.getInteger(sQuery, param));

        return param;
    }

    /**
     * for insert EventType
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int insertDetail(Map param) throws Exception {

        String sQuery = "INSERT INTO EVENTTYPE("
                + " EVENTTYPENAME, ADDTIME, EDITTIME\n"
                + ") VALUES (\n"
                + " :EVENTTYPENAME, NOW(), NOW()\n"
                + ")";
        return objSQL.persistKey(sQuery, param);
    }

    /**
     * check EventType name exists or not
     *
     * @param param
     * @return
     */
    public Map availabilityUpdate(Map param) {
        String sBNameQuery;
        sBNameQuery = "SELECT COUNT(*)\n"
                + " FROM EVENTTYPE\n"
                + " WHERE EVENTTYPENAME = :EVENTTYPENAME AND EVENTTYPEID != :EVENTTYPEID";
        param.put("RSLTEVENTTYPENAME", objSQL.getInteger(sBNameQuery, param));
        return param;
    }

    /**
     * for update EventType
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateDetail(Map param) throws Exception {
        String sQuery = "UPDATE EVENTTYPE "
                + " SET "
                + " EVENTTYPENAME = :EVENTTYPENAME,"
                + " EDITTIME = NOW() "
                + " WHERE EVENTTYPEID = :EVENTTYPEID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * for delete EventType
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int deleteDetail(Map param) throws Exception {
        String sQuery = "DELETE FROM EVENTTYPE WHERE EVENTTYPEID = :EVENTTYPEID";
        return objSQL.persist(sQuery, param);
    }
}
