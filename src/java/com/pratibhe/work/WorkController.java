/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.work;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pratibhe.common.Const;
import com.pratibhe.common.SessionBean;
import com.pratibhe.common.Status;
import com.pratibhe.util.FileUpload;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jay
 */
@Controller
@RequestMapping("/work")
public class WorkController {

    private static final Logger LOG = Logger.getLogger(WorkController.class);

    @Autowired
    SessionBean sBean;

    @Autowired
    WorkService service;

    /**
     * For All Combo Load Data
     *
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/getAllCombo")
    public ResponseEntity<Map> getAllCombo() throws Exception {
        Map param = new HashMap();
        param.put("USERID", sBean.getUserId());
        Map mCombos = service.getAllCombo(param);
        return new ResponseEntity<>(mCombos, HttpStatus.OK);
    }

    /**
     * Get All work Data.
     *
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/viewDetail")
    public ResponseEntity<Map> viewDetail() throws Exception {
        Map map = new HashMap();
        Map param = new HashMap();
        param.put("USERID", sBean.getUserId());
        map.put("WORKDATA", service.viewDetail(param));
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    /**
     * Get Work Data For Update.
     *
     * @param id
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<Map> getUpdateData(@PathVariable int id) throws Exception {
        Map param = new HashMap();
        param.put("USERWORKID", String.valueOf(id));
        return new ResponseEntity<>(service.getUpdateData(param), HttpStatus.OK);
    }

    /**
     * Insert Work Detail.
     *
     * @param jObj
     * @return
     * @throws java.lang.Exception
     */
//    @PostMapping(value = "/")
//    public ResponseEntity<Status> insertDetail(@RequestBody String jObj) throws Exception {
//        Status s = new Status();
//        ResponseEntity<Status> retResEnt;
//        Map param = new ObjectMapper().readValue(jObj, Map.class);
//        param.put("USERID", sBean.getUserId());
//        Map map = service.insertDetail(param);
//        if (Integer.parseInt(map.get("RESULT").toString()) > 0) {
//            s.setResponseCode(Const.MSG_SUCCESS_CODE);
//            s.setResponseMessage(Const.MSG_ADD_SUCESS);
//            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
//        } else {
//            s.setResponseCode(Const.MSG_FAIL_CODE);
//            s.setResponseMessage(Const.MSG_ERROR);
//            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
//        }
//        return retResEnt;
//    }
    
    @PostMapping(value = "/")
    public ResponseEntity<Status> insertDetail(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request, @RequestParam(value = "formData") String jObj) throws Exception {
        Status s = new Status();
        ResponseEntity<Status> retResEnt;
        Map param = new ObjectMapper().readValue(jObj, Map.class);

        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sLocation = prop.getProperty("STORAGEBOX") + prop.getProperty("WORK_PATH");

        param.put("LOCATION", sLocation);
        param.put("FILE", file);

        param.put("CREATERID", sBean.getUserId());
        param.put("USERID", sBean.getUserId());
        Map map = service.insertDetail(param);
        if (Integer.parseInt(map.get("RESULT").toString()) > 0) {
            s.setResponseCode(Const.MSG_SUCCESS_CODE);
            s.setResponseMessage(Const.MSG_ADD_SUCESS);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        } else {
            s.setResponseCode(Const.MSG_FAIL_CODE);
            s.setResponseMessage(Const.MSG_ERROR);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        }
        return retResEnt;
    }

//    /**
//     * Update Work Detail.
//     *
//     * @param id
//     * @param jObj
//     * @return
//     * @throws java.lang.Exception
//     */
//    @PutMapping(value = "/{id}")
//    public ResponseEntity<Status> updateDetail(@PathVariable int id, @RequestBody String jObj) throws Exception {
//        Status s = new Status();
//        ResponseEntity<Status> retResEnt;
//        Map param = new ObjectMapper().readValue(jObj, Map.class);
//        param.put("USERID", sBean.getUserId());
//        Map map = service.updateDetail(param);
//        if (Integer.parseInt(map.get("RESULT").toString()) > 0) {
//            s.setResponseCode(Const.MSG_SUCCESS_CODE);
//            s.setResponseMessage(Const.MSG_UPDATE_SUCESS);
//            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
//        } else {
//            s.setResponseCode(Const.MSG_FAIL_CODE);
//            s.setResponseMessage(Const.MSG_ERROR);
//            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
//        }
//        return retResEnt;
//    }

    /**
     * Update work detail.
     *
     * @param file
     * @param request
     * @param jObj
     * @return
     * @throws java.lang.Exception
     */
    @PostMapping(value = "/updateWork")
    public ResponseEntity<Status> updateDetail(@RequestParam(value = "file", required = false) MultipartFile file, HttpServletRequest request, @RequestParam(value = "formData") String jObj) throws Exception {
        Status s = new Status();
        ResponseEntity<Status> retResEnt;
        Map param = new ObjectMapper().readValue(jObj, Map.class);

        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sLocation = prop.getProperty("STORAGEBOX") + prop.getProperty("WORK_PATH");

        param.put("LOCATION", sLocation);
        param.put("FILE", file);

        param.put("CREATERID", sBean.getUserId());
        param.put("USERID", sBean.getUserId());
        Map map = service.updateDetail(param);
        if (Integer.parseInt(map.get("RESULT").toString()) > 0) {
            s.setResponseCode(Const.MSG_SUCCESS_CODE);
            s.setResponseMessage(Const.MSG_UPDATE_SUCESS);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        } else {
            s.setResponseCode(Const.MSG_FAIL_CODE);
            s.setResponseMessage(Const.MSG_ERROR);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        }
        return retResEnt;
    }

    /**
     * Delete Work detail
     *
     * @param id
     * @return
     * @throws java.lang.Exception
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Status> deleteDetail(@PathVariable int id) throws Exception {
        Status s = new Status();
        ResponseEntity<Status> retResEnt;
        Map param = new HashMap();
        param.put("USERWORKID", id);
        
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sLocation = prop.getProperty("STORAGEBOX") + prop.getProperty("WORK_PATH");
        param.put("LOCATION", sLocation);
        
        int iRes = service.deleteDetail(param);
        if (iRes > 0) {
            s.setResponseCode(Const.MSG_SUCCESS_CODE);
            s.setResponseMessage(Const.MSG_DELETE_SUCESS);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        } else {
            s.setResponseCode(Const.MSG_FAIL_CODE);
            s.setResponseMessage(Const.MSG_DELETE_ALERT);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        }
        return retResEnt;
    }
    
    /**
     * Download Thumb File.
     *
     * @param id
     * @param res
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/getWorkFile/{id}")
    public void getWorkFile(@PathVariable("id") String id, HttpServletResponse res) throws Exception {
        try {
            InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
            Properties prop = new Properties();
            prop.load(input);
            Map param = new HashMap();
            param.put("USERWORKID", id);
            Map mFileDetail = service.getFile(param);
            String sLocation = prop.getProperty("STORAGEBOX") + prop.getProperty("WORK_PATH") + "/" + mFileDetail.get("FILE").toString();
            FileUpload.downloadFileFromDisk(sLocation, res);
        } catch (Exception ex) {
            System.out.println("ex=" + ex.getMessage());
        }
    }
    
    /**
     * Update City Detail.
     *
     * @param id
     * @param jObj
     * @return
     * @throws java.lang.Exception
     */
    @PutMapping(value = "/featurework")
    public ResponseEntity<Status> setFetureWork(@RequestBody String jObj) throws Exception {
        Status s = new Status();
        ResponseEntity<Status> retResEnt;
        Map param = new ObjectMapper().readValue(jObj, Map.class);
        Map map = service.setFetureWork(param);
        if (Integer.parseInt(map.get("RESULT").toString()) > 0) {
            s.setResponseCode(Const.MSG_SUCCESS_CODE);
            s.setResponseMessage(Const.MSG_UPDATE_SUCESS);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        } else {
            s.setResponseCode(Const.MSG_EXISTS_CODE);
            if (Integer.parseInt(map.get("RSLTCITYNAME").toString()) > 0) {
                s.setResponseMessage("City Name " + Const.MSG_EXISTS);
            }
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        }
        return retResEnt;
    }

    /**
     * Catch all Controller exception
     *
     * @param req
     * @param ex
     * @return
     */
    @ExceptionHandler({Exception.class})
    public ResponseEntity<Status> exceptionHandler(HttpServletRequest req, Exception ex) {
        Status error = new Status(500, ex.getMessage());
        LOG.error("Request: " + req.getRequestURL() + " raised " + ex);
        return new ResponseEntity<>(error, HttpStatus.OK);
    }
}
