/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.work;

import com.pratibhe.city.*;
import com.pratibhe.util.FileUpload;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jay
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class WorkService {

    @Autowired
    WorkDataManager datamanager;

    /**
     * get All Combo
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getAllCombo(Map param) throws Exception {
        Map mCombos = new HashMap();
        mCombos.put("ART", datamanager.getArt(param));
        return mCombos;
    }

    /**
     * get All Work Data
     *
     * @param param
     * @return
     * @throws Exception
     */
    public List viewDetail(Map param) throws Exception {
        return datamanager.viewDetail(param);
    }

    /**
     * get Work
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getUpdateData(Map param) throws Exception {
        return datamanager.getUpdateData(param);
    }

    /**
     * for insert Work
     *
     * @param param
     * @return
     * @throws Exception
     */
//    public Map insertDetail(Map param) throws Exception {
//        Map map = new HashMap();
//        Iterator it = param.entrySet().iterator();
//        while (it.hasNext()) {
//            Map.Entry pair = (Map.Entry) it.next();
//            param.replace(pair.getKey(), "", null);
//        }
//        int refId = datamanager.insertDetail(param);
//        map.put("RESULT", refId);
//        return map;
//    }
    
    /**
     * for insert events
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map insertDetail(Map param) throws Exception {
        Map map = new HashMap();

        MultipartFile file = (MultipartFile) param.get("FILE");

        Iterator it = param.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            param.replace(pair.getKey(), "", null);
        }
        int iWorkId = datamanager.insertDetail(param);
        if (iWorkId > 0) {
            String extension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."), file.getOriginalFilename().length());
            String sFile = iWorkId + "_file" + extension;
            param.replace("FILE", sFile);
            param.put("USERWORKID", iWorkId);
            int iRes = datamanager.updateWorkFile(param);
            if (iRes > 0) {
                FileUpload.uploadFile(param.get("LOCATION").toString(), String.valueOf(iWorkId) + "_file", file, true);
            }
        }
        map.put("RESULT", 1);

        return map;
    }
    
    /**
     * for update Work
     *
     * @param param
     * @return
     * @throws Exception
     */
//    public Map updateDetail(Map param) throws Exception {
//        Map map = new HashMap();
//        Iterator it = param.entrySet().iterator();
//        while (it.hasNext()) {
//            Map.Entry pair = (Map.Entry) it.next();
//            param.replace(pair.getKey(), "", null);
//        }
//        map.put("RESULT", datamanager.updateDetail(param));
//        return map;
//    }
    
    /**
     * for update events
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map updateDetail(Map param) throws Exception {
        Map map = new HashMap();

        MultipartFile file = (MultipartFile) param.get("FILE");

        Iterator it = param.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            param.replace(pair.getKey(), "", null);
        }
        int iResult = datamanager.updateDetail(param);
        if (iResult > 0) {
            Map sPath = datamanager.getFilePath(param);
            String iWorkId = String.valueOf(param.get("USERWORKID"));
            if (file != null) {
                String extension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."), file.getOriginalFilename().length());
                String sFile = iWorkId + "_file" + extension;
                param.replace("FILE", sFile);
                param.put("USERWORKID", iWorkId);
                int iRes = datamanager.updateWorkFile(param);

                if (iRes > 0) {
                    String sOldFile = param.get("LOCATION").toString() + sPath.get("FILE").toString();
                    FileUpload.deleteFileFromDisk(sOldFile);
                    FileUpload.uploadFile(param.get("LOCATION").toString(), String.valueOf(iWorkId) + "_file", file, true);
                }
            }
        }
        map.put("RESULT", 1);
        return map;
    }
    
    public Map getFile(Map param) throws Exception{
        return datamanager.getFilePath(param);
    }

    /**
     * for delete Work
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int deleteDetail(Map param) throws Exception {
        int iResult = 0;
        Map sPath = datamanager.getFilePath(param);
        int iDelRslt = datamanager.deleteDetail(param);
        if (iDelRslt > 0) {
            String sFile = param.get("LOCATION").toString() + sPath.get("FILE").toString();
            FileUpload.deleteFileFromDisk(sFile);
            iResult = 1;
        }
        return iResult;
    }
    
    public Map setFetureWork(Map param) throws Exception {
        param.put("RESULT", datamanager.setFetureWork(param));
        return param;
    }
}
