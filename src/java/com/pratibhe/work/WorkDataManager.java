/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.work;

import com.pratibhe.common.Const;
import com.pratibhe.util.TranSqlServices;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jay
 */
@Repository
public class WorkDataManager {

    @Autowired
    TranSqlServices objSQL;

    /**
     * get Art Form
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public List getArt(Map param) throws Exception {
        String sQuery = " SELECT A.ARTID VALUE,A.ARTNAME TEXT"
                + " FROM noiseindia.art A "
                + " INNER JOIN USER_ART UA ON A.ARTID = UA.ARTID AND UA.USERID = :USERID";
        return objSQL.getList(sQuery, param);
    }

    /**
     * get all Work
     *
     * @param param
     * @return
     * @throws Exception
     */
    public List viewDetail(Map param) throws Exception {
        String sQuery = "SELECT USERWORKID, USERID, UW.ARTID, A.ARTNAME,"
                + " FILE,WORKNAME, DESCRIPTION, ISFETURED "
                + " FROM USERWORK UW "
                + " INNER JOIN ART A ON A.ARTID = UW.ARTID "
                + " WHERE UW.USERID = :USERID "
                + " ORDER BY UW.USERWORKID DESC";
        return objSQL.getList(sQuery, param);
    }

    /**
     * get Work
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public Map getUpdateData(Map param) throws Exception {
        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sQuery = "SELECT USERWORKID, USERID, ARTID,"
                + " FILE,CONCAT('" + prop.getProperty("DOMAIN") + "','/work/getWorkFile/',USERWORKID) FILEPATH,WORKNAME, DESCRIPTION, ISFETURED, ADDTIME, EDITTIME "
                + " FROM USERWORK"
                + " WHERE USERWORKID = :USERWORKID";
        return objSQL.getMap(sQuery, param);
    }

    /**
     * for insert Work
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int insertDetail(Map param) throws Exception {

        String sQuery = "INSERT INTO USERWORK ("
                + " USERID, ARTID, FILE, WORKNAME, DESCRIPTION,ADDID,EDITID, ADDTIME, EDITTIME\n"
                + " ) VALUES (\n"
                + " :USERID, :ARTID, 'file1', :WORKNAME, :DESCRIPTION,:USERID,:USERID, NOW(), NOW()\n"
                + " )";
        return objSQL.persistKey(sQuery, param);
    }

    /**
     * update work file
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateWorkFile(Map param) throws Exception {
        String sQuery = "UPDATE USERWORK"
                + " SET "
                + " FILE = :FILE "
                + " WHERE USERWORKID = :USERWORKID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * for update Work
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int updateDetail(Map param) throws Exception {
        String sQuery = "UPDATE USERWORK "
                + " SET "
                + " USERID = :USERID, ARTID = :ARTID,WORKNAME=:WORKNAME,"
                + " DESCRIPTION = :DESCRIPTION, EDITID = :USERID, EDITTIME = NOW() "
                + " WHERE USERWORKID = :USERWORKID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * for delete Work
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int deleteDetail(Map param) throws Exception {
        String sQuery = "DELETE FROM USERWORK WHERE USERWORKID = :USERWORKID";
        return objSQL.persist(sQuery, param);
    }

    /**
     * get File Path
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getFilePath(Map param) throws Exception {
        String sQuery = "SELECT FILE FROM USERWORK WHERE USERWORKID=:USERWORKID";
        return objSQL.getMap(sQuery, param);
    }
    
     /**
     * set featured work
     *
     * @param param
     * @return
     * @throws Exception
     */
    public int setFetureWork(Map param) throws Exception {
        String sQuery = "UPDATE USERWORK "
                + " SET "
                + " ISFETURED = :ISFETURED "
                + " WHERE USERWORKID = :USERWORKID";
        System.out.println("");
        return objSQL.persist(sQuery, param);
    }

}
