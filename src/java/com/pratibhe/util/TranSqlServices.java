// Project : TranSqlServices.java
// Created : 18 Apr, 2017 10:29:36 AM
// Author  : Sandip kotadiya
package com.pratibhe.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sandip Kotadiya
 */
@Repository
public class TranSqlServices {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public TranSqlServices() {
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * return underlying connection
     *
     * @return
     */
    public Connection getConnection() {
        Connection conn = null;
        try {
            //conn = (Connection) jdbcTemplate.getDataSource().getConnection();
            conn = DataSourceUtils.getConnection(jdbcTemplate.getDataSource()); // underlying connection
        } catch (CannotGetJdbcConnectionException ex) {
        }
        return conn;

    }

    /**
     * Insert Records
     *
     * @param query
     * @return no of records affected.
     * @throws Exception
     */
    public int persist(final String query) throws Exception {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update((Connection connection) -> {
            PreparedStatement ps = connection.prepareStatement(query);
            return ps;
        },
                keyHolder);
        return Integer.parseInt(keyHolder.getKey().toString());
    }

    /**
     * Insert Records with parameter
     *
     * @param query
     * @param paramMap
     * @return
     */
    public int persist(String query, Map paramMap) throws DataAccessException {
        NamedParameterJdbcTemplate namedTempalte = new NamedParameterJdbcTemplate(jdbcTemplate);
        return namedTempalte.update(query, paramMap);
    }

    /**
     * Insert Records and return auto generated ID.
     *
     * @param sql
     * @param paramMap
     * @return
     */
    public int persistKey(String sql, Map paramMap) throws DataAccessException {
        //Assert.doesNotContain("INSERT", sql);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource paramkey = new MapSqlParameterSource(paramMap);
        NamedParameterJdbcTemplate namedTempalte = new NamedParameterJdbcTemplate(jdbcTemplate);
        namedTempalte.update(sql, paramkey, keyHolder);
        return Integer.parseInt(keyHolder.getKey().toString());
    }

    /**
     * Operation: Method to update or delete record in database
     *
     * @param sql static SQL to execute
     * @return the number of rows affected
     */
    public int updateDelete(String sql) throws DataAccessException {
        return jdbcTemplate.update(sql);
    }

    /**
     *
     * Operation: Method returns list from database
     *
     * @param sql
     * @return List<Map<String,Object>>
     */
    public List getList(String sql) throws DataAccessException {
        return jdbcTemplate.queryForList(sql);
    }

    /**
     * Return Single Record in Map
     *
     * @param sql
     * @return
     * @throws DataAccessException
     */
    public Map getMap(String sql) throws DataAccessException {
        return jdbcTemplate.queryForMap(sql);
    }

    /**
     * Return Single Record in Map
     *
     * @param sql
     * @param paramMap
     * @return
     * @throws DataAccessException
     */
    public Map getMap(String sql, Map paramMap) throws DataAccessException {
        NamedParameterJdbcTemplate namedTempalte = new NamedParameterJdbcTemplate(jdbcTemplate);
        return namedTempalte.queryForMap(sql, paramMap);
    }

    /**
     * The results will be mapped to a List (one entry for each row) of Maps
     * (one entry for each column, using the column name as the key).
     *
     * @param sql
     * @param paramMap
     * @return List<Map<String,Object>>
     */
    public List getList(String sql, Map paramMap) throws DataAccessException {
        NamedParameterJdbcTemplate namedTempalte = new NamedParameterJdbcTemplate(jdbcTemplate);
        return namedTempalte.queryForList(sql, paramMap);
    }

    /**
     * Execute a query for a result Integer
     *
     * @param sql
     * @return
     */
    public int getInteger(String sql) {
        try {
            return jdbcTemplate.queryForObject(sql, Integer.class);
        } catch (NullPointerException e) {
            return 0;
        }
    }

    /**
     * Execute a query for a result Integer
     *
     * @param sql
     * @param parmaMap
     * @return
     */
    public int getInteger(String sql, Map parmaMap) {
        try {
            NamedParameterJdbcTemplate namedTempalte = new NamedParameterJdbcTemplate(jdbcTemplate);
            return namedTempalte.queryForObject(sql, parmaMap, Integer.class);
        } catch (DataAccessException ex) {
            return 0;
        }
    }

    /**
     * Execute a query for a result Double
     *
     * @param sql
     * @return
     */
    public double getDouble(String sql) {
        try {
            return jdbcTemplate.queryForObject(sql, java.lang.Double.class);
        } catch (NullPointerException ex) {
            return 0d;
        }
    }

    /**
     * Execute a query for a result String
     *
     * @param sql
     * @return
     */
    public String getString(String sql) {
        try {
            return (String) jdbcTemplate.queryForObject(sql, java.lang.String.class);
        } catch (NullPointerException ex) {
            return null;
        }
    }

    /**
     * Execute a query for a result String
     *
     * @param sql
     * @param parmaMap
     * @return
     */
    public String getString(String sql, Map parmaMap) {
        try {
            NamedParameterJdbcTemplate namedTempalte = new NamedParameterJdbcTemplate(jdbcTemplate);
            return namedTempalte.queryForObject(sql, parmaMap, java.lang.String.class);
        } catch (DataAccessException ex) {
            return null;
        }
    }
}
