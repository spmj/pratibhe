// Project : ecomadmin FileUpload.java
// Created : 9 Sep, 2017 11:45:43 AM
// Author  : Sandip Kotadiya

package com.pratibhe.util;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Base64;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Sandip kotadiya
 */
public class FileUpload {

    private static final Logger LOGER = Logger.getLogger(FileUpload.class);

    /**
     * Store file in Disk from Browser Upload
     *
     * @param pFilePath destination location to store file
     * @param dFileName
     * @param multipartFile
     * @param OverWrite true /false
     * @return
     * @throws Exception
     */
    public static String uploadFile(String pFilePath, String dFileName, MultipartFile multipartFile, boolean OverWrite) throws Exception {
        if (pFilePath == null || pFilePath.trim().equals("") || multipartFile == null) {
            return "";
        }

        String fileName = pFilePath.replace("\\", "/");

        if (multipartFile.getSize() > 0) {

            InputStream inputStream = multipartFile.getInputStream();

            // CREATE DIRECOTORIE(S) IF NOT EXIST;
            new File(fileName).mkdirs();
            if (dFileName == null || dFileName.trim().equals("")) {
                fileName += multipartFile.getOriginalFilename();
            } else {
                fileName += dFileName + multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."), multipartFile.getOriginalFilename().length());
            }

            if (!OverWrite) {
                // OVERWRITE FALG IS FALSE
                java.io.File f2 = new File(fileName);
                if (f2.exists()) {
                    fileName = fileName.substring(0, fileName.lastIndexOf(".")) + "1" + fileName.substring(fileName.lastIndexOf("."), fileName.length());
                }
                f2 = null;
            }
            OutputStream outputStream = new FileOutputStream(fileName);
            int readBytes = 0;
            final int SIZE = 1024 * 100; // 100 kb
            byte[] buffer = new byte[SIZE];
            while ((readBytes = inputStream.read(buffer, 0, SIZE)) != -1) {
                outputStream.write(buffer, 0, readBytes);
            }
            buffer = null;
            outputStream.close();
            inputStream.close();
        } else {
            return "";
        }

        // WITH FULL FILE PATH
        return fileName;
    }

    /**
     * Store file in Disk from Browser Upload
     *
     * @param sLocation
     * @param base64
     * @param iId
     * @return
     * @throws Exception
     */
    public static String uploadBase64Img(String sLocation, String base64, String iId) throws Exception {
        File theDir = new File(sLocation);
        if (!theDir.exists()) {
            theDir.mkdir();
        }

        String sourceData = base64;
        String[] parts = sourceData.split(",");
        String imageString = parts[1];

        BufferedImage image;

//        BASE64Decoder decoder = new BASE64Decoder();
//        imageByte = decoder.decodeBuffer(imageString);
        byte[] imageDecodedByte = Base64.getDecoder().decode(imageString);
        try (ByteArrayInputStream bis = new ByteArrayInputStream(imageDecodedByte)) {
            image = ImageIO.read(bis);
        }
        String empLogo = sLocation + iId + ".png";
        File outputfile = new File(empLogo);
        ImageIO.write(image, "png", outputfile);
        return iId + ".png";
    }

    /**
     * Delete the given file name
     *
     * @param pFilePath
     * @return
     */
    public static boolean deleteFileFromDisk(String pFilePath) {
        if (!pFilePath.trim().equals("")) {
            String fileName = pFilePath.replace("\\", "/");
//            System.out.println("fileName = " + fileName);
            java.io.File fl = new File(fileName);
            if (fl.exists()) {
                fl.delete();
            }
        }

        return true;
    }

    public static boolean deleteDirFromDisk(String pFilePath) {
        File directory = new File(pFilePath);
        if (directory.exists() && directory.isDirectory()) {
            final File[] files = directory.listFiles();
            for (File f : files) {
                f.delete();
            }
            directory.delete();
        }
        return true;
    }

    public static String getRelativePath(final String parent, final String child) {
        if (!child.startsWith(parent)) {
            throw new IllegalArgumentException("Invalid child '" + child
                    + "' for parent '" + parent + "'");
        }
        // a String.replace() also would be fine here
        final int parentLen = parent.length();
        return child.substring(parentLen);
    }

    public static boolean isImmediateDescendant(final String parent, final String child) {
        if (!child.startsWith(parent)) {
            // maybe we just should return false
            throw new IllegalArgumentException("Invalid child '" + child
                    + "' for parent '" + parent + "'");
        }
        final int parentLen = parent.length();
        final String childWithoutParent = child.substring(parentLen);
        if (childWithoutParent.contains("/")) {
            return false;
        }
        return true;
    }

//    public static String getBase64(String pFilePath) throws FileNotFoundException, IOException {
//
//        File readfile = new File(pFilePath);
//        String result2 = "";
//        if (readfile.exists()) {
//            FileInputStream fis = new FileInputStream(readfile);
//            ByteArrayOutputStream bos = new ByteArrayOutputStream();
//            byte[] buf = new byte[1024];
//            try {
//                for (int readNum; (readNum = fis.read(buf)) != -1;) {
//                    bos.write(buf, 0, readNum);
//                }
//            } catch (IOException ex) {
//            }
//            byte[] bytes = bos.toByteArray();
//            result2 = "data:image/" + pFilePath.substring(pFilePath.lastIndexOf("."), pFilePath.length()) + ";base64," + Base64.encodeArray(bytes);
//            fis.close();
//            bos.close();
//        }
//
//        return result2;
//
//    }
    public static boolean moveFileArray(String sFilePath, String dFilePath, String files[]) {
        if (!sFilePath.trim().equals("") && !dFilePath.trim().equals("")) {
            sFilePath = sFilePath.replace("\\", "/");
            dFilePath = dFilePath.replace("\\", "/");
            new File(dFilePath).mkdirs();
            for (String file : files) {
                File afile = new File(sFilePath + file);
                if (afile.exists()) {
                    afile.renameTo(new File(dFilePath + afile.getName()));
                }
            }
        }
        return true;
    }

    public static boolean moveFileFromDisk(String sFilePath, String dFilePath, String files) {
        if (!sFilePath.trim().equals("") && !dFilePath.trim().equals("")) {
            sFilePath = sFilePath.replace("\\", "/");
            dFilePath = dFilePath.replace("\\", "/");
            new File(dFilePath).mkdirs();
            File afile = new File(sFilePath + files);
            if (afile.exists()) {
                afile.renameTo(new File(dFilePath + afile.getName()));
            }
        }
        return true;
    }

    /**
     *
     * @param sFilePath
     * @param dFilePath
     * @param sFiles
     * @param dFiles
     * @return
     */
    public static boolean copyFile(String sFilePath, String dFilePath, String sFiles, String dFiles) {
        if (!sFilePath.trim().equals("") && !dFilePath.trim().equals("")) {
            sFilePath = sFilePath.replace("\\", "/");
            dFilePath = dFilePath.replace("\\", "/");
            if (!sFiles.equals(dFiles)) {
                new File(dFilePath).mkdirs();
            }
            try {
                File oldFile = new File(sFilePath + sFiles);
                File newFile = new File(dFilePath + dFiles);
                InputStream inStream = new FileInputStream(oldFile);
                OutputStream outStream = new FileOutputStream(newFile);
                byte[] buffer = new byte[16384];//16 * 1024
                int length;
                //copy the file content in bytes 
                while ((length = inStream.read(buffer)) > 0) {
                    outStream.write(buffer, 0, length);
                }
                inStream.close();
                outStream.close();
            } catch (IOException e) {
                LOGER.error(e);
            }

        }
        return true;
    }

    /**
     * Generate file for Export from String
     *
     * @param filename
     * @param sRetStr
     * @return
     * @throws IOException
     */
    public static int generateFile(String filename, String sRetStr) throws IOException {
        new File(filename.substring(0, filename.lastIndexOf("/"))).mkdirs();
        try (PrintWriter out = new java.io.PrintWriter(filename)) {
            out.print(sRetStr);
            out.flush();
        }
        return 1;
    }

    public static int generateFile(String filename, byte[] sRetStr) throws IOException {

        new File(filename.substring(0, filename.lastIndexOf("/"))).mkdirs();
        FileOutputStream out = new FileOutputStream(filename);
        byte[] buf = new byte[1024];
        InputStream in = new ByteArrayInputStream(sRetStr);
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        out.flush();
        out.close();
        out = null;
        return 1;

    }

    /**
     * Generate the Zip file and send to response stream.
     *
     * @param files String [] String Content
     * @param filenames String[] of File names in Zip
     * @param zip Name of Zip File
     * @param response
     * @return
     * @throws Exception
     */
    public static int generateZipFile(String[] files, String[] filenames, String zip, HttpServletResponse response) throws Exception {
        byte[] buf = new byte[1024];

        try {
            zip = zip.replace("\\", "/");

            // CREATE DIRECOTORIE(S) IF NOT EXIST;
            new File(zip.substring(0, zip.lastIndexOf("/"))).mkdirs();
            // Create the ZIP file
            //String outFilename = "outfile.zip";
            File tmpfile;
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zip));
            Writer output;
            // Compress the files
            for (int i = 0; i < filenames.length; i++) {
                if (files[i].length() > 0) {
                    tmpfile = new File(filenames[i]);

                    output = new BufferedWriter(new FileWriter(tmpfile));
                    output.write(files[i]);
                    output.close();

                    FileInputStream in = new FileInputStream(filenames[i]);

                    // Add ZIP entry to output stream.
                    out.putNextEntry(new ZipEntry(filenames[i].substring(filenames[i].lastIndexOf("/") + 1, filenames[i].length())));

                    // Transfer bytes from the file to the ZIP file
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }

                    // Complete the entry
                    out.closeEntry();
                    in.close();
                    tmpfile.delete();
                }
            }
            // Complete the ZIP file
            out.close();

            response.setContentType("APPLICATION/OCTET-STREAM");
            FileInputStream fileIn = new FileInputStream(new File(zip));
            String disHeader = "Attachment;Filename=\"" + zip.substring(zip.lastIndexOf("/") + 1, zip.length()) + "\"";
            response.setHeader("Content-Disposition", disHeader);
            ServletOutputStream out1 = response.getOutputStream();

            byte[] outputByte = new byte[4096];
            //copy binary contect to output stream
            while (fileIn.read(outputByte, 0, 4096) != -1) {
                out1.write(outputByte, 0, 4096);
            }
            fileIn.close();
            out1.flush();
            out1.close();
            return 1;
        } catch (IOException e) {
            throw new Exception(e);
        }

    }

    /**
     * ONLY GENERATE THE ZIP FILE.
     *
     * @date21-10-2012
     *
     * @param files
     * @param filenames
     * @param zip
     * @throws Exception
     */
    public static void generateZipFile(String[] files, String[] filenames, String zip) throws Exception {
        byte[] buf = new byte[1024];
        try {
            zip = zip.replace("\\", "/");

            // CREATE DIRECOTORIE(S) IF NOT EXIST;
            new File(zip.substring(0, zip.lastIndexOf("/"))).mkdirs();
            // Create the ZIP file

            File tmpfile;
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zip));
            Writer output;
            // Compress the files
            for (int i = 0; i < filenames.length; i++) {
                if (files[i].length() > 0) {
                    tmpfile = new File(filenames[i]);

                    output = new BufferedWriter(new FileWriter(tmpfile));
                    output.write(files[i]);
                    output.close();

                    FileInputStream in = new FileInputStream(filenames[i]);

                    // Add ZIP entry to output stream.
                    out.putNextEntry(new ZipEntry(filenames[i].substring(filenames[i].lastIndexOf("/") + 1, filenames[i].length())));

                    // Transfer bytes from the file to the ZIP file
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }

                    // Complete the entry
                    out.closeEntry();
                    in.close();
                    tmpfile.delete();
                }
            }
            // Complete the ZIP file
            out.close();

        } catch (IOException e) {
            throw new Exception(e);
        }
    }

    /**
     * APPEND THE FILES IN EXISTING ZIP.
     *
     * @throws java.io.IOException
     * @date21-10-2012
     *
     * @param zipFileName name of zip file
     * @param filesName
     */
    public static void addFilesToExistingZip(String zipFileName, String[] filesName) throws IOException {

        // CONVERT FILE NAME TO FILE OBJECT
        File zipFile = new File(zipFileName);

        File[] files = new File[filesName.length];
        for (int i = 0; i < filesName.length; i++) {
            files[i] = new File(filesName[i]);

        }

        // GET A TEMP FILE
        File tempFile = File.createTempFile(zipFile.getName(), null, new File(zipFileName.substring(0, zipFileName.lastIndexOf("/"))));
        // DELETE IT, OTHERWISE YOU CANNOT RENAME YOUR EXISTING ZIP TO IT.
        tempFile.delete();

        boolean renameOk = zipFile.renameTo(tempFile);
        if (!renameOk) {
            throw new RuntimeException("could not rename the file " + zipFile.getAbsolutePath() + " to " + tempFile.getAbsolutePath());
        }
        byte[] buf = new byte[4096]; // 4K

        ZipInputStream zin = new ZipInputStream(new FileInputStream(tempFile));
        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFile));

        ZipEntry entry = zin.getNextEntry();
        while (entry != null) {
            String name = entry.getName();
            boolean notInFiles = true;
            for (File f : files) {
                if (f.getName().equals(name)) {
                    notInFiles = false;
                    break;
                }
            }
            if (notInFiles) {
                // ADD ZIP ENTRY TO OUTPUT STREAM.
                out.putNextEntry(new ZipEntry(name));
                // TRANSFER BYTES FROM THE ZIP FILE TO THE OUTPUT FILE
                int len;
                while ((len = zin.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
            entry = zin.getNextEntry();
        }
        // CLOSE THE STREAMS        
        zin.close();
        // COMPRESS THE FILES
        for (File file : files) {
            InputStream in = new FileInputStream(file);
            // ADD ZIP ENTRY TO OUTPUT STREAM.
            out.putNextEntry(new ZipEntry(file.getName()));
            // TRANSFER BYTES FROM THE FILE TO THE ZIP FILE
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            // COMPLETE THE ENTRY
            out.closeEntry();
            in.close();
        }
        // COMPLETE THE ZIP FILE
        out.close();
        tempFile.delete();
    }

    /**
     * CONVERT EXCEL FILE TO CSV FILE.
     *
     * @param Source source file name
     * @return csv file name.
     * @throws Exception
     */
//    public String convertExcelToCSV(String Source) throws Exception {
//        String Destination = "";
//        try {
//            String filename = Source;
//
//            File oldFile = new File(filename);
//
//            WorkbookSettings ws = new WorkbookSettings();
//            ws.setLocale(new Locale("en", "EN"));
//            Workbook w = Workbook.getWorkbook(oldFile, ws);
//
//            Destination = Source.substring(0, Source.lastIndexOf("/")) + Source.substring(Source.lastIndexOf("/"), Source.lastIndexOf(".")) + ".csv";
//
//            File f = new File(Destination);
//            OutputStream os = (OutputStream) new FileOutputStream(f);
//            String encoding = "UTF8";
//            OutputStreamWriter osw = new OutputStreamWriter(os, encoding);
//            BufferedWriter bw = new BufferedWriter(osw);
//            for (int sheet = 0;
//                    sheet < w.getNumberOfSheets();
//                    sheet++) {
//                Sheet s = w.getSheet(sheet);
//
//                //bw.write(s.getName());
//                // bw.newLine();
//
//                Cell[] row = null;
//
//                for (int i = 0; i < s.getRows(); i++) {
//                    row = s.getRow(i);
//
//                    if (row.length > 0) {
//                        bw.write(row[0].getContents());
//                        for (int j = 1; j < row.length; j++) {
//                            if (!row[j].getContents().trim().equals("")) {
//                                bw.write(',');
//                                bw.write(row[j].getContents());
//                            }
//
//                        }
//                        bw.newLine();
//                    }
//
//                }
//            }
//
//            bw.flush();
//
//            bw.close();
//        } catch (Exception ex) {
//            loger.error(ex);
//            throw new Exception(ex);
//        }
//        //  deleteFile(Source);
//        return Destination;
//    }
    /**
     * CREATE THE EXCEL FILE from csv data as string
     *
     * @param filename location where file will be saved
     * @param Header comma separated data
     * @param FileContent comma separated data
     * @return 1
     * @throws Exception
     */
//    public int createExcel(String filename, String Header, String FileContent) throws Exception {
//
//        new File(filename.substring(0, filename.lastIndexOf("/"))).mkdirs();
//        WorkbookSettings ws = new WorkbookSettings();
//        ws.setLocale(new Locale("en", "EN"));
//        WritableWorkbook workbook = Workbook.createWorkbook(new File(filename), ws);
//        WritableSheet s = workbook.createSheet("Sheet1", 0);
//
//        WritableFont wf = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
//        WritableCellFormat cf = new WritableCellFormat(wf);
//        cf.setWrap(true);
//        WritableFont wf1 = new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD);
//        WritableCellFormat cf1 = new WritableCellFormat(wf1);
//
//        cf1.setWrap(true);
//
//        /*
//         * Creates Label and writes date to one cell of sheet
//         */
//        String[] hrows = Header.split("\n");
//
//        for (int i = 0; i < hrows.length; i++) {
//            String[] hclm = hrows[i].split("\\,");
//            for (int j = 0; j < hclm.length; j++) {
//
//                Label l = new Label(j, i, hclm[j], cf);
//                s.addCell(l);
//                s.setColumnView(j, 25);
//            }
//        }
//        String[] rows = FileContent.split("\n");
//
//        for (int i = 0; i < rows.length; i++) {
//            String[] clm = rows[i].split("\\,");
//            for (int j = 0; j < clm.length; j++) {
//                Label l = new Label(j, i + hrows.length, clm[j], cf1);
//                s.addCell(l);
//            }
//        }
//        workbook.write();
//        workbook.close();
//        //  deleteFile(Source);
//        return 1;
//    }
    /**
     * WRITE THE ZIP FILE INTO RESPONSE STREAM.
     *
     * @param fileName
     * @param response
     * @throws Exception
     */
    public static void downloadFileFromDisk(String fileName, HttpServletResponse response) throws Exception {
        response.setContentType("APPLICATION/OCTET-STREAM");
        File f = new File(fileName);
        if (!f.exists()) {
            //System.out.println("fileName = " + fileName);
            response.sendError(404, "No resource found");
            response.setStatus(404);
            return;
        }
        FileInputStream fileIn = new FileInputStream(f);
        String disHeader = "Attachment;Filename=\"" + fileName.substring(fileName.lastIndexOf("/") + 1, fileName.length()) + "\"";
        response.setHeader("Content-Disposition", disHeader);
        ServletOutputStream out1 = response.getOutputStream();

        byte[] outputByte = new byte[(int) f.length()];
        // f.length();
        //copy binary contect to output stream
        int len;
        while ((len = fileIn.read(outputByte)) > 0) {
            out1.write(outputByte, 0, len);
        }
//        while (fileIn.read(outputByte, 0, 4096) != -1) {
//            out1.write(outputByte, 0, 4096);
//        }
        fileIn.close();
        out1.flush();
        out1.close();
    }

    /**
     * WRITE THE ZIP FILE INTO RESPONSE STREAM.
     *
     * @param fileName
     * @param NewName
     * @param response
     * @throws Exception
     */
    public static void downloadFileWithRename(String fileName, String NewName, HttpServletResponse response) throws Exception {
        response.setContentType("APPLICATION/OCTET-STREAM");
        File f = new File(fileName);
        if (!f.exists()) {
            //System.out.println("downloadFileWithRename.fileName = " + fileName);
            response.sendError(404, "No resource found");
            response.setStatus(404);
            return;
        }
        FileInputStream fileIn = new FileInputStream(f);
        String sFileExt = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
        String disHeader = "Attachment;Filename=\"" + NewName + "." + sFileExt + "\"";
        response.setHeader("Content-Disposition", disHeader);
        ServletOutputStream out1 = response.getOutputStream();

        byte[] outputByte = new byte[(int) f.length()];
        // f.length();
        //copy binary contect to output stream
        int len;
        while ((len = fileIn.read(outputByte)) > 0) {
            out1.write(outputByte, 0, len);
        }
//        while (fileIn.read(outputByte, 0, 4096) != -1) {
//            out1.write(outputByte, 0, 4096);
//        }
        fileIn.close();
        out1.flush();
        out1.close();
    }


    /**
     * kk@24-12-2014 : download Image file from <img> tag.
     *
     * @param fileName
     * @param response
     * @throws Exception
     */
    public static void downloadImageFromDisk(String fileName, HttpServletResponse response) throws Exception {
        File f = new File(fileName);
        ServletOutputStream out1;
        try (FileInputStream fileIn = new FileInputStream(f)) {
            out1 = response.getOutputStream();
            byte[] outputByte = new byte[(int) f.length()];
            int len;
            while ((len = fileIn.read(outputByte)) > 0) {
                out1.write(outputByte, 0, len);
            }
        }
        out1.flush();
        out1.close();
    }

    public static int generateAnyFile(String FileName, String ext, String sRetStr, HttpServletResponse response) throws IOException {

        response.setHeader("Content-disposition", "attachment; filename=" + FileName);
        //    response.addHeader("Content-Type", "application/force-download");
        response.addHeader("Content-Type", "application/" + ext);
        //    response.addHeader("Content-Type", "application/x-download");

        response.setContentLength(sRetStr.length());
        response.getOutputStream().write(sRetStr.getBytes(), 0, sRetStr.length());
        response.getOutputStream().flush();
        response.getOutputStream().close();
        return 1;
    }

    /**
     * read file and get data as string
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static String readFile(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line;
        StringBuilder stringBuilder = new StringBuilder();
        //String ls = "\n";

        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            //stringBuilder.append(ls);
        }

        return stringBuilder.toString();
    }

//    /**
//     * CREATE THE EXCEL FILE from csv data as string
//     *
//     * TAKING TOO LONG TIME, CONNECTION GETS CLOSED
//     *
//     * @param filename location where file will be saved
//     * @param Header comma separated data
//     * @param FileContent comma separated data
//     * @param pSeparater separater IF Separater = NULL OR "" THEN DEFAULT
//     * ,(COMMA SEPARATE)
//     * @param response
//     * @throws Exception
//     */
//    public static void createExcel(String filename, String Header, String FileContent, String pSeparater, HttpServletResponse response) throws Exception {
//
//        WizAuthDataManager wdm = new WizAuthDataManager();
//        HSSFWorkbook wb = new HSSFWorkbook();
//        HSSFSheet sheet = wb.createSheet();
//
//        wdm.setCellStyles(wb);
//        Row row;
//        org.apache.poi.ss.usermodel.Cell cell;
//
//        int rownum = 0;
//        row = sheet.createRow(rownum++);
//
//        String[] hrows = Header.split("\n");
//        for (int i = 0; i < hrows.length; i++) {
//            String[] hclm = hrows[i].split(",");
////            System.out.println("header cols = " + hclm.length);
//            for (int j = 0; j < hclm.length; j++) {
//                cell = row.createCell(j);
//                cell.setCellValue(hclm[j]);
//                cell.setCellStyle(wdm.csHVBoldCenter);
//                sheet.autoSizeColumn((short) (j));
//            }
//        }
//
//        // rows
//        rownum++;
//        if (FileContent.length() != 0) {
//            String[] rows = FileContent.split("\n");
////            System.out.println("total rows = " + rows.length);
//            String sSepareter = ",";
//            for (int i = 0; i < rows.length; i++) {
//                row = sheet.createRow(rownum++);
//                String[] clm = rows[i].split(sSepareter);
//                for (int j = 0; j < clm.length; j++) {
//                    cell = row.createCell(j);
//                    cell.setCellValue(clm[j]);
//                    cell.setCellStyle(wdm.csHVLeft);
//                    sheet.autoSizeColumn((short) (j));
//                }
//            }
//        }
//
//        FileOutputStream fileOut = new FileOutputStream(filename);
//        wb.write(fileOut);
//        fileOut.close();
//        
//        // ----------- to download file
//        response.setContentType("APPLICATION/OCTET-STREAM");
//        File f = new File(filename);
//        FileInputStream fileIn = new FileInputStream(f);
//        String disHeader = "Attachment;Filename=\"" + filename.substring(filename.lastIndexOf("/") + 1, filename.length()) + "\"";
//        response.setHeader("Content-Disposition", disHeader);
//        ServletOutputStream out1 = response.getOutputStream();
//
//        byte[] outputByte = new byte[(int) f.length()];
//        int len;
//        while ((len = fileIn.read(outputByte)) > 0) {
//            out1.write(outputByte, 0, len);
//        }
//        fileIn.close();
//    }
}