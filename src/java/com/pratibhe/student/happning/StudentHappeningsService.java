/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.student.happning;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jay
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class StudentHappeningsService {

    @Autowired
    StudentHappeningsDataManager datamanager;

    /**
     * get All happenings Data
     *
     * @return
     * @throws Exception
     */
    public List viewDetail() throws Exception {
        return datamanager.viewDetail();
    }

    /**
     * get happenings
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map getUpdateData(Map param) throws Exception {
        return datamanager.getUpdateData(param);
    }

    /**
     * for update happenings
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Map updateDetail(Map param) throws Exception {
        Map map = new HashMap();

        MultipartFile file_thumb = (MultipartFile) param.get("FILE_THUMB");
        MultipartFile file_banner = (MultipartFile) param.get("FILE_BANNER");

        Iterator it = param.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            param.replace(pair.getKey(), "", null);
        }
        int iResult = datamanager.updateDetail(param);
        if (iResult > 0) {
            map.put("RESULT", 1);
        } else {
            map.put("RESULT", 0);
        }

        return map;
    }

    /**
     * for delete happenings
     *
     * @param param
     * @return
     * @throws java.lang.Exception
     */
    public int deleteDetail(Map param) throws Exception {
        return datamanager.deleteDetail(param);
    }
}
