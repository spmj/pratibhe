/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pratibhe.student.happning;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pratibhe.common.Const;
import com.pratibhe.common.SessionBean;
import com.pratibhe.common.Status;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jay
 */
@Controller
@RequestMapping("/studenthappenings")
public class StudentHappeningsController {

    private static final Logger LOG = Logger.getLogger(StudentHappeningsController.class);

    @Autowired
    SessionBean sBean;

    @Autowired
    StudentHappeningsService service;


    /**
     * Get All events Data.
     *
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping(value = "/viewDetail")
    public ResponseEntity<Map> viewDetail() throws Exception {
        Map map = new HashMap();
        map.put("HAPPENINGSSDATA", service.viewDetail());
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    /**
     * Get happenings Data For Update.
     *
     * @param id
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<Map> getUpdateData(@PathVariable int id) throws Exception {
        Map param = new HashMap();
        param.put("HAPPENINGSID", String.valueOf(id));
        return new ResponseEntity<>(service.getUpdateData(param), HttpStatus.OK);
    }

    /**
   
    /**
     * Update happenings Detail.
     *
     * @param file_thumb
     * @param file_banner
     * @param request
     * @param jObj
     * @return
     * @throws java.lang.Exception
     */
    @PostMapping(value = "/updateHappenings")
    public ResponseEntity<Status> updateDetail(@RequestParam(value = "file_thumb", required = false) MultipartFile file_thumb, @RequestParam(value = "file_banner", required = false) MultipartFile file_banner, HttpServletRequest request, @RequestParam(value = "formData") String jObj) throws Exception {
        Status s = new Status();
        ResponseEntity<Status> retResEnt;
        Map param = new ObjectMapper().readValue(jObj, Map.class);

        InputStream input = new FileInputStream(Const.getAppPath() + "/WEB-INF/classes/conf.properties");
        Properties prop = new Properties();
        prop.load(input);
        String sLocation = prop.getProperty("STORAGEBOX") + prop.getProperty("HAPPENING_PATH");

        param.put("LOCATION", sLocation);
        param.put("FILE_THUMB", file_thumb);
        param.put("FILE_BANNER", file_banner);

        param.put("CREATERID", sBean.getUserId());
        Map map = service.updateDetail(param);
        if (Integer.parseInt(map.get("RESULT").toString()) > 0) {
            s.setResponseCode(Const.MSG_SUCCESS_CODE);
            s.setResponseMessage(Const.MSG_UPDATE_SUCESS);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        } else {
            s.setResponseCode(Const.MSG_FAIL_CODE);
            s.setResponseMessage(Const.MSG_ERROR);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        }
        return retResEnt;
    }

    /**
     * Delete happenings detail
     *
     * @param id
     * @return
     * @throws java.lang.Exception
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Status> deleteDetail(@PathVariable int id) throws Exception {
        Status s = new Status();
        ResponseEntity<Status> retResEnt;
        Map param = new HashMap();
        param.put("HAPPENINGSID", id);
        
        int iRes = service.deleteDetail(param);
        if (iRes > 0) {
            s.setResponseCode(Const.MSG_SUCCESS_CODE);
            s.setResponseMessage(Const.MSG_DELETE_SUCESS);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        } else {
            s.setResponseCode(Const.MSG_FAIL_CODE);
            s.setResponseMessage(Const.MSG_DELETE_ALERT);
            retResEnt = new ResponseEntity<>(s, HttpStatus.OK);
        }
        return retResEnt;
    }
    
    /**
     * Catch all Controller exception
     *
     * @param req
     * @param ex
     * @return
     */
    @ExceptionHandler({Exception.class})
    public ResponseEntity<Status> exceptionHandler(HttpServletRequest req, Exception ex) {
        Status error = new Status(500, ex.getMessage());
        LOG.error("Request: " + req.getRequestURL() + " raised " + ex);
        return new ResponseEntity<>(error, HttpStatus.OK);
    }
}
